#!/usr/bin/perl

use File::Find;
use Cwd;

$input_file_name = @ARGV[0];

open(INPUT_FILE, $input_file_name);

print "Checking file $input_file_name\n";

while(<INPUT_FILE>) {
    local($line) = $_;

    if ($line=~/([^=]*)=(.*)/) {
        $key = $1;
        # Now check if this key is used in any source file (including java and xml)

        $found = 0;
        &checkDir(cwd,$key);
        if ($found==0) {
            print "Found unused string $key\n";
        }
    }
}

close(INPUT_FILE);


#recursive directory listing
#
#sub loadFiles(); #udf
#sub mySub(); #udf
#
#my @files = ();
#my $dir = shift || die "Argument missing: directory name\n";
#
#loadFiles(); #call
#map { print "$_\n"; } @files;

sub checkDir()
{
    find(\&mySub,$_[0]);
}

#
# following gets called recursively for each file in $dir, check $_ to see if you want the file!
sub mySub()
{
    if ($found==1) {
        return;
    }
    my $file = $File::Find::name;

    return unless -f $file;

    # Check only .java and .xml files
    #if ($file=~/[.*\.java|.*\.xml]/) {
    if ($file=~/[\.java|\.xml]$/) {
        if ($file=~/strings.xml$/) {
            #print "Skipping $file\n";
        } elsif ($file=~/R.java$/) {
            #print "Skipping $file\n";
        } else {
            open F, $file or print "couldn't open $file\n" && return;
            while (<F>) {
                $row=$_;
                if ($row=~/$key/) {
                    #print "key $key found in row $row\n";
                    $found=1;
                    break;
                }
            }
            close F;
        }
    }
}

