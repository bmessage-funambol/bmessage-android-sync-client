/*
 * Funambol is a mobile platform developed by Funambol, Inc. 
 * Copyright (C) 2009 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission 
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE 
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License 
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite 
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably 
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.platform;

import java.io.PrintStream;
import java.io.InputStream;
import java.util.Date;

import com.funambol.android.AndroidBaseTest;

import com.funambol.util.Log;
import com.funambol.util.ConsoleAppender;

import junit.framework.*;


/**
 * Testing TimerHandler
 */
public class TimerHandlerTest extends AndroidBaseTest {

    private int received;

    public TimerHandlerTest() {
        super();
        Log.initLog(new ConsoleAppender());
        Log.setLogLevel(Log.DEBUG);
    }

    public void setUp() {
        TimerHandler timerHandler = TimerHandler.getInstance();
        timerHandler.setContext(getContext());
        received = 0;
    }

    public void testSingleTimer() throws Exception {
        TimerHandler timerHandler = TimerHandler.getInstance();
        timerHandler.setTimer(System.currentTimeMillis() + 500, new Runnable() {
            public void run() {
                timerReceived();
            }
        });

        sleep(1000);
        assertTrue(received == 1);
        assertTrue(timerHandler.getHashSize1() == 0);
        assertTrue(timerHandler.getHashSize2() == 0);
    }

    public void testMultipleTimer() throws Exception {
        TimerHandler timerHandler = TimerHandler.getInstance();
        timerHandler.setTimer(System.currentTimeMillis() + 500, new Runnable() {
            public void run() {
                timerReceived();
            }
        });
        timerHandler.setTimer(System.currentTimeMillis() + 750, new Runnable() {
            public void run() {
                timerReceived();
            }
        });
        timerHandler.setTimer(System.currentTimeMillis() + 1000, new Runnable() {
            public void run() {
                timerReceived();
            }
        });

        sleep(3500);
        assertTrue(received == 3);
        assertTrue(timerHandler.getHashSize1() == 0);
        assertTrue(timerHandler.getHashSize2() == 0);
    }

    public void testCancelTimer() throws Exception {
        TimerHandler timerHandler = TimerHandler.getInstance();

        Runnable action = new Runnable() {
            public void run() {
                timerReceived();
            }
        };

        timerHandler.setTimer(System.currentTimeMillis() + 700, action);
        sleep(100);
        timerHandler.cancelTimer(action);

        sleep(1500);
        assertTrue(received == 0);
        assertTrue(timerHandler.getHashSize1() == 0);
        assertTrue(timerHandler.getHashSize2() == 0);
    }


    private void timerReceived() {
        received++;
    }

    private void sleep(long t) {
        try {
            Thread.sleep(t);
        } catch (Exception e) {
        }
    }
}

