/**
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2011 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */
package com.funambol.platform;

import java.io.InputStream;
import java.net.SocketException;

import junit.framework.TestCase;

/**
 * @author rainbowbreeze
 *
 */
public class HttpConnectionAdapterTest extends TestCase {
    private static final String URI_DOWNLOAD = "http://releases.ubuntu.com/lucid/ubuntu-10.04.3-desktop-amd64.iso";

    /**
     * Tests cancelling of a download before the download starts
     */
    public void testCancelOperation_Immediate() throws Exception {
        final HttpConnectionAdapter conn = new HttpConnectionAdapter();

        //a very big files
        String url = URI_DOWNLOAD;
        conn.cancelOperation();
        conn.open(url, null);
        boolean interrupted = false;
        try {
            conn.execute(null, -1);
            InputStream is = conn.openInputStream();
            assertNotNull("Wrong input stream", is);
            
            byte[] data = new byte[1024];
            int cycles = 0;
            while ((is.read(data)) != -1) {
                cycles++;
                //force exit (less that 3 cycles will fail the test, I don't know why :(
                if (5 == cycles) break;
            }
        } catch (SocketException e) {
            interrupted = true;
        } finally {
            conn.close();
        }
        
        assertTrue("Thread not interrupted", interrupted);
    }
    
    
    /**
     * Tests cancelling of a download as soon as the download starts
     */
    public void testCancelOperation_JustStarted() throws Exception {
        final HttpConnectionAdapter conn = new HttpConnectionAdapter();

        //a very big files
        String url = URI_DOWNLOAD;
        conn.open(url, null);
        conn.execute(null, -1);
        InputStream is = conn.openInputStream();
        assertNotNull("Wrong input stream", is);

        conn.cancelOperation();
        byte[] data = new byte[1024];
        int cycles = 0;
        boolean interrupted = false;
        try {
            while ((is.read(data)) != -1) {
                cycles++;
                //force exit (less that 3 cycles will fail the test, I don't know why :(
                if (5 == cycles) break;
            }
        } catch (SocketException e) {
            interrupted = true;
        } finally {
            conn.close();
        }
        
        assertTrue("Thread not interrupted", interrupted);
    }
    
    
    /**
     * Tests cancelling of a download while downloading
     */
    public void testCancelOperation_WhileDownloading() throws Exception {
        final HttpConnectionAdapter conn = new HttpConnectionAdapter();

        //a very big files
        String url = URI_DOWNLOAD;
        conn.open(url, null);
        conn.execute(null, -1);
        
        InputStream is = conn.openInputStream();
        assertNotNull("Wrong input stream", is);

        //when started, download is cancelled
        Thread thread = new Thread() {
            public void run() {
                conn.cancelOperation();
            };
        };
        
        byte[] data = new byte[1024];
        int cycles = 0;
        boolean interrupted = false;
        try {
            while ((is.read(data)) != -1) {
                cycles++;
                //starts cancellation of download operation
                if (4 == cycles) thread.start();
                //force exit
                if (100 == cycles) break;
            }
        } catch (SocketException e) {
            interrupted = true;
        } finally {
            conn.close();
        }
        
        assertTrue("Thread not interrupted", interrupted);
    }
}
