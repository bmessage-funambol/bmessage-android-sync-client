/**
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2011 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */
package com.funambol.sapisync.source.util;

import java.io.FileOutputStream;

import android.content.Context;
import android.test.AndroidTestCase;

/**
 * 
 */
public class HttpDownloaderTest extends AndroidTestCase {
    private static final String TESTFILE = "testoutput";
    private static final String URI_HUGE_DOWNLOAD = "http://releases.ubuntu.com/lucid/ubuntu-10.04.3-desktop-amd64.iso";
    private static final String URI_SMALL_DOWNLOAD = "http://www.italia.it/";
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        getContext().deleteFile(TESTFILE);
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        getContext().deleteFile(TESTFILE);
    }
    
    public void testCancelDownload() throws Exception {
        //creates the download
        Listener listener = new Listener();
        HttpDownloader downloader = new HttpDownloader(listener);
        listener.setDownloader(downloader);
        
        long actualDownloaded = -1;
        long expectedSmallTotalSize = -1;
        
        //perform a small download, no interruption
        listener.setInterruption(0);
        actualDownloaded = performDownload(downloader, URI_SMALL_DOWNLOAD);
        //workaround because totalsize doesn't work
        expectedSmallTotalSize = (listener.totalSize > 0) 
                ? listener.totalSize
                : actualDownloaded;
        assertTrue("Download not finished", listener.finished);
        assertTrue("Wrong downloaded bytes", listener.progress > 0);
        assertTrue("Wrong downloaded bytes, maybe site is down?", expectedSmallTotalSize > 0);
        
        //interrupts download before the start
        listener.setInterruption(0);
        downloader.cancelCurrentDownload();
        actualDownloaded = performDownload(downloader, URI_HUGE_DOWNLOAD);
        assertTrue("Download finished", listener.finished);
        assertEquals("Wrong downloaded bytes", 0, listener.progress);
        assertEquals("Wrong downloaded bytes", 0, actualDownloaded);
        
        //performs another download, without interruption
        listener.setInterruption(0);
        actualDownloaded = performDownload(downloader, URI_SMALL_DOWNLOAD);
        assertTrue("Download not finished", listener.finished);
        assertEquals("Wrong downloaded bytes", expectedSmallTotalSize, actualDownloaded);
        
        //interrupts download some bytes after the begin
        final int downloadThreshold = 10000;
        listener.setInterruption(downloadThreshold);
        actualDownloaded = performDownload(downloader, URI_HUGE_DOWNLOAD);
        assertTrue("Download not finished", listener.finished);
        assertTrue("Wrong downloaded bytes", listener.progress >= downloadThreshold);
        assertTrue("Wrong downloaded bytes", listener.progress < downloadThreshold * 5);
        assertTrue("Wrong downloaded bytes", actualDownloaded >= downloadThreshold);
        assertTrue("Wrong downloaded bytes", actualDownloaded < downloadThreshold * 5);
        
        //performs last download, without interruption
        listener.setInterruption(0);
        actualDownloaded = performDownload(downloader, URI_SMALL_DOWNLOAD);
        assertTrue("Download not finished", listener.finished);
        assertEquals("Wrong downloaded bytes", expectedSmallTotalSize, actualDownloaded);
    }
    
    
    private long performDownload(HttpDownloader downloader, String urlToDownload) throws Exception {
        FileOutputStream os = getContext().openFileOutput(TESTFILE, Context.MODE_PRIVATE);
        long downloaded = -1;
        try {
            downloaded = downloader.download(URI_SMALL_DOWNLOAD, os, null);
        } catch (DownloadException e) {
            downloaded = e.getPartialLength();
        } finally {
            os.close();
        }
        return downloaded;
    }
    
    private class Listener implements HttpDownloader.DownloadListener {
        public boolean finished;
        public long progress;
        public long totalSize;
        private long interruptAfter;
        private HttpDownloader httpDownloader;
        private boolean interrupted;
        
        public void setDownloader(HttpDownloader httpDownloader) {
            this.httpDownloader = httpDownloader;
        }
        
        /**
         * Reset all internal fields and sets new interruption threshold
         * @param interruptAfter how many bytes prior to interrupt download. If 0, no interruption
         */
        public void setInterruption(long interruptAfter) {
            this.interruptAfter = interruptAfter;
            finished = false;
            progress = 0;
            interrupted = false;
        }

        public void downloadStarted(long totalSize) {
            this.totalSize = totalSize;
        }

        public void downloadProgress(long size) {
            progress = size;
            if (interruptAfter > 0 && size >= interruptAfter && !interrupted) {
                interrupted = true; //only one interruption
                //simulates external thread
                (new Thread(new Runnable() {
                    public void run() {
                        httpDownloader.cancelCurrentDownload();
                    }
                })).start();
            }
        }

        public void downloadEnded() {
            finished = true;
        }

        public void setGood(boolean good) {}

    }
}
