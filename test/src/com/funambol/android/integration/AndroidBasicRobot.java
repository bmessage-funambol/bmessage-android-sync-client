/*
 * Funambol is a mobile platform developed by Funambol, Inc. 
 * Copyright (C) 2003 - 2009 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission 
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE 
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License 
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite 
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably 
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.integration;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Hashtable;
import java.util.Vector;

import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.EditText;
import android.app.Activity;
import android.app.Instrumentation;
import android.content.Context;
import android.accounts.AccountManager;
import android.accounts.Account;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.SystemClock;
import android.view.MotionEvent;

import com.funambol.android.AndroidConfiguration;
import com.funambol.android.AppInitializer;
import com.funambol.android.BuildInfo;
import com.funambol.androidsync.R;
import com.funambol.android.activities.AndroidDisplayManager;
import com.funambol.android.controller.AndroidController;
import com.funambol.android.source.media.MediaThumbnailView;
import com.funambol.client.ui.DisplayManager;
import com.funambol.client.configuration.Configuration;
import com.funambol.client.controller.Controller;
import com.funambol.client.controller.MainScreenController;
import com.funambol.client.source.AppSyncSource;
import com.funambol.client.source.AppSyncSourceManager;
import com.funambol.client.test.ClientTestException;
import com.funambol.client.test.basic.BasicCommandRunner;
import com.funambol.client.test.basic.BasicRobot;
import com.funambol.client.test.util.TestFileManager;
import com.funambol.sync.NonBlockingSyncException;
import com.funambol.sync.SyncException;
import com.funambol.sync.SyncSource;
import com.funambol.util.Log;


public class AndroidBasicRobot extends BasicRobot {
    
    private static final String TAG_LOG = AndroidBasicRobot.class.getSimpleName();

    private final Instrumentation instrumentation;
    private final Context appContext;

    public AndroidBasicRobot(Instrumentation instrumentation,
                             TestFileManager fileManager,
                             Hashtable vars,
                             Context context)
    {
        super(fileManager, vars);
        this.instrumentation = instrumentation;
        this.appContext = context;
    }

    public void waitForActivity(String name, int timeout) throws Throwable {
        String currentActivityName = getCurrentActivityName();
        while(!name.equals(currentActivityName)) {
            Thread.sleep(WAIT_DELAY);
            timeout -= WAIT_DELAY;
            if (timeout < 0) {
                throw new ClientTestException("Timeout waiting activity: " + name);
            }
            currentActivityName = getCurrentActivityName();
        }
        // Wait a bit so the activity can be built
        delay(500);
    }

    private String getCurrentActivityName() {
        DisplayManager displayManager = AppInitializer.i(appContext).getDisplayManager();
        Activity activity = (Activity)displayManager.getCurrentUiScreen();

        if (activity == null) {
            return null;
        } else {
            return activity.getClass().getSimpleName();
        }
    }

    public void keyPress(String keyName, int count) throws Throwable {
        int keyCode = -1;
        boolean done = false;
        Activity activity = getCurrentActivity();
        View currentView = getFocusedView();

        if (BasicCommandRunner.DOWN_KEY_NAME.equals(keyName)) {
            // At the moment we handle only components that do not handle down
            // internally, but the down simply moves the focus
            // This is necessary because some devices such as the Motorola
            // DROIDX do not handle properly these events
            if (currentView != null) {
                int nextFocusDownId = currentView.getNextFocusDownId();
                if (nextFocusDownId != View.NO_ID) {
                    final View nextView = activity.findViewById(nextFocusDownId);
                    activity.runOnUiThread(new Runnable() {
                        public void run() {
                            nextView.requestFocus();
                        }
                    });
                    done = true;
                }
            }
            if (!done) {
                keyCode = KeyEvent.KEYCODE_DPAD_DOWN;
            }
        } else if (BasicCommandRunner.UP_KEY_NAME.equals(keyName)) {
            keyCode = KeyEvent.KEYCODE_DPAD_UP;
        } else if (BasicCommandRunner.LEFT_KEY_NAME.equals(keyName)) {
            keyCode = KeyEvent.KEYCODE_DPAD_LEFT; 
        } else if (BasicCommandRunner.RIGHT_KEY_NAME.equals(keyName)) {
            keyCode = KeyEvent.KEYCODE_DPAD_RIGHT;
        } else if (BasicCommandRunner.FIRE_KEY_NAME.equals(keyName)) {
            if (currentView != null) {
                if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER) {
                    if (currentView instanceof Button) {
                        final Button btn = (Button)currentView;
                        for(int i=0; i<count; i++) {
                            activity.runOnUiThread(new Runnable() {
                                public void run() {
                                    btn.performClick();
                                    delay(1000);
                                }
                            });
                        }
                        done = true;
                    } else if (currentView instanceof Spinner) {
                        final Spinner spn = (Spinner)currentView;
                        for(int i=0; i<count; i++) {
                            activity.runOnUiThread(new Runnable() {
                                public void run() {
                                    spn.performClick();
                                    delay(1000);
                                }
                            });
                        }
                        done = true;
                    }
                } 
            }
            if (!done) {
                keyCode = KeyEvent.KEYCODE_DPAD_CENTER;
            }
        } else if (BasicCommandRunner.MENU_KEY_NAME.equals(keyName)) {
            keyCode = KeyEvent.KEYCODE_MENU;
        } else if (BasicCommandRunner.BACK_KEY_NAME.equals(keyName)) {
            keyCode = KeyEvent.KEYCODE_BACK;
        } else if (BasicCommandRunner.DEL_KEY_NAME.equals(keyName)) {
            keyCode = KeyEvent.KEYCODE_DEL;
        } else {
            Log.error(TAG_LOG, "Unknown keyName: " + keyName);
            throw new IllegalArgumentException("Unknown keyName: " + keyName);
        }
        // There is a bug on the Samsung Galaxy S, so that the instrumentation
        // throws a NullPointerException when generating a DPAD_CENTER on
        // buttons. For this reason on this device we invoke the performClick
        // directly
        if (!done) {
            instrumentation.waitForIdleSync();
            for(int i=0; i<count; i++) {
                instrumentation.sendCharacterSync(keyCode);
                instrumentation.waitForIdleSync();
                delay(100);
            }
        } else {
            delay(500);
        }
    }

    public void clickOnView(String tag) throws Throwable {
        final View view = getViewByTag(tag);
        if(view != null) {
            view.post(new Runnable() {
                public void run() {
                    view.requestFocus();
                    if(view instanceof MediaThumbnailView) {
                        ((MediaThumbnailView)view).dispatchTouchEvent(
                                createSimulatedMotionEvent());
                    } else {
                        view.performClick();
                    }
                    
                }
            });
            delay(500);
        } else {
            Log.error(TAG_LOG, "Cannot find view with tag: " + tag);
            throw new IllegalArgumentException("Cannot find view with tag: " + tag);
        }
    }

    private MotionEvent createSimulatedMotionEvent() {
        long downTime = SystemClock.uptimeMillis();
        long eventTime = SystemClock.uptimeMillis() + 100;
        MotionEvent motionEvent = MotionEvent.obtain(
            downTime,
            eventTime,
            MotionEvent.ACTION_UP,
            0.0f,
            0.0f,
            0
        );
        return motionEvent;
    }

    public void writeString(String text) throws Throwable {
        for(int i=0;i<text.length();++i) {
            char ch = text.charAt(i);
            instrumentation.waitForIdleSync();
            instrumentation.sendStringSync(String.valueOf(ch));
            instrumentation.waitForIdleSync();
        }
        delay(750);
    }

    public void clearTextField() throws Throwable {
        View currentView = getFocusedView();
        if (currentView instanceof EditText) {
            final EditText edit = (EditText)currentView;
            Activity activity = getCurrentActivity();
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    edit.setText("");
                }
            });
        } else {
            throw new ClientTestException("Cannot clear text on non editable field");
        }
        delay(500);
    }

    private View getFocusedView() {
        Activity activity = getCurrentActivity();
        if (activity != null && activity.hasWindowFocus()) {
            View currentView = activity.getCurrentFocus();
            return currentView;
        }
        return null;
    }

    private View getViewByTag(String tag) {
        Activity activity = getCurrentActivity();
        if (activity != null) {
            View content = activity.findViewById(android.R.id.content);
            View root = content.getRootView();
            return root.findViewWithTag(tag);
        }
        return null;
    }

    private Activity getCurrentActivity() {
        DisplayManager displayManager = AppInitializer.i(appContext).getDisplayManager();
        Activity activity = (Activity)displayManager.getCurrentUiScreen();
        return activity;
    }

    public static void waitDelay(int d) {
        delay(d * 1000);
    }

    public static void removeAccount(Context context) throws Throwable {
        AccountManager am = AccountManager.get(context);
        Account[] accounts = am.getAccountsByType(context.getString(R.string.account_type));
        for(int i=0; i<accounts.length; i++) {
            am.removeAccount(accounts[i], null, null);
        }
        SharedPreferences settings = context.getSharedPreferences(
                AndroidConfiguration.KEY_FUNAMBOL_PREFERENCES, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.commit();
    }

    public void cancelSync() throws Throwable {
        AndroidController controller = AppInitializer.i(appContext).getController();
        // TODO FIXME
    }

    public void cancelSyncAfterPhase(String phaseName, int num, int progress)
            throws Throwable {
        Log.debug(TAG_LOG, "Preparing to interrupt sync after phase " +
                phaseName + "," + num + "," + progress);
        // TODO FIXME
    }

    public void waitForSyncPhase(String phaseName, int num, int progress,
            int timeout) throws Throwable {
        Log.debug(TAG_LOG, "Waiting for sync phase " + phaseName + "," + 
                num + "," + progress);
        // TODO FIXME
    }

    public void checkLastAlertMessage(String message) throws Throwable {
        AndroidDisplayManager dm = (AndroidDisplayManager)
                AppInitializer.i(appContext).getDisplayManager();
        String lastMessage = dm.readAndResetLastMessage();
        assertTrue(lastMessage, message, "Last alert message mismatch");
    }
    
    private SyncSource createProxySource(SyncSource source, SyncSourceProxy sourceProxy) {
        Vector allInterfaces = new Vector();
        addInterfaces(allInterfaces, source.getClass());
        Class cl[] = new Class[allInterfaces.size()];
        for(int i=0;i<allInterfaces.size();++i) {
            cl[i] = (Class)allInterfaces.elementAt(i);
        }
        return (SyncSource) java.lang.reflect.Proxy.newProxyInstance(
                source.getClass().getClassLoader(), cl, sourceProxy);
    }
    
    private class SyncSourceProxy implements java.lang.reflect.InvocationHandler {

        private Object obj;
        private boolean errorOnReceiving;
        private int errorOnItem = -1;
        private int receivedItems;
        private boolean blockingError;

        private SyncSourceProxy(SyncSource obj) {
              this.obj = obj;
        }

        public Object invoke(Object proxy, Method m, Object[] args) throws Throwable {
            Object result = null;
            // TODO FIXME: handle the sending part
            try {
                boolean done = false;
                if ("beginSync".equals(m.getName())) {
                    receivedItems = 0;
                } else if ("applyChanges".equals(m.getName())) {
                    // This method has a single parameter
                    Vector items = (Vector)args[0];
                    
                    if (Log.isLoggable(Log.TRACE)) {
                        Log.trace(TAG_LOG, "applyChanges invoked");
                        Log.trace(TAG_LOG, "errorOnReceiving=" + errorOnReceiving);
                        Log.trace(TAG_LOG, "errorOnItem=" + errorOnItem);
                        Log.trace(TAG_LOG, "receivedItems=" + receivedItems);
                        Log.trace(TAG_LOG, "items.size()=" + items.size());
                    }
                    if (errorOnReceiving && errorOnItem >= 0 && receivedItems + items.size() >= errorOnItem) {
                        // We must interrupt this group
                        int idx = errorOnItem - receivedItems;
                        Vector newItems = new Vector();
                        for(int i=0;i<idx;++i) {
                            newItems.addElement(items.elementAt(i));
                        }
                        args[0] = newItems;
                        done = true;
                        result = m.invoke(obj, args);
                        // Clean up the break info
                        errorOnItem = -1;
                        // Now we can throw the exception
                        if (blockingError) {
                            throw new SyncException(SyncException.CLIENT_ERROR, "Simulated error on receiving item");
                        } else {
                            throw new NonBlockingSyncException(SyncException.CLIENT_ERROR,
                                                               "Simulated error on receiving item");
                        }
                    }
                    receivedItems += items.size();
                }
                if (!done) {
                    result = m.invoke(obj, args);
                }
                Log.trace(TAG_LOG, "result = " + result);
            } catch (InvocationTargetException e) {
                throw e.getTargetException();
            } catch (Exception e) {
                Log.error(TAG_LOG, "Runtime exception", e);
                throw e;
            } finally {
                Log.trace(TAG_LOG, "end method " + m.getName());
            }
            return result;
        }
        
        public void setInterruption(boolean receiving, int number, boolean blocking) {
            this.errorOnReceiving = receiving;
            this.errorOnItem = number;
            this.blockingError = blocking;
        }
    }
    
    public void simulateSourceError(String sourceName, boolean receiving, int number, boolean blocking)
    throws Throwable
    {
        AppSyncSource appSource = getAppSyncSource(sourceName);
        SyncSource ss = appSource.getSyncSource();
        SyncSourceProxy syncSourceProxy = (SyncSourceProxy)proxyTable.get(sourceName);
        if (syncSourceProxy == null) {
            syncSourceProxy = new SyncSourceProxy(ss);
            SyncSource proxySource = createProxySource(ss, syncSourceProxy);
            proxyTable.put(sourceName, syncSourceProxy);
            appSource.setSyncSource(proxySource);
        }
        syncSourceProxy.setInterruption(receiving, number, blocking);
    }

    private static void delay(int msec) {
        try {
            Thread.sleep(msec);
        } catch (Exception e) {
            Log.error(TAG_LOG, "Exception Occurred while sleeping", e);
        }
    }

    protected Configuration getConfiguration() {
        return AppInitializer.i(appContext).getConfiguration();
    }

    protected Controller getController() {
        return AppInitializer.i(appContext).getController();
    }

    protected AppSyncSourceManager getAppSyncSourceManager() {
        return AppInitializer.i(appContext).getAppSyncSourceManager();
    }

    protected void startMainApp() throws Throwable {
        PackageManager pm = instrumentation.getTargetContext().getPackageManager();
        Intent intent = pm.getLaunchIntentForPackage(BuildInfo.PACKAGE_NAME);
        instrumentation.startActivitySync(intent);
    }

    /**
     * This implementation works as long as the main screen is currently on top of the stack
     */
    protected void closeMainApp() throws Throwable {
        Controller controller = AppInitializer.i(appContext).getController();
        MainScreenController mainScreenController = controller.getMainScreenController();
        Activity mainScreen = (Activity)mainScreenController.getMainScreen();
        if (mainScreen != null) {
            mainScreen.finish();
        }
    }
    
    private void addInterfaces(Vector iList, Class klass) {
        if (klass == null) {
            return;
        }
        Class interfaces[] = klass.getInterfaces();
        if (interfaces != null) {
            for(int i=0;i<interfaces.length;++i) {
                iList.addElement(interfaces[i]);
            }
        }
        Class superClass  = klass.getSuperclass();
        addInterfaces(iList, superClass);
    }
 
}
