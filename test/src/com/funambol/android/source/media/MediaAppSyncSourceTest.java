/*
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2011 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.source.media;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.funambol.android.AppInitializer;
import com.funambol.android.MimeTypeMap;
import com.funambol.client.customization.Customization;
import com.funambol.client.source.MediaMetadata;
import com.funambol.storage.Table;
import com.funambol.storage.Tuple;
import com.funambol.util.Log;

import android.test.AndroidTestCase;

public class MediaAppSyncSourceTest extends AndroidTestCase {
    private static final String TAG_LOG = MediaAppSyncSourceTest.class.getSimpleName();

    private int backupLogLevel;
    private MediaAppSyncSource mediaAppSyncSource;

    @Override
    protected void setUp() throws Exception {
        backupLogLevel = Log.getLogLevel();
        Log.setLogLevel(Log.TRACE);

        Customization customization = AppInitializer.i(getContext()).getCustomization();
        mediaAppSyncSource = new MediaAppSyncSource("mediatest", customization);
        clearTable();
    }

    @Override
    protected void tearDown() throws Exception {
        Log.setLogLevel(backupLogLevel);
    }
    
    public void testIsItemModified() throws Exception {
        Log.debug(TAG_LOG, "testIsItemModified has started");
        
        File cacheDir = getContext().getCacheDir();
        File tempFile = new File(cacheDir, "test1023.txt");
        if (tempFile.exists()) tempFile.delete();
        
        tempFile.deleteOnExit();
        FileOutputStream fos = new FileOutputStream(tempFile);
        fos.write("This is a first test".getBytes());
        fos.close();

        //not modified item
        //add the file to metadata table
        Table table = mediaAppSyncSource.getMetadataTable();
        table.open();
        Tuple row = table.createNewRow();
        row.setField(table.getColIndexOrThrow(MediaMetadata.METADATA_NAME), tempFile.getName());
        row.setField(table.getColIndexOrThrow(MediaMetadata.METADATA_THUMBNAIL_PATH),"");
        row.setField(table.getColIndexOrThrow(MediaMetadata.METADATA_PREVIEW_PATH),"");
        row.setField(table.getColIndexOrThrow(MediaMetadata.METADATA_ITEM_PATH), tempFile.getAbsolutePath());
        row.setField(table.getColIndexOrThrow(MediaMetadata.METADATA_ORDER_DATE), tempFile.lastModified());
        row.setField(table.getColIndexOrThrow(MediaMetadata.METADATA_LAST_MODIFIED_DATE), tempFile.lastModified());
        row.setField(table.getColIndexOrThrow(MediaMetadata.METADATA_SYNCHRONIZED),0);
        String extension = MimeTypeMap.getFileExtensionFromUrl(tempFile.getName());                    
        row.setField(table.getColIndexOrThrow(MediaMetadata.METADATA_MIME), MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension));
        row.setField(table.getColIndexOrThrow(MediaMetadata.METADATA_ITEM_REMOTE_URL), "");
        row.setField(table.getColIndexOrThrow(MediaMetadata.METADATA_UPLOAD_CONTENT_STATUS), 0L);
        row.setField(table.getColIndexOrThrow(MediaMetadata.METADATA_SIZE), tempFile.length());
        table.insert(row);
        table.save();
        table.close();

        //now checks
        assertFalse("Unmodified file not correctly detected", mediaAppSyncSource.isItemModified(row));
        
        //modified item
        fos = new FileOutputStream(tempFile);
        fos.write("This is a second test".getBytes());
        fos.close();

        //now checks
        assertTrue("Modified file not correctly detected", mediaAppSyncSource.isItemModified(row));
    }
    

    // --------------------------------------------------------- Private Method
    /**
     * Clear table
     * @throws IOException
     */
    private void clearTable() throws IOException {
        Table table = mediaAppSyncSource.getMetadataTable();
        table.open();
        table.reset();
        table.close();
    }

    
}
