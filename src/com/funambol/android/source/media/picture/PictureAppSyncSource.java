/*
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2011 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.source.media.picture;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.media.ExifInterface;

import com.funambol.android.source.media.MediaAppSyncSource;
import com.funambol.client.customization.Customization;
import com.funambol.client.source.FunambolFileSyncSource;
import com.funambol.client.source.MediaMetadata;
import com.funambol.client.source.PictureMetadata;
import com.funambol.storage.Table;
import com.funambol.storage.Tuple;
import com.funambol.util.MediaUtils;
import com.funambol.util.Log;
import com.funambol.util.StringUtil;
import java.util.TimeZone;

public class PictureAppSyncSource extends MediaAppSyncSource {

    private static final String TAG_LOG = PictureAppSyncSource.class.getSimpleName();

    public PictureAppSyncSource(String name, FunambolFileSyncSource source, Customization customization) {
        super(name, source, customization);
    }

    public PictureAppSyncSource(String name, Customization customization) {
        super(name, customization);
    }

    @Override
    protected boolean createThumbnail(String inFile, String outFile, int width, int height) {
        int counterRotate = computeCounterRotation(inFile);
        return MediaUtils.resizeImage(inFile, outFile, width, height, counterRotate);
    }

    @Override
    protected boolean createThumbnailUsingMediaProvider(long itemId, String outFile, int width, int height) {
        MediaUtils utils = new MediaUtils(getAppContext());
        return utils.createPictureThumbnailFromProvider(itemId, outFile, width, height);
    }
    
    protected int computeCounterRotation(String inFile) {
        int counterRotate = 0; 
        try {
            ExifInterface exif = new ExifInterface(inFile);
            int exifOrientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            if (ExifInterface.ORIENTATION_ROTATE_90 == exifOrientation) {
                counterRotate = 90;
            } else if (ExifInterface.ORIENTATION_ROTATE_180 == exifOrientation) {
                counterRotate = 180;
            } else if (ExifInterface.ORIENTATION_ROTATE_270 == exifOrientation) {
                counterRotate = 270;
            }
        } catch (Exception e) {
            Log.error(TAG_LOG, "Cannot determine orientation", e);
        }
        return counterRotate;
    }

    @Override
    protected MediaMetadata createMediaMetadata() {
        // Use a standard plain media metadata without source specific
        // extensions
        return new PictureMetadata(this);
    }
    
    @Override
    public void addSpecificMetadata(Tuple item) {
        super.addSpecificMetadata(item);
        Table metadata = getMetadataTable();
        String itemPath = item.getStringField(item.getColIndexOrThrow(MediaMetadata.METADATA_ITEM_PATH));
        if (itemPath.startsWith(FILE_PROTOCOL)) {
            itemPath = itemPath.substring(FILE_PROTOCOL.length());
        }
        ExifInterface exif;
        try {
            exif = new ExifInterface(itemPath);
        } catch (IOException e) {
            if (Log.isLoggable(Log.TRACE)) {
                Log.debug(TAG_LOG, "Unable to extract EXIF information from " + "local file " + itemPath);
            }
            return;
        }
        String creationDateDescription = exif.getAttribute(ExifInterface.TAG_DATETIME);
        if (!StringUtil.isNullOrEmpty(creationDateDescription)) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("y:M:d H:m:s");
                // We assume this is an UTC date
                sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
                Date creationDate = sdf.parse(creationDateDescription);
                if (creationDate != null && creationDate.getTime() > 0) {
                    item.setField(metadata.getColIndexOrThrow(
                            PictureMetadata.METADATA_CREATION_DATE), creationDate.getTime());
                }
            } catch (ParseException pe) {
                if (Log.isLoggable(Log.ERROR)) {
                    Log.error("Unable to parse " + creationDateDescription);
                }
                // Skips it 
            }
        }
        String model = exif.getAttribute(ExifInterface.TAG_MODEL);
        if (!StringUtil.isNullOrEmpty(model)) {                       
            item.setField(metadata.getColIndexOrThrow(PictureMetadata.METADATA_DEVICE_MODEL), model);
        }
        String maker = exif.getAttribute(ExifInterface.TAG_MAKE);
        if (!StringUtil.isNullOrEmpty(maker)) {                       
            item.setField(metadata.getColIndexOrThrow(PictureMetadata.METADATA_DEVICE_MAKER), maker);
        }
        int height = exif.getAttributeInt(ExifInterface.TAG_IMAGE_LENGTH, -1);
        int width = exif.getAttributeInt(ExifInterface.TAG_IMAGE_WIDTH, -1);
        if (height > 0 && width > 0) {
            item.setField(metadata.getColIndexOrThrow(PictureMetadata.METADATA_HEIGHT), height);
            item.setField(metadata.getColIndexOrThrow(PictureMetadata.METADATA_WIDTH), width);
        }
    }
}
