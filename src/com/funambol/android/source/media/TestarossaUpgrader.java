/**
 * Funambol is a mobile platform developed by Funambol, Inc. 
 * Copyright (C) 2011 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission 
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE 
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License 
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite 
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably 
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol". 
 */


package com.funambol.android.source.media;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Vector;

import android.content.Context;

import com.funambol.android.AndroidAppSyncSource;
import com.funambol.android.controller.AndroidCreateThumbnailsTask;
import com.funambol.android.source.media.picture.PictureAppSyncSource;
import com.funambol.client.source.AppSyncSource;
import com.funambol.client.source.MediaMetadata;
import com.funambol.client.source.PictureMetadata;
import com.funambol.concurrent.TaskExecutor;
import com.funambol.platform.FileAdapter;
import com.funambol.platform.PlatformEnvironment;
import com.funambol.storage.StringKeyValuePair;
import com.funambol.storage.StringKeyValueSQLiteStore;
import com.funambol.storage.StringKeyValueStore;
import com.funambol.storage.StringKeyValueStoreFactory;
import com.funambol.storage.Table;
import com.funambol.storage.Table.BulkOperation;
import com.funambol.storage.Table.BulkOperationException;
import com.funambol.storage.Tuple;
import com.funambol.util.Log;
import com.funambol.util.StringUtil;

public class TestarossaUpgrader {
    
    private static final String TAG_LOG = TestarossaUpgrader.class.getSimpleName();
    
    private AppSyncSource appSource;
    private TaskExecutor taskExecutor = new TaskExecutor();
    
    public TestarossaUpgrader(AppSyncSource appSource) {
        this.appSource = appSource;
        taskExecutor.setMaxThreads(1);
    }
    
    public void upgrade() {
        if (Log.isLoggable(Log.INFO)) {
            Log.info(TAG_LOG, "Upgrading source " + appSource.getName());
        }
        
        if (appSource instanceof MediaAppSyncSource) {
            MediaAppSyncSource mediaAppSyncSource = (MediaAppSyncSource)appSource;
            MediaAppSyncSourceConfig appSourceConfig = (MediaAppSyncSourceConfig)appSource.getConfig();
            
            // Create the data directory 
            String dataDirectory = mediaAppSyncSource.getDataDirectory();
            try {
                FileAdapter dataDir = new FileAdapter(dataDirectory);
                if (!dataDir.exists()) {
                    dataDir.mkdirs();
                }
            } catch (IOException ioe) {
                Log.error(TAG_LOG, "Cannot create data directory, refresh will handle the error later");
            }
            
            // We need to import into the user digital life everything that was in sync before
            OldMappingTable oldMapping = new OldMappingTable(appSource.getName().toLowerCase());
            Table metadata = mediaAppSyncSource.getMetadataTable();
            FileAdapter fa = null;
            try {
                metadata.open();
                oldMapping.load();
               
                Vector operations = new Vector();
                Enumeration keys = oldMapping.keys();
                while(keys.hasMoreElements()) {
                    String guid = (String)keys.nextElement();
                    String luid = oldMapping.getLuid(guid);
                    String name = oldMapping.getName(guid);
                    String crc  = oldMapping.getCRC(guid);
                    
                    if (Log.isLoggable(Log.INFO)) {
                        Log.info(TAG_LOG, "Importing an existing item " + luid);
                    }
                    
                    String itemPath = luid;
                    fa = new FileAdapter(itemPath);
                    
                    // This item was in sync. We must create a new entry in the metadata table and in the new mapping
                    // one
                    Tuple newRow = metadata.createNewRow();
                    newRow.setField(newRow.getColIndexOrThrow(MediaMetadata.METADATA_NAME), name);
                    newRow.setField(newRow.getColIndexOrThrow(MediaMetadata.METADATA_THUMBNAIL_PATH), "");
                    newRow.setField(newRow.getColIndexOrThrow(MediaMetadata.METADATA_PREVIEW_PATH), "");
                    newRow.setField(newRow.getColIndexOrThrow(MediaMetadata.METADATA_ITEM_PATH), "file://" + itemPath);
                    
                    String mimeType;
                    if (appSource instanceof AndroidAppSyncSource) {
                        AndroidAppSyncSource androidAppSyncSource = (AndroidAppSyncSource)appSource;
                        mimeType = androidAppSyncSource.inferMimeType(itemPath);
                    } else {
                        mimeType = "application/octet-stream";
                    }
                    newRow.setField(newRow.getColIndexOrThrow(MediaMetadata.METADATA_MIME), mimeType);
                   
                    newRow.setField(newRow.getColIndexOrThrow(MediaMetadata.METADATA_LAST_MODIFIED_DATE),
                                                              fa.lastModified());
                    newRow.setField(newRow.getColIndexOrThrow(MediaMetadata.METADATA_SYNCHRONIZED), 1L);
                    newRow.setField(newRow.getColIndexOrThrow(MediaMetadata.METADATA_DELETED), 0L);
                    newRow.setField(newRow.getColIndexOrThrow(MediaMetadata.METADATA_DIRTY), 0L);
                    newRow.setField(newRow.getColIndexOrThrow(MediaMetadata.METADATA_DIRTY_CONTENT), 0L);
                    long size = fa.getSize();
                    newRow.setField(newRow.getColIndexOrThrow(MediaMetadata.METADATA_SIZE), size);
                    newRow.setField(newRow.getColIndexOrThrow(MediaMetadata.METADATA_GUID), guid);

                    //public static final String METADATA_ITEM_REMOTE_URL       = "item_remote_url";
                    newRow.setField(newRow.getColIndexOrThrow(MediaMetadata.METADATA_UPLOAD_CONTENT_STATUS), 
                                    MediaMetadata.ITEM_NOT_UPLOADED);
                    newRow.setField(newRow.getColIndexOrThrow(MediaMetadata.METADATA_IGNORE_PREVIEW), 0L);
                    newRow.setField(newRow.getColIndexOrThrow(MediaMetadata.METADATA_REMOTE_CRC), crc);
                    
                    mediaAppSyncSource.addSpecificMetadata(newRow);
                  
                    int orderDateIdx = newRow.getColIndexOrThrow(MediaMetadata.METADATA_ORDER_DATE);
                    if (appSource instanceof PictureAppSyncSource) {
                        int creationIdx = newRow.getColIndexOrThrow(PictureMetadata.METADATA_CREATION_DATE);
                        if (!newRow.isUndefined(creationIdx)) {
                            Long creationDate = newRow.getLongField(creationIdx);
                            if (creationDate.longValue() > 0) {
                                newRow.setField(orderDateIdx, creationDate);
                            }
                        }
                    }
                    
                    // If an order date has not been set, the we use the last modified
                    if (newRow.isUndefined(orderDateIdx)) {
                        newRow.setField(orderDateIdx, fa.lastModified());
                    }
                    
                    Table.BulkOperation op = metadata.new BulkOperation(BulkOperation.INSERT, newRow);
                    operations.addElement(op);
                }
                if (operations.size() > 0) {
                    int retryCount = 0;
                    boolean retry = false;
                    Vector remainingOps = operations;
                    do {
                        retry = false;
                        try {
                            metadata.bulkOperations(remainingOps);
                        } catch (BulkOperationException boe) {
                            int numSuccess = boe.getNumSuccess();
                            if (Log.isLoggable(Log.INFO)) {
                                Log.info(TAG_LOG, "Found items with the same order date, disambiguate them " + numSuccess);
                            }
                            Table.BulkOperation failOp = (Table.BulkOperation)remainingOps.elementAt(numSuccess);
                            Tuple failedTuple = failOp.getTuple();
                            int orderIdx = failedTuple.getColIndexOrThrow(MediaMetadata.METADATA_ORDER_DATE);
                            Long failedDate = failedTuple.getLongField(orderIdx);
                            failedTuple.setField(orderIdx, failedDate.longValue() + 1);
                           
                            // Prepare the list of remaining operations
                            Vector newRemainingOps = new Vector();
                            for(int i=numSuccess;i<remainingOps.size();++i) {
                                newRemainingOps.addElement(remainingOps.elementAt(i));
                            }
                            remainingOps = newRemainingOps;
                            if (Log.isLoggable(Log.DEBUG)) {
                                Log.debug(TAG_LOG, "Remaining operations " + remainingOps.size());
                            }
                            retryCount++;
                            if (retryCount < 100) {
                                retry = true;
                            }
                        } catch (Exception e) {
                            // TODO FIXME
                            Log.error(TAG_LOG, "Cannot import existing data", e);
                        }
                    } while(retry);
                    
                    // Thumb and preview are created in a separate task
                    for(int i=0;i<operations.size();++i) {
                        Table.BulkOperation op = (Table.BulkOperation)operations.elementAt(i);
                        Tuple item = op.getTuple();
                        AndroidCreateThumbnailsTask task = new AndroidCreateThumbnailsTask(metadata, item, appSource);
                        taskExecutor.scheduleTask(task);
                    }
                }
            } catch (Exception ioe) {
                Log.error(TAG_LOG, "Cannot migrate config, user is losing his media info", ioe);
            } finally {
                try {
                    metadata.close();
                } catch (IOException ioe) {}
                if (fa != null) {
                    try {
                        fa.close();
                    } catch (IOException ioe) {}
                }
            }
        }
    }
    
    private class OldMappingTable {
        
        private static final String TAG_LOG = "OldMappingTable";
        private StringKeyValueStore store;
        
        private class OldMappingStore extends StringKeyValueSQLiteStore {
            public OldMappingStore(Context c, String dbName, String tableName) {
                super(c, dbName, tableName);
            }
            
            protected String createDbName(String dbName, String tableName) {
                return dbName;
            }
        }

        public OldMappingTable(String sourceName) {
            StringKeyValueStoreFactory mappingFactory = StringKeyValueStoreFactory.getInstance();
            store = new OldMappingStore(PlatformEnvironment.getInstance().getContext(),
                                        "funambol.db" ,"mapping_" + sourceName);
        }

        public void reset() throws IOException {
            store.reset();
        }

        public void load() throws IOException {
            store.load();
        }

        public void save() throws IOException {
            store.save();
        }

        public void remove(String guid) {
            store.remove(guid);
        }

        public void add(String guid, String luid, String crc, String name) {
            String value = createValue(luid, crc, name);
            store.add(guid, value);
        }

        public void update(String guid, String luid, String crc, String name) {
            String value = createValue(luid, crc, name);
            store.update(guid, value);
        }

        public String getLuid(String guid) {
            String value = store.get(guid);
            return getFieldFromValue(value, 0);
        }

        public String getCRC(String guid) {
            String value = store.get(guid);
            return getFieldFromValue(value, 1);
        }

        public String getName(String guid) {
            String value = store.get(guid);
            return getFieldFromValue(value, 2);
        }

        public Enumeration keyValuePairs() {
            return store.keyValuePairs();
        }

        public Enumeration keys() {
            return store.keys();
        }

        public String getGuid(String luid) {
            Enumeration keyValuePairs = store.keyValuePairs();
            while (keyValuePairs.hasMoreElements()) {
                StringKeyValuePair pair = (StringKeyValuePair)keyValuePairs.nextElement();
                String value = pair.getValue();
                if (luid.equals(getFieldFromValue(value, 0))) {
                    return pair.getKey();
                }
            }
            return null;
        }

        public void add(String guid, String luid) {
            throw new IllegalArgumentException("Missing CRC");
        }

        public void put(String guid, String luid) {
            throw new IllegalArgumentException("Missing CRC");
        }

        public void update(String guid, String luid) {
            throw new IllegalArgumentException("Missing CRC");
        }

        // Both luid and crc cannot contain commas
        private String createValue(String luid, String crc, String name) {
            StringBuffer buf = new StringBuffer();
            buf.append(luid).append(",").append(crc).append(",").append(name);
            return buf.toString();
        }

        private String getFieldFromValue(String value, int idx) {
            if (value == null) {
                return null;
            }
            String v[] = StringUtil.split(value, ",");
            if (v == null || idx >= v.length) {
                Log.error(TAG_LOG, "Mapping table invalid value " + value);
                return "-1";
            } else {
                return v[idx];
            }
        }
    }

     

}
