/*
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2010 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.source.media;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Vector;

import com.funambol.client.source.MediaMetadata;
import com.funambol.storage.QueryFilter;
import com.funambol.storage.QueryResult;
import com.funambol.storage.Table;
import com.funambol.storage.Tuple;
import com.funambol.storage.Table.BulkOperation;
import com.funambol.storage.Table.BulkOperationException;
import com.funambol.sync.ItemStatus;
import com.funambol.sync.SyncItem;
import com.funambol.sync.SyncSource;
import com.funambol.sync.client.ChangesTracker;
import com.funambol.sync.client.TrackableSyncSource;
import com.funambol.sync.client.TrackerException;
import com.funambol.util.Log;

public class MediaTracker implements ChangesTracker {

    private static final String TAG_LOG = MediaTracker.class.getSimpleName();

    protected Table metadata;
    
    protected Vector newItems;
    protected Vector deletedItems;
    protected Vector updatedItems;

    protected int syncMode;

    protected TrackableSyncSource syncSource;

    public MediaTracker(Table metadata) {
        this.metadata = metadata;
    }

    public void setSyncSource(TrackableSyncSource syncSource) {
        this.syncSource = syncSource;
    }

    public void begin(int syncMode, boolean resume) throws TrackerException {
        if(Log.isLoggable(Log.TRACE)) {
            Log.trace(TAG_LOG, "begin");
        }

        this.syncMode = syncMode;

        this.newItems      = new Vector<String>();
        this.updatedItems  = new Vector<String>();
        this.deletedItems  = new Vector<String>();

        if(syncMode == SyncSource.INCREMENTAL_SYNC ||
           syncMode == SyncSource.INCREMENTAL_UPLOAD ||
           syncMode == SyncSource.INCREMENTAL_DOWNLOAD) {
            computeIncrementalChanges();
        } else if (syncMode == SyncSource.FULL_SYNC ||
                   syncMode == SyncSource.FULL_UPLOAD ||
                   syncMode == SyncSource.FULL_DOWNLOAD) {
            // Reset the status when performing a full sync
            if (resume && syncMode != SyncSource.FULL_DOWNLOAD) {
                // In this case we need to know if items that were sent in the
                // previous sync attempt have changed
                computeIncrementalChanges();
                // We only need to keep the list of updated items
                newItems = null;
                deletedItems = null;
            } else {
                reset();
            }
        }
    }

    public void end() throws TrackerException {
        if(Log.isLoggable(Log.TRACE)) {
            Log.trace(TAG_LOG, "end");
        }
        // Allow the GC to pick this memory
        newItems      = null;
        updatedItems  = null;
        deletedItems  = null;
    }

    public boolean hasChangedSinceLastSync(String key, long ts) {
        if (updatedItems != null) {
            return updatedItems.contains(key);
        } else {
            return false;
        }
    }

    public boolean supportsResume() {
        return true;
    }

    private void computeIncrementalChanges() {
        QueryResult result = null;
        try {
            metadata.open();
        
            int idIndex = metadata.getColIndexOrThrow(MediaMetadata.METADATA_ID);
            int dirtyIndex = metadata.getColIndexOrThrow(MediaMetadata.METADATA_DIRTY);
            int deletedIndex = metadata.getColIndexOrThrow(MediaMetadata.METADATA_DELETED);
            int syncedIndex = metadata.getColIndexOrThrow(MediaMetadata.METADATA_SYNCHRONIZED);

            // Filter with dirty data only
            QueryFilter filter = metadata.createQueryFilter();
            filter.setValueFilter(dirtyIndex, false, QueryFilter.EQUAL, 1);

            result = metadata.query(filter);
            while(result.hasMoreElements()) {

                Tuple tuple = result.nextElement();

                Long id = tuple.getLongField(idIndex);
                long deleted = tuple.getLongField(deletedIndex);
                long synced = tuple.getLongField(syncedIndex);

                // Deleted items have the deleted flag set to 1
                if(deleted == 1) {
                    if(Log.isLoggable(Log.DEBUG)) {
                        Log.debug(TAG_LOG, "Found a deleted item with id: " + id);
                    }
                    deletedItems.addElement(id.toString());
                }
                // New items have the synced flag set to 0
                else if(synced == 0) {
                    if(Log.isLoggable(Log.DEBUG)) {
                        Log.debug(TAG_LOG, "Found a new item with id: " + id);
                    }
                    newItems.addElement(id.toString());
                }
                // Dirty items which are not new or deleted are updated
                else {
                    if(Log.isLoggable(Log.DEBUG)) {
                        Log.debug(TAG_LOG, "Found an updated item with id: " + id);
                    }
                    updatedItems.addElement(id.toString());
                }
            }
        } catch(IOException ex) {
            Log.error(TAG_LOG, "Error in computing changes", ex);
            throw new TrackerException(ex.getMessage());
        } finally {
            if (result != null) {
                result.close();
            }
            try {
                metadata.close();
            } catch(IOException ex) {
                Log.error(TAG_LOG, "Cannot close metadata table", ex);
                throw new TrackerException(ex.getMessage());
            }
        }
    }
    
    public Enumeration getNewItems() throws TrackerException {
        if(Log.isLoggable(Log.TRACE)) {
            Log.trace(TAG_LOG, "getNewItems");
        }
        if (newItems != null) {
            return newItems.elements();
        } else {
            return null;
        }
    }

    public int getNewItemsCount() throws TrackerException {
        if (newItems != null) {
            return newItems.size();
        } else {
            return 0;
        }
    }

    public Enumeration getUpdatedItems() throws TrackerException {
        if(Log.isLoggable(Log.TRACE)) {
            Log.trace(TAG_LOG, "getUpdatedItems");
        }
        if (updatedItems != null) {
            return updatedItems.elements();
        } else {
            return null;
        }
    }

    public int getUpdatedItemsCount() throws TrackerException {
        if (updatedItems != null) {
            return updatedItems.size();
        } else {
            return 0;
        }
    }

    public Enumeration getDeletedItems() throws TrackerException {
        if(Log.isLoggable(Log.TRACE)) {
            Log.trace(TAG_LOG, "getDeletedItems");
        }
        if (deletedItems != null) {
            return deletedItems.elements();
        } else {
            return null;
        }
    }

    public int getDeletedItemsCount() throws TrackerException {
        if (deletedItems != null) {
            return deletedItems.size();
        } else {
            return 0;
        }
    }

    public void setItemsStatus(Vector itemsStatus) throws TrackerException {
        if (Log.isLoggable(Log.TRACE)) {
            Log.trace(TAG_LOG, "setItemsStatus");
        }
        try {
            metadata.open();
            Vector operations = new Vector();
            for(int i=0;i<itemsStatus.size();++i) {
                ItemStatus status = (ItemStatus)itemsStatus.elementAt(i);
                String key = status.getKey();
                long keyLong = Long.parseLong(key);

                int itemStatus = status.getStatus();
                if (Log.isLoggable(Log.TRACE)) {
                    Log.trace(TAG_LOG, "setItemStatus " + key + "," + itemStatus);
                }
                if (isSuccess(itemStatus) && itemStatus != SyncSource.CHUNK_SUCCESS_STATUS) {
                    if (deletedItems != null && deletedItems.contains(key)) {
                        Table.BulkOperation op = metadata.new BulkOperation(BulkOperation.DELETE, key);
                        operations.addElement(op);
                    } else {
                        Tuple updatedTuple = metadata.createNewRow(keyLong);
                        updatedTuple.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_SYNCHRONIZED), 1);
                        updatedTuple.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_DIRTY), 0);
                        Table.BulkOperation op = metadata.new BulkOperation(BulkOperation.UPDATE, updatedTuple);
                        operations.addElement(op);
                    }
                }
            }
            metadata.bulkOperations(operations);
        } catch(Exception ex) {
            Log.error(TAG_LOG, "Cannot set item status", ex);
            throw new TrackerException(ex.getMessage());
        } finally {
            try {
                metadata.save();
            } catch(IOException ex) {
                Log.error(TAG_LOG, "Cannot save metadata table", ex);
                throw new TrackerException(ex.getMessage());
            }
            try {
                metadata.close();
            } catch(IOException ex) {
                Log.error(TAG_LOG, "Cannot close metadata table", ex);
                throw new TrackerException(ex.getMessage());
            }
        }
    }

    protected boolean isSuccess(int status) {
        if(Log.isLoggable(Log.TRACE)) {
            Log.trace(TAG_LOG, "isSuccess " + status);
        }
        return SyncSource.SUCCESS_STATUS == status;
    }
    
    public boolean filterItem(String key, boolean removed) {
        // Nothing to filter out so far
        return false;
    }

    public void empty() throws TrackerException {
        try {
            metadata.open();
            metadata.reset();
        } catch(IOException ex) {
            Log.error(TAG_LOG, "Cannot reset metadata table", ex);
            throw new TrackerException(ex.toString());
        } finally {
            try {
                metadata.save();
            } catch(IOException ex) {
                Log.error(TAG_LOG, "Cannot save metadata table", ex);
                throw new TrackerException(ex.getMessage());
            }
            try {
                metadata.close();
            } catch(IOException ex) {
                Log.error(TAG_LOG, "Cannot close metadata table", ex);
                throw new TrackerException(ex.getMessage());
            }
        }
    }
    
    public int removeItems(Vector items) throws TrackerException {
        int res = 0;
        try {
            metadata.open();
            Vector operations = new Vector();
            for(int i=0;i<items.size();++i) {
                SyncItem item = (SyncItem)items.elementAt(i);
                long longKey;
                try { 
                    longKey = Long.parseLong(item.getKey());
                } catch (Exception e) {
                    Log.error(TAG_LOG, "Item key is not a long number as expected", e);
                    throw new TrackerException(e.toString());
                }

                switch (item.getState()) {
                    case SyncItem.STATE_NEW:
                    case SyncItem.STATE_UPDATED:
                    {
                        Tuple updatedTuple = metadata.createNewRow(longKey);
                        updatedTuple.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_SYNCHRONIZED), 1);
                        updatedTuple.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_DIRTY), 0);
                        Table.BulkOperation op = metadata.new BulkOperation(BulkOperation.UPDATE, updatedTuple);
                        operations.addElement(op);
                        break;
                    }
                    case SyncItem.STATE_DELETED:
                    {
                        Table.BulkOperation op = metadata.new BulkOperation(BulkOperation.DELETE, item.getKey());
                        operations.addElement(op);
                        break;
                    }
                }
            }
            metadata.bulkOperations(operations);
            res = operations.size();
        } catch (BulkOperationException boe) {
            if (Log.isLoggable(Log.DEBUG)) {
                Log.debug(TAG_LOG, "Removing items from tracker individually");
            }
            int numSuccess = boe.getNumSuccess();
            // Try to apply all the remaining one by one
            for(int i=numSuccess;i<items.size();++i) {
                SyncItem item = (SyncItem)items.elementAt(i);
                try {
                    removeItem(item);
                } catch(TrackerException te) {
                    // This is likely because the item is no longer in the DB. In this case we can safely ignore the
                    // error
                }
            }
        } catch(Exception ex) {
            Log.error(TAG_LOG, "Cannot remove item", ex);
            throw new TrackerException(ex.toString());
        } finally {
            try {
                metadata.save();
            } catch(IOException ex) {
                Log.error(TAG_LOG, "Cannot save metadata table", ex);
                throw new TrackerException(ex.getMessage());
            }
            try {
                metadata.close();
            } catch(IOException ex) {
                Log.error(TAG_LOG, "Cannot close metadata table", ex);
                throw new TrackerException(ex.getMessage());
            }
        }
        return res;
    }

    public boolean removeItem(SyncItem item) throws TrackerException {
        if (Log.isLoggable(Log.TRACE)) {
            Log.trace(TAG_LOG, "removeItem state: " +
                    item.getState() + ", key: " + item.getKey());
        }
        boolean res = true;
        try {
            long longKey;
            try { 
                longKey = Long.parseLong(item.getKey());
            } catch (Exception e) {
                Log.error(TAG_LOG, "Item key is not a long number as expected", e);
                throw new TrackerException(e.toString());
            }
            metadata.open();
            switch (item.getState()) {
                case SyncItem.STATE_NEW:
                case SyncItem.STATE_UPDATED:
                    Tuple updatedTuple = metadata.createNewRow(longKey);
                    updatedTuple.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_SYNCHRONIZED), 1);
                    updatedTuple.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_DIRTY), 0);
                    metadata.update(updatedTuple);
                    break;
                case SyncItem.STATE_DELETED:
                    metadata.delete(item.getKey());
                    break;
                default:
                    res = false;
            }
        } catch(Exception ex) {
            Log.error(TAG_LOG, "Cannot remove item", ex);
            throw new TrackerException(ex.toString());
        } finally {
            try {
                metadata.save();
            } catch(IOException ex) {
                Log.error(TAG_LOG, "Cannot save metadata table", ex);
                throw new TrackerException(ex.getMessage());
            }
            try {
                metadata.close();
            } catch(IOException ex) {
                Log.error(TAG_LOG, "Cannot close metadata table", ex);
                throw new TrackerException(ex.getMessage());
            }
        }
        return res;
    }

    public void reset() throws TrackerException {

        try {
            metadata.open();

            int dirtyIndex = metadata.getColIndexOrThrow(MediaMetadata.METADATA_DIRTY);
            int deletedIndex = metadata.getColIndexOrThrow(MediaMetadata.METADATA_DELETED);
            int syncedIndex = metadata.getColIndexOrThrow(MediaMetadata.METADATA_SYNCHRONIZED);

            // Reset deleted items
            QueryFilter filter = metadata.createQueryFilter();
            filter.setValueFilter(deletedIndex, false, QueryFilter.EQUAL, 1);
            QueryResult result = metadata.query(filter);
            while(result.hasMoreElements()) {
                Tuple tuple = result.nextElement();
                metadata.delete(tuple.getKey());
            }
            result.close();
            // Reset new/updated items
            filter = metadata.createQueryFilter();
            filter.setValueFilter(dirtyIndex, false, QueryFilter.EQUAL, 1);
            result = metadata.query(filter);
            while(result.hasMoreElements()) {
                Tuple tuple = result.nextElement();
                tuple.setField(dirtyIndex, 0);
                tuple.setField(syncedIndex, 1);
                metadata.update(tuple);
            }
            result.close();
        } catch(Exception ex) {
            Log.error(TAG_LOG, "Cannot reset tracker", ex);
            throw new TrackerException(ex.toString());
        } finally {
            try {
                metadata.save();
            } catch(IOException ex) {
                Log.error(TAG_LOG, "Cannot save metadata table", ex);
                throw new TrackerException(ex.getMessage());
            }
            try {
                metadata.close();
            } catch(IOException ex) {
                Log.error(TAG_LOG, "Cannot close metadata table", ex);
                throw new TrackerException(ex.getMessage());
            }
        }
    }

    public boolean hasChanges() {
        try {
            metadata.open();
            int dirtyIndex = metadata.getColIndexOrThrow(MediaMetadata.METADATA_DIRTY);
            QueryFilter filter = metadata.createQueryFilter();
            filter.setValueFilter(dirtyIndex, false, QueryFilter.EQUAL, 1);
            QueryResult result = metadata.query(filter);
            boolean hasChanges = result.hasMoreElements();
            result.close();
            return hasChanges;
        } catch(IOException ex) {
            Log.error(TAG_LOG, "Cannot retrieve changes", ex);
            return false;
        } finally {
            try {
                metadata.close();
            } catch(IOException ex) {
                Log.error(TAG_LOG, "Cannot close metadata table", ex);
            }
        }
    }
}


