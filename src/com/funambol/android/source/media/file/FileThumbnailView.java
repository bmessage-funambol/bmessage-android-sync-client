/*
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2011 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.source.media.file;

import android.app.Activity;
import android.graphics.Bitmap;
import android.widget.TextView;

import com.funambol.android.source.media.MediaThumbnailView;
import com.funambol.androidsync.R;
import com.funambol.client.source.MediaMetadata;
import com.funambol.client.test.BasicConstants;
import com.funambol.storage.Tuple;

/**
 * Represents a thumbnail view for files
 */
public class FileThumbnailView extends MediaThumbnailView {
    
    private static final String TAG_LOG = FileThumbnailView.class.getSimpleName();

    public FileThumbnailView(Activity activity) {
        super(activity);
    }

    protected int getLayoutResourceId() {
        return R.layout.vwfilethumbnail;
    }

    protected int getThumbnailResourceId() {
        return R.id.filethumbnail_laythumb;
    }

    protected int getThumbnailImageResourceId() {
        // No thumbnail image for files
        return -1;
    }
    
    protected int getPlaceHolderResourceId() {
        // No place holder for files
        return -1;
    }

    @Override
    public void setThumbnail(Tuple item) {
        this.item = item;
        String fullName = item.getStringField(item.getColIndexOrThrow(MediaMetadata.METADATA_NAME));

        String fileName = fullName;
        String fileExtension = "";
        
        int sep = fullName.lastIndexOf('.');
        if(sep >= 0) {
            fileExtension = fullName.substring(sep+1);
            fileName = fullName.substring(0, sep);
        }
        
        TextView textView = (TextView)findViewById(R.id.filethumbnail_lblfileextension);
        textView.setText(fileExtension.toUpperCase());

        TextView nameView = (TextView)findViewById(R.id.filethumbnail_lblfilename);
        nameView.setText(fileName);

        fixThumbnailSize();

        // Set tag value for testing purposes only
        setTag(BasicConstants.THUMB_PREFIX + item.getStringField(item.getColIndexOrThrow(MediaMetadata.METADATA_NAME)));
    }

    public boolean isThumbnailUpdated(Tuple updatedItem) {
        // The thumbnail view must be updated one of the following fields changed:
        // - MediaMetadata.METADATA_NAME;
        int nameIdx = item.getColIndexOrThrow(MediaMetadata.METADATA_NAME);
        boolean result = false;
        if(!updatedItem.isUndefined(nameIdx)) {
            String currentName = item.getStringField(nameIdx);
            String updatedName = updatedItem.getStringField(nameIdx);
            result |= !currentName.equals(updatedName);
        }
        return result;
    }

    protected Bitmap getThumbnailFromProvider(long itemId) {
        // No providers for files
        return null;
    }
}
