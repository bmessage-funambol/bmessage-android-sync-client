/**
 * Funambol is a mobile platform developed by Funambol, Inc. 
 * Copyright (C) 2003 - 2011 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission 
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE 
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License 
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite 
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably 
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol". 
 */
package com.funambol.android.source.media.file;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import android.content.Context;
import android.os.FileObserver;

import com.funambol.android.AppInitializer;
import com.funambol.android.MimeTypeMap;
import com.funambol.android.services.AutoSyncServiceHandler;
import com.funambol.android.source.media.MediaAppSyncSource;
import com.funambol.client.configuration.Configuration;
import com.funambol.client.controller.Controller;
import com.funambol.client.controller.ItemsIgnoredNotification;
import com.funambol.client.controller.RefreshTrigger;
import com.funambol.client.notification.NotificationMessage;
import com.funambol.client.source.AppSyncSource;
import com.funambol.client.source.AppSyncSourceManager;
import com.funambol.client.source.IgnoredMediaMetadata;
import com.funambol.client.source.MediaMetadata;
import com.funambol.storage.QueryFilter;
import com.funambol.storage.QueryResult;
import com.funambol.storage.Table;
import com.funambol.storage.Tuple;
import com.funambol.util.Log;
import com.funambol.util.StringUtil;
import com.funambol.util.bus.Bus;

/**
 * Observer for a generic files folder
 */
public class AndroidFileObserver extends FileObserver {

    private static final String TAG_LOG = AndroidFileObserver.class.getSimpleName();
    private static final String[] ALL_EXTENSIONS = new String[0];
    private static final String FILE_PROTOCOL = "file://";
    
    private String baseDirectory = null;

    private Context context;
    private Configuration configuration;

    private Hashtable<MediaAppSyncSource, String[]> sourcesExtensions =
            new Hashtable<MediaAppSyncSource,String[]>();

    private File lastRenamedFrom =null;

    public AndroidFileObserver(MediaAppSyncSource appSource, String baseDirectory,
            String extensions[], Context appContext, Configuration configuration) {
        //listening to CLOSE_WRITE is better than listening to CREATE and MODIFY.
        // For example in video, while the video is recorded, the
        // corresponding file is modified times after times. Or,
        // if video recording lasts for more than one minutes and C2S is
        // enabled, a sync will start, but the video is incomplete.
        super(baseDirectory, FileObserver.DELETE | FileObserver.CLOSE_WRITE
                           | FileObserver.MOVED_FROM | FileObserver.MOVED_TO );
        this.baseDirectory = baseDirectory;
        this.context = appContext;
        this.configuration = configuration;
        add(appSource, extensions);
    }

    public void add(MediaAppSyncSource appSource, String extensions[]) {
        if (extensions != null && extensions.length > 0) {
            sourcesExtensions.put(appSource, extensions);
        } else {
            sourcesExtensions.put(appSource, ALL_EXTENSIONS);
        }
    }

   /* (non-Javadoc)
     * @see android.os.FileObserver#onEvent(int, java.lang.String)
     */
    @Override
    public void onEvent(int event, String fileName) {
        if(Log.isLoggable(Log.TRACE)) {
            Log.trace(TAG_LOG, "Detected change type=" + event + " fileName=" + fileName);
        }
        // Sometimes the system triggers events with path=null and unknown event codes
        if(StringUtil.isNullOrEmpty(fileName)) {
            return;
        }
        String fullPath = getFileFullName(fileName);
        MediaAppSyncSource appSource = findSource(fullPath);
        
        // If the event is not related to a file interesting any source, we just ignore it
        if (appSource == null) {
            if(Log.isLoggable(Log.DEBUG)) {
                Log.debug(TAG_LOG, "Cannot find source for file: " + fileName);
            }
            return;
        }
        
        Table metadata = appSource.getMetadataTable();
        boolean isExcluded = false;
        // If the source is currently disabled, we shall ignore this change and allow this item to be part of the
        // selective upload (if the source supports it)
        if (!appSource.getConfig().getEnabled() && appSource.getSupportsSelectiveUpload()) {
            if (Log.isLoggable(Log.INFO)) {
                Log.info( TAG_LOG, "Ignoring incoming change as source is disabled and update only the excluded table");
            }
            metadata = appSource.getExcludedMetadataTable();
            isExcluded = true;
        }

        File file = new File(fullPath);

        // Renames must be handled separately
        if((event & FileObserver.MOVED_FROM) == FileObserver.MOVED_FROM ||
           (event & FileObserver.MOVED_TO) == FileObserver.MOVED_TO) {
            processRename(file, event, appSource);
            return;
        }

        //Handles a particular case: for videos, some devices creates an empty
        // placeholder file prior to start video recording. When it starts,
        // file is filled with video data, otherwise is deleted when the
        // user exit from camera application. This file hasn't be synchronized,
        // so we ignore it
        if (appSource.getId() == AppSyncSourceManager.VIDEOS_ID &&
                file.exists() && file.length() <= 0) {
            if (Log.isLoggable(Log.DEBUG)) {
                Log.trace(TAG_LOG, "Ignore " + fullPath + " because it's "
                        + "an empty video placeholder");
            }
            return;
        }

        //Some devices first delete video empty placeholder and after a
        // CLOSE_WRITE event is generated. Pure madness!
        if (appSource.getId() == AppSyncSourceManager.VIDEOS_ID &&
                (event & FileObserver.CLOSE_WRITE) == FileObserver.CLOSE_WRITE &&
                !file.exists()) {
            if (Log.isLoggable(Log.DEBUG)) {
                Log.trace(TAG_LOG, "Ignore " + fullPath + " because it "
                        + "was modified by it doesn't exist. Cannot be true!");
            }
            return;
        }
        updateSourceMetadata(appSource, metadata, file, event, isExcluded);
    }
    
    private void updateSourceMetadata(MediaAppSyncSource appSource, Table metadata, File file, int event,
                                      boolean isExcluded) {

        String fileName = file.getName();
        String fullPath = file.getPath();

        if (Log.isLoggable(Log.TRACE)) {
            Log.trace(TAG_LOG, "updateSourceMetadata " + appSource.getName() + " " +
                    fileName + " " + fullPath + " " + event + " " + isExcluded);
        }

        Tuple itemTuple = retrieveItemTuple(metadata, file);
        if(itemTuple == null) {
            Table excludedMetadata = appSource.getExcludedMetadataTable();
            itemTuple = retrieveItemTuple(excludedMetadata, file);
            if(itemTuple != null) {
                if(Log.isLoggable(Log.DEBUG)) {
                    Log.debug(TAG_LOG, "This is an excluded item");
                }
                metadata = excludedMetadata;
            }
        }

        boolean insert = false;
        boolean update = false;
        try {
            metadata.open();
            boolean ignore = false;
            // We perform a preliminary check to ignore files created by out
            // application when downloading actual
            // items
            if (itemTuple != null) {
                // If this item has still remote content, then it cannot be
                // a local change, this must be the donwload of its content
                String itemPath = itemTuple.getStringField(
                        itemTuple.getColIndexOrThrow(MediaMetadata.METADATA_ITEM_PATH));
                if (!itemPath.startsWith("file://") &&
                    !itemPath.startsWith("/")) {
                    // The item is remote, ignore the event
                    ignore = true;
                }
            }
            if (ignore) {
                if (Log.isLoggable(Log.INFO)) {
                    Log.info(TAG_LOG, "Ignoring self generated change");
                }
                return;
            }

            if (itemTuple == null && ((event & FileObserver.DELETE) == FileObserver.DELETE)) {
                //file was never inserted inside table and it's a delete command.
                // It generally happens for video, when the empty placeholder
                // is deleted when the camera app closes.
                // Anyway, it's impossible to manage because it's impossible
                // to remove from db something that never was in the db
                if (Log.isLoggable(Log.INFO)) {
                    Log.info(TAG_LOG, "Ignoring deletion (item not on DB)");
                }
                return;
            }
            File fa = new File(fullPath);
            
            // Check if this items exceeds the source max size
            if (appSource.getConfig().getMaxItemSize() > 0 && fa.length() > appSource.getConfig().getMaxItemSize()) {
                if (Log.isLoggable(Log.INFO)) {
                    Log.info(TAG_LOG, "Ignoring " + fullPath + " because it exceeds max allowed size " + fa.length());
                }
                AppInitializer appInitializer = AppInitializer.i(context);
                // Generate a notification
                ItemsIgnoredNotification not = new ItemsIgnoredNotification(appInitializer.getController());
                not.addItem(appSource, fullPath, fa.length(), ItemsIgnoredNotification.REASON_SIZE);
                NotificationMessage msg = new NotificationMessage(not);
                Bus.getInstance().sendMessage(msg);
                
                // If the item does not belong to the ignored items table, then we add it
                Table ignoredTable = appSource.getIgnoredMediaMetadataTable();
                QueryResult qr = null; 
                try {
                    ignoredTable.open();
                    QueryFilter qf = ignoredTable.createQueryFilter();
                    qf.setValueFilter(ignoredTable.getColIndexOrThrow(IgnoredMediaMetadata.ITEM_PATH), true,
                                      QueryFilter.EQUAL, fullPath);
                    qr = ignoredTable.query(qf);
                    if (!qr.hasMoreElements()) {
                        // The item is not present already, add it now
                        Tuple newRow = ignoredTable.createNewRow();
                        newRow.setField(newRow.getColIndexOrThrow(IgnoredMediaMetadata.ITEM_PATH), fullPath);
                        newRow.setField(newRow.getColIndexOrThrow(IgnoredMediaMetadata.REASON),
                                        IgnoredMediaMetadata.ITEM_TOO_BIG);
                        ignoredTable.insert(newRow);
                    }
                } catch (IOException ioe) {
                    Log.error(TAG_LOG, "Cannot update ignored items table", ioe);
                } finally {
                    if (qr != null) {
                        qr.close();
                    }
                    try {
                        ignoredTable.close();
                    } catch (IOException ioe) {}
                }
                return;
            }
            
            if (itemTuple == null) {
                itemTuple = metadata.createNewRow();
                itemTuple.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_NAME), fileName);
                itemTuple.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_THUMBNAIL_PATH), "");
                itemTuple.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_PREVIEW_PATH), "");
                itemTuple.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_ITEM_PATH), FILE_PROTOCOL + fullPath);
                itemTuple.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_ITEM_REMOTE_URL), "");
                itemTuple.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_ORDER_DATE), fa.lastModified());
                itemTuple.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_UPLOAD_CONTENT_STATUS), 0L);
                itemTuple.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_SYNCHRONIZED), 0L);
                String extension = MimeTypeMap.getFileExtensionFromUrl(fileName);
                itemTuple.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_MIME),
                             MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension));
                insert = true;
            } else {
                // Item already exists, check if it was modified
                update = appSource.isItemModified(itemTuple);
            }

            // These fields could have been updated
            itemTuple.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_LAST_MODIFIED_DATE), fa.lastModified());
            itemTuple.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_SIZE), fa.length());

            // Check if we need to change the order date
            if(update && appSource.getChangeOrderDateOnUpdates()) {
                itemTuple.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_ORDER_DATE), fa.lastModified());
            }

            if ((event & FileObserver.DELETE) == FileObserver.DELETE) {
                processDeletion(metadata, itemTuple);
            } else if(insert || update) {
                // This row is dirty, regardless of the change applied
                itemTuple.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_DIRTY), 1);
                // New and updates items always have a new content
                itemTuple.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_DIRTY_CONTENT), 1);
                itemTuple.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_DELETED), 0);
                appSource.addSpecificMetadata(itemTuple);
                boolean done = false;
                int retries = 0;
                do {
                    try {
                        if (insert) {
                            metadata.insert(itemTuple);
                        } else {
                            metadata.update(itemTuple);
                        }
                        metadata.save();
                        done = true;
                    } catch (Table.ConstraintViolationException cv) {
                        retries++;
                        // Constraint failed, retry with a different constraint value
                        int fieldIdx = metadata.getColIndexOrThrow(
                                MediaMetadata.METADATA_ORDER_DATE);
                        long fieldValue = itemTuple.getLongField(fieldIdx).longValue();
                        itemTuple.setField(fieldIdx, fieldValue + 1);
                    }
                } while(!done);
                if (retries > 0) {
                    if (Log.isLoggable(Log.INFO)) {
                        Log.info(TAG_LOG, "Item violated unique constraint " + retries + "x");
                    }
                }
                // Now create the thumbnail and the preview. Do not modify itemTuple as it can be cached
                // and therefore it shall not be changed
                Long id = itemTuple.getLongField(itemTuple.getColIndexOrThrow(MediaMetadata.METADATA_ID));
                Tuple newItemTuple = metadata.createNewRow(id);
                int itemPathIdx = metadata.getColIndexOrThrow(MediaMetadata.METADATA_ITEM_PATH);
                newItemTuple.setField(itemPathIdx, itemTuple.getStringField(itemPathIdx));

                appSource.createThumbnail(newItemTuple, isExcluded);
                appSource.createPreview(newItemTuple, isExcluded);
                metadata.update(newItemTuple);
                metadata.save();
            }
        } catch (Exception e) {
            Log.error(TAG_LOG, "Cannot update metadata table", e);
            // TODO FIXME: shall we generate an event on the bus?
        } finally {
            try {
                metadata.close();
            } catch (Exception e) {}
        }
        if (appSource.getConfig().getEnabled()) {
            // Trigger synchronization if needed
            if (configuration.getCredentialsCheckPending()) {
                if (Log.isLoggable(Log.TRACE)) {
                    Log.trace(TAG_LOG, "Ignoring change because user is not logged in");
                }
            } else {
                if(configuration.getSyncMode() == Configuration.SYNC_MODE_AUTO) {
                    if (insert || update) {
                        // Check the source to which this file belongs to
                        triggerC2SPushSynchronization(appSource);
                    }
                } else {
                    if(Log.isLoggable(Log.TRACE)) {
                        Log.trace(TAG_LOG, "Do not fire a refresh because sync mode is manual or source disabled");
                    }
                }
            }
        }
    }

    private Tuple retrieveItemTuple(Table table, File file) {
        QueryResult res = null;
        try {
            table.open();
            QueryFilter filter = table.createQueryFilter();
            filter.setValueFilter(
                    table.getColIndexOrThrow(MediaMetadata.METADATA_NAME), true,
                    QueryFilter.EQUAL, file.getName());
            res = table.query(filter);
            Tuple tuple = res.nextElement();
            if (tuple != null) {
                if (Log.isLoggable(Log.DEBUG)) {
                    Log.debug(TAG_LOG, "Tuple retrieved from " + table.getName());
                }
            }
            return tuple;
        } catch(IOException ex) {
            Log.error(TAG_LOG, "Failed to retrieve item tuple", ex);
        } finally {
            if(table != null) {
                try {
                    table.close();
                } catch(Exception ex) { }
            }
            if(res != null) {
                try {
                    res.close();
                } catch(Exception ex) { }
            }
        }
        return null;
    }

    private void processRename(File file, int event, MediaAppSyncSource appSource) {
        if((event & FileObserver.MOVED_FROM) == FileObserver.MOVED_FROM) {
            lastRenamedFrom = file;
        } else if((event & FileObserver.MOVED_TO) == FileObserver.MOVED_TO) {
            if(lastRenamedFrom != null) {
                File oldFile = lastRenamedFrom;

                // Search the right table to use
                Table metadata = appSource.getMetadataTable();
                Tuple itemTuple = retrieveItemTuple(metadata, oldFile);
                if(itemTuple == null) {
                    Table excludedMetadata = appSource.getExcludedMetadataTable();
                    itemTuple = retrieveItemTuple(excludedMetadata, oldFile);
                    if(itemTuple != null) {
                        metadata = excludedMetadata;
                    }
                }
                if(itemTuple != null) {
                    // Check if the item tuple actually needs rename
                    String existingName = itemTuple.getStringField(metadata.getColIndexOrThrow(
                            MediaMetadata.METADATA_NAME));
                    if(!existingName.equals(file.getName())) {
                        // Update name and path
                        itemTuple.setField(metadata.getColIndexOrThrow(
                                MediaMetadata.METADATA_NAME),
                                file.getName());
                        itemTuple.setField(metadata.getColIndexOrThrow(
                                MediaMetadata.METADATA_ITEM_PATH),
                                FILE_PROTOCOL + file.getPath());
                        itemTuple.setField(metadata.getColIndexOrThrow(
                                MediaMetadata.METADATA_DIRTY), 1L);
                        try {
                            metadata.open();
                            metadata.update(itemTuple);
                            metadata.save();
                        } catch(Exception ex) {
                            Log.error(TAG_LOG, "Error while updating metadata for "
                                    + "item: " + file.getPath());
                        } finally {
                            try {
                                metadata.close();
                            } catch(IOException ex) { }
                        }
                    }
                }
                lastRenamedFrom = null;
            } else {
                Log.debug(TAG_LOG, "Stopped processing rename: original file name is unknown");
            }
        }
    }

    private void processDeletion(Table metadata, Tuple row) throws IOException {
        if (row.getLongField(row.getColIndexOrThrow(MediaMetadata.METADATA_SYNCHRONIZED))
                .longValue() == 1L) {
            if(Log.isLoggable(Log.DEBUG)) {
                Log.debug(TAG_LOG, "Deleted item was synchronized with the cloud");
            }
            localDelete(metadata, row);
        } else {
            if(Log.isLoggable(Log.DEBUG)) {
                Log.debug(TAG_LOG, "Deleted item was never synchronized with the cloud");
            }
            fullDelete(metadata, row);
        }
    }

    private void fullDelete(Table metadata, Tuple row) throws IOException {
        if(Log.isLoggable(Log.DEBUG)) {
            Log.info(TAG_LOG, "Removing metadata from the database");
        }
        removeFile(metadata, row, MediaMetadata.METADATA_PREVIEW_PATH);
        removeFile(metadata, row, MediaMetadata.METADATA_THUMBNAIL_PATH);
        metadata.delete(row.getKey());
        metadata.save();
    }

    private void localDelete(Table metadata, Tuple row) throws IOException {
        if (Log.isLoggable(Log.INFO)) {
            Log.info(TAG_LOG, "Restoring item path to remote");
        }
        Tuple updated = metadata.createNewRow((Long)row.getKey());
        String itemRemoteUrl = row.getStringField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_ITEM_REMOTE_URL));
        updated.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_ITEM_PATH), itemRemoteUrl);
        try {
            metadata.update(updated);
        } catch (Table.ConstraintViolationException cv) {
            throw new IOException("Constraint violation for field " + cv.getFieldIdx());
        }
        metadata.save();
    }

    private boolean removeFile(Table metadata, Tuple row, String colName) {
        if (Log.isLoggable(Log.INFO)) {
            Log.info(TAG_LOG, "Removing " + colName);
        }
        try {
            int colIdx = metadata.getColIndexOrThrow(colName);
            String path = row.getStringField(colIdx);
            if (path.startsWith(FILE_PROTOCOL)) {
                path = path.substring(FILE_PROTOCOL.length());
            }
            File file = new File(path);
            if (file.exists()) {
                file.delete();
                if (Log.isLoggable(Log.INFO)) {
                    Log.info(TAG_LOG, "Removed file " + path);
                }
                return true;
            } else {
                if (Log.isLoggable(Log.ERROR)) {
                    Log.error(TAG_LOG, "File " + path + " does not exist");
                }
                return false;
            }
        } catch (Throwable t) {
            if (Log.isLoggable(Log.ERROR)) {
                Log.error(TAG_LOG, "Unable to remove file: " + t.getMessage());
            }
            return false;
        }
    }

    protected void triggerC2SPushSynchronization(MediaAppSyncSource appSource) {
        // We trigger a sync for all the available sources
        Vector<AppSyncSource> sourcesToSync = new Vector<AppSyncSource>();
        AppInitializer appInitializer = AppInitializer.i(context);
        Controller controller = appInitializer.getController();
        Enumeration sourcesEnum = controller.getAppSyncSourceManager().getEnabledAndWorkingSources();
        while(sourcesEnum.hasMoreElements()) {
            AppSyncSource source = (AppSyncSource)sourcesEnum.nextElement();
            if (source.getConfig().getAllowed()) {
                sourcesToSync.add(source);
            }
        }

        int delay = appInitializer.getCustomization().getC2SPushDelay();
        AutoSyncServiceHandler autoSyncHandler = new AutoSyncServiceHandler(context);
        autoSyncHandler.startSync(RefreshTrigger.PUSH, sourcesToSync, delay);
    }

    private MediaAppSyncSource findSource(String path) {
        if (Log.isLoggable(Log.TRACE)) {
            Log.trace(TAG_LOG, "Searching a source to handle " + path);
        }
        Enumeration<MediaAppSyncSource> keys = sourcesExtensions.keys();
        while(keys.hasMoreElements()) {
            MediaAppSyncSource appSource = keys.nextElement();
            String extensions[] = sourcesExtensions.get(appSource);
            if (extensions == ALL_EXTENSIONS) {
                return appSource;
            }
            for(int i=0;i<extensions.length;++i) {
                String ext = extensions[i];
                if (StringUtil.endsWithIgnoreCase(path, ext)) {
                    if (Log.isLoggable(Log.TRACE)) {
                        Log.trace(TAG_LOG, "Found source " + appSource.getName());
                    }
                    return appSource;
                }
            }
        }
        return null;
    }

    private String getFileFullName(String fileName) {
        StringBuffer fullName = new StringBuffer();
        fullName.append(baseDirectory);
        fullName.append("/");
        fullName.append(fileName);
        return fullName.toString();
    }
}
