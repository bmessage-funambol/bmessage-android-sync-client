/*
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2010 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.source.media;

import android.net.Uri;
import java.util.Enumeration;
import java.io.IOException;

import com.funambol.android.AndroidAppSyncSource;

import com.funambol.storage.Table;
import com.funambol.storage.Tuple;
import com.funambol.client.localization.Inflexion;
import com.funambol.client.source.FunambolFileSyncSource;
import com.funambol.client.source.IgnoredMediaMetadata;
import com.funambol.client.source.MediaMetadata;
import com.funambol.client.source.MetadataBusProxy;
import com.funambol.client.customization.Customization;
import com.funambol.platform.FileAdapter;

import com.funambol.util.StringUtil;
import com.funambol.util.Log;
import com.funambol.util.MediaUtils;

public class MediaAppSyncSource extends AndroidAppSyncSource {

    private static final String TAG_LOG = MediaAppSyncSource.class.getSimpleName();
    
    protected static final String FILE_PROTOCOL = "file://";
    protected static final int ITEMID_NOT_FOUND = -1;

    protected MediaMetadata mediaMetadata;
    protected IgnoredMediaMetadata ignoredMediaMetadata;
    protected Customization customization;

    private MediaDirectoryScanner mediaDirectoryScanner;
    private int numberOfItemsToImportOnVeryFirstSync;
    
    /**
     * Stores thumbnail and preview properties, like width, height etc.
     */
    public class ImageProperties  {
        public int maxWidth;
        public int maxHeight;
        public int minWidth;
        public int minHeight;
        public boolean isDip;
        
        public ImageProperties(int maxWidth, int maxHeight, int minWidth, int minHeight) {
            this.maxWidth = maxWidth;
            this.maxHeight = maxHeight;
            this.minWidth = minWidth;
            this.minHeight = minHeight;
            this.isDip = true;
        }
    }

    protected final ImageProperties thumbnailProperties;
    protected final ImageProperties previewProperties;

    private Uri thumbnailsProviderUri;
    private String thumbnailsProviderItemIdColumn;
    private String thumbnailsProviderDataColumn;
    private boolean waitForFirstScanToComplete;

    public MediaAppSyncSource(String name, FunambolFileSyncSource source, Customization customization) {
        super(name, source);
        this.customization = customization;
        setIsMedia(true);

        mediaMetadata = createMediaMetadata();
        ignoredMediaMetadata = createIgnoredMediaMetadata();

        // At this point the metadata table has been setup. We can now register
        // the observer needed to proxy events on the bus
        Table metadataTable = mediaMetadata.getMetadataTable();
        Table excludedMetadataTable = mediaMetadata.getExcludedMetadataTable();

        // Setup a proxy for each metadata table in order to send bus messages
        // for any table updates
        MetadataBusProxy proxy = new MetadataBusProxy(metadataTable, this);
        metadataTable.registerObserver(proxy);

        MetadataBusProxy excludedProxy = new MetadataBusProxy(excludedMetadataTable, this);
        excludedMetadataTable.registerObserver(excludedProxy);
        
        //default values for thumbnail and preview
        thumbnailProperties = new ImageProperties(80, 80, 70, 70);
        previewProperties = new ImageProperties(504, 400, 504, 400);
    }

    public MediaAppSyncSource(String name, Customization customization) {
        this(name, null, customization);
    }

    public void setThumbnailsProviderUri(Uri uri) {
        thumbnailsProviderUri = uri;
    }

    public Uri getThumbnailsProviderUri() {
        return thumbnailsProviderUri;
    }

    public void setThumbnailsProviderItemIdColumn(String column) {
        thumbnailsProviderItemIdColumn = column;
    }

    public String getThumbnailsProviderItemIdColumn() {
        return thumbnailsProviderItemIdColumn;
    }

    public void setThumbnailsProviderDataColumn(String column) {
        thumbnailsProviderDataColumn = column;
    }

    public String getThumbnailsProviderDataColumn() {
        return thumbnailsProviderDataColumn;
    }

    public String getDataDirectory() {
        StringBuffer dataDirectory = new StringBuffer(customization.getPrivateDataDirectory());
        // We use a hidden directory for thumbnails because we don't want users or applications to access it easily
        String itemPluralName =
            getItemInflexion().using(Inflexion.PLURAL).toString();
        dataDirectory.append(FileAdapter.getFileSeparator()).append(itemPluralName)
                     .append(FileAdapter.getFileSeparator()).append(".thumbnails");
        String directory = dataDirectory.toString();
        return directory;
    }

    public String getTempDirectory() {
        StringBuffer tempDirectory = new StringBuffer(customization.getPrivateDataDirectory());
        String itemPluralName =
            getItemInflexion().using(Inflexion.PLURAL).toString();
        tempDirectory.append(FileAdapter.getFileSeparator()).append(".tmp")
                .append(FileAdapter.getFileSeparator()).append(itemPluralName);
        return tempDirectory.toString();
    }

    public boolean getChangeOrderDateOnUpdates() {
        FunambolFileSyncSource ffss = (FunambolFileSyncSource)getSyncSource();
        return ffss.getChangeOrderDateOnUpdates();
    }
    
    public void setThumbnailProperties(int maxWidth, int maxHeight, int minWidth, int minHeight, boolean isDip) {
        setImageProperties(thumbnailProperties, maxWidth, maxHeight, minWidth, minHeight, isDip);
    }
    
    public ImageProperties getThumbnailProperties() {
        return thumbnailProperties;
    }
    
    /**
     * Sets preview properties
     * 
     * @param width
     * @param height
     */
    public void setPreviewProperties(int maxWidth, int maxHeight, int minWidth, int minHeight, boolean isDip) {
        setImageProperties(previewProperties, maxWidth, maxHeight, minWidth, minHeight, isDip);
    }
    
    public ImageProperties getPreviewProperties() {
        return previewProperties;
    }

    /**
     * Sets thumbnail properties
     * 
     * @param width
     * @param height
     */
    private void setImageProperties(ImageProperties properties, int maxWidth, int maxHeight,
                                    int minWidth, int minHeight, boolean isDip)
    {
        if (maxWidth > 0) {
            properties.maxWidth = maxWidth;
        }
        if (maxHeight > 0) {
            properties.maxHeight = maxHeight;
        }
        if (minWidth > 0) {
            properties.minWidth = minWidth;
        }
        if (maxHeight > 0) {
            properties.minHeight = minHeight;
        }
        properties.isDip = isDip;
    }

    public void setMediaDirectoryScanner(MediaDirectoryScanner scanner) {
        this.mediaDirectoryScanner = scanner;
    }

    public MediaDirectoryScanner getMediaDirectoryScanner() {
         return mediaDirectoryScanner;
    }

    @Override
    public void reset() {
        super.reset();

        String directory = getDataDirectory();

        try {
            String components[] = StringUtil.split(directory, "" + FileAdapter.getFileSeparator());
            // Recursively create the required directories
            StringBuffer currDir = new StringBuffer();

            for(int i=0;i<components.length;++i) {
                String comp = components[i];

                if (currDir.length() > 0) {
                    currDir.append(FileAdapter.getFileSeparator());
                }
                currDir.append(comp);

                String dir = currDir.toString();

                if (dir.length() <= 1) {
                    continue;
                }

                // Create the default folder if it doesn't exist
                FileAdapter d = new FileAdapter(dir);
                if(!d.exists()) {
                    if (Log.isLoggable(Log.DEBUG)) {
                        Log.debug(TAG_LOG, "Creating directory " + dir);
                    }
                    d.mkdirs();
                } else if (i == components.length - 1) {
                    // Remove the private data
                    Enumeration files = d.list(false);
                    while(files.hasMoreElements()) {
                        String f = (String)files.nextElement();
                        FileAdapter fi = new FileAdapter(dir + FileAdapter.getFileSeparator() + f);
                        try {
                            fi.delete();
                            fi.close();
                        } catch (IOException ioe) {
                            Log.error(TAG_LOG, "Cannot remove file ", ioe);
                        }
                    }
                }
                d.close();
            }
            // Now clear all the metadata table associated
            Table table = getMetadataTable();
            try {
                table.open();
                table.reset();
            } finally {
                try {
                    table.close();
                } catch (IOException ioe) {}
            }
        } catch(IOException ex) {
            Log.error(TAG_LOG, "Cannot create directory: " + directory, ex);
            // Disable the source
            getConfig().setEnabled(false);
        }
    }

    /**
     * This method returns the table where metadata is stored for this source.
     * This method must be re-defined if a source provides its data in a table.
     */
    @Override
    public Table getMetadataTable() {
        return mediaMetadata.getMetadataTable();
    }

    @Override
    public Table getExcludedMetadataTable() {
        return mediaMetadata.getExcludedMetadataTable();
    }
    
    public Table getIgnoredMediaMetadataTable() {
        return ignoredMediaMetadata.getTable();
    }

    protected MediaMetadata createMediaMetadata() {
        // Use a standard plain media metadata without source specific
        // extensions
        return new MediaMetadata(this);
    }
    
    protected IgnoredMediaMetadata createIgnoredMediaMetadata() {
        return new IgnoredMediaMetadata(this);
    }

    /**
     * Creates a thumbnail for the given item and updates the thumbnail path
     * from the item's metadata Tuple.
     * 
     * @param item
     * @param isExcluded
     */
    public void createThumbnail(Tuple item, boolean isExcluded) {
        if(Log.isLoggable(Log.TRACE)) {
            Log.trace(TAG_LOG, "createThumbnail");
        }
        long itemId = item.getLongField(item.getColIndexOrThrow(MediaMetadata.METADATA_ID));
        String itemPath = item.getStringField(item.getColIndexOrThrow(MediaMetadata.METADATA_ITEM_PATH));
        if(itemPath.startsWith(FILE_PROTOCOL)) {
            itemPath = itemPath.substring(FILE_PROTOCOL.length());
        }
        StringBuffer id = new StringBuffer();
        id.append(itemId);
        if(isExcluded) {
            id.append("_excluded");
        }
        FunambolFileSyncSource ffss = (FunambolFileSyncSource)getSyncSource();
        String thumbFileName = ffss.getThumbnailFullName(id.toString());
        boolean thumbCreated = createThumbnail(itemPath, thumbFileName,
                thumbnailProperties.maxWidth, thumbnailProperties.maxHeight);
        if (thumbCreated) {
            item.setField(item.getColIndexOrThrow(MediaMetadata.METADATA_THUMBNAIL_PATH),
                    FILE_PROTOCOL + thumbFileName);
        }
    }

    /**
     * Creates a thumbnail for the given item using the native provider and
     * updates the thumbnail path from the item's metadata Tuple.
     * 
     * @param item
     * @param isExcluded
     * @param providerItemId
     */
    public void createThumbnailUsingNativeProvider(Tuple item,
            boolean isExcluded, long providerItemId) {
        if(Log.isLoggable(Log.TRACE)) {
            Log.trace(TAG_LOG, "createThumbnailUsingNativeProvider");
        }
        long itemId = item.getLongField(item.getColIndexOrThrow(
                MediaMetadata.METADATA_ID));
        StringBuffer id = new StringBuffer();
        id.append(itemId);
        if(isExcluded) {
            id.append("_excluded");
        }
        FunambolFileSyncSource ffss = (FunambolFileSyncSource)getSyncSource();
        String thumbFileName = ffss.getThumbnailFullName(id.toString());
        boolean thumbCreated = createThumbnailUsingMediaProvider(
                providerItemId, thumbFileName,
                thumbnailProperties.maxWidth, thumbnailProperties.maxHeight);
        if (thumbCreated) {
            item.setField(item.getColIndexOrThrow(MediaMetadata.METADATA_THUMBNAIL_PATH),
                    FILE_PROTOCOL + thumbFileName);
        }
    }

    /**
     * Creates a preview for the given item and updates the preview path
     * from the item's metadata Tuple.
     *
     * @param item
     * @param isExcluded
     */
    public void createPreview(Tuple item, boolean isExcluded) {
        if(Log.isLoggable(Log.TRACE)) {
            Log.trace(TAG_LOG, "createPreview");
        }
        long itemId = item.getLongField(item.getColIndexOrThrow(
                MediaMetadata.METADATA_ID));
        String itemPath = item.getStringField(item.getColIndexOrThrow(
                MediaMetadata.METADATA_ITEM_PATH));
        if(itemPath.startsWith(FILE_PROTOCOL)) {
            itemPath = itemPath.substring(FILE_PROTOCOL.length());
        }
        StringBuffer id = new StringBuffer();
        id.append(itemId);
        if(isExcluded) {
            id.append("_excluded");
        }
        FunambolFileSyncSource ffss = (FunambolFileSyncSource)getSyncSource();
        String previewFileName = ffss.getPreviewFullName(id.toString());
        boolean previewCreated = createThumbnail(itemPath, previewFileName,
                previewProperties.maxWidth, previewProperties.maxHeight);
        if (previewCreated) {
            item.setField(item.getColIndexOrThrow(MediaMetadata.METADATA_PREVIEW_PATH),
                    FILE_PROTOCOL + previewFileName);
        }
    }

    /**
     * Creates a preview for the given item using the native provider and
     * updates the preview path from the item's metadata Tuple.
     *
     * @param item
     * @param isExcluded
     * @param providerItemId
     */
    public void createPreviewUsingNativeProvider(Tuple item,
            boolean isExcluded, long providerItemId) {
        if(Log.isLoggable(Log.TRACE)) {
            Log.trace(TAG_LOG, "createPreview");
        }
        long itemId = item.getLongField(item.getColIndexOrThrow(
                MediaMetadata.METADATA_ID));
        StringBuffer id = new StringBuffer();
        id.append(itemId);
        if(isExcluded) {
            id.append("_excluded");
        }
        FunambolFileSyncSource ffss = (FunambolFileSyncSource)getSyncSource();
        String previewFileName = ffss.getPreviewFullName(id.toString());
        boolean previewCreated = createThumbnailUsingMediaProvider(
                providerItemId, previewFileName,
                previewProperties.maxWidth, previewProperties.maxHeight);
        if (previewCreated) {
            item.setField(item.getColIndexOrThrow(MediaMetadata.METADATA_PREVIEW_PATH),
                    FILE_PROTOCOL + previewFileName);
        }
    }

    /**
     * Adds source-specific metadata
     *
     * @param row
     * @param itemFullPath
     */
    public void addSpecificMetadata(Tuple item) {
        //nothig to add in this implementation
        return;
    }

    protected boolean createThumbnail(String inFile, String outFile, int width, int height) {
        return false;
    }

    protected boolean createThumbnailUsingMediaProvider(long itemId,
            String outFile, int width, int height) {
        return false;
    }

    protected long getItemIdForMediaProvider(Tuple item) {
        String itemPath = item.getStringField(item.getColIndexOrThrow(
                MediaMetadata.METADATA_ITEM_PATH));
        if(itemPath.startsWith(FILE_PROTOCOL)) {
            itemPath = itemPath.substring(FILE_PROTOCOL.length());
        }
        Log.debug(TAG_LOG, "Requiring media provider id for item " + itemPath);
        MediaUtils utils = new MediaUtils(getAppContext());
        return utils.getMediaItemIdFromProvider(getProviderUri(), itemPath);
    }
    
    /**
     * Checks if a file was modified based on data stored in its metadata.
     * Currently check is done against last modified date and size. If
     * at least one of these two parameters is different, file was modified.
     * 
     * @param itemPath
     * @return true if the item was modified or is new, otherwise false
     */
    public boolean isItemModified(Tuple itemTuple) {
        return ((FunambolFileSyncSource)getSyncSource()).isItemModified(itemTuple);
    }
    
    public boolean getWaitForFirstScanToComplete() {
        return waitForFirstScanToComplete;
    }
    
    public void setWaitForFirstScanToComplete(boolean value) {
        waitForFirstScanToComplete = value;
    }
    
    public int getNumberOfItemsToImportOnVeryFirstSync() {
        return numberOfItemsToImportOnVeryFirstSync;
    }
    
    public void setNumberOfItemsToImportOnVeryFirstSync(int num) {
        this.numberOfItemsToImportOnVeryFirstSync = num;
    }
}
