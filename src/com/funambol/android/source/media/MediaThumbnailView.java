/*
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2009 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.source.media;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.funambol.android.AndroidUtils;
import com.funambol.androidsync.R;
import com.funambol.client.source.MediaMetadata;
import com.funambol.client.test.BasicConstants;
import com.funambol.client.ui.view.ImagePool;
import com.funambol.client.ui.view.ThumbnailView;
import com.funambol.storage.Tuple;
import com.funambol.util.Log;
import com.funambol.util.MediaUtils;

/**
 * Represents a generic thumbnail view for a media item
 */
public abstract class MediaThumbnailView extends FrameLayout implements ThumbnailView {
    
    private static final String TAG_LOG = MediaThumbnailView.class.getSimpleName();

    private static final int BORDER_SIZE = 2; //dip
    
    protected View thumbView;
    protected final ImageView thumbImageView;
    protected LinearLayout borderLayout;
    protected ImageView selectedCheckMark;
    protected String thumbPath;
    protected long mediaProviderId = MediaUtils.ITEMID_NOT_FOUND;
    
    protected int thumbSize;
    protected int leftPadding;
    protected int topPadding;
    protected int rightPadding;
    protected int bottomPadding;
    
    protected abstract int getLayoutResourceId();
    protected abstract int getThumbnailResourceId();
    protected abstract int getThumbnailImageResourceId();
    protected abstract int getPlaceHolderResourceId();

    private boolean visible = false;
    private Integer status;
    private VisibilityObserver visibilityObserver;
    private Activity activity;
    protected Tuple item;
    private boolean showingRecycableImage;

    private ThumbnailView.OnClickListener onClickListener;
    private ThumbnailView.OnLongClickListener onLongClickListener;

    public MediaThumbnailView(Activity activity) {
        super(activity);
        this.activity = activity;
        borderLayout = new LinearLayout(getContext());
        borderLayout.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        borderLayout.setPadding(AndroidUtils.dipToPx(BORDER_SIZE, getContext()),
                                AndroidUtils.dipToPx(BORDER_SIZE, getContext()),
                                AndroidUtils.dipToPx(BORDER_SIZE, getContext()),
                                AndroidUtils.dipToPx(BORDER_SIZE, getContext()));
        View.inflate(getContext(), getLayoutResourceId(), borderLayout);

        selectedCheckMark = new ImageView(getContext());
        selectedCheckMark.setLayoutParams(new LinearLayout.LayoutParams(
                AndroidUtils.dipToPx(25, getContext()),
                AndroidUtils.dipToPx(25, getContext())));
        selectedCheckMark.setImageResource(R.drawable.common_imgcheckmark_32x32);
        selectedCheckMark.setPadding(AndroidUtils.dipToPx(8, getContext()),
                                    AndroidUtils.dipToPx(6, getContext()),
                                    0, 0);
        selectedCheckMark.setVisibility(View.GONE);

        addView(borderLayout);
        addView(selectedCheckMark);
        setClickable(true);
        thumbView = findViewById(getThumbnailResourceId());
        thumbImageView = (ImageView)findViewById(getThumbnailImageResourceId());

        // On strange conditions the OnClickListener is not notified immediately
        // after the user clicks on the view. For that reason we catch the
        setOnTouchListener(new ThumbnailTouchListener());
    }

    public void setThumbnail(Tuple item) {
        
        this.item = item;

        // The standard implementation takes the small thumb path. If this is undefined, then this is an update
        // for of other fields and we can ignore it
        int thumbIdx = item.getColIndexOrThrow(MediaMetadata.METADATA_THUMBNAIL_PATH);
        if (item.isUndefined(thumbIdx)) {
            return;
        }
        thumbPath = item.getStringField(thumbIdx);
        
        // If the path is still remote, we just ignore its value
        if (thumbPath.startsWith("file://") && thumbPath.length() > 7) {
            thumbPath = thumbPath.substring(7);
        } else if (thumbPath.startsWith(MediaUtils.MEDIA_PROVIDER_PATH) && thumbPath.length() > 11) {
            mediaProviderId = Long.parseLong(thumbPath.substring(11));
        } else if (!thumbPath.startsWith("/")) {
            thumbPath = "";
        }

        fixThumbnailSize();

        // By default we show the place holder
        showPlaceHolder();

        ImagePool.getInstance().putImage(this);

        // Set tag value for testing purposes only
        setTag(BasicConstants.THUMB_PREFIX + item.getStringField(item.getColIndexOrThrow(
                MediaMetadata.METADATA_NAME)));
    }

    protected void fixThumbnailSize() {
        if (thumbSize > 0) {
            if (Log.isLoggable(Log.TRACE)) {
                Log.trace(TAG_LOG, "Limiting thumb size to " + thumbSize);
            }
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    getActualThumbnailSize(), getActualThumbnailSize());
            thumbView.setLayoutParams(lp);
        }
    }

    private int getActualThumbnailSize() {
        return thumbSize - AndroidUtils.dipToPx(BORDER_SIZE, getContext())*2;
    }
    
    public Object getThumbnailView() {
        return this;
    }

    public void setOnClickListener(ThumbnailView.OnClickListener listener) {
        this.onClickListener = listener;
    }

    public void setOnLongClickListener(ThumbnailView.OnLongClickListener listener) {
        this.onLongClickListener = listener;
    }

    private void setHighlighted(boolean highlighted) {
        // If selected it is already highlighted
        if(!isSelected()) {
            if(highlighted) {
                borderLayout.setBackgroundResource(R.color.lightBlue);
            } else {
                borderLayout.setBackgroundDrawable(null);
            }
        }
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        if(selected) {
            borderLayout.setBackgroundResource(R.color.lightBlue);
            if(thumbImageView != null) {
                thumbImageView.setAlpha(160);
            }
            selectedCheckMark.setVisibility(View.VISIBLE);
        } else {
            borderLayout.setBackgroundDrawable(null);
            if(thumbImageView != null) {
                thumbImageView.setAlpha(255);
            }
            selectedCheckMark.setVisibility(View.GONE);
        }
    }

    public boolean isVisible() {
        return visible;
    }
    
    public void setVisible(boolean value) {
        if(visible != value) {
            visible = value;
            if(visibilityObserver != null) {
                visibilityObserver.onVisibleChanged(this, visible);
            }
        }
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public void showRealImage() {
        if(thumbImageView == null) {
            // No image view available
            return;
        }
        if (thumbPath == null || thumbPath.length() == 0) {
            showPlaceHolder();
        } else {
            if (Log.isLoggable(Log.TRACE)) {
                Log.trace(TAG_LOG, "Showing real thumbnail for: " + thumbPath);
            }
            // If we have a media provider id we can read the thumbnail directly
            // from the provider
            if(mediaProviderId != MediaUtils.ITEMID_NOT_FOUND) {
                Bitmap bitmap = getThumbnailFromProvider(mediaProviderId);
                BitmapDrawable drawable = new BitmapDrawable(getContext().getResources(), bitmap);
                setThumbImageDrawable(drawable, true);
            } else {
                BitmapDrawable drawable = new BitmapDrawable(getContext().getResources(), thumbPath);
                boolean recyclable = thumbPath.startsWith("file://") || thumbPath.startsWith("/");
                setThumbImageDrawable(drawable, recyclable);
            }
        }
    }

    /**
     * Retrieves the thumbnail bitmap from the native media provider.
     * 
     * @param itemId
     * @return
     */
    protected abstract Bitmap getThumbnailFromProvider(long itemId);

    public void showPlaceHolder() {
        if(thumbImageView == null) {
            // No image view available
            return;
        }
        if (Log.isLoggable(Log.TRACE)) {
            Log.trace(TAG_LOG, "Showing place holder for: " + thumbPath);
        }
        setThumbImageResource(getPlaceHolderResourceId(), false);
    }

    private void setThumbImageDrawable(final Drawable d, final boolean recycable) {
        activity.runOnUiThread(new Runnable() {
            public void run() {
                synchronized(thumbImageView) {
                    freeBitmap();
                    thumbImageView.setImageDrawable(d);
                    showingRecycableImage = recycable;
                }
            }
        });
    }

    private void setThumbImageResource(final int resId, final boolean recycable) {
        activity.runOnUiThread(new Runnable() {
            public void run() {
                synchronized(thumbImageView) {
                    freeBitmap();
                    thumbImageView.setImageResource(resId);
                    showingRecycableImage = recycable;
                }
            }
        });
    }
   
    /**
     * In Android Bitmap live in a separate heap, not the standard application one. This heap is shared among all
     * apps and has a different GC cycle. Even if the official documentation says that invoking the "recycle" method
     * is not required, by doing it we see the memory is made available right away and we avoid OutOfMemory errors.
     */
    protected void freeBitmap() {
        // Recycle the old bitmap if we are currently showing an image which is recycable
        if (showingRecycableImage) {
            if (thumbImageView.getDrawable() instanceof BitmapDrawable) {
                BitmapDrawable bd = (BitmapDrawable) thumbImageView.getDrawable();
                if (bd.getBitmap() != null) {
                    if (Log.isLoggable(Log.TRACE)) {
                        Log.trace(TAG_LOG, "Recycling bitmap " + bd.getBitmap());
                    }
                    bd.getBitmap().recycle();
                }
                thumbImageView.setImageDrawable(null);
            }
        }
    }

    public void setVisibilityObserver(VisibilityObserver observer) {
        this.visibilityObserver = observer;
    }

    public void setSize(int thumbSize) {
        this.thumbSize = thumbSize;
    }
    
    public Tuple getItem() {
        return item;
    }

    private class ThumbnailTouchListener implements OnTouchListener {

        private Handler handler;
        private boolean pressed = false;
        private boolean longClickPerformed = false;

        private CheckClickRunnable checkClick = new CheckClickRunnable();
        private CheckLongClickRunnable checkLongClick = new CheckLongClickRunnable();

        public boolean onTouch(View view, MotionEvent me) {
            switch(me.getAction()) {
                case MotionEvent.ACTION_DOWN:
                {
                    setPressed(true);
                    longClickPerformed = false;
                    if(onLongClickListener != null) {
                        removeCallbacks(checkLongClick);
                        getHandler().postDelayed(checkLongClick, ViewConfiguration.getLongPressTimeout());
                    }
                    if(onClickListener != null) {
                        removeCallbacks(checkClick);
                        getHandler().postDelayed(checkClick, ViewConfiguration.getTapTimeout());
                    }
                    return true;
                }
                case MotionEvent.ACTION_UP:
                {
                    if(isPressed()) {
                        setHighlighted(true);
                        if(!longClickPerformed && onClickListener != null) {
                            if(!getHandler().post(new PerformClickRunnable())) {
                                performClick();
                            }
                        } else {
                            setHighlighted(false);
                        }
                    }
                    return true;
                }
                case MotionEvent.ACTION_OUTSIDE:
                case MotionEvent.ACTION_CANCEL:
                {
                    setPressed(false);
                    setHighlighted(false);
                    return true;
                }
            }
            return false;
        }

        private Handler getHandler() {
            if(handler == null) {
                handler = new Handler();
            }
            return handler;
        }

        private void performClick() {
            onClickListener.onClick();
            setPressed(false);
            setHighlighted(false);
        }

        private void performLongClick() {
            onLongClickListener.onLongClick();
            longClickPerformed = true;
        }

        private void setPressed(boolean pressed) {
            this.pressed = pressed;
        }

        private boolean isPressed() {
            return pressed;
        }

        private class CheckClickRunnable implements Runnable {
            public void run() {
                if(pressed) {
                    setHighlighted(true);
                }
            }
        }

        private class CheckLongClickRunnable implements Runnable {
            public void run() {
                if(pressed) {
                    performLongClick();
                }
            }
        }

        private class PerformClickRunnable implements Runnable {
            public void run() {
                performClick();
            }
        }
    }
}
