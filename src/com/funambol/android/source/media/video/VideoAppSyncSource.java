/*
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2011 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.source.media.video;

import com.funambol.android.source.media.MediaAppSyncSource;

import com.funambol.storage.Tuple;
import com.funambol.util.Log;
import com.funambol.util.MediaUtils;
import com.funambol.client.source.MediaMetadata;
import com.funambol.client.source.VideoMetadata;
import com.funambol.client.customization.Customization;
import com.funambol.client.source.FunambolFileSyncSource;

public class VideoAppSyncSource extends MediaAppSyncSource {

    private static final String TAG_LOG = VideoAppSyncSource.class.getSimpleName();
    
    public VideoAppSyncSource(String name, FunambolFileSyncSource source, Customization customization) {
        super(name, source, customization);
    }

    public VideoAppSyncSource(String name, Customization customization) {
        super(name, customization);
    }

    @Override
    protected MediaMetadata createMediaMetadata() {
        // Use a standard plain media metadata without source specific
        // extensions
        return new VideoMetadata(this);
    }

    @Override
    protected boolean createThumbnail(String inFile, String outFile, int width, int height) {
        MediaUtils utils = new MediaUtils(getAppContext());
        return utils.createVideoThumbnail(inFile, outFile, width, height);
    }

    @Override
    protected boolean createThumbnailUsingMediaProvider(long itemId,
            String outFile, int width, int height) {
        MediaUtils utils = new MediaUtils(getAppContext());
        return utils.createVideoThumbnailFromProvider(itemId, outFile, width, height);
    }

    @Override
    public void addSpecificMetadata(Tuple item) {
        super.addSpecificMetadata(item);
        long itemId = getItemIdForMediaProvider(item);
        if (itemId != MediaUtils.ITEMID_NOT_FOUND) {
            MediaUtils utils = new MediaUtils(getAppContext());
            long duration = utils.getVideoDuration(itemId);
            if (Log.isLoggable(Log.DEBUG)) {
                Log.debug(TAG_LOG, "Duration for video with media provider id " + itemId + " is " + duration);
            }
            item.setField(item.getColIndexOrThrow(VideoMetadata.METADATA_DURATION), duration);
        } else {
            if (Log.isLoggable(Log.DEBUG)) {
                Log.debug(TAG_LOG, "Cannot obtain a valid media provider id for item");
            }
        }
    }

    public boolean isItemThumbnailUpdated(Tuple item) {
        boolean result = super.isItemThumbnailUpdated(item);
        result |= !item.isUndefined(item.getColIndexOrThrow(VideoMetadata.METADATA_DURATION));
        return result;
    }

}
