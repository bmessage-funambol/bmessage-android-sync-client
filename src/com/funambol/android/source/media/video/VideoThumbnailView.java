/**
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2011 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.source.media.video;

import android.app.Activity;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.widget.TextView;

import com.funambol.android.source.media.MediaThumbnailView;
import com.funambol.androidsync.R;
import com.funambol.storage.Tuple;
import com.funambol.client.source.VideoMetadata;

/**
 * Represents a thumbnail view for videos
 */
public class VideoThumbnailView extends MediaThumbnailView {
    
    private static final String TAG_LOG = VideoThumbnailView.class.getSimpleName();

    public VideoThumbnailView(Activity activity) {
        super(activity);
    }

    protected int getLayoutResourceId() {
        return R.layout.vwvideothumbnail;
    }

    protected int getThumbnailResourceId() {
        return R.id.videothumbnail_laythumb;
    }

    protected int getThumbnailImageResourceId() {
        return R.id.videothumbnail_imgthumb;
    }

    protected int getPlaceHolderResourceId() {
        return R.drawable.common_imgvideo_192x192;
    }
    
    @Override
    public void setThumbnail(Tuple row) {
        super.setThumbnail(row);
        // Set the video duration
        if (!row.isUndefined(row.getColIndexOrThrow(VideoMetadata.METADATA_DURATION))) {
            Long d = row.getLongField(row.getColIndexOrThrow(
                                    VideoMetadata.METADATA_DURATION));
            long dur = d.longValue();
            if (dur > 0) {
                String duration = getStringDuration(dur);
                TextView lengthView = (TextView)findViewById(R.id.videothumbnail_lblduration);
                lengthView.setText(duration);
            }
        }
    }

    private String getStringDuration(long millis) {
        millis = millis / 1000;
        long secs = millis % 60;
        long mins = millis / 60;
        long hours = mins / 60;
        mins = mins % 60;
        StringBuffer result = new StringBuffer();
        if (hours > 0) {
            if (hours < 10) {
                result.append("0");
            }
            result.append(hours).append(":");
        }
        if (mins < 10) {
            result.append("0");
        }
        result.append(mins).append(":");
        if (secs < 10) {
            result.append("0");
        }
        result.append(secs);
        return result.toString();
    }

    protected Bitmap getThumbnailFromProvider(long itemId) {
        Bitmap bitmap = MediaStore.Video.Thumbnails.getThumbnail(
                            getContext().getContentResolver() ,
                            itemId,
                            MediaStore.Video.Thumbnails.MICRO_KIND, null);
        return bitmap;
    }

    public boolean isThumbnailUpdated(Tuple updatedItem) {
        // The thumbnail view must be updated one of the following fields changed:
        // - VideoMetadata.METADATA_THUMBNAIL_PATH;
        // - VideoMetadata.METADATA_DURATION;
        // - VideoMetadata.METADATA_SIZE;
        // - VideoMetadata.METADATA_SIZE;
        int thumbIdx = item.getColIndexOrThrow(VideoMetadata.METADATA_THUMBNAIL_PATH);
        int durationIdx = item.getColIndexOrThrow(VideoMetadata.METADATA_DURATION);
        int sizeIdx = item.getColIndexOrThrow(VideoMetadata.METADATA_SIZE);
        int nameIdx = item.getColIndexOrThrow(VideoMetadata.METADATA_NAME);
        boolean result = false;
        if(!updatedItem.isUndefined(thumbIdx)) {
            String currentThumb = item.getStringField(thumbIdx);
            String updatedThumb = updatedItem.getStringField(thumbIdx);
            result |= !currentThumb.equals(updatedThumb);
        }
        if(!updatedItem.isUndefined(durationIdx)) {
            Long currentDuration = item.getLongField(durationIdx);
            Long updatedDuration = updatedItem.getLongField(durationIdx);
            result |= !currentDuration.equals(updatedDuration);
        }
        if(!updatedItem.isUndefined(sizeIdx)) {
            long currentSize = item.getLongField(sizeIdx).longValue();
            long updatedSize = updatedItem.getLongField(sizeIdx).longValue();
            result |= currentSize != updatedSize;
        }
        // Check olso the name, since on renames the thumbnail could change
        if(!updatedItem.isUndefined(nameIdx)) {
            String currentName = item.getStringField(nameIdx);
            String updatedName = updatedItem.getStringField(nameIdx);
            result |= !currentName.equals(updatedName);
        }
        return result;
    }
}
