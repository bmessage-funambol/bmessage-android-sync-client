/*
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2010 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.source.media;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Vector;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore.MediaColumns;

import com.funambol.android.AppInitializer;
import com.funambol.android.MimeTypeMap;
import com.funambol.android.services.AutoSyncServiceHandler;
import com.funambol.client.configuration.Configuration;
import com.funambol.client.controller.ItemsIgnoredNotification;
import com.funambol.client.controller.RefreshTrigger;
import com.funambol.client.notification.NotificationMessage;
import com.funambol.client.source.AppSyncSource;
import com.funambol.client.source.AppSyncSourceManager;
import com.funambol.client.source.FunambolFileSyncSource;
import com.funambol.client.source.FunambolMediaSyncSource;
import com.funambol.client.source.IgnoredMediaMetadata;
import com.funambol.client.source.MediaMetadata;
import com.funambol.platform.FileAdapter;
import com.funambol.storage.QueryFilter;
import com.funambol.storage.QueryResult;
import com.funambol.storage.Table;
import com.funambol.storage.Table.BulkOperation;
import com.funambol.storage.Tuple;
import com.funambol.sync.SyncSource;
import com.funambol.util.Log;
import com.funambol.util.MediaUtils;
import com.funambol.util.StringUtil;
import com.funambol.util.bus.Bus;
import com.funambol.util.bus.BusMessage;
import com.funambol.util.bus.BusService;


public class MediaDirectoryScanner {

    private static final String TAG_LOG = MediaDirectoryScanner.class.getSimpleName();
    private static final String FILE_PROTOCOL = "file://";

    private MediaAppSyncSource appSource;
    private Configuration configuration;
    private Context appContext;

    private ScannerThread scannerThread;
    private ArrayList<IgnoredItem> ignoredItems = new ArrayList<IgnoredItem>();
    
    public MediaDirectoryScanner(MediaAppSyncSource appSource, Configuration configuration, Context appContext) {
        this.appSource = appSource;
        this.configuration = configuration;
        this.appContext = appContext;
    }

    public void fullScan() {
        scan(true, true, -1, true);
    }

    public void scan(boolean ensureThumbnailsCreation) {
        scan(ensureThumbnailsCreation, -1, false);
    }
    
    public void scan(boolean ensureThumbnailsCreation, int numItemsToImport, boolean ensureThumbnailsOnImported) {
        scan(false, ensureThumbnailsCreation, numItemsToImport, ensureThumbnailsOnImported);
    }
    
    private void scan(boolean isFullScan, boolean ensureThumbnailsCreation, int numItemsToImport,
                      boolean ensureThumbnailsPreviewsOnImported)
    {
        if(scannerThread != null && !scannerThread.isDone()) {
            if(Log.isLoggable(Log.DEBUG)) {
                Log.debug(TAG_LOG, "Scan is already in progress for " + "source: " + appSource.getName());
            }
            return;
        }
        startNewScan(isFullScan, ensureThumbnailsCreation, numItemsToImport, ensureThumbnailsPreviewsOnImported);
    }

    private void startNewScan(boolean isFullScan, boolean ensureThumbnailsCreation, int numItemsToImport,
                              boolean ensureThumbnailsPreviewsOnImported)
    {
        String baseDirectory = getBaseDirectory();
        scannerThread = new ScannerThread(appSource, baseDirectory,
                getIsFirstScan(), isFullScan, ensureThumbnailsCreation, numItemsToImport,
                ensureThumbnailsPreviewsOnImported);
        scannerThread.start();
    }

    public String getBaseDirectory() {
        SyncSource ss = appSource.getSyncSource();
        String baseDirectory = null;
        if(ss instanceof FunambolMediaSyncSource) {
            baseDirectory = ((FunambolMediaSyncSource)ss).getMediaGalleryDirectory();
        } else if(ss instanceof FunambolFileSyncSource) {
            baseDirectory = ((FunambolFileSyncSource)ss).getDirectory();
        } else {
            throw new IllegalStateException("appSource can't provide media gallery folder");
        }
        return baseDirectory;
    }

    public boolean getIsFirstScan() {
        MediaAppSyncSourceConfig config = (MediaAppSyncSourceConfig)appSource.getConfig();
        return !config.getFirstScanDone();
    }

    private void firstScanCompleted() {
        MediaAppSyncSourceConfig config = (MediaAppSyncSourceConfig)appSource.getConfig();
        config.setFirstScanDone(true);
        config.commit();
    }

    private class ScannerThread extends Thread {

        private final String TAG_LOG = ScannerThread.class.getSimpleName();
        
        private final int BULK_OPERATION_SIZE_NORMAL = 50;
        private final int BULK_OPERATION_SIZE_WITH_THUMB_CREATION = 3;
        
        private MediaAppSyncSource appSource;
        private ContentResolver contentResolver;

        private String baseDirectory;
        private boolean isFirstScan;
        private boolean isFullScan;
        private boolean ensureThumbnailsCreation;
        private boolean ensureThumbnailsPreviewsOnImported;
        private int numItemsToImport;

        private FunambolFileSyncSource fileSyncSource;

        private boolean done = false;
        private boolean changesDetected = false;
        private final Object lock = new Object();

        private Vector dlOperations = new Vector();
        private Vector exOperations = new Vector();

        public ScannerThread(MediaAppSyncSource appSource, String baseDirectory, boolean isFirstScan,
                             boolean isFullScan, boolean ensureThumbnailsCreation, int numItemsToImport,
                             boolean ensureThumbnailsPreviewsOnImported)
        {
            this.appSource = appSource;
            this.contentResolver = appContext.getContentResolver();
            this.isFirstScan = isFirstScan;
            this.isFullScan = isFullScan;
            this.baseDirectory = baseDirectory;
            this.fileSyncSource = (FunambolFileSyncSource)appSource.getSyncSource();
            this.ensureThumbnailsCreation = ensureThumbnailsCreation;
            this.numItemsToImport = numItemsToImport;
            this.ensureThumbnailsPreviewsOnImported = ensureThumbnailsPreviewsOnImported;
            this.setPriority(Thread.MIN_PRIORITY);
        }

        @Override
        public void run() {
            try {
                if(Log.isLoggable(Log.TRACE)) {
                    Log.trace(TAG_LOG, "run: " + isFirstScan);
                }
                scanStarted();
                dlOperations.removeAllElements();
                exOperations.removeAllElements();

                if(isFullScan) {
                    // Deletions must be computed for both the metadata tables
                    computeDeletions(appSource.getMetadataTable());
                    computeDeletions(appSource.getExcludedMetadataTable());
                }
                // We try to scan using the native media provider. If the scan
                // will not be performed we use the file system directly.
                boolean scanPerformed = scanFromMediaProvider();
                if(!scanPerformed) {
                    scanFromFileSystem();
                }
            } finally {
                scanCompleted();
            }
        }

        private void computeDeletions(Table metadata) {
            try {
                if(Log.isLoggable(Log.DEBUG)) {
                    Log.debug(TAG_LOG, "Computing deletions for table: " + metadata.getName());
                }
                metadata.open();
                Tuple row = null;
                QueryFilter f = metadata.createQueryFilter();
                f.setValueFilter(metadata.getColIndexOrThrow(MediaMetadata.METADATA_DELETED),
                        true, QueryFilter.EQUAL, 0);
                QueryResult res = metadata.query(f);
                while (res.hasMoreElements()) {
                    row = res.nextElement();
                    checkItemDeletion(metadata, row);
                }
                res.close();
            } catch (Exception e) {
                Log.error(TAG_LOG, "Cannot update metadata table to record deletion", e);
            } finally {
                try {
                    metadata.close();
                } catch (Exception e) {}
            }
        }

        private void checkItemDeletion(Table metadata, Tuple row) throws IOException {
            String filePath = row.getStringField(metadata.getColIndexOrThrow(
                    MediaMetadata.METADATA_ITEM_PATH));
            if (!filePath.startsWith(FILE_PROTOCOL) && !filePath.startsWith("/")) {
                // this item is still remote
                return;
            }
            if(filePath.startsWith(FILE_PROTOCOL)) {
                filePath = filePath.substring(FILE_PROTOCOL.length());
            }
            File file = new File(filePath);
            if (!file.exists()) {
                if(row.getLongField(row.getColIndexOrThrow(MediaMetadata.METADATA_SYNCHRONIZED))
                    .longValue() == 1L) {
                    if(Log.isLoggable(Log.DEBUG)) {
                        Log.debug(TAG_LOG, "Deleted item was synchronized with the cloud");
                    }
                    localDelete(metadata, row);
                } else {
                    if(Log.isLoggable(Log.DEBUG)) {
                        Log.debug(TAG_LOG, "Deleted item was never synchronized with the cloud");
                    }
                    fullDelete(metadata, row);
                }
            }
        }

        private void localDelete(Table metadata, Tuple row) throws IOException {
            if (Log.isLoggable(Log.INFO)) {
                Log.info(TAG_LOG, "Restoring item path to remote");
            }
            // Create a new tuple with only this field set to avoid overwriting other fields
            Long id = row.getLongField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_ID));
            Tuple updateRow = metadata.createNewRow(id);
            
            String itemRemoteUrl = row.getStringField(metadata.getColIndexOrThrow(
                    MediaMetadata.METADATA_ITEM_REMOTE_URL));
            updateRow.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_ITEM_PATH), itemRemoteUrl);
            try {
                metadata.update(updateRow);
                changesDetected = true;
            } catch (Table.ConstraintViolationException cv) {
                throw new IOException("Constraint violation for field " + cv.getFieldIdx());
            }
            metadata.save();
        }

        private void fullDelete(Table metadata, Tuple row) throws IOException {
            if(Log.isLoggable(Log.DEBUG)) {
                Log.info(TAG_LOG, "Removing metadata from the database");
            }
            removeFile(metadata, row, MediaMetadata.METADATA_PREVIEW_PATH);
            removeFile(metadata, row, MediaMetadata.METADATA_THUMBNAIL_PATH);
            metadata.delete(row.getKey());
            metadata.save();
            changesDetected = true;
        }

        private boolean removeFile(Table metadata, Tuple row, String colName) {
            if (Log.isLoggable(Log.INFO)) {
                Log.info(TAG_LOG, "Removing " + colName);
            }
            try {
                int colIdx = metadata.getColIndexOrThrow(colName);
                String path = row.getStringField(colIdx);
                
                if (path.startsWith(FILE_PROTOCOL)) {
                    path = path.substring(FILE_PROTOCOL.length());
                }
                File file = new File(path);
                if (file.exists()) {
                    file.delete();
                    if (Log.isLoggable(Log.INFO)) {
                        Log.info(TAG_LOG, "Removed file " + path);
                    }
                    return true;
                } else {
                    if (Log.isLoggable(Log.ERROR)) {
                        Log.error(TAG_LOG, "File " + path + " does not exist");
                    }
                    return false;
                }
            } catch (Throwable t) {
                Log.error(TAG_LOG, "Unable to remove file: " + t.getMessage());
                return false;
            }
        }

        private boolean scanFromMediaProvider() {
            if(Log.isLoggable(Log.TRACE)) {
                Log.trace(TAG_LOG, "scanFromMediaProvider");
            }
            if(isFullScan) {
                if(Log.isLoggable(Log.DEBUG)) {
                    Log.debug(TAG_LOG, "Cannot scan from media provider in full scan");
                }
                return false;
            }
            boolean result = false;
            Uri providerUri = appSource.getProviderUri();
            if(providerUri != null) {
                MediaUtils utils = new MediaUtils(appContext);
                // Check if the provider returns at least one item
                Cursor items = utils.getAllMediaItemsFromProvider(providerUri,
                        MediaColumns.DATE_MODIFIED + " DESC");
                if(items != null) {
                    if(items.moveToFirst()) {
                        result = true;
                        do {
                            long id = items.getLong(0);
                            String fullPath = items.getString(1);
                            handleLocalFile(id, fullPath);
                        } while(items.moveToNext());
                        applyRemainingBulkOperations();
                    }
                    items.close();
                }
            }
            return result;
        }

        private void scanFromFileSystem() {
            if(Log.isLoggable(Log.TRACE)) {
                Log.trace(TAG_LOG, "scanFromFileSystem");
            }
            File directory = new File(baseDirectory);
            if (!directory.isDirectory()) {
                Log.error(TAG_LOG, baseDirectory + " is not a directory");
                return;
            }
            for(String fileName : directory.list()) {
                if(!supports(fileName)) {
                    continue;
                }
                handleLocalFile(getFileFullName(fileName));
            }
            applyRemainingBulkOperations();
        }

        private boolean supports(String path) {
            return fileSyncSource.isSupportedExtension(path);
        }

        private String getFileFullName(String fileName) {
            StringBuffer fullName = new StringBuffer();
            fullName.append(baseDirectory);
            fullName.append("/");
            fullName.append(fileName);
            return fullName.toString();
        }

        private void handleLocalFile(String fullPath) {
            handleLocalFile(MediaUtils.ITEMID_NOT_FOUND, fullPath);
        }
        
        // Handle the given file returning the proper BulkOperation to be applyed
        private void handleLocalFile(long itemProviderId, String fullPath)
        {
            if(Log.isLoggable(Log.TRACE)) {
                Log.trace(TAG_LOG, "handleLocalFile: " + fullPath);
            }
            
            // Ignore the item if its size exceeds the max for local items
            try {
               
                FileAdapter fa = new FileAdapter(fullPath);
                long size = fa.getSize();
                if (appSource.getConfig().getMaxItemSize() > 0 && size > appSource.getConfig().getMaxItemSize()) {
                    if (Log.isLoggable(Log.INFO)) {
                        Log.info(TAG_LOG, "Ignoring change for item exceeding max item size " + fullPath + "," + size);
                    }
                    IgnoredItem ignoredItem = new IgnoredItem(fullPath, size);
                    
                    // Check if this item has already been detected (and reported as ignored)
                    Table ignoredMediaMetadataTable = appSource.getIgnoredMediaMetadataTable();
                    QueryResult qr = null;
                    try {
                        ignoredMediaMetadataTable.open();
                        QueryFilter qf = ignoredMediaMetadataTable.createQueryFilter();
                        qf.setValueFilter(ignoredMediaMetadataTable.getColIndexOrThrow(IgnoredMediaMetadata.ITEM_PATH), true,
                                          QueryFilter.EQUAL, fullPath);
                        qr = ignoredMediaMetadataTable.query(qf);
                        if (qr.hasMoreElements()) {
                            // This item has already been reported as ignored, skip it 
                            if (Log.isLoggable(Log.INFO)) {
                                Log.info(TAG_LOG, "Found an item that has already been reported as ignored, Skip it"
                                                  + fullPath);
                            }
                            ignoredItem.setAlreadyNotified(true);
                            return;
                        }
                    } catch (IOException ioe) {
                        Log.error(TAG_LOG, "Error accessing ignored items table", ioe);
                    } finally {
                        if (qr != null) {
                            qr.close();
                        }
                        try {
                            ignoredMediaMetadataTable.close();
                        } catch (IOException ioe) {
                        }
                    }
                    
                    ignoredItems.add(ignoredItem);
                    

                    return;
                }
            } catch (IOException ioe) {
                if (Log.isLoggable(Log.INFO)) {
                    Log.info(TAG_LOG, "Cannot check item size " + fullPath, ioe);
                }
            }
            
            Tuple itemTuple = null;
            
            boolean insert = false;
            boolean update = false;
            boolean signalEndOfImport = false;
            
            boolean isDigitalLifeItem = true;
            boolean ensureThumbnailPreview = false;

            if(isFullScan || isFirstScan) {
                Table dlTable = appSource.getMetadataTable();
                Tuple dlTuple = retrieveItemTuple(dlTable, fullPath);
                if(dlTuple != null) {
                    itemTuple = dlTuple;
                    isDigitalLifeItem = true;
                } else {
                    Table exTable = appSource.getExcludedMetadataTable();
                    Tuple exTuple = retrieveItemTuple(exTable, fullPath);
                    if(exTuple != null) {
                        itemTuple = exTuple;
                        isDigitalLifeItem = false;
                    }
                }
                if(itemTuple == null) {
                    File file = new File(fullPath);
                    if (numItemsToImport > 0) {
                        isDigitalLifeItem = true;
                        numItemsToImport--;
                        ensureThumbnailPreview = ensureThumbnailsPreviewsOnImported;
                    } else {
                        isDigitalLifeItem = file.lastModified() > configuration.getFirstRunTimestamp();
                    }
                    if (numItemsToImport == 0) {
                        signalEndOfImport = true;
                    }
                }
            } else {
                isDigitalLifeItem = false;
            }

            if(Log.isLoggable(Log.TRACE)) {
                Log.trace(TAG_LOG, "isDigitalLifeItem: " + isDigitalLifeItem);
            }

            Table metadata = getMetadataTable(isDigitalLifeItem);

            if(itemTuple != null) {
                update = isUpdated(fullPath, itemTuple, metadata);
            } else {
                if(isFirstScan || isFullScan) {
                    if(Log.isLoggable(Log.TRACE)) {
                        Log.trace(TAG_LOG, "Creating new item tuple for first/full scan");
                    }
                    itemTuple = metadata.createNewRow();
                    insert = true;
                } else {
                    itemTuple = retrieveItemTuple(metadata, fullPath);
                    if(itemTuple == null) {
                        if(!isDigitalLifeItem) {
                            // We have to make sure that this item is not
                            // already part of the digital life
                            if(retrieveItemTuple(appSource.getMetadataTable(), 
                                    fullPath, true) != null) {
                                if(Log.isLoggable(Log.DEBUG)) {
                                    Log.debug(TAG_LOG, "Item is already part of "
                                            + "the user's digital life");
                                }
                                return;
                            }
                        }
                        if(Log.isLoggable(Log.TRACE)) {
                            Log.trace(TAG_LOG, "Creating new item tuple");
                        }
                        itemTuple = metadata.createNewRow();
                        insert = true;
                    } else {
                        if(!isDigitalLifeItem) {
                            // Check if the item became part of the digital life
                            // Here we check only using the file name since the
                            // item path could still be remote
                            if(retrieveItemTuple(appSource.getMetadataTable(), fullPath, true) != null) {
                                if(Log.isLoggable(Log.DEBUG)) {
                                    Log.debug(TAG_LOG, "Item became part of the user's digital life, "
                                            + "removing from excluded items");
                                }
                                singleDelete(metadata, itemTuple);
                                return;
                            }
                        }
                        update = isUpdated(fullPath, itemTuple, metadata);
                    }
                }
            }
            if(!insert && !update) {
                if(Log.isLoggable(Log.TRACE)) {
                    Log.trace(TAG_LOG, "No changes to track");
                }
                return;
            }
            File file = new File(fullPath);
            if(insert) {
                itemTuple.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_NAME), file.getName());
                itemTuple.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_THUMBNAIL_PATH), "");
                itemTuple.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_PREVIEW_PATH), "");
                itemTuple.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_ITEM_PATH), FILE_PROTOCOL + fullPath);
                itemTuple.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_ITEM_REMOTE_URL), "");
                itemTuple.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_ORDER_DATE), file.lastModified());
                itemTuple.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_UPLOAD_CONTENT_STATUS), 0L);
                itemTuple.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_SYNCHRONIZED), 0L);
                itemTuple.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_DELETED), 0);
                String extension = MimeTypeMap.getFileExtensionFromUrl(file.getName());
                itemTuple.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_MIME),
                             MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension));
            }
            itemTuple.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_SIZE), file.length());
            itemTuple.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_DIRTY), 1);
            itemTuple.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_LAST_MODIFIED_DATE),
                    file.lastModified());

            // Check if we need to change the order date
            if(update && appSource.getChangeOrderDateOnUpdates()) {
                itemTuple.setField(metadata.getColIndexOrThrow(MediaMetadata.METADATA_ORDER_DATE), file.lastModified());
            }

            appSource.addSpecificMetadata(itemTuple);

            int thumbIdx = metadata.getColIndexOrThrow(MediaMetadata.METADATA_THUMBNAIL_PATH);

            //
            // Check if we have to create a thumbnail for this item or we can
            // pick it from the media provider
            //
            if(ensureThumbnailsCreation || ensureThumbnailPreview) {
                boolean created = false;
                
                if(insert) {
                    // If the thumbnail path was never set we try to pick it from
                    // the media provider
                    created = setThumbnailFromMediaProvider(itemProviderId, itemTuple, thumbIdx);
                }
                
                if(!created) {
                    if(insert) {
                        // If this is a new item and the the thumbnail was never
                        // created we have to add the item to the metadata table
                        // in order to get the item id
                        itemTuple.setField(thumbIdx, "");
                        singleInsert(metadata, itemTuple);
                        // The insert now becomes an update
                        insert = false;
                        update = true;
                        appSource.createThumbnail(itemTuple, !isDigitalLifeItem);
                    } else if(StringUtil.isNullOrEmpty(itemTuple.getStringField(thumbIdx))) {
                        // Create the thumbnail if this is an update and the
                        // thumbnail path was never set
                        appSource.createThumbnail(itemTuple, !isDigitalLifeItem);
                    } else {
                        // The thumbnail path is already set
                    }
                }
            
                if (ensureThumbnailPreview) {
                    int previewIdx = itemTuple.getColIndexOrThrow(MediaMetadata.METADATA_PREVIEW_PATH);
                    if (StringUtil.isNullOrEmpty(itemTuple.getStringField(previewIdx))) {
                        // For the preview, we can only generate the preview ourselves
                        if (Log.isLoggable(Log.TRACE)) {
                            Log.trace(TAG_LOG, "Generating preview for item " + fullPath);
                        }
                        int idIdx = itemTuple.getColIndexOrThrow(MediaMetadata.METADATA_ID);
                        if (itemTuple.isUndefined(idIdx)) {
                            singleInsert(metadata, itemTuple);
                            insert = false;
                            update = true;
                        }
                        appSource.createPreview(itemTuple, !isDigitalLifeItem);
                    }
                }
            } else {
                if(insert) {
                    setThumbnailFromMediaProvider(itemProviderId, itemTuple, thumbIdx);
                }
            }
            BulkOperation operation = metadata.new BulkOperation(
                    insert ? BulkOperation.INSERT : BulkOperation.UPDATE, itemTuple);
            if(isDigitalLifeItem) {
                dlOperations.addElement(operation);
            } else {
                exOperations.addElement(operation);
            }
            if (signalEndOfImport) {
                // Force committing to the DB
                applyBulkOperationsIfNeeded(true);
                // Now signal on the bus
                ScanMessage msg = new ScanMessage(ScanMessage.FIRST_IMPORT_COMPLETED, appSource);
                BusService.sendMessage(msg);
                numItemsToImport = -1;
            } else {
                applyBulkOperationsIfNeeded();
            }
        }

        private Table getMetadataTable(boolean isDigitalLife) {
            return isDigitalLife ? appSource.getMetadataTable() :
                                   appSource.getExcludedMetadataTable();
        }

        private boolean isUpdated(String itemPath, Tuple existingTuple, Table metadata) {
            long newLastModified = new File(itemPath).lastModified();
            long exiLastModified = existingTuple.getLongField(
                    metadata.getColIndexOrThrow(MediaMetadata.METADATA_LAST_MODIFIED_DATE));
            if(newLastModified > exiLastModified) {
                if(Log.isLoggable(Log.TRACE)) {
                    Log.trace(TAG_LOG, "The item has been modified");
                }
                return true;
            }
            boolean requiresThumb = appSource.getThumbnailsProviderUri() != null;
            boolean isThumbnailSet = !StringUtil.isNullOrEmpty(
                    existingTuple.getStringField(
                    metadata.getColIndexOrThrow(MediaMetadata.METADATA_THUMBNAIL_PATH)));
            if(requiresThumb && !isThumbnailSet) {
                if(Log.isLoggable(Log.TRACE)) {
                    Log.trace(TAG_LOG, "The item needs a new thumbnail");
                }
                return true;
            }
            return false;
        }

        private boolean setThumbnailFromMediaProvider(long itemProviderId,
                Tuple itemTuple, int thumbIdx) {

            // On SE XPeria Arc media provider thumbnail are rotated by 90 degreees
            if ("LT15i".equalsIgnoreCase(configuration.getDeviceInfo().getDeviceModel())) {
                // Thumbnails must be created
                return false;
            }

            if(itemProviderId != MediaUtils.ITEMID_NOT_FOUND) {
                String mediaProviderThumbnailPath = getMediaProviderThumbnailPath(itemProviderId);
                if(mediaProviderThumbnailPath != null && mediaProviderThumbnailPath.endsWith(".jpg")) {
                    if(Log.isLoggable(Log.DEBUG)) {
                        Log.debug(TAG_LOG, "Using media provider thumbnail");
                    }
                    itemTuple.setField(thumbIdx,
                            MediaUtils.MEDIA_PROVIDER_PATH + itemProviderId);
                    return true;
                }
            }
            return false;
        }

        private void singleInsert(Table metadata, Tuple itemTuple) {
            boolean insertDone = false;
            int retries = 0;
            do {
                try {
                    metadata.open();
                    metadata.insert(itemTuple);
                    metadata.save();
                    insertDone = true;
                    changesDetected = true;
                } catch (Table.ConstraintViolationException ex) {
                    retries++;
                    // Constraint failed, retry with a different constraint value
                    int fieldIdx = metadata.getColIndexOrThrow(
                            MediaMetadata.METADATA_ORDER_DATE);
                    long fieldValue = itemTuple.getLongField(fieldIdx).longValue();
                    itemTuple.setField(fieldIdx, fieldValue + 1);
                } catch(IOException ex) {
                    Log.error(TAG_LOG, "Cannot update metadata table", ex);
                } finally {
                    try {
                        metadata.close();
                    } catch(IOException ex) { }
                }
            } while(!insertDone);
            if (retries > 0) {
                if (Log.isLoggable(Log.INFO)) {
                    Log.info(TAG_LOG, "Item violated unique constraint " + retries + "x");
                }
            }
        }

        private void singleDelete(Table metadata, Tuple itemTuple) {
            try {
                metadata.open();
                metadata.delete(itemTuple.getKey());
                metadata.save();
                changesDetected = true;
            } catch(IOException ex) {
                Log.error(TAG_LOG, "Cannot delete item from metadata table", ex);
            } finally {
                try {
                    metadata.close();
                } catch(IOException ex) { }
            }
        }

        private String getMediaProviderThumbnailPath(long itemProviderId) {
            Cursor cursor = null;
            try {
                Uri providerUri = appSource.getThumbnailsProviderUri();
                String itemIdColumn = appSource.getThumbnailsProviderItemIdColumn();
                String dataColumn = appSource.getThumbnailsProviderDataColumn();
                cursor = contentResolver.query(providerUri, new String[] {dataColumn},
                        itemIdColumn+"="+itemProviderId, null, null);
                if(cursor != null && cursor.moveToFirst()) {
                    String path = cursor.getString(0);
                    if(new File(path).exists()) {
                        if(Log.isLoggable(Log.DEBUG)) {
                            Log.debug(TAG_LOG, "Found thumbnail from media "
                                    + "provider: " + path);
                        }
                        return FILE_PROTOCOL + path;
                    }
                }
                if(Log.isLoggable(Log.DEBUG)) {
                    Log.debug(TAG_LOG, "Connot find thumbnail from media provider");
                }
                return null;
            } finally {
                if(cursor != null) {
                    cursor.close();
                }
            }
        }

        private Tuple retrieveItemTuple(Table table, String path) {
            return retrieveItemTuple(table, path, false);
        }
        
        private Tuple retrieveItemTuple(Table table, String path, boolean checkOnlyFileName) {
            QueryResult res = null;
            try {
                table.open();
                QueryFilter filter = table.createQueryFilter();
                if(checkOnlyFileName) {
                    File file = new File(path);
                    filter.setValueFilter(
                            table.getColIndexOrThrow(MediaMetadata.METADATA_NAME), true,
                            QueryFilter.EQUAL, file.getName());
                } else {
                    filter.setValueFilter(
                            table.getColIndexOrThrow(MediaMetadata.METADATA_ITEM_PATH), true,
                            QueryFilter.EQUAL, FILE_PROTOCOL + path);
                }
                res = table.query(filter);
                Tuple tuple = res.nextElement();
                if (tuple != null) {
                    if (Log.isLoggable(Log.DEBUG)) {
                        Log.debug(TAG_LOG, "Tuple retrieved from " + table.getName());
                    }
                }
                return tuple;
            } catch(IOException ex) {
                Log.error(TAG_LOG, "Failed to retrieve item tuple", ex);
            } finally {
                if(table != null) {
                    try {
                        table.close();
                    } catch(Exception ex) { }
                }
                if(res != null) {
                    try {
                        res.close();
                    } catch(Exception ex) { }
                }
            }
            return null;
        }
        
        private void applyBulkOperationsIfNeeded() {
            applyBulkOperationsIfNeeded(false);
        }

        private void applyBulkOperationsIfNeeded(boolean force) {
            applyBulkOperationsIfNeeded(dlOperations, appSource.getMetadataTable(), force);
            applyBulkOperationsIfNeeded(exOperations, appSource.getExcludedMetadataTable(), force);
        }
        
        private void applyBulkOperationsIfNeeded(Vector ops, Table metadata, boolean force) {
            if(ops.size() >= getBulkOperationsSize() || force) {
                applyBulkOperations(ops, metadata);
                ops.removeAllElements();
            }
        }
        
        private void applyRemainingBulkOperations() {
            applyBulkOperations(dlOperations, appSource.getMetadataTable());
            applyBulkOperations(exOperations, appSource.getExcludedMetadataTable());
        }

        private int getBulkOperationsSize() {
            return ensureThumbnailsCreation ? BULK_OPERATION_SIZE_WITH_THUMB_CREATION :
                                              BULK_OPERATION_SIZE_NORMAL;
        }
        
        private void applyBulkOperations(Vector ops, Table metadata) {
            if(Log.isLoggable(Log.TRACE)) {
                Log.trace(TAG_LOG, "applyBulkOperations: " + ops.size());
            }
            if(ops.isEmpty()) {
                // Nothing to do
                return;
            }
            // We only track changes to the metadata table, not the external table
            if (metadata == appSource.getMetadataTable()) {
                changesDetected = true;
            }
            try {
                metadata.open();
                applyOperations(metadata, ops);
                metadata.save();
            } catch(Exception ex) {
                Log.error(TAG_LOG, "Failed to apply bulk operations", ex); 
            } finally {
                try {
                    metadata.close();
                } catch(IOException ex) { }
            }
        }

        private int applyOperations(Table metadata, Vector operations) throws IOException,
                Table.BulkOperationException {
            int opsPerformed = 0;
            try {
                metadata.bulkOperations(operations);
                opsPerformed = operations.size();
            } catch (Table.BulkOperationException boe) {
                opsPerformed = boe.getNumSuccess();
                Exception rootException = boe.getRootException();
                if (rootException instanceof Table.ConstraintViolationException) {
                    if (Log.isLoggable(Log.INFO)) {
                        Log.info(TAG_LOG, "Received an item violating unique constraint " + operations.size()
                                          + "," + opsPerformed);
                    }
                    // Change the timestamp of the failing item
                    BulkOperation offendingOperation = (BulkOperation)
                            operations.elementAt(opsPerformed);
                    Tuple offendingTuple = offendingOperation.getTuple();
                    if (Log.isLoggable(Log.INFO)) {
                        int nameIdx = offendingTuple.getColIndexOrThrow(MediaMetadata.METADATA_NAME);
                        String name = offendingTuple.getStringField(nameIdx);
                        Log.info(TAG_LOG, "Offending item is named " + name);
                    }
                    if (offendingTuple != null) {
                        int orderDateIdx = offendingTuple.getColIndexOrThrow(MediaMetadata.METADATA_ORDER_DATE);
                        Long offendingDate = offendingTuple.getLongField(orderDateIdx);
                        long newDate = offendingDate.longValue();

                        // Check if there are other items in that neighborhood
                        QueryFilter qf = metadata.createQueryFilter();
                        qf.setValueFilter(orderDateIdx, true, QueryFilter.LESS_EQUAL, new Long(newDate));
                        QueryResult qr = metadata.query(qf, orderDateIdx, false, -1);
                        // Scan forward to find an empty slot
                        while(qr.hasMoreElements()) {
                            Tuple tuple = qr.nextElement();
                            Long tupleDate = tuple.getLongField(orderDateIdx);
                            if (tupleDate.longValue() == newDate) {
                                // This slot it taken, move forward
                                newDate--;
                            } else {
                                if (Log.isLoggable(Log.DEBUG)) {
                                    Log.debug(TAG_LOG, "Found new slot for confliting date " + newDate);
                                }
                                break;
                            }
                        }
                        qr.close();
                        // Prepare to apply the remaining operations
                        offendingTuple.setField(orderDateIdx, newDate);
                        Vector remainingOperations = new Vector();
                        for(int i=opsPerformed;i<operations.size();++i) {
                            remainingOperations.addElement(operations.elementAt(i));
                        }
                        // Fix other contraint violations we may have in the remaining operations
                        disambiguateOrderDate(metadata, remainingOperations);
                        if (Log.isLoggable(Log.DEBUG)) {
                            Log.debug(TAG_LOG, "Applying remaining operations " + remainingOperations.size());
                        }
                        return opsPerformed + applyOperations(metadata, remainingOperations);
                    }
                } else {
                    throw boe;
                }
            }
            return opsPerformed;
        }

        private void disambiguateOrderDate(Table metadata, Vector operations) {
            // Try to avoid ConstraintViolation on the ORDER_DATE. At least within this bunch of operations we
            // make the values unique
            int orderDateIdx = metadata.getColIndexOrThrow(MediaMetadata.METADATA_ORDER_DATE);
            Long previousDate = null;
            for(int i=0;i<operations.size();++i) {
                BulkOperation op = (BulkOperation) operations.elementAt(i);
                if (op.getType() == BulkOperation.INSERT || op.getType() == BulkOperation.UPDATE) {
                    Tuple tuple = op.getTuple();
                    Long date = tuple.getLongField(orderDateIdx);
                    if (previousDate != null && date.longValue() >= previousDate.longValue()) {
                        date = new Long(previousDate.longValue() - 1);
                        tuple.setField(orderDateIdx, date);
                    }
                    previousDate = date;
                }
            }
        }

        private void scanStarted() {
            ScanMessage msg = new ScanMessage(ScanMessage.SCAN_STARTED, appSource);
            BusService.sendMessage(msg);
            changesDetected = false;
            ignoredItems.clear();
        }
        
        private void scanCompleted() {
            synchronized(lock) {
                done = true;
                if(isFirstScan) {
                    firstScanCompleted();
                }
            }
            ScanMessage msg = new ScanMessage(ScanMessage.SCAN_COMPLETED, appSource);
            BusService.sendMessage(msg);
            
            // If the user is on automatic sync, this is a good time to fire an auto sync
            if (changesDetected) {
                if (!configuration.getCredentialsCheckPending() && 
                     configuration.getSyncMode() == Configuration.SYNC_MODE_AUTO) {
                    
                    AutoSyncServiceHandler autoSyncServiceHandler = new AutoSyncServiceHandler(appContext);
                    
                    AppInitializer appInitializer = AppInitializer.i(appContext);
                    AppSyncSourceManager appManager = appInitializer.getAppSyncSourceManager();
                    Enumeration<AppSyncSource> appSources = appManager.getEnabledAndWorkingSources();
                    Vector<AppSyncSource> sources = new Vector<AppSyncSource>();
                    while(appSources.hasMoreElements()) {
                        AppSyncSource appSource = appSources.nextElement();
                        sources.addElement(appSource);
                    }
                    autoSyncServiceHandler.startSync(RefreshTrigger.PUSH, sources, 10 * 1000);
                }
            } else {
                if (Log.isLoggable(Log.DEBUG)) {
                    Log.debug(TAG_LOG, "No changes detected on directory scan");
                }
            }
           
            // Handle ignored items here
            if (ignoredItems.size() > 0) {
                ItemsIgnoredNotification itemsIgnoredNotification = null;
                // Check if we have at least one item which has never been notified before
                for(IgnoredItem ignoredItem : ignoredItems) {
                    if (!ignoredItem.getAlreadyNotified()) {
                        if (itemsIgnoredNotification == null) {
                            AppInitializer appInitializer = AppInitializer.i(appContext);
                            // Generate a notification
                            itemsIgnoredNotification = new ItemsIgnoredNotification(appInitializer.getController());
                        }
                        itemsIgnoredNotification.addItem(appSource, ignoredItem.getFullPath(), ignoredItem.getSize(),
                                                         ItemsIgnoredNotification.REASON_SIZE);
                    }
                }
                if (itemsIgnoredNotification != null) {
                    if (Log.isLoggable(Log.TRACE)) {
                        Log.trace(TAG_LOG, "Sending message to report ignored items");
                    }
                    NotificationMessage nm = new NotificationMessage(itemsIgnoredNotification);
                    Bus.getInstance().sendMessage(nm);
                }
                // Now persist all the ignored items information. We reset the info every time to make sure we
                // hold only updated information and clean old stuff
                Table ignoredItemsTable = appSource.getIgnoredMediaMetadataTable();
                try {
                    ignoredItemsTable.open();
                    ignoredItemsTable.reset();
                    Vector ops = new Vector();
                    for(IgnoredItem ignoredItem : ignoredItems) {
                        Tuple newRow = ignoredItemsTable.createNewRow();
                        newRow.setField(newRow.getColIndexOrThrow(IgnoredMediaMetadata.ITEM_PATH),
                                        ignoredItem.getFullPath());
                        newRow.setField(newRow.getColIndexOrThrow(IgnoredMediaMetadata.REASON),
                                        IgnoredMediaMetadata.ITEM_TOO_BIG);
                        Table.BulkOperation op = ignoredItemsTable.new BulkOperation(BulkOperation.INSERT, newRow);
                        ops.addElement(op);
                        if (itemsIgnoredNotification == null) {
                            AppInitializer appInitializer = AppInitializer.i(appContext);
                            // Generate a notification
                            itemsIgnoredNotification = new ItemsIgnoredNotification(appInitializer.getController());
                        }
                        itemsIgnoredNotification.addItem(appSource, ignoredItem.getFullPath(), ignoredItem.getSize(),
                                                         ItemsIgnoredNotification.REASON_SIZE);
                    }
                    ignoredItemsTable.bulkOperations(ops);
                } catch (Exception e) {
                    Log.error(TAG_LOG, "Cannot update ignored items table", e);
                } finally {
                    try {
                        ignoredItemsTable.close();
                    } catch (IOException ioe) {
                    }
                }
            }
        }

        public boolean isDone() {
            synchronized(lock) {
                return done;
            }
        }
    }

    public static class ScanMessage extends BusMessage {

        public static final int SCAN_STARTED = 0;
        public static final int SCAN_COMPLETED = 1;
        public static final int FIRST_IMPORT_COMPLETED = 2;

        private int code;
        private MediaAppSyncSource appSource;
        
        public ScanMessage(int code, MediaAppSyncSource appSource) {
            super(null);
            this.code = code;
            this.appSource = appSource;
        }

        public int getCode() {
            return code;
        }

        public MediaAppSyncSource getAppSource() {
            return appSource;
        }
    }
    
    private class IgnoredItem {
        private String fullPath;
        private boolean alreadyNotified;
        private long size;
        
        public IgnoredItem(String fullPath, long size) {
            this.fullPath = fullPath;
            this.size = size;
        }
        
        public String getFullPath() {
            return fullPath;
        }
        
        public long getSize() {
            return size;
        }
        
        public void setAlreadyNotified(boolean value) {
            this.alreadyNotified = value;
        }
        
        public boolean getAlreadyNotified() {
            return alreadyNotified;
        }
        
    }
}


