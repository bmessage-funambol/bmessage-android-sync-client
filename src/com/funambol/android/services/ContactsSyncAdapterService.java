/*
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2003 - 2007 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.accounts.Account;
import android.content.Context;
import android.content.SyncResult;
import android.content.ContentProviderClient;
import android.content.AbstractThreadedSyncAdapter;
import android.os.Bundle;

/**
 * Represents an abstract SyncAdapterService which can the inherited by every
 * supported authority.
 *
 * If the service is invoked using a android.content.SyncAdapter intent action
 * the oBind method will return the SyncAdapterBinder handled by the native
 * SyncManager. The LocalBinder is returned otherwise, this is the use case of
 * activities that wish to be notified of the sync events.
 */
public class ContactsSyncAdapterService extends Service {

    private final String TAG_LOG = "ContactsSyncAdapterService";

    private SyncAdapterImpl syncAdapter = null;

    public ContactsSyncAdapterService() {
        super();
    }

    /**
     * Define a generic SyncAdapter implementation
     */
    private class SyncAdapterImpl extends AbstractThreadedSyncAdapter {

        public SyncAdapterImpl(Context context) {
            super(context, true);
        }

        @Override
        public void onPerformSync(Account account, Bundle extras, String authority,
                                  ContentProviderClient provider, SyncResult syncResult) {
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        syncAdapter = new SyncAdapterImpl(getApplicationContext());
    }

    @Override
    public IBinder onBind(Intent intent) {
        if("android.content.SyncAdapter".equals(intent.getAction() )) {
            return syncAdapter.getSyncAdapterBinder();
        }
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }
    
    @Override
    public void onDestroy() {
    }
}
