/*
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2010 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.services;

import java.io.File;
import java.util.Hashtable;
import java.util.Vector;
import java.util.Map.Entry;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;

import com.funambol.android.AndroidAppSyncSource;
import com.funambol.android.AndroidConfiguration;
import com.funambol.android.AndroidCustomization;
import com.funambol.android.AppInitializer;
import com.funambol.android.source.media.MediaAppSyncSource;
import com.funambol.android.source.media.file.AndroidFileObserver;
import com.funambol.client.controller.Controller;
import com.funambol.client.controller.PimChangedMessage;
import com.funambol.client.controller.RefreshTrigger;
import com.funambol.client.configuration.Configuration;
import com.funambol.client.source.AppSyncSource;
import com.funambol.client.source.AppSyncSourceManager;
import com.funambol.concurrent.Task;
import com.funambol.concurrent.TaskExecutor;
import com.funambol.platform.TimerHandler;
import com.funambol.sync.SyncSource;
import com.funambol.sync.client.ChangesTracker;
import com.funambol.sync.client.NotifiableTracker;
import com.funambol.sync.client.TrackableSyncSource;
import com.funambol.util.Log;
import com.funambol.util.StringUtil;
import com.funambol.util.bus.BusService;

/**
 * This class is responsible for handling automatic synchronizations. In the
 * client there are different types of automatic syncs:
 *
 * 1) scheduled syncs (periodic)
 * 2) pending syncs waiting for some triggering event (for example syncs pending
 *    on the WiFi availability)
 * 3) syncs pushed from the server
 */
public class AutoSyncService extends Service {
    private final String TAG = AutoSyncService.class.getSimpleName();

    public static final String AUTO_SYNC_ACTION  = "com.funambol.android.AUTO_SYNC";

    public static final String OPERATION = "OPERATION";
    public static final String PROGRAM_SCHEDULED_SYNC = "com.funambol.android.PROGRAM_SCHEDULED_SYNC";
    public static final String CANCEL_SCHEDULED_SYNC = "com.funambol.android.CANCEL_SCHEDULED_SYNC";
    public static final String PROGRAM_SYNC_RETRY = "com.funambol.android.PROGRAM_SYNC_RETRY";
    public static final String CANCEL_SYNC_RETRY = "com.funambol.android.CANCEL_SYNC_RETRY";
    public static final String START_MONITORING_URI = "com.funambol.android.START_MONITORING_URI";
    public static final String START_MONITORING_DIRECTORY = "com.funambol.android.START_MONITORING_DIRECTORY";
    public static final String RESTART_MONITORING_ALL_DIRECTORIS = "com.funambol.android.RESTART_MONITORING_ALL_DIRECTORIS";
    public static final String START_SYNC = "com.funambol.android.START_SYNC";

    public static final String SOURCE_ID = "SOURCE_ID";
    public static final String SOURCES_ID = "SOURCES_ID";
    public static final String DELAY = "DELAY";
    public static final String COUNT = "COUNT";
    public static final String URI = "URI";
    public static final String SYNC_MODE = "SYNC_MODE";
    public static final String DIRECTORY = "DIRECTORY";
    public static final String EXTENSIONS = "EXTENSIONS";

    private PendingIntent scheduledSyncIntent;
    private PendingIntent syncRetryIntent;

    private AndroidConfiguration configuration;
    private AndroidCustomization customization;
    private AppSyncSourceManager appSyncSourceManager;
    
    private TaskExecutor pendingTaskExecutor;

    private AlarmManager am;
    private AppInitializer initializer;
    private final Object serviceLock = new Object();
    private boolean initStarted = false;
    private boolean initCompleted = false;
    private int taskId = 0;

    private Hashtable<Uri,AndroidAppSyncSource> monitoredUris = 
            new Hashtable<Uri,AndroidAppSyncSource>();
    
    private Hashtable<String, AndroidFileObserver> fileObservers =
            new Hashtable<String, AndroidFileObserver>();

    public AutoSyncService() {
        super();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (Log.isLoggable(Log.DEBUG)) {
            Log.debug(TAG, "Service Created");
        }
        pendingTaskExecutor = new TaskExecutor();
        pendingTaskExecutor.setMaxThreads(1);
        
        AppInitializerTask initializerTask = new AppInitializerTask();
        pendingTaskExecutor.scheduleTask(initializerTask);
        
        try {
            // we wait for the init to start, then we are sure the lock is held until the initialization
            // has been performed
            while(!initStarted) {
                Thread.sleep(100);
            }
        } catch (Exception e) {}
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (Log.isLoggable(Log.DEBUG)) {
            Log.debug(TAG, "Service Started");
        }
        String operation = null;
        if(intent != null) {
            operation = intent.getStringExtra(OPERATION);
        }
        if (operation != null) {
            if (!initCompleted) {
                // The init has not completed yet, we need to queue this task and execute it later
                PendingOperationTask pot = new PendingOperationTask(operation, intent);
                pendingTaskExecutor.scheduleTask(pot);
            } else {
                performRequiredOperation(operation, intent);
            }
        } else  {
            startForeground(Notification.FLAG_FOREGROUND_SERVICE,
                    new Notification(0, null, System.currentTimeMillis()));
        }
        return START_STICKY;
    }
    
    private void performRequiredOperation(String operation, Intent intent) {
        if (START_MONITORING_URI.equals(operation)) {
            int sourceId = intent.getIntExtra(SOURCE_ID, -1);
            String uri = intent.getStringExtra(URI);
            startMonitoringUri(uri, sourceId);
        } else if (START_SYNC.equals(operation)) {
            String syncMode = intent.getStringExtra(SYNC_MODE);
            int sourcesId[] = intent.getIntArrayExtra(SOURCES_ID);
            int delay = intent.getIntExtra(DELAY, 0);
            startSync(syncMode, sourcesId, delay);
        } else if (PROGRAM_SCHEDULED_SYNC.equals(operation)) {
            programScheduledSync();
        } else if (CANCEL_SCHEDULED_SYNC.equals(operation)) {
            cancelScheduledSync();
        } else if (START_MONITORING_DIRECTORY.equals(operation)) {
            int sourceId = intent.getIntExtra(SOURCE_ID, -1);
            String dir = intent.getStringExtra(DIRECTORY);
            String extensions[] = intent.getStringArrayExtra(EXTENSIONS);
            startMonitoringDirectory(dir, sourceId, extensions);
        } else if (START_MONITORING_DIRECTORY.equals(operation)) {
            int sourceId = intent.getIntExtra(SOURCE_ID, -1);
            String dir = intent.getStringExtra(DIRECTORY);
            String extensions[] = intent.getStringArrayExtra(EXTENSIONS);
            startMonitoringDirectory(dir, sourceId, extensions);
        } else if (RESTART_MONITORING_ALL_DIRECTORIS.equals(operation)) {
            restartMonitoringAllDirectories();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        startForeground(Notification.FLAG_FOREGROUND_SERVICE,
                new Notification(0, null, System.currentTimeMillis()));
        return new AutoSyncBinder();
    }
    
    private class PendingOperationTask implements Task {
        
        private int id;
        private String operation;
        private Intent intent;
        
        public PendingOperationTask(String operation, Intent intent) {
            this.id = taskId++;
            this.operation = operation;
            this.intent = intent;
        }
        
        public void run() {
            performRequiredOperation(operation, intent);
        }

        public String getId() {
            return "PendingOperationTask" + id;
        }

        public long shallBeRescheduled(int attempt, Throwable t) {
            return -1;
        }
    }
    
    private class AppInitializerTask implements Task {
        
        public AppInitializerTask() {
        }
        
        public void run() {
            initStarted = true;
            initializer = AppInitializer.i(getApplicationContext());
            configuration = initializer.getConfiguration();
            customization = initializer.getCustomization();
            appSyncSourceManager = initializer.getAppSyncSourceManager();
            initCompleted = true;
        }

        public String getId() {
            return "AppInitializerTask";
        }

        public long shallBeRescheduled(int attempt, Throwable t) {
            return -1;
        }
    }  

    private void programScheduledSync() {
        // This period is expressed in minutes
        long period = configuration.getAutoSyncPeriod();
        if (period == 0) {
            period = customization.getAutoSyncPeriod();
        }
        period = period * 1000 * 60;
        
        // Check in the configuration if a form of auto synchronization is set
        if (Log.isLoggable(Log.DEBUG)) {
            Log.debug(TAG, "Programming repeating sync with interval: " + period);
        }

        Intent i = new Intent(AUTO_SYNC_ACTION);
        scheduledSyncIntent = PendingIntent.getBroadcast(getApplicationContext(),
                                                         TimerHandler.AUTO_SYNC_PENDING_INTENT_ID, i,
                                                         PendingIntent.FLAG_UPDATE_CURRENT);
        
        am = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + period, scheduledSyncIntent);
    }

    @Override
    public void onDestroy() {
        if (Log.isLoggable(Log.DEBUG)) {
            Log.debug(TAG, "Service Stopped");
        }
    }

    /**
     * Register the file system observer if the file sync source is currently
     * active and enabled.
     */
    private void startMonitoringDirectory(String baseDirectory, int sourceId, String extensions[]) {
        AndroidFileObserver fileObserver = null;
        if(!StringUtil.isNullOrEmpty(baseDirectory)) {
            fileObserver = fileObservers.get(baseDirectory);
            MediaAppSyncSource appSource =
                    (MediaAppSyncSource)appSyncSourceManager.getSource(sourceId);
            if (fileObserver == null) {
                fileObserver = new AndroidFileObserver(appSource, baseDirectory,
                        extensions, getApplicationContext(), configuration);
                if(isValidDirectory(baseDirectory)) {
                    if(Log.isLoggable(Log.DEBUG)) {
                        Log.debug(TAG, "Start watching directory: " + baseDirectory);
                    }
                    fileObserver.startWatching();
                } else {
                    if(Log.isLoggable(Log.DEBUG)) {
                        Log.debug(TAG, "Start watching failed: "
                                + "Cannot find directory: " + baseDirectory);
                    }
                }
                fileObservers.put(baseDirectory, fileObserver);
            } else {
                fileObserver.add(appSource, extensions);
            }
        } else {
            Log.error(TAG, "File source directory is empty");
        }
    }

    private void restartMonitoringAllDirectories() {
        for (Entry<String, AndroidFileObserver> entry : fileObservers.entrySet()) {
            String directory = entry.getKey();
            if(isValidDirectory(directory)) {
                if(Log.isLoggable(Log.DEBUG)) {
                    Log.debug(TAG, "Restart watching directory: " + directory);
                }
                AndroidFileObserver observer = entry.getValue();
                observer.stopWatching();
                observer.startWatching();
            } else {
                if(Log.isLoggable(Log.DEBUG)) {
                    Log.debug(TAG, "Restart watching failed: "
                            + "Cannot find directory: " + directory);
                }
            }
        }
    }

    private boolean isValidDirectory(String baseDirectory) {
        File dir = new File(baseDirectory);
        return dir.exists() && dir.isDirectory();
    }
    
    /**
     * LocalBinder used by activities that wish to be notified of the sync events.
     */
    public class AutoSyncBinder extends Binder {

        /*
        public void startMonitoringUri(String u, int sourceId) {
            AutoSyncService.this.startMonitoringUri(u, sourceId);
        }

        public void programScheduledSync() {
            AutoSyncService.this.programScheduledSync();
        }

        public void cancelScheduledSync() {
            AutoSyncService.this.cancelScheduledSync();
        }
        */
    }

    private void startMonitoringUri(String u, int sourceId) {
        AndroidAppSyncSource appSource = (AndroidAppSyncSource)appSyncSourceManager.getSource(sourceId);
        Uri uri = Uri.parse(u);

        ContentResolver contentResolver = getApplicationContext().getContentResolver();
        AppSyncSource s = AutoSyncService.this.monitoredUris.get(uri);
        if (s == null) {
            if (Log.isLoggable(Log.INFO)) {
                Log.info(TAG, "Start monitoring uri " + uri);
            }
            monitoredUris.put(uri, appSource);
            contentResolver.registerContentObserver(uri, true, new AndroidContentObserver(appSource, uri));
        }
    }

    private void cancelScheduledSync() {
        if (Log.isLoggable(Log.TRACE)) {
            Log.trace(TAG, "cancelScheduledSync");
        }
        if (am != null) {
            am.cancel(scheduledSyncIntent);
        }
    }

    private void startSync(String syncMode, int sourcesId[], int delay) {
        // We refresh the variables here, at the beginning of a sync because
        // with automatic tests we re-initialize the app initializer during
        // closeManiApp/startMainApp. For this reason we refresh the values
        // here.
        initializer = AppInitializer.i(getApplicationContext());

        appSyncSourceManager = initializer.getAppSyncSourceManager();
        configuration = initializer.getConfiguration();
        Controller controller = initializer.getController();

        RefreshTrigger trigger = controller.getRefreshTrigger();
        Vector sources = new Vector();
        for(int i=0;i<sourcesId.length;++i) {
            AndroidAppSyncSource appSource = (AndroidAppSyncSource)appSyncSourceManager.getSource(sourcesId[i]);
            sources.addElement(appSource);
        }
        if(delay > 0) {
            trigger.start(syncMode, sources, delay);
        } else {
            trigger.start(syncMode, sources);
        }
        
        // We schedule a timer to make sure a new sync is triggered at some point if the user is on auto sync
        if (configuration.getSyncMode() == Configuration.SYNC_MODE_AUTO) {
            cancelScheduledSync();
            programScheduledSync();
        }
    }

    /**
     * Implements a <code>ContentObserver</code> in order to keep track of
     * changes done on specific sources.
     */
    private class AndroidContentObserver extends ContentObserver {

        private static final String TAG_LOG = "AndroidContentObserver";

        private AndroidAppSyncSource appSource;
        private Uri uri;
        private NotifiableTracker notifiableTracker = null;

        public AndroidContentObserver(AndroidAppSyncSource appSource, Uri uri) {
            super(null);
            this.appSource = appSource;
            this.uri = uri;
            
            // If this source supports notifications for tracking changes, then we enable it
            SyncSource ss = appSource.getSyncSource();
            if (ss instanceof TrackableSyncSource) {
                TrackableSyncSource tss = (TrackableSyncSource)ss;
                ChangesTracker tracker = tss.getTracker();
                if (tracker instanceof NotifiableTracker) {
                    notifiableTracker = (NotifiableTracker)tracker;
                    notifiableTracker.enableNotifications();
                }
            }
            
        }

        @Override
        public void onChange(boolean selfChange) {
            if (Log.isLoggable(Log.TRACE)) {
                Log.trace(TAG_LOG, "Detected change for uri: " + uri + " self: " + selfChange);
            }
           
            // Ignore self changes
            if (selfChange) {
                return;
            }
            
            // If the user is currently not logged in, we ignore all changes
            Controller controller = initializer.getController();
            Configuration configuration = controller.getConfiguration();
            if (configuration.getCredentialsCheckPending()) {
                if (Log.isLoggable(Log.TRACE)) {
                    Log.trace(TAG_LOG, "Ignoring change because user is not logged in");
                }
                return;
            }
           
            // Does this change belong to us?
            boolean checked = false;
            boolean hasChanges = true;
            SyncSource ss = appSource.getSyncSource();
            if (ss instanceof TrackableSyncSource) {
                ChangesTracker tracker = ((TrackableSyncSource)ss).getTracker();
                // Note: we cannot use the standard hasChanges method here if the Tracker relies on us.
                // We must use a deep check
                if (notifiableTracker != null) {
                    hasChanges = notifiableTracker.hasChanges(true);
                } else {
                    hasChanges = tracker.hasChanges();
                }
                checked = true;
            }
            if (!checked) {
                if (Log.isLoggable(Log.INFO)) {
                    Log.info(TAG_LOG, "Cannot check if change is on our account, schedule a sync");
                }
                hasChanges = true;
            }
            if (Log.isLoggable(Log.INFO)) {
                Log.info(TAG_LOG, "Found changes for this source " + hasChanges);
            }
            if (notifiableTracker != null && hasChanges) {
                if (Log.isLoggable(Log.TRACE)) {
                    Log.trace(TAG_LOG, "Notifying tracker about this change");
                }
                notifiableTracker.changed();
            }
            
            // Generate a notification on the bus to signal this change
            PimChangedMessage pimChangedMessage = new PimChangedMessage(appSource);
            BusService.sendMessage(pimChangedMessage);
            
            if(configuration.getSyncMode() == Configuration.SYNC_MODE_AUTO && hasChanges) {
                if(Log.isLoggable(Log.TRACE)) {
                    Log.trace(TAG_LOG, "C2S push is enabled, programming a sync");
                }
            
                Vector sources = new Vector();
                sources.addElement(appSource);
                int delay = customization.getC2SPushDelay();
                AutoSyncServiceHandler autoSyncHandler = new AutoSyncServiceHandler(getApplicationContext());
                autoSyncHandler.startSync(RefreshTrigger.PUSH, sources, delay);
            }
        }
    }
}
