/**
 * 
 */
package com.funambol.android;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 */
public class MimeTypeMap {
    
    private static MimeTypeMap singleton;
    private android.webkit.MimeTypeMap nativeMap;
    private static List<String> alwaysSupported = new ArrayList<String>();
    static {
        alwaysSupported.add("audio/mpeg");
    }
    
    private MimeTypeMap() {
        this.nativeMap = android.webkit.MimeTypeMap.getSingleton();
        MimeTypeMap.singleton = this;
    }
    
    public static MimeTypeMap getSingleton() {
        if (singleton == null) {
            new MimeTypeMap();
        }
        return singleton;
    }

    /** We cannot use MimeTypeMap.getFileExtensionFromUrl because the API is buggy.
     * 
     * @param url
     * @return the file extension without dot, or empty string
     */
    public static String getFileExtensionFromUrl(String url) {

        
    	int firstQMark = url.indexOf("%3F"); // URL-encoded question mark
    	if (firstQMark == -1) {
    		firstQMark = url.indexOf("?"); // plain-text question mark
    		if (firstQMark == -1) {
    			firstQMark = url.length();
    		}
    	}
        int lastDot = url.substring(0, firstQMark).lastIndexOf(".");
        String extension;
        if ((lastDot != -1) && (lastDot + 1 < firstQMark)) {
            extension = url.substring(lastDot + 1, firstQMark);
        } else {
            extension = "";
        }
        return extension;
    }
    
    public String getMimeTypeFromExtension(String extension) {
        return nativeMap.getMimeTypeFromExtension(extension.toLowerCase(Locale.ENGLISH));
    }
    
    public String getExtensionFromMimeType(String mimeType) {
        return nativeMap.getExtensionFromMimeType(mimeType);
    }
    
    public boolean hasExtension(String extension) {
        return nativeMap.hasExtension(extension);
    }
    
    public boolean hasMimeType(String mimeType) {
        return nativeMap.hasMimeType(mimeType);
    }
    
    public static boolean isAlwaysSupportedMimeType(String mimeType) {
        return alwaysSupported.contains(mimeType);
    }
    
}
