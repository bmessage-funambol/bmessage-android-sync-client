/**
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2011 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Vector;

import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;

import com.funambol.android.activities.FilesChronologicalFragment;
import com.funambol.android.activities.FilesSelectiveUploadFragment;
import com.funambol.android.activities.PicturesChronologicalFragment;
import com.funambol.android.activities.PicturesSelectiveUploadFragment;
import com.funambol.android.activities.VideosChronologicalFragment;
import com.funambol.android.activities.VideosSelectiveUploadFragment;
import com.funambol.android.activities.settings.AndroidDevSettingsUISyncSource;
import com.funambol.android.activities.view.AndroidSourceChronologicalViewItem;
import com.funambol.android.activities.view.AndroidSourceNoThumbnailsView;
import com.funambol.android.activities.view.AndroidContactsThumbnailsView;
import com.funambol.android.activities.view.AndroidSourceThumbnailsView;
import com.funambol.android.controller.AndroidOpenFileScreenController;
import com.funambol.android.controller.AndroidOpenVideoScreenController;
import com.funambol.android.services.AutoSyncServiceHandler;
import com.funambol.android.source.AbstractDataManager;
import com.funambol.android.source.media.MediaAppSyncSource;
import com.funambol.android.source.media.MediaDirectoryScanner;
import com.funambol.android.source.media.MediaExternalAppManager;
import com.funambol.android.source.media.MediaTracker;
import com.funambol.android.source.media.file.FileAppSyncSource;
import com.funambol.android.source.media.file.FileAppSyncSourceConfig;
import com.funambol.android.source.media.file.FileNavigationBarButtonView;
import com.funambol.android.source.media.file.FileSettingsUISyncSource;
import com.funambol.android.source.media.file.FileThumbnailView;
import com.funambol.android.source.media.picture.PictureAppSyncSource;
import com.funambol.android.source.media.picture.PictureAppSyncSourceConfig;
import com.funambol.android.source.media.picture.PictureNavigationBarButtonView;
import com.funambol.android.source.media.picture.PictureSettingsUISyncSource;
import com.funambol.android.source.media.picture.PictureThumbnailView;
import com.funambol.android.source.media.video.VideoAppSyncSource;
import com.funambol.android.source.media.video.VideoAppSyncSourceConfig;
import com.funambol.android.source.media.video.VideoNavigationBarButtonView;
import com.funambol.android.source.media.video.VideoSettingsUISyncSource;
import com.funambol.android.source.media.video.VideoThumbnailView;
import com.funambol.android.source.pim.calendar.CalendarAppSyncSource;
import com.funambol.android.source.pim.calendar.CalendarAppSyncSourceConfig;
import com.funambol.android.source.pim.calendar.CalendarChangesTracker;
import com.funambol.android.source.pim.calendar.CalendarChangesTrackerMD5;
import com.funambol.android.source.pim.calendar.CalendarExternalAppManager;
import com.funambol.android.source.pim.calendar.CalendarManager;
import com.funambol.android.source.pim.calendar.CalendarSettingsUISyncSource;
import com.funambol.android.source.pim.calendar.EventSyncSource;
import com.funambol.android.source.pim.contact.ContactAppSyncSource;
import com.funambol.android.source.pim.contact.ContactAppSyncSourceConfig;
import com.funambol.android.source.pim.contact.ContactExternalAppManager;
import com.funambol.android.source.pim.contact.ContactManager;
import com.funambol.android.source.pim.contact.ContactSettingsUISyncSource;
import com.funambol.android.source.pim.contact.ContactSyncSource;
import com.funambol.android.source.pim.contact.DirtyChangesTracker;
import com.funambol.android.source.pim.contact.FunambolContactManager;
import com.funambol.androidsync.R;
import com.funambol.client.controller.Controller;
import com.funambol.client.controller.OpenPictureScreenController;
import com.funambol.client.controller.OpenItemScreenController;
import com.funambol.client.controller.SourceNoThumbnailsViewController;
import com.funambol.client.customization.Customization;
import com.funambol.client.engine.FunambolSourceMonitor;
import com.funambol.client.engine.SyncTask;
import com.funambol.client.localization.Localization;
import com.funambol.client.source.AppSyncSource;
import com.funambol.client.source.AppSyncSourceManager;
import com.funambol.client.source.FunambolFileSyncSource;
import com.funambol.client.source.FunambolMediaSyncSource;
import com.funambol.client.source.FunambolPictureSyncSource;
import com.funambol.client.source.FunambolVideoSyncSource;
import com.funambol.platform.DeviceInfo;
import com.funambol.platform.DeviceInfoInterface;
import com.funambol.platform.FileAdapter;
import com.funambol.sapisync.SapiSyncAnchor;
import com.funambol.storage.Table;
import com.funambol.sync.SourceConfig;
import com.funambol.sync.SyncSource;
import com.funambol.sync.client.ChangesTracker;
import com.funambol.syncml.protocol.CTCap;
import com.funambol.syncml.protocol.CTInfo;
import com.funambol.syncml.protocol.DataStore;
import com.funambol.syncml.protocol.SourceRef;
import com.funambol.syncml.protocol.SyncCap;
import com.funambol.syncml.protocol.SyncType;
import com.funambol.syncml.spds.SyncMLAnchor;
import com.funambol.syncml.spds.SyncMLSourceConfig;
import com.funambol.util.Log;
import com.funambol.util.bus.BusService;

/**
 * A manager for the AndroidAppSyncSource instances in use by the Android client.
 */
public class AndroidAppSyncSourceManager extends AppSyncSourceManager {

    private static final String TAG_LOG = "AndroidAppSyncSourceManager";

    private Controller controller;
    private Localization localization;
    private Context context;
    private DeviceInfoInterface deviceInfo;

    private AutoSyncServiceHandler autoSyncService;


    /**
     * Constructor
     * 
     * @param customization the Customization object to be referred to
     * @param localization the Localization pattern for this manager
     * @param appContext the Application Context object to be related to the manager
     */
    public AndroidAppSyncSourceManager(
            Context appContext,
            Customization customization,
            Localization localization) {
        super(customization);
        this.localization = localization;
        this.context = appContext;
        this.deviceInfo = new DeviceInfo(appContext);
        this.autoSyncService = new AutoSyncServiceHandler(appContext);
    }

    public void setController(Controller controller) {
        this.controller = controller;
    }

    /**
     * Setup the SyncSource identified by its sourceId index
     * @param sourceId the int that identifies the source
     * @param configuration the AndroidConfiguration object used to setup the
     * source
     * @return AppSyncSource the instance of the setup AppSyncSource
     * @throws Exception
     */
    public AppSyncSource setupSource(int sourceId, AndroidConfiguration configuration) throws Exception {
        if (Log.isLoggable(Log.INFO)) {
            Log.info(TAG_LOG, "Setting up source: " + sourceId);
        }
        AppSyncSource appSource;
        switch(sourceId) {
            case CONTACTS_ID:
            {
                appSource = setupContactsSource(configuration);
                break;
            }
            case EVENTS_ID:
            {
                appSource = setupEventsSource(configuration);
                break;
            }
            case PICTURES_ID:
            {
                appSource = setupPicturesSource(configuration);
                break;
            }
            case VIDEOS_ID:
            {
                appSource = setupVideosSource(configuration);
                break;
            }
            case FILES_ID:
            {
                appSource = setupFilesSource(configuration);
                break;
            }
            default:
                throw new IllegalArgumentException("Unknown source: " + sourceId);
        }
        return appSource;
    }

    /**
     * Setup the source for contacts
     * @param configuration the AndroidConfiguration to be used to setup the
     * source
     * @return AppSyncSource related to contacts
     * @throws Exception
     */
    protected AppSyncSource setupContactsSource(AndroidConfiguration configuration) throws Exception {

        int id = CONTACTS_ID;
        String name = localization.getLanguage("type_contacts");

        ContactAppSyncSource appSyncSource = new ContactAppSyncSource(context, name);
        appSyncSource.setId(id);
        appSyncSource.setHasContentView(false);
        appSyncSource.setSapiRemoteUri("contacts");
        appSyncSource.setSapiDataType("contact");
        appSyncSource.setSupportsSelectiveUpload(false);
        appSyncSource.setMultiSelectSupported(false);

        setItemInflexions(appSyncSource, localization, "item_inflexion_contacts");
        appSyncSource.setEmptyHint(localization.getLanguage("contacts_placeholder_empty_view"));
        
        appSyncSource.setProviderUri(ContactsContract.Contacts.CONTENT_URI);

        // Create the proper settings component for this source
        appSyncSource.setSettingsUIClass(ContactSettingsUISyncSource.class);
        // Create the dev settings for this source
        appSyncSource.setDevSettingsUIClass(AndroidDevSettingsUISyncSource.class);
        appSyncSource.setSourceBarViewControllerClass(SourceNoThumbnailsViewController.class);
        appSyncSource.setSourceBarViewClass(AndroidContactsThumbnailsView.class);

        int order = getSourcePosition(id);
        appSyncSource.setUiSourceIndex(order);
        appSyncSource.setBandwidthSaverUse(customization.useBandwidthSaverContacts());

        // Create the contact manager
        ContactManager cm = new FunambolContactManager(context);

        SourceConfig sc = null;
        if (SourceConfig.VCARD_TYPE.equals(customization.getContactType())) {
            // This is vcard format
            String defaultUri = customization.getDefaultSourceUri(id);
            sc = new SyncMLSourceConfig("contacts", SourceConfig.VCARD_TYPE, defaultUri, 
                                        createDataStore("contacts", SourceConfig.VCARD_TYPE, "2.1", cm));
            sc.setEncoding(SyncSource.ENCODING_NONE);
            sc.setSyncMode(customization.getDefaultSourceSyncMode(id, deviceInfo.getDeviceRole()));
            // Set this item anchor
            SyncMLAnchor anchor = new SyncMLAnchor();
            sc.setSyncAnchor(anchor);
        }

        if (sc != null) {
            // Load the source config from the configuration
            ContactAppSyncSourceConfig asc = new ContactAppSyncSourceConfig(
                    appSyncSource, customization, configuration);
            asc.load(sc);
            appSyncSource.setConfig(asc);

            if(!asc.getUseDirtyChangesTracker()) {
                // Migrate tracker store only if the user has already synced
                SyncMLAnchor anchor = (SyncMLAnchor)sc.getSyncAnchor();
                if(anchor.getLast() != 0) {
                    try {
                        ContactSyncSource.migrateToDirtyChangesTracker(sc, cm, context);
                    } catch(Throwable t) {
                        Log.error(TAG_LOG, "Failed to migrate changes tracker store", t);
                    }
                }
                asc.setUseDirtyChangesTracker(true);
                asc.save();
            }

            ChangesTracker tracker = new DirtyChangesTracker(context, cm);

            ContactSyncSource src = new ContactSyncSource(sc, tracker, context,
                    configuration, appSyncSource, cm);
            appSyncSource.setSyncSource(src);

            // Setup the external app manager
            ContactExternalAppManager appManager =
                    new ContactExternalAppManager(context, appSyncSource);
            appSyncSource.setAppManager(appManager);

            // Inform the auto sync service that we shall monitor contacts for
            // changes
            autoSyncService.startMonitoringUri(appSyncSource.getProviderUri().toString(), appSyncSource.getId());
            
            // Create the open item screen controller
            OpenItemScreenController openItemScreenController;
            openItemScreenController = appSyncSource.createOpenItemScreenController(controller);
            appSyncSource.setOpenItemScreenController(openItemScreenController);

            // Register a listener to monitor the sync status for this source
            FunambolSourceMonitor monitor = new FunambolSourceMonitor(controller, appSyncSource);
            monitor.setGenerateEventForIncomingMetadata(true);
            appSyncSource.setMonitor(monitor);
            BusService.registerMessageHandler(com.funambol.client.engine.SyncTaskMessage.class, monitor);
            BusService.registerMessageHandler(com.funambol.client.engine.DownloadTaskMessage.class, monitor);
            BusService.registerMessageHandler(com.funambol.client.engine.UploadTaskMessage.class, monitor);

            src.setListener(monitor);
        } else {
            Log.error(TAG_LOG, "The contact sync source does not support the type: " +
                    customization.getContactType());
            Log.error(TAG_LOG, "Contact source will be disabled as not working");
        }
        return appSyncSource;
    }

    /**
     * Setup the source for events
     * @param configuration the AndroidConfiguration to be used to setup the
     * source
     * @return AppSyncSource related to events
     * @throws Exception
     */
    protected AppSyncSource setupEventsSource(AndroidConfiguration configuration) throws Exception {

        int id = EVENTS_ID;
        String name = localization.getLanguage("type_calendar");

        CalendarAppSyncSource appSyncSource = new CalendarAppSyncSource(context, name);
        appSyncSource.setId(id);
        appSyncSource.setHasContentView(false);
        appSyncSource.setSapiRemoteUri("calendar");
        appSyncSource.setSapiDataType("calendar");
        appSyncSource.setSupportsSelectiveUpload(false);
        appSyncSource.setMultiSelectSupported(false);

        setItemInflexions(appSyncSource, localization, "item_inflexion_calendar");
        appSyncSource.setEmptyHint(localization.getLanguage("events_placeholder_empty_view"));

        appSyncSource.setProviderUri(CalendarManager.Events.CONTENT_URI);

        // Create the proper settings component for this source
        appSyncSource.setSettingsUIClass(CalendarSettingsUISyncSource.class);
        // Create the dev settings for this source
        appSyncSource.setDevSettingsUIClass(AndroidDevSettingsUISyncSource.class);
        appSyncSource.setSourceBarViewControllerClass(SourceNoThumbnailsViewController.class);
        appSyncSource.setSourceBarViewClass(AndroidSourceNoThumbnailsView.class);

        int order = getSourcePosition(id);
        appSyncSource.setUiSourceIndex(order);

        CalendarManager dm = new CalendarManager(context, appSyncSource);
        SourceConfig sc = null;
        String defaultUri = customization.getDefaultSourceUri(id);
        sc = new SyncMLSourceConfig("calendar", customization.getCalendarType(), defaultUri,
                                    createDataStore("calendar", customization.getCalendarType(), "1.0", dm));
        sc.setEncoding(SyncSource.ENCODING_NONE);
        sc.setSyncMode(customization.getDefaultSourceSyncMode(id, deviceInfo.getDeviceRole()));

        // Set this item anchor
        SyncMLAnchor anchor = new SyncMLAnchor();
        sc.setSyncAnchor(anchor);

        CalendarAppSyncSourceConfig asc = new CalendarAppSyncSourceConfig(
                appSyncSource, customization, configuration);
        asc.load(sc);

        appSyncSource.setConfig(asc);

        appSyncSource.setBandwidthSaverUse(customization.useBandwidthSaverEvents());

        // Create the sync source
        IntKeyValueSQLiteStore trackerStore =
            new IntKeyValueSQLiteStore(context, 
            ((AndroidCustomization)customization).getFunambolSQLiteDbName(),
            sc.getName());

        // There are two different trackers depending on the OS version, because
        // OS < 2.2 does not allow the sync_dirty to be handled properly
        CalendarChangesTracker tracker;
        if (AndroidUtils.isSimulator(context) || Build.VERSION.SDK_INT >= 8) {
            tracker = new CalendarChangesTracker(context, trackerStore, asc);
        } else {
            tracker = new CalendarChangesTrackerMD5(context, trackerStore, asc);
        }

        EventSyncSource src = new EventSyncSource(sc, tracker, context, configuration, appSyncSource, dm);
        appSyncSource.setSyncSource(src);

        // Setup the external app manager
        CalendarExternalAppManager appManager = new CalendarExternalAppManager(context, appSyncSource);
        appSyncSource.setAppManager(appManager);

        // If the user is already logged in and there is no valid calendar to
        // sync, then we better recreate it here
        if (!configuration.getCredentialsCheckPending()) {
            boolean recreateCalendar = false;
            // If the calendar id is undefined, or no longer exists, then we
            // recreate the funambol calendar
            long calendarId = asc.getCalendarId();
            if (calendarId == -1) {
                recreateCalendar = true;
            } else {
                if (!dm.exists(""+calendarId)) {
                    recreateCalendar = true;
                }
            }

            if (recreateCalendar) {
                Log.info(TAG_LOG, "Creating account calendar because it does not exist");
                try {
                    calendarId = appSyncSource.createCalendar();
                    Log.info(TAG_LOG, "Calendar created with id " + calendarId);
                    asc.setCalendarId(calendarId);
                    asc.save();
                } catch (Exception e) {
                    Log.error(TAG_LOG, "Cannot create account calendar", e);
                }
            }
        }

        // Inform the auto sync service that we shall monitor contacts for
        // changes
        autoSyncService.startMonitoringUri(CalendarManager.Events.CONTENT_URI.toString(), appSyncSource.getId());
        // Add the reminder table URI to the list of monitored uris
        autoSyncService.startMonitoringUri(CalendarManager.Reminders.CONTENT_URI.toString(), appSyncSource.getId());
        
        // Create the open item screen controller
        OpenItemScreenController openItemScreenController;
        openItemScreenController = appSyncSource.createOpenItemScreenController(controller);
        appSyncSource.setOpenItemScreenController(openItemScreenController);

        // Register a listener to monitor the sync status for this source
        FunambolSourceMonitor monitor = new FunambolSourceMonitor(controller, appSyncSource);
        monitor.setGenerateEventForIncomingMetadata(true);
        appSyncSource.setMonitor(monitor);
        BusService.registerMessageHandler(com.funambol.client.engine.SyncTaskMessage.class, monitor);
        BusService.registerMessageHandler(com.funambol.client.engine.DownloadTaskMessage.class, monitor);
        BusService.registerMessageHandler(com.funambol.client.engine.UploadTaskMessage.class, monitor);

        src.setListener(monitor);

        return appSyncSource;
    }

    /**
     * Setup the source for pictures
     * @param configuration the AndroidConfiguration to be used to setup the
     * source
     * @return AppSyncSource related to pictures
     * @throws Exception
     */
    protected AppSyncSource setupPicturesSource(AndroidConfiguration configuration) throws Exception {

        int id = PICTURES_ID;
        String name = localization.getLanguage("type_photos");
        // These are the supported extensions
        String extensions[] = { "jpg", "jpeg", "gif", "png" };

        PictureAppSyncSource appSyncSource = new PictureAppSyncSource(name, customization);
        appSyncSource.setId(id);
        appSyncSource.setContext(context);
        appSyncSource.setHasContentView(true);
        appSyncSource.setSapiRemoteUri("media/picture");
        appSyncSource.setSapiDataType("picture");
        appSyncSource.setWaitForFirstScanToComplete(false);
        appSyncSource.setSupportsSelectiveUpload(true);
        appSyncSource.setNumberOfItemsToImportOnVeryFirstSync(5);
        appSyncSource.setMultiSelectSupported(true);
       
        setItemInflexions(appSyncSource, localization, "item_inflexion_photos");
        appSyncSource.setSaveOptionLabel(localization.getLanguage("share_option_save_to_gallery"));
        appSyncSource.setEmptyHint(localization.getLanguage("pictures_placeholder_empty_view"));
        appSyncSource.setSavingItemsUserMessage(localization.getLanguage("share_option_save_to_gallery_user_message"));
        appSyncSource.setSaveItemsCompletedUserMessage(localization.getLanguage("share_option_save_to_gallery_completed_user_message"));
        appSyncSource.setSaveItemsFailedUserMessage(localization.getLanguage("share_option_save_to_gallery_failed_user_message"));
        
        appSyncSource.setIsRefreshSupported(SyncTask.REFRESH_FROM_SERVER, false);
        appSyncSource.setIsRefreshSupported(SyncTask.REFRESH_TO_SERVER, false);
        appSyncSource.setUiSourceIndex(getSourcePosition(id));
        appSyncSource.setProviderUri(MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        appSyncSource.setThumbnailsProviderUri(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI);
        appSyncSource.setThumbnailsProviderItemIdColumn(MediaStore.Images.Thumbnails.IMAGE_ID);
        appSyncSource.setThumbnailsProviderDataColumn(MediaStore.Images.Thumbnails.DATA);

        appSyncSource.setBandwidthSaverUse(customization.useBandwidthSaverMedia());

        // Create the proper settings component for this source
        appSyncSource.setSettingsUIClass(PictureSettingsUISyncSource.class);
        // No dev settings for this source
        appSyncSource.setDevSettingsUIClass(null);
        appSyncSource.setSourceBarViewClass(AndroidSourceThumbnailsView.class);
        appSyncSource.setThumbnailViewClass(PictureThumbnailView.class);
        appSyncSource.setOpenItemScreenControllerClass(OpenPictureScreenController.class);
        appSyncSource.setNavigationBarButtonViewClass(PictureNavigationBarButtonView.class);
        appSyncSource.setSourceChronologicalViewItemClass(AndroidSourceChronologicalViewItem.class);
        appSyncSource.setChronologicalWidgetClass(PicturesChronologicalFragment.class);
        appSyncSource.setSelectiveUploadWidgetClass(PicturesSelectiveUploadFragment.class);
       
        // Note: the actual size depends on the screen size ad the following dimensions are defined in different
        // ways according to screen size (see res/values-*)
        appSyncSource.setThumbnailProperties(
                (int) context.getResources().getDimension(R.dimen.common_thumbnail_max_width),
                (int) context.getResources().getDimension(R.dimen.common_thumbnail_max_height),
                (int) context.getResources().getDimension(R.dimen.common_thumbnail_min_width),
                (int) context.getResources().getDimension(R.dimen.common_thumbnail_min_height),
                false /* Dimensions returned by getDimension are already converted to pixels */
        );
        appSyncSource.setPreviewProperties(
                (int) context.getResources().getDimension(R.dimen.common_preview_max_width),
                (int) context.getResources().getDimension(R.dimen.common_preview_max_height),
                (int) context.getResources().getDimension(R.dimen.common_preview_min_width),
                (int) context.getResources().getDimension(R.dimen.common_preview_min_height),
                false /* Dimensions returned by getDimension are already converted to pixels */
        );
 
        MediaDirectoryScanner mediaScanner = new MediaDirectoryScanner(
                appSyncSource, configuration, context);
        appSyncSource.setMediaDirectoryScanner(mediaScanner);
        
        appSyncSource.setPlayable(Boolean.FALSE); // pictures are not "playable"

        SourceConfig sc = null;
        String defaultUri = customization.getDefaultSourceUri(id);
        sc = new SourceConfig("pictures", SourceConfig.FILE_OBJECT_TYPE, defaultUri);
        sc.setEncoding(SyncSource.ENCODING_NONE);
        sc.setSyncMode(customization.getDefaultSourceSyncMode(id, deviceInfo.getDeviceRole()));

        // Set the sync anchor to sync via media engine
        SapiSyncAnchor anchor = new SapiSyncAnchor();
        sc.setSyncAnchor(anchor);

        PictureAppSyncSourceConfig asc = new PictureAppSyncSourceConfig(
                context, appSyncSource, customization, configuration);
        asc.load(sc);
        asc.setMaxItemSize(FunambolFileSyncSource.NO_LIMIT_ON_ITEM_SIZE);
        appSyncSource.setConfig(asc);

        // Compose gallery directory
        String sdCardRoot = Environment.getExternalStorageDirectory().toString();
        StringBuffer galleryDirectory = new StringBuffer();
        galleryDirectory.append(sdCardRoot);
        galleryDirectory.append("/DCIM");
        String galleryBaseDirectory = galleryDirectory.toString();
        galleryDirectory.append("/");
        galleryDirectory.append(asc.getBucketId());

        MediaTracker tracker = new MediaTracker(appSyncSource.getMetadataTable());
        FunambolPictureSyncSource syncSource = new FunambolPictureSyncSource(sc, tracker,
                appSyncSource.getDataDirectory(),
                appSyncSource.getTempDirectory(), galleryDirectory.toString(), galleryBaseDirectory,
                appSyncSource.getConfig().getMaxItemSize(),
                controller, customization,
                configuration, appSyncSource.getMetadataTable(), 
                AppInitializer.i(context).getNetworkTaskExecutor());
        syncSource.setSupportedExtensions(extensions);
        syncSource.setDataTag("pictures");

        appSyncSource.setSyncSource(syncSource);

        // Setup the external app manager
        MediaExternalAppManager appManager = new MediaExternalAppManager(context, appSyncSource);
        appSyncSource.setAppManager(appManager);

        // Start monitoring the gallery directory
        startMonitoringDCIMDirectory(appSyncSource, extensions);

        // Make sure the metadata table is created during the initialization
        Table metadata = appSyncSource.getMetadataTable();
        if(metadata != null) {
            metadata.open();
            metadata.close();
        }
        
        asc.sourceSetupCompleted();
        
        // Create the open item screen controller
        OpenItemScreenController openItemScreenController;
        openItemScreenController = appSyncSource.createOpenItemScreenController(controller);
        appSyncSource.setOpenItemScreenController(openItemScreenController);

        // Register a listener to monitor the sync status for this source
        FunambolSourceMonitor monitor = new FunambolSourceMonitor(controller, appSyncSource);
        appSyncSource.setMonitor(monitor);
        BusService.registerMessageHandler(com.funambol.client.engine.SyncTaskMessage.class, monitor);
        BusService.registerMessageHandler(com.funambol.client.engine.DownloadTaskMessage.class, monitor);
        BusService.registerMessageHandler(com.funambol.client.engine.UploadTaskMessage.class, monitor);

        syncSource.setListener(monitor);
        
        return appSyncSource;
    }

    /**
     * Setup the source for videos
     * @param configuration the AndroidConfiguration to be used to setup the
     * source
     * @return AppSyncSource related to pictures
     * @throws Exception
     */
    protected AppSyncSource setupVideosSource(AndroidConfiguration configuration) throws Exception {

        int id = VIDEOS_ID;
        String name = localization.getLanguage("type_videos");
        // These are the extensions supported by this source
        String extensions[] = {
        		
        		// Same as Windows Sync Client:
        		"wmv", 
        		"mp4", 
        		"mov", 
        		"3g2", 
        		"3gp", 
        		"mpeg", 
        		"mpg",
        		"asf",
        		"movie",
        		"avi",
        		"mpa",
        		"mp2",
        		"m4u",
        		"m4v",
        		"swf",
        		"flv",
        		
        		// Extra extension for QuickTime videos:
        		"qt"
        		};      

        VideoAppSyncSource appSyncSource = new VideoAppSyncSource(name, customization);
        appSyncSource.setId(id);
        appSyncSource.setContext(context);
        appSyncSource.setHasContentView(true);
        appSyncSource.setSapiRemoteUri("media/video");
        appSyncSource.setSapiDataType("video");
        appSyncSource.setWaitForFirstScanToComplete(false);
        appSyncSource.setSupportsSelectiveUpload(true);
        appSyncSource.setNumberOfItemsToImportOnVeryFirstSync(2);
        appSyncSource.setMultiSelectSupported(false);

        setItemInflexions(appSyncSource, localization, "item_inflexion_videos");
        appSyncSource.setSaveOptionLabel(localization.getLanguage("share_option_save_to_gallery"));        
        appSyncSource.setEmptyHint(localization.getLanguage("videos_placeholder_empty_view"));
        appSyncSource.setSavingItemsUserMessage(localization.getLanguage("share_option_save_to_gallery_user_message"));
        appSyncSource.setSaveItemsCompletedUserMessage(localization.getLanguage("share_option_save_to_gallery_completed_user_message"));
        appSyncSource.setSaveItemsFailedUserMessage(localization.getLanguage("share_option_save_to_gallery_failed_user_message"));
        
        appSyncSource.setIsRefreshSupported(SyncTask.REFRESH_FROM_SERVER, false);
        appSyncSource.setIsRefreshSupported(SyncTask.REFRESH_TO_SERVER, false);
        appSyncSource.setUiSourceIndex(getSourcePosition(id));
        appSyncSource.setProviderUri(MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        appSyncSource.setThumbnailsProviderUri(MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI);
        appSyncSource.setThumbnailsProviderItemIdColumn(MediaStore.Video.Thumbnails.VIDEO_ID);
        appSyncSource.setThumbnailsProviderDataColumn(MediaStore.Video.Thumbnails.DATA);

        appSyncSource.setBandwidthSaverUse(customization.useBandwidthSaverMedia());

        // Create the proper settings component for this source
        appSyncSource.setSettingsUIClass(VideoSettingsUISyncSource.class);
        // No dev settings for this source
        appSyncSource.setDevSettingsUIClass(null);
        appSyncSource.setSourceBarViewClass(AndroidSourceThumbnailsView.class);
        appSyncSource.setOpenItemScreenControllerClass(AndroidOpenVideoScreenController.class);
        appSyncSource.setThumbnailViewClass(VideoThumbnailView.class);
        appSyncSource.setNavigationBarButtonViewClass(VideoNavigationBarButtonView.class);
        appSyncSource.setSourceChronologicalViewItemClass(AndroidSourceChronologicalViewItem.class);
        appSyncSource.setChronologicalWidgetClass(VideosChronologicalFragment.class);
        appSyncSource.setSelectiveUploadWidgetClass(VideosSelectiveUploadFragment.class);
        appSyncSource.setThumbnailProperties(
                (int) context.getResources().getDimension(R.dimen.common_thumbnail_max_width),
                (int) context.getResources().getDimension(R.dimen.common_thumbnail_max_height),
                (int) context.getResources().getDimension(R.dimen.common_thumbnail_min_width),
                (int) context.getResources().getDimension(R.dimen.common_thumbnail_min_height),
                false /* Dimensions returned by getDimension are already converted to pixels */
        );
        appSyncSource.setPreviewProperties(
                (int) context.getResources().getDimension(R.dimen.common_preview_max_width),
                (int) context.getResources().getDimension(R.dimen.common_preview_max_height),
                (int) context.getResources().getDimension(R.dimen.common_preview_min_width),
                (int) context.getResources().getDimension(R.dimen.common_preview_min_height),
                false /* Dimensions returned by getDimension are already converted to pixels */
        );

        MediaDirectoryScanner mediaScanner = new MediaDirectoryScanner(
                appSyncSource, configuration, context);
        appSyncSource.setMediaDirectoryScanner(mediaScanner);

        SourceConfig sc = null;
        String defaultUri = customization.getDefaultSourceUri(id);
        sc = new SourceConfig("videos", SourceConfig.FILE_OBJECT_TYPE, defaultUri);
        sc.setEncoding(SyncSource.ENCODING_NONE);
        sc.setSyncMode(customization.getDefaultSourceSyncMode(id, deviceInfo.getDeviceRole()));

        // Set the sync anchor to sync via media engine
        SapiSyncAnchor anchor = new SapiSyncAnchor();
        sc.setSyncAnchor(anchor);

        VideoAppSyncSourceConfig asc = new VideoAppSyncSourceConfig(context,
                appSyncSource, customization, configuration);
        asc.load(sc);
        asc.setMaxItemSize(customization.getMaxAllowedFileSizeForVideos());
        appSyncSource.setConfig(asc);

        // Compose gallery directory
        String sdCardRoot = Environment.getExternalStorageDirectory().toString();
        StringBuffer galleryDirectory = new StringBuffer();
        galleryDirectory.append(sdCardRoot);
        galleryDirectory.append("/DCIM");
        String galleryBaseDirectory = galleryDirectory.toString();
        galleryDirectory.append("/").append(asc.getBucketId());

        MediaTracker tracker = new MediaTracker(appSyncSource.getMetadataTable());
        FunambolVideoSyncSource syncSource = new FunambolVideoSyncSource(sc, tracker,
                appSyncSource.getDataDirectory(), 
                appSyncSource.getTempDirectory(), galleryDirectory.toString(), galleryBaseDirectory,
                appSyncSource.getConfig().getMaxItemSize(),
                controller, customization,
                configuration, appSyncSource.getMetadataTable(), 
                AppInitializer.i(context).getNetworkTaskExecutor());
        syncSource.setSupportedExtensions(extensions);
        syncSource.setDataTag("videos");

        appSyncSource.setSyncSource(syncSource);
        
        appSyncSource.setPlayable(Boolean.TRUE); // all videos are playable by default
        // NB: Change the argument to null to have the behavior depend on the type of 
        // each individual file
        
        // Setup the external app manager
        MediaExternalAppManager appManager = new MediaExternalAppManager(context, appSyncSource);
        appSyncSource.setAppManager(appManager);

        // Start monitoring the gallery directory
        startMonitoringDCIMDirectory(appSyncSource, extensions);

        // Make sure the metadata table is created during the initialization
        Table metadata = appSyncSource.getMetadataTable();
        if(metadata != null) {
            metadata.open();
            metadata.close();
        }
        asc.sourceSetupCompleted();
        
        // Create the open item screen controller
        OpenItemScreenController openItemScreenController;
        openItemScreenController = appSyncSource.createOpenItemScreenController(controller);
        appSyncSource.setOpenItemScreenController(openItemScreenController);

        // Register a listener to monitor the sync status for this source
        FunambolSourceMonitor monitor = new FunambolSourceMonitor(controller, appSyncSource);
        appSyncSource.setMonitor(monitor);
        BusService.registerMessageHandler(com.funambol.client.engine.SyncTaskMessage.class, monitor);
        BusService.registerMessageHandler(com.funambol.client.engine.DownloadTaskMessage.class, monitor);
        BusService.registerMessageHandler(com.funambol.client.engine.UploadTaskMessage.class, monitor);

        syncSource.setListener(monitor);
        
        return appSyncSource;
    }

    /**
     * Setup the source for files
     * @param configuration the AndroidConfiguration to be used to setup the
     * source
     * @return AppSyncSource related to pictures
     * @throws Exception
     */
    protected AppSyncSource setupFilesSource(AndroidConfiguration configuration) throws Exception {

        int id = FILES_ID;
        String name = localization.getLanguage("type_files");

        MediaAppSyncSource appSyncSource = new FileAppSyncSource(name, customization);
        appSyncSource.setId(id);
        appSyncSource.setContext(context);
        appSyncSource.setHasContentView(true);
        appSyncSource.setSapiRemoteUri("media/file");
        appSyncSource.setSapiDataType("file");
        appSyncSource.setWaitForFirstScanToComplete(true);
        appSyncSource.setSupportsSelectiveUpload(false);
        // Until we have selective upload for files, we import everything into the user's digital life upon
        // installation
        appSyncSource.setNumberOfItemsToImportOnVeryFirstSync(Integer.MAX_VALUE);
        appSyncSource.setMultiSelectSupported(false);

        setItemInflexions(appSyncSource, localization, "item_inflexion_files");
        appSyncSource.setSaveOptionLabel(localization.getLanguage("share_option_save_to_device"));  
        appSyncSource.setEmptyHint(localization.getLanguage("files_placeholder_empty_view"));
        appSyncSource.setSavingItemsUserMessage(localization.getLanguage("share_option_save_to_device_user_message"));
        appSyncSource.setSaveItemsCompletedUserMessage(localization.getLanguage("share_option_save_to_device_completed_user_message"));
        
        appSyncSource.setIsRefreshSupported(SyncTask.REFRESH_FROM_SERVER, false);
        appSyncSource.setIsRefreshSupported(SyncTask.REFRESH_TO_SERVER, false);
        appSyncSource.setUiSourceIndex(getSourcePosition(id));

        appSyncSource.setBandwidthSaverUse(customization.useBandwidthSaverMedia());

        // Create the proper settings component for this source
        appSyncSource.setSettingsUIClass(FileSettingsUISyncSource.class);
        // No dev settings for this source
        appSyncSource.setDevSettingsUIClass(null);
        appSyncSource.setSourceBarViewClass(AndroidSourceThumbnailsView.class);
        appSyncSource.setThumbnailViewClass(FileThumbnailView.class);
        appSyncSource.setOpenItemScreenControllerClass(AndroidOpenFileScreenController.class);
        appSyncSource.setNavigationBarButtonViewClass(FileNavigationBarButtonView.class);
        appSyncSource.setSourceChronologicalViewItemClass(AndroidSourceChronologicalViewItem.class);
        appSyncSource.setChronologicalWidgetClass(FilesChronologicalFragment.class);
        appSyncSource.setSelectiveUploadWidgetClass(FilesSelectiveUploadFragment.class);
        appSyncSource.setThumbnailProperties(
                (int) context.getResources().getDimension(R.dimen.common_thumbnail_max_width),
                (int) context.getResources().getDimension(R.dimen.common_thumbnail_max_height),
                (int) context.getResources().getDimension(R.dimen.common_thumbnail_min_width),
                (int) context.getResources().getDimension(R.dimen.common_thumbnail_min_height),
                false /* Dimensions returned by getDimension are already converted to pixels */
        );
        appSyncSource.setPreviewProperties(
                (int) context.getResources().getDimension(R.dimen.common_preview_max_width),
                (int) context.getResources().getDimension(R.dimen.common_preview_max_height),
                (int) context.getResources().getDimension(R.dimen.common_preview_min_width),
                (int) context.getResources().getDimension(R.dimen.common_preview_min_height),
                false /* Dimensions returned by getDimension are already converted to pixels */
        );


        MediaDirectoryScanner mediaScanner = new MediaDirectoryScanner(
                appSyncSource, configuration, context);
        appSyncSource.setMediaDirectoryScanner(mediaScanner);
        
        appSyncSource.setPlayable(Boolean.TRUE); // all files are playable by default 
        // NB: Change the argument to null to have the behavior depend on the type of 
        // each individual file

        SourceConfig sc = null;
        String defaultUri = customization.getDefaultSourceUri(id);
        sc = new SourceConfig("files", SourceConfig.BRIEFCASE_TYPE, defaultUri);
        sc.setEncoding(SyncSource.ENCODING_NONE);
        sc.setSyncMode(customization.getDefaultSourceSyncMode(id, deviceInfo.getDeviceRole()));

        // Set the sync anchor to sync via media engine
        SapiSyncAnchor anchor = new SapiSyncAnchor();
        sc.setSyncAnchor(anchor);

        FileAppSyncSourceConfig assc = new FileAppSyncSourceConfig(context,
                appSyncSource, customization, configuration);
        assc.load(sc);
        assc.setMaxItemSize(customization.getMaxAllowedFileSizeForFiles());
        appSyncSource.setConfig(assc);

        StringBuffer defaultDir = new StringBuffer();
        String sdCardRoot = Environment.getExternalStorageDirectory().toString();
        String directoryName = ((AndroidCustomization)customization)
                    .getDefaultFilesSDCardDir();
        defaultDir = new StringBuffer();
        defaultDir.append(sdCardRoot);
        defaultDir.append("/");
        defaultDir.append(directoryName);

        if(Log.isLoggable(Log.TRACE)) {
            Log.trace(TAG_LOG, "Path for files sync is " + defaultDir);
        }
        assc.setBaseDirectory(defaultDir.toString());
        
        try {
            // Create the default folder if it doesn't exist
            FileAdapter d = new FileAdapter(defaultDir.toString());
            if(!d.exists()) {
                d.mkdirs();
            }
            d.close();
        } catch(IOException ex) {
            Log.error(TAG_LOG, "Cannot create directory: " + defaultDir.toString(), ex);
        }

        // Set the current file tracker to be notified of rename operations
        autoSyncService.startMonitoringDirectory(assc.getBaseDirectory(), appSyncSource.getId(), null);

        MediaTracker tracker = new MediaTracker(appSyncSource.getMetadataTable());
        FunambolMediaSyncSource syncSource = new FunambolMediaSyncSource(sc, tracker,
                appSyncSource.getDataDirectory(), 
                appSyncSource.getTempDirectory(), defaultDir.toString(), defaultDir.toString(),
                appSyncSource.getConfig().getMaxItemSize(),
                controller, customization,
                configuration, appSyncSource.getMetadataTable(), 
                AppInitializer.i(context).getNetworkTaskExecutor());
        syncSource.setContentUploadDownload(true, false);
        syncSource.setChangeOrderDateOnUpdates(true);
        syncSource.setDataTag("files");
        
        appSyncSource.setSyncSource(syncSource);

        // Make sure the metadata table is created during the initialization
        Table metadata = appSyncSource.getMetadataTable();
        if(metadata != null) {
            metadata.open();
            metadata.close();
        }
        assc.sourceSetupCompleted();
        
        // Create the open item screen controller
        OpenItemScreenController openItemScreenController;
        openItemScreenController = appSyncSource.createOpenItemScreenController(controller);
        appSyncSource.setOpenItemScreenController(openItemScreenController);

        // Register a listener to monitor the sync status for this source
        FunambolSourceMonitor monitor = new FunambolSourceMonitor(controller, appSyncSource);
        appSyncSource.setMonitor(monitor);
        BusService.registerMessageHandler(com.funambol.client.engine.SyncTaskMessage.class, monitor);
        BusService.registerMessageHandler(com.funambol.client.engine.DownloadTaskMessage.class, monitor);
        BusService.registerMessageHandler(com.funambol.client.engine.UploadTaskMessage.class, monitor);

        syncSource.setListener(monitor);
        
        return appSyncSource;
    }
    
    private void startMonitoringDCIMDirectory(AppSyncSource appSyncSource, String extensions[]) throws IOException {
        String sdCardRoot = Environment.getExternalStorageDirectory().toString();
        StringBuffer dcimDirectory = new StringBuffer();
        dcimDirectory.append(sdCardRoot);
        dcimDirectory.append("/DCIM");
        FileAdapter fa = new FileAdapter(dcimDirectory.toString(), true);
        if(fa.exists()) {
            Enumeration subdirs = fa.list(true);
            while(subdirs.hasMoreElements()) {
                String subdir = (String)subdirs.nextElement();
                // Skip hidden folders
                if (!subdir.startsWith(".")) {
                    subdir = dcimDirectory.toString() + FileAdapter.getFileSeparator() + subdir;
                    FileAdapter subdirFa = new FileAdapter(subdir);
                    if (subdirFa.isDirectory()) {
                        if (Log.isLoggable(Log.INFO)) {
                            Log.info(TAG_LOG, "Requiring monitoring for " + subdir);
                        }
                        autoSyncService.startMonitoringDirectory(subdir, appSyncSource.getId(), extensions);
                    }
                }
            }
        } else {
            Log.error(TAG_LOG, "Cannot monitor unexisting directory: " + dcimDirectory);
        }
    }

    private DataStore createDataStore(String name, String type, String version, AbstractDataManager dm) {

        DataStore ds = new DataStore();
        SourceRef sr = new SourceRef();
        sr.setValue(name);
        ds.setSourceRef(sr);

        CTInfo rxPref = new CTInfo();
        rxPref.setCTType(type);
        rxPref.setVerCT(version);
        ds.setRxPref(rxPref);

        CTInfo txPref = new CTInfo();
        txPref.setCTType(type);
        txPref.setVerCT(version);
        ds.setTxPref(txPref);

        SyncCap syncCap = new SyncCap();
        Vector types = new Vector();
        types.addElement(SyncType.TWO_WAY);
        types.addElement(SyncType.SLOW);
        types.addElement(SyncType.SERVER_ALERTED);
        syncCap.setSyncType(types);
        ds.setSyncCap(syncCap);

        // Max GUID size set to 2 bytes as default
        ds.setMaxGUIDSize(2);

        Vector properties = dm.getSupportedProperties();
        if(properties != null) {
            Vector ctCaps = new Vector();
            CTCap ctCap = new CTCap();
            ctCap.setCTInfo(new CTInfo(type, version));
            ctCap.setProperties(properties);
            ctCaps.add(ctCap);
            ds.setCTCaps(ctCaps);
        }
        return ds;
    }
}
