/*
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2009 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android;

import android.os.Build;
import android.content.SharedPreferences;
import android.content.Context;

import com.funambol.syncml.spds.DeviceConfig;
import com.funambol.client.configuration.Configuration;
import com.funambol.client.customization.Customization;
import com.funambol.client.source.AppSyncSourceManager;
import com.funambol.platform.DeviceInfo;
import com.funambol.platform.DeviceInfoInterface;
import com.funambol.util.Base64;
import com.funambol.util.Log;
import com.funambol.util.StringUtil;

/**
 * Container for the main client client configuration information.
 */
public class AndroidConfiguration extends Configuration {

    private static final String TAG_LOG = AndroidConfiguration.class.getSimpleName();

    public static final String KEY_FUNAMBOL_PREFERENCES = "fnblPref";
    
    private Context context;
    private SharedPreferences settings;
    private SharedPreferences.Editor editor;
    private DeviceConfig devconf;

    /**
     * Constructor
     * 
     * @param context the application Context
     * @param customization the Customization object
     * @param appSyncSourceManager the AppSyncSourceManager object. Better to
     * use an AndroidAppSyncSourceManager or an extension of its super class
     */
    public AndroidConfiguration(Context context,
                                 Customization customization,
                                 AppSyncSourceManager appSyncSourceManager)
    {
        super(customization, appSyncSourceManager);
        this.context = context;
        settings = context.getSharedPreferences(KEY_FUNAMBOL_PREFERENCES, 0);
        editor = settings.edit();
    }

    /**
     * Dispose this object referencing it with the null object
     */
    public static void dispose() {
    }

    /**
     * Load the value referred to the configuration given the key
     * @param key the String formatted key representing the value to be loaded
     * @return String String formatted vlaue related to the give key
     */
    protected String loadKey(String key) {
        return settings.getString(key, null);
    }

    /**
     * Save the loaded twin key-value using the android context package
     * SharedPreferences.Editor instance
     * @param key the key to be saved
     * @param value the value related to the key String formatted
     */
    protected void saveKey(String key, String value) {
        editor.putString(key, value);
    }

    /**
     * Save the loaded twin key-value using the android context package
     * SharedPreferences.Editor instance
     * @param key the key to be saved
     * @param value the value related to the key byte[] formatted
     */
    public void saveByteArrayKey(String key, byte[] value) {
        String b64 = new String(Base64.encode(value));
        saveKey(key, b64);
    }

    /**
     * Load the value referred to the configuration given the key and the
     * default value
     * @param key the String formatted key representing the value to be loaded
     * @param defaultValue the default byte[] formatted value related to the
     * given key
     * @return byte[] String formatted vlaue related to the give key byte[]
     * formatted
     */
    public byte[] loadByteArrayKey(String key, byte[] defaultValue) {
        String b64 = loadKey(key);
        if (b64 != null) {
            return Base64.decode(b64);
        } else {
            return defaultValue;
        }
    }

    public boolean commit() {
        return editor.commit();
    }

    /**
     * Get the device id related to this client. Useful when doing syncml
     * requests
     * @return String the device id that is formatted as the string "fac-" plus
     * the information of the deviceId field got by the TelephonyManager service
     */
    protected String createDeviceId() {
        // Some devices return a null device id, so we must generate the device id in a different way in these cases
        String deviceId = getDeviceInfo().getDeviceId();
        if (StringUtil.isNullOrEmpty(deviceId)) {
            // If we have no unique device id, we use the current time in milliseconds
            deviceId = "ts" + System.currentTimeMillis();
        }
        return ((AndroidCustomization)customization).getDeviceIdPrefix() + deviceId;
    }

    /**
     * Get the device related configuration for PIM synchronization
     * 
     * @return DeviceConfig the DeviceConfig object related to this device
     */
    public DeviceConfig getDeviceConfig() {
        if (devconf != null) {
            return devconf;
        }
        devconf = new DeviceConfig();
        devconf.setMan(Build.MANUFACTURER);
        devconf.setMod(Build.MODEL);
        // See here for possible values of SDK_INT
        // http://developer.android.com/reference/android/os/Build.VERSION_CODES.html
        devconf.setFwV(Build.VERSION.CODENAME + "(" + Build.VERSION.SDK_INT + ")");
        devconf.setSwV(BuildInfo.VERSION);
        devconf.setHwV(Build.FINGERPRINT);
        
        // Check if we have the device id in the configuration
        if (deviceId == null) {
            deviceId = createDeviceId();
        }
        devconf.setDevID(deviceId);
        
        devconf.setMaxMsgSize(64 * 1024);
        devconf.setLoSupport(true);
        devconf.setUtc(true);
        devconf.setNocSupport(true);
        devconf.setWBXML(customization.getUseWbxml());
        
        // Set the download limits according to the device type. The GT-I9000 has a very slow DB when it comes
        // to writing, so we limit the size of the transactions to avoid locking the DB for too long

        // The Galaxy S has two SDCards. One is internal and the other one is removable. The Environment class
        // refers to the first one. Unfortunately such an SDCard is very slow (maybe because formatted with a poor
        // performance file system type) and storing DBs and files on it makes the application unusable. Because
        // of this we try to use the external SD which has much better performance on writing.
        if ("GT-I9000".equalsIgnoreCase(getDeviceInfo().getDeviceModel())) {
            if (Log.isLoggable(Log.INFO)) {
                Log.info(TAG_LOG, "Lowering download limits to 50 on GT-I9000");
            }
            devconf.setIncrementalSyncDownloadLimit(50);
            devconf.setFullSyncDownloadLimit(50);
        }
        
        return devconf;
    }
    
    /**
     * Get the user agent id related to this client. Useful when doing syncml
     * requests
     * @return String the user agent that is formatted as the string
     * "Funambol Android Sync Client " plus the version of the client
     */
    protected String getUserAgent() {
        StringBuffer ua = new StringBuffer(
                ((AndroidCustomization)customization).getUserAgentName());
        ua.append(" ");
        ua.append(BuildInfo.VERSION);
        return ua.toString();
    }

    /**
     * Migrate the configuration (anything specific to the client)
     */
    @Override
    protected void migrateConfig() {

        // From 6 to 7 means from Diablo to Gallardo, where we introduced a new
        // mechanism for picture sync. We need to check what the server supports
        // to switch to the new method.
        if ("6".equals(version)) {
            setForceServerCapsRequest(true);
        }

        // Now migrate the basic configuration (this will update version)
        super.migrateConfig();
    }

    @Override
    protected DeviceInfoInterface createDeviceInfo() {
        return new DeviceInfo(context);
    }
}
