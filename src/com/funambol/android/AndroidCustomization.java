/**
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2009 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Map;

import android.content.Context;
import android.os.Environment;

import com.funambol.client.configuration.Configuration;
import com.funambol.client.controller.SignupHandler;
import com.funambol.client.customization.Customization;
import com.funambol.client.source.AppSyncSourceManager;
import com.funambol.client.ui.Bitmap;
import com.funambol.util.Log;
import com.funambol.android.activities.AndroidLoginScreen;
import com.funambol.androidsync.R;
import com.funambol.platform.DeviceInfo;
import com.funambol.platform.FileAdapter;
import com.funambol.platform.DeviceInfoInterface;
import com.funambol.platform.DeviceInfoInterface.DeviceRole;
import com.funambol.sync.SyncConfig;
import com.funambol.sync.SyncSource;
import com.funambol.sync.client.PercentageStorageLimit;
import com.funambol.sync.client.StorageLimit;

/**
 * Implements the Customization interface for Android platform
 */
public class AndroidCustomization implements Customization {

    private static final String TAG_LOG = AndroidCustomization.class.getSimpleName();

    private static final String   SERVER_URI              = "https://my.funambol.com/sync";
    private static final String   USERNAME                = "";
    private static final String   PASSWORD                = "";

    private static final String   VERSION                 = null;
    private static final boolean  PERFORM_URL_MATCHING_FOR_UPGRADE = true;
    
    // this is the string used to populate the user agent
    private static final String   USER_AGENT_NAME         = "Funambol Android Sync Client";

    private static final String   COPYRIGHT               = "Copyright " + (char)169 + " 2009 - 2012  Funambol, Inc.";
    private static final String   PORTAL_URL       = "https://my.funambol.com";

    // Sync sources customization
    private static final String   CONTACTS_DEFAULT_URI    = "card";
    private static final boolean  CONTACTS_AVAILABLE      = true;
    private static final boolean  CONTACTS_ENABLED        = true;

    private static final String   EVENTS_DEFAULT_URI      = "event";
    private static final boolean  EVENTS_AVAILABLE        = true;
    private static final boolean  EVENTS_ENABLED          = true;

    private static final String   PICTURES_DEFAULT_URI    = "picture";
    private static final boolean  PICTURES_AVAILABLE      = true;
    private static final boolean  PICTURES_ENABLED        = true;

    private static final String   VIDEOS_DEFAULT_URI      = "video";
    private static final boolean  VIDEOS_AVAILABLE        = true;
    private static final boolean  VIDEOS_ENABLED          = true;

    private static final String   FILES_DEFAULT_URI      = "file";
    private static final boolean  FILES_AVAILABLE        = true;
    private static final boolean  FILES_ENABLED          = true;

    private static final String   DEVICE_ID_PREFIX        = "fac-";

    // Log customization
    private static final boolean  SEND_LOG_ENABLED        = true;
    private static final boolean  LOG_IN_SETTINGS_SCREEN  = true;
    private static final boolean  LOCK_LOG_LEVEL          = false;
    private static final int      LOCKED_LOG_LEVEL        = Log.TRACE;
    private static final String   LOG_FILE_NAME           = "synclog.txt";
    
    // App internals (usually removed since v11)
    private static final boolean  DEV_SETTINGS_ENABLED    = false;

    // Sync items type
    private static final String   DEFAULT_CALENDAR_TYPE = "text/x-vcalendar";
    private static final String   DEFAULT_CONTACT_TYPE  = "text/x-vcard";

    private static final boolean  DISPLAY_NAME_SUPPORTED = false;

    private static final boolean  USE_BANDWIDTH_SAVER_CONTACTS = false;
    private static final boolean  USE_BANDWIDTH_SAVER_EVENTS = false;
    private static final boolean  USE_BANDWIDTH_SAVER_MEDIA = true;
    
    // The auto sync period in minutes
    private static final int      AUTO_SYNC_PERIOD = 120;
    
    // This is the account screen class name. It can be customized for versions
    // with a different account screen implementation
    private static final String  LOGIN_SCREEN_CLASS_NAME = AndroidLoginScreen.class.getName();

    // This is the account screen authenticator for the unit tests
    // cannot use direct link, otherwise app doesn't compile :(
    private static final String  UT_ACCOUNT_SCREEN_CLASS_NAME = "com.funambol.android.UnitTestAuthenticator";

    // Normally the sync adapter set the account as syncable during its
    // initialization. Set this value to false to change this behavior.
    private static boolean ENABLE_SYNC_AUTOMATICALLY = true;

    // Note: this array must be kept aligned with the list of sources that we
    // register (see initSourcesInfo below)
    private static final int SOURCES_ORDER[] = {
            AndroidAppSyncSourceManager.PICTURES_ID,
            AndroidAppSyncSourceManager.VIDEOS_ID,
            AndroidAppSyncSourceManager.FILES_ID,
            AndroidAppSyncSourceManager.CONTACTS_ID,
            AndroidAppSyncSourceManager.EVENTS_ID
            };

    // Updater customization
    private static final boolean ENABLE_UPDATER_MANAGER  = false; // Used for Funambol update
    private static final long CHECK_UPDATE_INTERVAL   = (long)24*(long)60*(long)60*(long)1000; // 1 day in milliseconds
    private static final long REMINDER_UPDATE_INTERVAL= (long)2 *(long)60*(long)60*(long)1000; // 2 hours in milliseconds

    private static final String   SUPPORT_EMAIL_ADDRESS   = "fac_log@funambol.com";

    // Tha name of the SQLite Database used to store application data
    // (e.g. the configuration, sources tracker data)
    private static final String   FUNAMBOL_SQLITE_DB_NAME = "funambol.db";
    
    // Specifies if the sync retry feature is enabled. It allows to start a
    // scheduled synchronization after a connectivity error
    private final boolean SYNC_RETRY_ENABLED   = false;
    private final int[]   SYNC_RETRY_INTERVALS = {};

    // Bandwidth Saver customization
    private final boolean  BANDWIDTH_SAVER_ENABLED = true;

    private final boolean  SHOW_SYNC_ICON_ON_SELECTION = false;

    // Specifies if the S2C SMS push must be enabled in the client
    private final boolean ENABLE_S2C_SMS_PUSH           = false;

    private final int C2S_PUSH_DELAY                    = 60 * 1000; // 1 minute

    // Show non working sources in the home screen
    private final boolean SHOW_NON_WORKING_SOURCES      = false;

    private final boolean  SOURCE_URI_VISIBLE          = true;
    private final boolean  SYNC_DIRECTION_VISIBLE      = true;

    private final int      DEFAULT_SYNC_MODE           = Configuration.SYNC_MODE_AUTO;
    private final int      S2CPUSH_SMS_PORT            = 50011;

    private final boolean  SYNC_MODE_IN_SETTINGS_SCREEN = true;

    private final int[]    AVAILABLE_SYNC_MODES = {Configuration.SYNC_MODE_AUTO,
                                                   Configuration.SYNC_MODE_MANUAL};

    private final boolean  SHOW_ABOUT_LICENCE          = true;
    private final boolean  SHOW_POWERED_BY             = false;
    private final boolean  SHOW_PORTAL_INFO            = true;
    private final boolean  SHOW_TERMS_AND_PRIVACY      = true;

    private final boolean  ENABLE_REFRESH_COMMAND      = true;

    private final int      DEFAULT_AUTH_TYPE = SyncConfig.AUTH_TYPE_BASIC;

    // Specifies if the sync messages shall be exchanged using binary xml. If this value is set to false
    // the client decides what is the best encoding based on the server capabilities
    private final boolean USE_WBXML                     = false;

    private final String  HTTP_UPLOAD_PREFIX            = "sapi/media";

    private final boolean CONTACTS_IMPORT_ENABLED       = true;

    // Mobile Sign Up customizations
    private final boolean MOBILE_SIGNUP_ENABLED         = true;
    private final int     DEFAULT_MSU_VALIDATION_MODE   = SignupHandler.VALIDATION_MODE_CAPTCHA;
    private final boolean ADD_SHOW_PASSWORD_FIELD       = true;
    private final String  TERMS_AND_CONDITIONS_URL      = "http://my.funambol.com/ui/mobile/jsp/toc.jsp";
    private final String  PRIVACY_POLICY_URL            = "http://my.funambol.com/ui/mobile/jsp/pp.jsp";
    private final boolean PREFILL_PHONE_NUMBER          = false;
    private final String  DEFAULT_MSU_COUNTRY_CODE      = null;

    /**
     * Source sync modes (i.e. sync directions) available for PIM sync.
     */
    private static final int[] AVAILABLE_SOURCE_SYNC_MODES_PIM_SYNC = {
        SyncSource.INCREMENTAL_SYNC,
        SyncSource.NO_SYNC };

    private static final int[] AVAILABLE_SOURCE_SYNC_MODES_MEDIA_SYNC = {
        SyncSource.INCREMENTAL_SYNC,
        SyncSource.NO_SYNC };

    private static final StorageLimit LOCAL_STORAGE_SAFETY_THRESHOLD = new PercentageStorageLimit(98);

    private final String DEFAULT_FILES_SDCARD_DIR = "MediaHub-Files";

    private Hashtable<Integer, String> sourcesUri = new Hashtable<Integer, String>();
    private Hashtable activeSourcesEnabledState = new Hashtable();
    
    private Hashtable sourcesIcon               = new Hashtable();
    private Hashtable sourcesDisabledIcon       = new Hashtable();
    private Hashtable sourcesPlaceHolderIcon    = new Hashtable();
    private final Context appContext;
 
    /** Max allowed size for videos, 250Mb */
    private static final long MAX_ALLOWED_FILE_SIZE_FOR_VIDEOS = 250 * 1024 * 1024;
    /** Max allowed size for files, 250Mb */
    private static final long MAX_ALLOWED_FILE_SIZE_FOR_FILES = 250 * 1024 * 1024;
   
    /** Min server quota to enable video sync on app startup */ 
    private static final long MIN_QUOTA_TO_ENABLE_VIDEO_ON_STARTUP = 250 * 1024 * 1024;
    
    /** Max number of upload permanent errors before discarding the item */
    private static final int UPLOAD_PERMANENT_ERRORS_LIMIT = 5;

    /** 
     * If this flag is on (because of a limitation of Android framework), streaming
     *  over HTTPS is forbidden and remote HTTPS URLs to play videos or music are 
     *  converted into HTTP URLs.
     */
    private static final boolean STREAMING_OVER_HTTP_ONLY = true;
    
    // The max number of thumbnails to show in the source thumbnails view
    private static final int MAX_THUMBNAILS_COUNT_IN_MAIN_SCREEN = 25;

    private static final String  DATA_PLAN_URL = PORTAL_URL; //Temporary value
    
    private static final String[] EXTENSIONS_DOCUMENT = {
        "odt", 
        "ott", 
        "oth", 
        "odm", 
        "sxw",
        "stw",
        "sxg", 
        "doc",
        "docm", 
        "docx",
        "dot",
        "dotx",
        "dotm",
        "wpd",
        "rtf",
        "xml",
        "pdf"
    };
    private static final String[] EXTENSIONS_SPREADSHEET = {
        "ods",
        "ots",
        "sxc",
        "stc",
        "xls",
        "xlw",
        "xlt",
        "xlsx",
        "xlsm",
        "xlts",
        "xltm",
        "xlsb"
    };
    private static final String[] EXTENSIONS_PRESENTATION = {
        "ppt",
        "pps",
        "pot",
        "pptx",
        "pptm",
        "potx",
        "potm",
        "ppsx",
        "odp",
        "sxi"
    };
    private static final String[] EXTENSIONS_WEBPAGE = {
        "htm",
        "html"
    };
    private static final String[] EXTENSIONS_TEXT = {
        "txt",
        "csv"
    };
    private static final String[] EXTENSIONS_ARCHIVE = {
        "zip",
        "tar",
        "tgz",
        "rar",
        "bz2",
        "gz",
        "7z"
    };
    private static final String[] EXTENSIONS_MUSIC = {
        "mp3",
        "mid",
        "midi",
        "wma",
        "aac",
        "wav",
        "aiff",
        "m4a"
    };
    private static final String[] EXTENSIONS_IMAGE = {
        "jpg",
        "jpeg",
        "png",
        "bmp",
        "gif",
        "tiff",
        "tif",
        "jfif",
        "jif"
    };
    private static final String[] EXTENSIONS_VIDEO = {
        "mov",
        "3gp",
        "mp4",
        "avi",
        "3gpp",
        "3g2",
        "wmv",
        "mpeg",
        "flv",
        "m4v",
        "mpg",
        "vob",
        "swf"
    };
    private static final Map<String[], Bitmap> FILE_TYPE_ICONS =
        new Hashtable<String[], Bitmap>();
    private static final Bitmap FILE_TYPE_ICON_GENERIC = new Bitmap(R.drawable.openitem_imgfile_generic_128x128);

    //// ------------------- END OF CUSTOMIZABLE FIELDS --------------------////

    /**
     * Don't instantiate this object directly, but always use {@link AppInitializer#getCustomization()}
     * 
     * @param appContext Application context
     */
    public AndroidCustomization(Context appContext) {
        initSourcesInfo();
        this.appContext = appContext;
    }
    
    public void dispose() {
    }

    public String getFunambolSQLiteDbName() {
        return FUNAMBOL_SQLITE_DB_NAME;
    }

    public String getUserAgentName() {
        return USER_AGENT_NAME;
    }

    public String getPrivateDataDirectory() {
        DeviceInfo devInf = new DeviceInfo(appContext);
        // The Galaxy S has two SDCards. One is internal and the other one is removable. The Environment class
        // refers to the first one. Unfortunately such an SDCard is very slow (maybe because formatted with a poor
        // performance file system type) and storing DBs and files on it makes the application unusable. Because
        // of this we try to use the external SD which has much better performance on writing.
        String sdCardDirectory = null;
        /*
        if ("GT-I9000".equalsIgnoreCase(devInf.getDeviceModel())) {
            File extDir = new File("/mnt/sdcard/external_sd");
            if (extDir.exists()) {
                if (Log.isLoggable(Log.INFO)) {
                    Log.info(TAG_LOG, "Using external SDCard for private data");
                }
                sdCardDirectory = "/mnt/sdcard/external_sd";
            }
        }
        */
        
        if (sdCardDirectory == null) {
            sdCardDirectory = Environment.getExternalStorageDirectory().toString();
        }
        
        StringBuffer res = new StringBuffer(sdCardDirectory);
        res.append(FileAdapter.getFileSeparator())
           .append("Android")
           .append(FileAdapter.getFileSeparator())
           .append("data")
           .append(FileAdapter.getFileSeparator())
           .append(BuildInfo.PACKAGE_NAME);
        return res.toString();
    }
   
    /**
     * Return a directory where the db files shall be stored. The method may return null if the standard location
     * shall be used.
     */
    public String getDBDirectory() {
        DeviceInfo devInf = new DeviceInfo(appContext);
        // The Galaxy S has two SDCards. One is internal and the other one is removable. The Environment class
        // refers to the first one. Unfortunately such an SDCard is very slow (maybe because formatted with a poor
        // performance file system type) and storing DBs and files on it makes the application unusable. Because
        // of this we try to use the external SD which has much better performance on writing.
        /*
        if ("GT-I9000".equalsIgnoreCase(devInf.getDeviceModel())) {
            File extDir = new File("/mnt/sdcard/external_sd");
            if (extDir.exists()) {
                if (Log.isLoggable(Log.INFO)) {
                    Log.info(TAG_LOG, "Using external SDCard for private data");
                }
                return getPrivateDataDirectory();
            }
        }
        */
        //return getPrivateDataDirectory();
        return null;
    }

    public int[] getSourcesOrder() {
        return SOURCES_ORDER;
    }

    public Bitmap getPoweredByLogo() {
        return new Bitmap(new Integer(R.drawable.common_imgpoweredby_171x32));
    }

    public String getDeviceIdPrefix() {
        return DEVICE_ID_PREFIX;
    }

    public Bitmap getOkIcon() {
        return new Bitmap(new Integer(R.drawable.common_imgcompleted_32x32));
    }

    public Bitmap getErrorIcon() {
        return new Bitmap(new Integer(R.drawable.common_imgfailed_32x32));
    }

    public Bitmap getStatusSelectedIcon() {
        return new Bitmap(new Integer(android.R.drawable.ic_popup_sync));
    }

    public String getContactType() {
        return DEFAULT_CONTACT_TYPE;
    }

    public String getCalendarType() {
        return DEFAULT_CALENDAR_TYPE;
    }

    public String getTaskType() {
        return null;
    }

    public String getNoteType() {
        return null;
    }

    public boolean useBandwidthSaverContacts(){
        return USE_BANDWIDTH_SAVER_CONTACTS;
    }

    public boolean useBandwidthSaverEvents(){
        return USE_BANDWIDTH_SAVER_EVENTS;
    }

    public boolean useBandwidthSaverMedia(){
        return USE_BANDWIDTH_SAVER_MEDIA;
    }

    public boolean isDisplayNameSupported() {
        return DISPLAY_NAME_SUPPORTED;
    }

    public String getSupportEmailAddress() {
        return SUPPORT_EMAIL_ADDRESS;
    }

    public String getLoginScreenClassName() {
        if (BuildInfo.UNIT_TEST) {
            return UT_ACCOUNT_SCREEN_CLASS_NAME;
        } else {
            return LOGIN_SCREEN_CLASS_NAME;
        }
    }

    public boolean getEnableSyncAutomatically() {
        return ENABLE_SYNC_AUTOMATICALLY;
    }
    
    public boolean getSyncRetryEnabled() {
        return SYNC_RETRY_ENABLED;
    }

    public int[] getSyncRetryIntervals() {
        return SYNC_RETRY_INTERVALS;
    }

    public boolean isSourceUriVisible() {
        return SOURCE_URI_VISIBLE;
    }

    public boolean isSyncDirectionVisible() {
        return SYNC_DIRECTION_VISIBLE;
    }

    public boolean isSourceActive(int id) {
        return activeSourcesEnabledState.containsKey(new Integer(id));
    }

    public boolean isSourceEnabledByDefault(int id) {
        Boolean active = (Boolean) activeSourcesEnabledState.get(new Integer(id));
        return active != null ? active.booleanValue() : false;
    }

    public boolean enableUpdaterManager() {
        return ENABLE_UPDATER_MANAGER;
    }

    public String getDefaultSourceUri(int id) {
        return (String)sourcesUri.get(new Integer(id));
    }


    public int[] getDefaultSourceSyncModes(int id, DeviceInfoInterface.DeviceRole role) {
        return getDefaultSourceSyncModes(id);
    }

    /**
     * Gets default sync source available direction modes.
     * 
     * @deprecated use {@link #getDefaultSourceSyncModes(int, DeviceInfoInterface.DeviceRole)} instead 
     */
    public int[] getDefaultSourceSyncModes(int id) {
        
        // Media sync
        if ((AndroidAppSyncSourceManager.PICTURES_ID == id) || (AndroidAppSyncSourceManager.VIDEOS_ID == id) ||
             AndroidAppSyncSourceManager.FILES_ID == id)
        {
            return AVAILABLE_SOURCE_SYNC_MODES_MEDIA_SYNC;
        } else {
            // PIM sync
            // Currently, the device role makes no difference whatsoever for PIM
            return AVAILABLE_SOURCE_SYNC_MODES_PIM_SYNC;
        }
    }

    public boolean lockLogLevel(){
        return LOCK_LOG_LEVEL;
    }

    public int getLockedLogLevel(){
        return LOCKED_LOG_LEVEL;
    }

    public boolean isLogEnabledInSettingsScreen() {
        return LOG_IN_SETTINGS_SCREEN;
    }

    public boolean isBandwidthSaverEnabled() {
        return BANDWIDTH_SAVER_ENABLED;
    }

    public boolean sendLogEnabled() {
        return SEND_LOG_ENABLED;
    }

    public String getServerUriDefault() {
        return SERVER_URI;
    }

    public String getUserDefault() {
        return USERNAME;
    }

    public String getPasswordDefault() {
        return PASSWORD;
    }

    public boolean getPerformUrlMatchingForUpgrade() {
        return PERFORM_URL_MATCHING_FOR_UPGRADE;
    }
    
    public long getCheckUpdtIntervalDefault(){
        return CHECK_UPDATE_INTERVAL;
    }

    public long getReminderUpdtIntervalDefault(){
        return REMINDER_UPDATE_INTERVAL;
    }

    public String getLogFileName() {
        return LOG_FILE_NAME;
    }

    public String getCopyright() {
        return COPYRIGHT;
    }

    public boolean showAboutLicence() {
        return SHOW_ABOUT_LICENCE;
    }

    public boolean showPoweredBy() {
        return SHOW_POWERED_BY;
    }

    public boolean enableRefreshCommand() {
        return ENABLE_REFRESH_COMMAND;
    }

    public boolean isS2CSmsPushEnabled() {
        return ENABLE_S2C_SMS_PUSH;
    }
    
    public int getC2SPushDelay() {
        return C2S_PUSH_DELAY;
    }
    
    public int getAutoSyncPeriod() {
        return AUTO_SYNC_PERIOD;
    }

    public int getS2CPushSmsPort() {
        return S2CPUSH_SMS_PORT;
    }

    public int getDefaultSyncMode() {
        return DEFAULT_SYNC_MODE;
    }

    public boolean showSyncModeInSettingsScreen() {
        return SYNC_MODE_IN_SETTINGS_SCREEN;
    }

    public int[] getAvailableSyncModes() {
        return AVAILABLE_SYNC_MODES;
    }

    /**
     * Get default sync source direction mode
     * @deprecated use {@link #getDefaultSourceSyncMode(int, DeviceInfoInterface.DeviceRole)} instead 
     */
    public int getDefaultSourceSyncMode(int id) {
        
        //FIXME: It's a quick and dirty workaround for obtaining the device role
        DeviceInfo deviceInfo = new DeviceInfo(appContext);
        
        return getDefaultSourceSyncMode(id, deviceInfo.getDeviceRole());
    }

    /**
     * Get default sync source direction mode, based on device information
     * @param id
     * @param use one of the predefined values of {@link DeviceInfoInterface.DeviceRole}
     */
    public int getDefaultSourceSyncMode(int id, DeviceRole deviceRole) {
        if (!isSourceEnabledByDefault(id)) {
            return SyncSource.NO_SYNC;
        }

        return SyncSource.INCREMENTAL_SYNC;
    }

    public boolean showSyncIconOnSelection() {
        return SHOW_SYNC_ICON_ON_SELECTION;
    }

    public boolean showNonWorkingSources() {
        return SHOW_NON_WORKING_SOURCES;
    }

    public int getDefaultAuthType() {
        return DEFAULT_AUTH_TYPE;
    }

    public boolean getUseWbxml() {
        return USE_WBXML;
    }

    public boolean getContactsImportEnabled() {
        return CONTACTS_IMPORT_ENABLED;
    }

    public boolean getMobileSignupEnabled() {
        return MOBILE_SIGNUP_ENABLED;
    }

    public int getDefaultMSUValidationMode() {
        return DEFAULT_MSU_VALIDATION_MODE;
    }

    public String getDefaultMSUCountryCode() {
        return DEFAULT_MSU_COUNTRY_CODE;
    }

    public boolean getAddShowPasswordField() {
        return ADD_SHOW_PASSWORD_FIELD;
    }

    public String getTermsAndConditionsUrl() {
        return TERMS_AND_CONDITIONS_URL;
    }

    public String getPrivacyPolicyUrl() {
        return PRIVACY_POLICY_URL;
    }

    public boolean getPrefillPhoneNumber() {
        return PREFILL_PHONE_NUMBER;
    }

    public String getHttpUploadPrefix() {
        return HTTP_UPLOAD_PREFIX;
    }

    public String getDefaultFilesSDCardDir() {
        return DEFAULT_FILES_SDCARD_DIR;
    }

    private void initSourcesInfo() {

        if (Log.isLoggable(Log.INFO)) {
            Log.info(TAG_LOG, "Initializing sources info");
        }

        // Initialize the sources available in this application
        if(CONTACTS_AVAILABLE) {
            int id = AndroidAppSyncSourceManager.CONTACTS_ID;
            if (Log.isLoggable(Log.DEBUG)) {
                Log.debug(TAG_LOG, "Initializing source: " + id);
            }
            sourcesUri.put(new Integer(id), CONTACTS_DEFAULT_URI);
            activeSourcesEnabledState.put(new Integer(id), CONTACTS_ENABLED);
            sourcesIcon.put(new Integer(id), new Bitmap(R.drawable.common_selcontact));
            sourcesDisabledIcon.put(new Integer(id), new Bitmap(R.drawable.common_selcontact));
        }
        if(EVENTS_AVAILABLE) {
            int id = AndroidAppSyncSourceManager.EVENTS_ID;
            if (Log.isLoggable(Log.DEBUG)) {
                Log.debug(TAG_LOG, "Initializing source: " + id);
            }
            sourcesUri.put(new Integer(id), EVENTS_DEFAULT_URI);
            activeSourcesEnabledState.put(new Integer(id), EVENTS_ENABLED);
            sourcesIcon.put(new Integer(id), new Bitmap(R.drawable.common_selcalendar));
            sourcesDisabledIcon.put(new Integer(id), new Bitmap(R.drawable.common_selcalendar));
        }
        if(PICTURES_AVAILABLE) {
            int id = AndroidAppSyncSourceManager.PICTURES_ID;
            if (Log.isLoggable(Log.DEBUG)) {
                Log.debug(TAG_LOG, "Initializing source: " + id);
            }
            sourcesUri.put(new Integer(id), PICTURES_DEFAULT_URI);
            activeSourcesEnabledState.put(new Integer(id), PICTURES_ENABLED);
            sourcesIcon.put(new Integer(id), new Bitmap(R.drawable.common_imgpicture_192x192));
            sourcesDisabledIcon.put(new Integer(id), new Bitmap(R.drawable.common_imgpicture_192x192));
            sourcesPlaceHolderIcon.put(new Integer(id), new Bitmap(R.drawable.common_imgpicture_192x192));
        }
        if(VIDEOS_AVAILABLE) {
            int id = AndroidAppSyncSourceManager.VIDEOS_ID;
            if (Log.isLoggable(Log.DEBUG)) {
                Log.debug(TAG_LOG, "Initializing source: " + id);
            }
            sourcesUri.put(new Integer(id), VIDEOS_DEFAULT_URI);
            activeSourcesEnabledState.put(new Integer(id), VIDEOS_ENABLED);
            sourcesIcon.put(new Integer(id), new Bitmap(R.drawable.common_imgvideo_192x192));
            sourcesDisabledIcon.put(new Integer(id), new Bitmap(R.drawable.common_imgvideo_192x192));
            sourcesPlaceHolderIcon.put(new Integer(id), new Bitmap(R.drawable.common_imgvideo_192x192));
        }
        if(FILES_AVAILABLE) {
            
            FILE_TYPE_ICONS.put(EXTENSIONS_ARCHIVE, new Bitmap(R.drawable.openitem_imgfile_zip_128x128));
            FILE_TYPE_ICONS.put(EXTENSIONS_DOCUMENT, new Bitmap(R.drawable.openitem_imgfile_pdf_128x128));
            FILE_TYPE_ICONS.put(EXTENSIONS_IMAGE, new Bitmap(R.drawable.openitem_imgfile_image_128x128));
            FILE_TYPE_ICONS.put(EXTENSIONS_MUSIC, new Bitmap(R.drawable.openitem_imgfile_music_128x128));
            FILE_TYPE_ICONS.put(EXTENSIONS_PRESENTATION, new Bitmap(R.drawable.openitem_imgfile_presentation_128x128));
            FILE_TYPE_ICONS.put(EXTENSIONS_SPREADSHEET, new Bitmap(R.drawable.openitem_imgfile_spreadsheet_128x128));
            FILE_TYPE_ICONS.put(EXTENSIONS_TEXT, new Bitmap(R.drawable.openitem_imgfile_txt_128x128));
            FILE_TYPE_ICONS.put(EXTENSIONS_VIDEO, new Bitmap(R.drawable.openitem_imgfile_video_128x128));
            FILE_TYPE_ICONS.put(EXTENSIONS_WEBPAGE, new Bitmap(R.drawable.openitem_imgfile_html_128x128));            
            
            int id = AndroidAppSyncSourceManager.FILES_ID;
            if (Log.isLoggable(Log.DEBUG)) {
                Log.debug(TAG_LOG, "Initializing source: " + id);
            }
            sourcesUri.put(new Integer(id), FILES_DEFAULT_URI);
            activeSourcesEnabledState.put(new Integer(id), FILES_ENABLED);
            sourcesIcon.put(new Integer(id), new Bitmap(R.drawable.common_imgfile_192x192));
            sourcesDisabledIcon.put(new Integer(id), new Bitmap(R.drawable.common_imgfile_192x192));
            sourcesPlaceHolderIcon.put(new Integer(id), new Bitmap(R.drawable.common_imgfile_192x192));
        }
    }

    /**
     * Returns an Enumeration of Integer where each item represents the id of an
     * available source. The source is not ready yet to be used, but it is
     * available in this client version. To check if a source is really working
     * and enabled, use the corresponding AppSyncSource methods.
     *
     * @return an enumeration of Integer
     */
    public Enumeration getAvailableSources() {
        Enumeration keys = activeSourcesEnabledState.keys();
        return keys;
    }

    public Bitmap getSourceIcon(int id) {
        Bitmap icon = (Bitmap)sourcesIcon.get(new Integer(id));
        return icon;
    }

    public Bitmap getSourceDisabledIcon(int id) {
        Bitmap icon = (Bitmap)sourcesDisabledIcon.get(new Integer(id));
        return icon;
    }

    public Bitmap getSourcePlaceHolderIcon(int id) {
        Bitmap icon = (Bitmap)sourcesPlaceHolderIcon.get(new Integer(id));
        return icon;
    }

    public String getPoweredBy() {
        return null;
    }

    // Note that this is hardcoded here because it cannot be translated
    public String getLicense() {
        StringBuffer license = new StringBuffer();

        license.append("This program is provided AS IS, without warranty licensed under AGPLV3. The ")
               .append("Program is free software; you can redistribute it and/or modify it under the ")
               .append("terms of the GNU Affero General Public License version 3 as published by the Free ")
               .append("Software Foundation including the additional permission set forth source code ")
               .append("file header.\n\n")
               .append("The interactive user interfaces in modified source and object code versions of ")
               .append("this program must display Appropriate Legal Notices, as required under Section 5 ")
               .append("of the GNU Affero General Public License version 3.\n\n")
               .append("In accordance with Section 7(b) of the GNU Affero General Public License version 3, ")
               .append("these Appropriate Legal Notices must retain the display of the \"Powered by ")
               .append("Funambol\" logo. If the display of the logo is not reasonably feasible for ")
               .append("technical reasons, the Appropriate Legal Notices must display the words \"Powered ")
               .append("by Funambol\". Funambol is a trademark of Funambol, Inc.");
        
        return license.toString();
    }

    public String getVersion() {
        StringBuffer result = new StringBuffer();
        if (VERSION == null) {
            // Grab the version from the BuildInfo
            result.append(BuildInfo.VERSION);
        } else {
            result.append(VERSION);
        }
        if (!"release".equals(BuildInfo.MODE)) {
            result.append(" (").append(BuildInfo.DATE).append(")");
        }
        return result.toString();
    }

    public StorageLimit getStorageLimit() {
        return LOCAL_STORAGE_SAFETY_THRESHOLD;
    }

    public long getMaxAllowedFileSizeForVideos() {
        return MAX_ALLOWED_FILE_SIZE_FOR_VIDEOS;
    }

    public long getMaxAllowedFileSizeForFiles() {
        return MAX_ALLOWED_FILE_SIZE_FOR_FILES;
    }
    
    public long getMinQuotaToEnableVideoOnStartup() {
        return MIN_QUOTA_TO_ENABLE_VIDEO_ON_STARTUP;
    }
    
    public int getUploadPermanentErrorsLimit() {
        return UPLOAD_PERMANENT_ERRORS_LIMIT;
    }

    public String getPortalURL() {
        return PORTAL_URL;
    }

    public boolean showPortalInfo() {
        return SHOW_PORTAL_INFO;
    }
    
    public boolean showTermsAndPrivacyPolicyInAboutScreen() {
        return SHOW_TERMS_AND_PRIVACY;
    }


    public String getDataPlanUrl() {
        return DATA_PLAN_URL;
    }
    
    public int getFirstSyncMediaUploadLimit(int mediaId, DeviceRole deviceRole) {
        //actually, device role is useless for upload limit
        switch (mediaId) {
            case AppSyncSourceManager.PICTURES_ID:
                return 5;
            case AppSyncSourceManager.VIDEOS_ID:
                return 2;
            default:
                //fallback values
                return 0;
        }
    }

    public int getFirstSyncMediaDownloadLimit(int mediaId, DeviceRole deviceRole) {
        if (DeviceRole.TABLET.equals(deviceRole)) {
            switch (mediaId) {
                case AppSyncSourceManager.PICTURES_ID:
                    return 20;
                case AppSyncSourceManager.VIDEOS_ID:
                    return 5;
                default:
                    //fallback value
                    return 0;
            }
        }

        //fallback role
        switch (mediaId) {
            case AppSyncSourceManager.PICTURES_ID:
                return 0;
            case AppSyncSourceManager.VIDEOS_ID:
                return 0;
            default:
                //fallback value
                return 0;
        }
    }

    public int getMaxThumbnailsCountInMainScreen() {
        return MAX_THUMBNAILS_COUNT_IN_MAIN_SCREEN;
    }
    
    public boolean isStreamingOverHttpOnly() {
        return STREAMING_OVER_HTTP_ONLY;
        
    }
    
    public Bitmap getFilePreviewIcon(String extension) {
        if (extension != null) {
            for (String[] candidateExtensions : FILE_TYPE_ICONS.keySet()) {
                for (String candidateExtension : candidateExtensions) {
                    if (extension.equalsIgnoreCase(candidateExtension)) {
                        return FILE_TYPE_ICONS.get(candidateExtensions);
                    }
                }
            }
        }
        return FILE_TYPE_ICON_GENERIC;
    }

	public boolean isDevSettingsEnabled() {
		return DEV_SETTINGS_ENABLED;
	}
}
