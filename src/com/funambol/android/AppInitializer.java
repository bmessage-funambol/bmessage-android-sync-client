/**
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2009 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android;

import java.io.IOException;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;

import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.os.Build;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;

import com.funambol.android.activities.AndroidActivitiesFactory;
import com.funambol.android.activities.AndroidDisplayManager;
import com.funambol.android.controller.AndroidController;
import com.funambol.android.controller.AndroidSendItemScreenController;
import com.funambol.android.controller.AndroidSyncModeHandler;
import com.funambol.android.services.AutoSyncServiceHandler;
import com.funambol.android.source.pim.PimTestRecorder;
import com.funambol.client.configuration.Configuration;
import com.funambol.client.controller.RefreshTrigger;
import com.funambol.client.controller.ServiceAuthenticatorScreenController;
import com.funambol.client.source.AppSyncSource;
import com.funambol.client.ui.AppFlowNavigator;
import com.funambol.client.engine.SyncMessageHandler;
import com.funambol.client.engine.NetworkTaskExecutor;
import com.funambol.client.engine.RefreshMessageHandler;
import com.funambol.client.engine.TasksPriorities;
import com.funambol.client.localization.Localization;
import com.funambol.client.notification.NotificationMessageHandler;
import com.funambol.client.services.ExternalServiceCache;
import com.funambol.platform.PlatformEnvironment;
import com.funambol.platform.NetworkStatus;
import com.funambol.concurrent.TaskExecutor;
import com.funambol.platform.FileAdapter;
import com.funambol.platform.TimerHandler;
import com.funambol.storage.SQLiteTable;
import com.funambol.util.Log;
import com.funambol.util.MultipleAppender;
import com.funambol.util.bus.BusService;
import com.funambol.util.FileAppender;
import com.funambol.util.AndroidLogAppender;

/**
 * This class is used to initialize the entire application. It can be invoked by
 * a starting activity or by a service. Once the application got initialized
 * once, any call to init has no effect. The singleton instance is realized, so
 * this class reference can be got using the static related getter method.
 */
public class AppInitializer {

    private static final String TAG_LOG = "AppInitializer";

    private static final Object mSyncObject = new Object();
    private static AppInitializer instance;

    private Localization localization;
    private AndroidSyncModeHandler syncModeHandler;
    private AndroidConfiguration configuration;
    private AndroidCustomization customization;
    private AndroidAppSyncSourceManager appSyncSourceManager;
    private AndroidController controller;
    private AppFlowNavigator appFlowNavigator;
    private WifiLock wifiLock = null;
    private SyncLock syncLock;
    private NetworkStatus netStatus;
    private TaskExecutor networkTaskExecutor;
    private SyncMessageHandler syncMessageHandler;
    private NotificationMessageHandler notificationMessageHandler;
    private RefreshMessageHandler refreshMessageHandler;
    private ExternalAccountManager externalAccountManager;
    
    private boolean initialized = false;

    /**
     * Enforce singleton pattern
     */
    private AppInitializer(Context context) {
    }

    // ---------- Singleton
    public static AppInitializer i(Context context) {
        synchronized (mSyncObject) {
            if (null == instance) {
                instance = new AppInitializer(context);
                instance.init(context);
            }
        }
        return instance;
    }

    /**
     * AndroidController instance Getter method
     * 
     * @return AndroidController the instance of AndroidController object
     *         initialized by this class
     */
    public AndroidController getController() {
        return controller;
    }

    public AndroidDisplayManager getDisplayManager() {
        return controller != null ? controller.getAndroidDisplayManager()
                : null;
    }

    /**
     * AndroidLocalization instance Getter method
     * 
     * @return AndroidLocalization the instance of AndroidLocalization object
     *         used by this class
     */
    public Localization getLocalization() {
        return localization;
    }

    /**
     * AndroidConfiguration instance Getter method
     * 
     * @return AndroidConfiguration the instance of AndroidConfiguration object
     *         used by this class
     */
    public AndroidConfiguration getConfiguration() {
        return configuration;
    }

    /**
     * AndroidCustomization instance Getter method
     * 
     * @return AndroidCustomization the instance of AndroidCustomization object
     *         used by this class
     */
    public AndroidCustomization getCustomization() {
        return customization;
    }

    public SyncLock getSyncLock() {
        return syncLock;
    }

    public AndroidSyncModeHandler getSyncModeHandler() {
        return syncModeHandler;
    }

    /**
     * AndroidAppSyncSourceManager instance Getter method
     * 
     * @return AndroidAppSyncSourceManager the instance of
     *         AndroidAppSyncSourceManager initialized by this class
     */
    public AndroidAppSyncSourceManager getAppSyncSourceManager() {
        return appSyncSourceManager;
    }

    public TaskExecutor getNetworkTaskExecutor() {
        return networkTaskExecutor;
    }
    
    public ExternalAccountManager getExternalAccountManager() {
        return externalAccountManager;
    }

    /**
     * Initialize the application, global objects (log, services etc)
     */
    private synchronized void init(Context context) {
        
        if (initialized) {
            return;
        }

        Context appContext = context.getApplicationContext();
        
        // Init the timer handler
        TimerHandler.getInstance().setContext(appContext);
        
        // Init the tasks executor
        initTasksExecutor();
        
        // Init all the Controller components
        initObjects(appContext);

        // Init the SyncLock
        syncLock = new SyncLock();

        // Reset the proper log level
        Log.setLogLevel(configuration.getLogLevel());

        // Init the wifi lock if we need one
        initWifiLock(appContext);

        initBusGlobalListeners();

        // Init the TestRecorder if we are in test recording mode
        // ///////////////
        if (BuildInfo.TEST_RECORDING_ENABLED) {
            PimTestRecorder.getInstance(context, getDisplayManager(), getAppSyncSourceManager());
        }
        // //////////////////////////////////////////////////////////////////////////
        
        initialized = true;

        if (!configuration.getCredentialsCheckPending() && 
             configuration.getSyncMode() == Configuration.SYNC_MODE_AUTO) {
            
            Enumeration sse = appSyncSourceManager.getEnabledAndWorkingSources();
            Vector<AppSyncSource> sources = new Vector<AppSyncSource>();
            while(sse.hasMoreElements()) {
                AppSyncSource appSource = (AppSyncSource)sse.nextElement();
                sources.addElement(appSource);
            }
            AutoSyncServiceHandler autoSyncServiceHandler = new AutoSyncServiceHandler(appContext);
            // We wait one minute before triggering this sync, so we leave enough time to the device to perform
            // the whole boot process
            autoSyncServiceHandler.startSync(RefreshTrigger.SCHEDULED, sources, 60 * 1000); 
        }
    }

    private void initBusGlobalListeners() {
        // Register the sync task service (when a sync request is received, it
        // fires a sync)
        syncMessageHandler = new SyncMessageHandler(customization, configuration,
                                                    appSyncSourceManager, netStatus, networkTaskExecutor,
                                                    TasksPriorities.SYNC_PRIORITY);
        BusService.registerMessageHandler(com.funambol.client.engine.SyncMessage.class,
                syncMessageHandler);
    }

    private void initTasksExecutor() {
        // All the network tasks shall run in low priority (system level)
        networkTaskExecutor = new NetworkTaskExecutor(Thread.MIN_PRIORITY);
    }

    /**
     * Initializes the controller of this application. This represents the core
     * of the initialization logic; in particular: - Init the log system - Set
     * our own contacts activity as preferred - Try to create all the necessary
     * sources - Create the home screen controller - Set the
     * HomeScreenController reference to the Controller
     */
    private synchronized void initObjects(Context appContext) {

        //initializes object
        customization = new AndroidCustomization(appContext);
        
        // Init the log system
        initLog(appContext);
        
        // Install the app default uncaught exception handler
        AppDefaultUncaughtExceptionHandler.installHandler();
        
        // Create the app private data directory if it does not exist
        try {
            initPrivateDataDirectory(appContext);
        } catch (IOException ioe) {
            Log.error(TAG_LOG, "Cannot initialize private directory", ioe);
        }
        
        // Init the SQLite DB subsystem
        if (customization.getDBDirectory() != null) {
            SQLiteTable.setDBPath(customization.getDBDirectory());
        }
        
        // Set our own contacts activity as preferred
        setPreferredContactsActivity(appContext);

        localization = new AndroidLocalization(appContext);

        appSyncSourceManager = new AndroidAppSyncSourceManager(appContext, customization, localization);

        configuration = new AndroidConfiguration(appContext, customization, appSyncSourceManager);
        configuration.load();
        
        syncModeHandler = new AndroidSyncModeHandler(appContext, configuration);

        controller = new AndroidController(
                //TODO
                //this factory creates DisplayManager instance
                new AndroidActivitiesFactory(appContext),
                configuration,
                customization,
                localization,
                appSyncSourceManager,
                syncModeHandler,
                networkTaskExecutor,
                new AndroidEmailSender(localization.getLanguage("select_email_service")),
                appContext);

        configuration.setController(controller);
        appSyncSourceManager.setController(controller);

        appFlowNavigator = new AndroidAppFlowNavigator(appContext,
                configuration, customization);
        controller.setAppFlowNavigator(appFlowNavigator);

        externalAccountManager = new ExternalAccountManager(appContext, configuration);

        // Init the Android environment so that APIs can access to platform
        // specific values
        String dbName = customization.getFunambolSQLiteDbName();
        PlatformEnvironment.getInstance().init(appContext, dbName);

        // Register all the necessary sources
        Enumeration sources = customization.getAvailableSources();
        while (sources.hasMoreElements()) {
            Integer appSourceId = (Integer) sources.nextElement();
            try {
                AppSyncSource appSource = appSyncSourceManager.setupSource(
                        appSourceId.intValue(), configuration);

 
                appSyncSourceManager.registerSource(appSource);
            } catch (Exception e) {
                Log.error(TAG_LOG, "Cannot setup source: " + appSourceId, e);
            }
        }
        netStatus = new NetworkStatus();

        // Init controllers not related to a specific screen
        AndroidSendItemScreenController sendItemScreenController = new AndroidSendItemScreenController(
                controller, networkTaskExecutor, TasksPriorities.SEND_TO_PRIORITY);
        controller.setSendItemScreenController(sendItemScreenController);

        ServiceAuthenticatorScreenController serviceAuthenticatorScreenController = 
                new ServiceAuthenticatorScreenController(controller);
        controller.setServiceAuthenticatorScreenController(serviceAuthenticatorScreenController);

        initExternalServiceCache(appContext);

        initNotificationHandlers();
    }
    
    private void initPrivateDataDirectory(Context appContext) throws IOException {
        String directory = getApplicationDataDirectory(appContext);
        FileAdapter fa = new FileAdapter(directory);
        if (!fa.exists()) {
            // Create the default folder if it doesn't exist
            if (Log.isLoggable(Log.DEBUG)) {
                Log.debug(TAG_LOG, "Creating directory " + directory);
            }
            fa.mkdirs();
        }
    }

    /**
     * Finds the application data directory, considering the SD card mounted or not
     * @return
     */
    private String getApplicationDataDirectory(Context appContext) {
        String directory;
        if (AndroidUtils.isSDCardMounted()) {
            directory = customization.getPrivateDataDirectory();
        } else {
            directory = appContext.getFilesDir().getAbsolutePath();
        }
        if (Log.isLoggable(Log.DEBUG)) {
            Log.debug(TAG_LOG, "Required data directory is " + directory);
        }
        return directory;
    }

    private void initExternalServiceCache(Context appContext) {
        StringBuffer iconsBaseDirectory = new StringBuffer(getApplicationDataDirectory(appContext));
        iconsBaseDirectory.append(FileAdapter.getFileSeparator()).append("services");

        ExternalServiceCache externalServiceCache = new ExternalServiceCache(controller,
                networkTaskExecutor, TasksPriorities.REFRESH_SERVICES_PRIORITY,
                iconsBaseDirectory.toString());
        controller.setExternalServiceCache(externalServiceCache);
    }

    private void initNotificationHandlers() {
        notificationMessageHandler = new NotificationMessageHandler();
        BusService.registerMessageHandler(com.funambol.client.notification.NotificationMessage.class,
                notificationMessageHandler);
        refreshMessageHandler = new RefreshMessageHandler(controller);
        BusService.registerMessageHandler(com.funambol.client.engine.RefreshMessage.class,
                refreshMessageHandler);
    }

    private void initLog(Context appContext) {
        MultipleAppender ma = new MultipleAppender();

        String fileName = "synclog.txt";
        StringBuffer logsDir = new StringBuffer();
        logsDir.append(getApplicationDataDirectory(appContext));
        logsDir.append(FileAdapter.getFileSeparator());
        logsDir.append("logs");
        logsDir.append(FileAdapter.getFileSeparator());

        try {
            FileAdapter dir = new FileAdapter(logsDir.toString());
            if(!dir.exists()) {
                android.util.Log.v(TAG_LOG, "Creating logs directory " +
                        logsDir.toString());
                dir.mkdirs();
            }
        } catch(IOException ex) {
            // Failed to create logs directory
            android.util.Log.e(TAG_LOG, "Cannot create logs directory: " + ex);
        }

        FileAppender fileAppender = new FileAppender(logsDir.toString(), fileName);
        fileAppender.setLogContentType(!AndroidUtils.isSDCardMounted());
        fileAppender.setMaxFileSize(256 * 1024); // Set 256KB log size
        ma.addAppender(fileAppender);

        // If we are running in the emulator, we also use the AndroidLogger
        TelephonyManager tm = (TelephonyManager) appContext
                .getSystemService(Context.TELEPHONY_SERVICE);
        // must have android.permission.READ_PHONE_STATE
        String deviceId = tm.getDeviceId();
        if ("000000000000000".equals(deviceId) || "debug".equals(BuildInfo.MODE)) {
            // This is an emulator, or a debug build
            AndroidLogAppender androidLogAppender = new AndroidLogAppender(
                    "FunambolSync");
            ma.addAppender(androidLogAppender);
        }

        Log.initLog(ma, Log.TRACE);

        // for customer who wants to have log locked on specified level
        if (customization.lockLogLevel()) {
            Log.lockLogLevel(customization.getLockedLogLevel());
        }
        if (Log.isLoggable(Log.INFO)) {
            Log.info(TAG_LOG, "Memory card present: " + AndroidUtils.isSDCardMounted());
        }
        if (Log.isLoggable(Log.INFO)) {
            Log.info(TAG_LOG, "Log file created into: " + logsDir + fileName);
        }
    }

    /**
     * Acquires a lock on wifi connection if it is not already locked
     */
    public void acquireWiFiLock(Context context) {
        if (wifiLock == null) {
            WifiManager wm = (WifiManager) context
                    .getSystemService(Context.WIFI_SERVICE);
            // WIFI_FULL_MODE is bugged, couldn't work with some devices :(
            // http://www.mail-archive.com/android-developers@googlegroups.com/msg145553.html
            wifiLock = wm.createWifiLock(WifiManager.WIFI_MODE_FULL,
                    "Funambol sync");
        }

        if (!wifiLock.isHeld()) {
            wifiLock.setReferenceCounted(false);
            wifiLock.acquire();
            if (Log.isLoggable(Log.DEBUG)) {
                Log.debug(TAG_LOG, "wifi lock=" + wifiLock.toString());
            }
        }
    }

    /**
     * Releases a lock on wifi, only if it's already locked and if the bandwidth
     * saver feature is not enabled.
     * 
     * @param requestComesFromSync
     *            True if the release request comes from a sync. In this case,
     *            the lock is released only if the bandwidth option is disabled.
     *            If false, releases the wifi without further checks
     */
    public void releaseWiFiLock(boolean requestComesFromSync) {
        if (wifiLock != null && wifiLock.isHeld()) {
            if (!requestComesFromSync
                    || (requestComesFromSync && !configuration
                            .getBandwidthSaverActivated())) {
                if (Log.isLoggable(Log.INFO)) {
                    Log.info(TAG_LOG, "Releasing wifi lock");
                }
                wifiLock.release();
            }
        }
    }

    private void initWifiLock(Context context) {
        if (configuration.getBandwidthSaverActivated()) {
            acquireWiFiLock(context);
        }
    }

    private void setPreferredContactsActivity(Context context) {

        // This method is only available in Android <= 2.1
        // On newer versions it can throw an exception, so we shall not invoke
        // it
        if (Build.VERSION.SDK_INT >= 8) {
            return;
        }

        PackageManager pm = context.getPackageManager();

        // Remove the native contacts app as default
        pm.clearPackagePreferredActivities("com.android.contacts");

        Intent edit_intent = new Intent("android.intent.action.EDIT");
        Intent insert_intent = new Intent("android.intent.action.INSERT");

        edit_intent.addCategory("android.intent.category.DEFAULT");
        insert_intent.addCategory("android.intent.category.DEFAULT");

        edit_intent.setData(ContentUris.withAppendedId(
                ContactsContract.RawContacts.CONTENT_URI, 100));
        insert_intent.setData(ContactsContract.RawContacts.CONTENT_URI);

        List<ResolveInfo> editList = pm.queryIntentActivities(edit_intent,
                PackageManager.MATCH_DEFAULT_ONLY
                        | PackageManager.GET_RESOLVED_FILTER);

        List<ResolveInfo> insertList = pm.queryIntentActivities(insert_intent,
                PackageManager.MATCH_DEFAULT_ONLY
                        | PackageManager.GET_RESOLVED_FILTER);

        ResolveInfo editRI = null;
        ResolveInfo insertRI = null;

        ComponentName[] editRIS = new ComponentName[editList.size()];
        ComponentName[] insertRIS = new ComponentName[insertList.size()];

        if (editList.size() > 0) {
            editRI = editList.get(0);
            for (int i = 0; i < editList.size(); i++) {
                ResolveInfo r = editList.get(i);
                editRIS[i] = new ComponentName(r.activityInfo.packageName,
                        r.activityInfo.name);
            }
        }
        if (insertList.size() > 0) {
            insertRI = insertList.get(0);
            for (int i = 0; i < insertList.size(); i++) {
                ResolveInfo r = insertList.get(i);
                insertRIS[i] = new ComponentName(r.activityInfo.packageName,
                        r.activityInfo.name);
            }
        }

        pm.addPreferredActivity(editRI.filter, editRI.match, editRIS,
                new ComponentName(BuildInfo.PACKAGE_NAME,
                        "com.funambol.android.edit_contact.AndroidEditContact"));
        pm.addPreferredActivity(insertRI.filter, insertRI.match, insertRIS,
                new ComponentName(BuildInfo.PACKAGE_NAME,
                        "com.funambol.android.edit_contact.AndroidEditContact"));
    }

}
