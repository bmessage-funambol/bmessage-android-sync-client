/**
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2011 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.activities;

import java.util.Vector;
import java.util.Enumeration;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.funambol.androidsync.R;
import com.funambol.android.AppInitializer;
import com.funambol.client.ui.AllSourcesSummaryWidget;
import com.funambol.client.controller.AllSourcesSummaryController;
import com.funambol.client.ui.view.SourceBarView;
import com.funambol.util.Log;

/**
 * All sources resume screen. Inside this fragment a resume of all sources status are shown.
 */
public class AllSourcesSummaryFragment extends BasicFragment implements AllSourcesSummaryWidget {
    private static final String TAG_LOG = AllSourcesSummaryFragment.class.getSimpleName();

    private AllSourcesSummaryController mAllSourcesResumeController;
    private LinearLayout mSourcesContainer;

    private Vector pendingUITasks = new Vector();;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if(mAllSourcesResumeController == null) {
            AppInitializer appInitializer = AppInitializer.i(getContainerActivity());
            mAllSourcesResumeController = new AllSourcesSummaryController(
                    appInitializer.getController(), this);
            setWidgetController(mAllSourcesResumeController);
        }
        View v = inflater.inflate(R.layout.fraallsourcessummary, container, false);
        mSourcesContainer = (LinearLayout)v.findViewById(R.id.allsourcessummary_laycontainer);
        mAllSourcesResumeController.refreshUI();
        return v;
    }
   
    @Override
    public void onDestroy() {
        mAllSourcesResumeController.onDestroy();
        mAllSourcesResumeController = null;
        mSourcesContainer.removeAllViews();
        setWidgetController(null);
        super.onDestroy();
    }
    
    /**
     * This method can be used to clear the current list of bars.
     */
    public void resetList() {
        // Remove any pending update task
        Enumeration tasks = pendingUITasks.elements();
        while(tasks.hasMoreElements()) {
            AddThumbnailViewRunnable r = (AddThumbnailViewRunnable)tasks.nextElement();
            r.cancel();
        }
        mSourcesContainer.removeAllViews();
    }

    public void addSourceBarView(final SourceBarView view) {
        if(view instanceof View) {
            AddThumbnailViewRunnable add = new AddThumbnailViewRunnable((View)view, mSourcesContainer);
            pendingUITasks.addElement(add);
            Activity activity = (Activity)getContainerUiScreen();
            if(activity != null) {
                activity.runOnUiThread(add);
            } else {
                Log.error(TAG_LOG, "Activity reference is null");
            }
        } else {
            throw new IllegalArgumentException("Invalid thumbnails view: " + view);
        }
    }

    private class AddThumbnailViewRunnable implements Runnable {

        private View view;
        private LinearLayout container;

        private boolean cancelled;

        public AddThumbnailViewRunnable(View view, LinearLayout container) {
            this.view = view;
            this.container = container;
        }
        
        public void run() {
            if(!isCancelled()) {
                container.addView(view);
            }
            pendingUITasks.removeElement(this);
        }

        public void cancel() {
            cancelled = true;
        }

        public boolean isCancelled() {
            return cancelled;
        }
    }
}
