/*
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2009 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */
package com.funambol.android.activities.view;

import android.view.View;

import com.funambol.android.ContactsImporter;
import com.funambol.androidsync.R;
import com.funambol.client.source.AppSyncSource;
import com.funambol.client.source.ExternalAppManager;
import com.funambol.client.ui.Widget;
import com.funambol.util.Log;

public class AndroidContactsThumbnailsView extends AndroidSourceNoThumbnailsView {
    private String TAG_LOG = AndroidContactsThumbnailsView.class.getSimpleName();
    
    public AndroidContactsThumbnailsView(Widget widget, AppSyncSource source) {
        super(widget, source);
    }

    @Override
    protected View.OnClickListener createOnClickListener() {
        return new ContactsOnClickListener();
    }
   
    @Override
    protected int getVeryFirstIconId() {
        int resId = R.drawable.common_selcontactarrow;
        return resId;
    }
 
    
    @Override
    protected void updateContentView(final Status status) {
        super.updateContentView(status);
        imageView.post(new Runnable() {
            public void run() {
                if (status == Status.VERY_FIRST_EMPTY) {
                    imageView.setImageResource(R.drawable.common_selcontactarrow);
                } else {
                    imageView.setImageResource(R.drawable.common_selcontact);
                }
            }
        });
    }
    
    private class ContactsOnClickListener implements View.OnClickListener {

        public void onClick(View view) {
            if (status == Status.ENABLED) {
                ExternalAppManager manager = appSource.getAppManager();
                if (manager != null) {
                    try {
                        manager.launch(appSource, null);
                    } catch (Exception e) {
                        Log.error(TAG_LOG, "Cannot launch external app manager, because: " + e);
                    }
                } else {
                    Log.error(TAG_LOG, "No external manager associated to source: " + appSource.getName());
                }
            } else if (status == Status.VERY_FIRST_EMPTY) {
                Thread t = new Thread() {
                    public void run() {
                        // In this case we shall open the import contacts
                        ContactsImporter importer = new ContactsImporter(activity);
                        importer.importContacts(controller.getMainScreenController().getMainScreen());
                    }
                };
                t.start();
            }
        }
    }
}
