package com.funambol.android.activities.view;

import com.funambol.androidsync.R;
import com.funambol.client.ui.view.SourceThumbnailsView.OnMoreListener;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class LinkToMoreView extends LinearLayout {

    public LinkToMoreView(Context context, final OnMoreListener listener) {
        super(context);
        View.inflate(getContext(), R.layout.vwmorethumbnails, this);
        setOnMoreListener(listener);
        setClickable(true);
    }

    public void setOnMoreListener(final OnMoreListener listener) {
        setOnClickListener(new OnClickListener() {
            
            public void onClick(View v) {
                listener.onMore();
            }
        });        
    }
}
