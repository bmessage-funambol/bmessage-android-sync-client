/**
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2011 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.activities.view;

import java.util.ArrayList;
import java.util.List;

import com.funambol.android.activities.BasicFragment;
import com.funambol.androidsync.R;
import com.funambol.client.ui.Bitmap;
import com.funambol.client.ui.view.ActionBarAction;
import com.funambol.client.ui.view.ActionBarWidget;
import com.funambol.util.Log;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.funambol.client.controller.ActionBarController;

/**
 * Android UI implementation for {@link ActionBarWidget} widget 
 *
 */
public class AndroidActionBarView extends RelativeLayout implements ActionBarWidget {
    private static final String TAG_LOG = AndroidActionBarView.class.getSimpleName();
    private static final int WRONG_VALUE = Integer.MIN_VALUE;
    
    private LayoutInflater mInflater;
    private int layoutType;

    private ActionBarController controller;
    
    public AndroidActionBarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        
        //get the actions container (ViewGroup)
        //mActionsContainer = (ViewGroup) findViewById(R.id.actionbar_layactions);
    }

    public ActionBarController getController() {
        return controller;
    }

    public void setController(ActionBarController controller) {
        this.controller = controller;
    }
    
    public void setAction(ActionBarAction actionToAdd, int position) {
        final int imgResourceId = getResourceIdForPosition(position);
        if (WRONG_VALUE == imgResourceId) {
            return; //wrong layout
        }
        View button = findViewById(imgResourceId);
        button.post(new SetActionRunnable(button, actionToAdd, layoutType));
    }

    private class SetActionRunnable implements Runnable {

        private View button;
        private ActionBarAction actionToAdd;
        private int originalLayoutType;

        public SetActionRunnable(View button, ActionBarAction actionToAdd, int originalLayoutType) {
            this.button = button;
            this.actionToAdd = actionToAdd;
            this.originalLayoutType = originalLayoutType;
        }

        public void run() {

            // Check if the current layout is still valid
            if(layoutType != originalLayoutType) {
                return;
            }

            button.setTag(actionToAdd);

            //hide the button
            if (null == actionToAdd) {
                button.setVisibility(View.GONE);
                return;
            }

            button.setOnClickListener(actionOnClickListener);
            button.setVisibility(View.VISIBLE);
            button.setEnabled(actionToAdd.isEnabled());

            if(button instanceof ImageButton) {
                ImageButton imageButton = (ImageButton)button;
                Bitmap bitmap = actionToAdd.getActionBitmap();
                if (null != bitmap) {
                    Integer drawableId = (Integer) bitmap.getOpaqueDescriptor();
                    if (null != drawableId) {
                        imageButton.setImageResource(drawableId);
                    }
                } else {
                    //remove button icons
                    imageButton.setImageBitmap(null);
                }
            } else if(button instanceof Button) {
                Button textButton = (Button)button;
                ActionBarAction av = actionToAdd;
                textButton.setText(av.getText());
            }
        }
    }

    private View.OnClickListener actionOnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            Object tag = v.getTag();
            if (tag instanceof ActionBarAction) {
                ActionBarAction actionBarAction = (ActionBarAction) tag;
                if (Log.isLoggable(Log.TRACE)) {
                    Log.trace(TAG_LOG, "Clicked on actionbar button with id " + actionBarAction.getActionId());
                }
                actionBarAction.executeAction(actionBarAction);
            }
        }
    };

    public void setTitle(final String title) {
        //avoid issues when this method is called from external thread
        post(new Runnable() {
            public void run() {
                TextView lblTitle = (TextView) findViewById(R.id.actionbar_lbltitle);
                if (null != lblTitle) {
                    lblTitle.setText(title);
                }
            }
        });
    }

    /**
     * Enables / disables an action in the actionbar.
     * 
     * @param actionId id of the action to set
     * @param enabled true for enabled, false for disabled
     */
    public void setEnabled(final int actionId, final boolean enabled) {
        //avoid issues when this method is called from external thread
        post(new Runnable() {
            public void run() {
                List<View> actionViews = getActionViewByActionId(actionId);
                if (Log.isLoggable(Log.TRACE)) {
                    Log.trace(TAG_LOG,
                            "Setting enabled status to " + enabled + 
                            " for action id " + actionId + 
                            " for " + actionViews.size() + " view(s)");
                }
                for (View view : actionViews) {
                    view.setEnabled(enabled);
                }
            }
        });
    }

    /**
     * Set actionbar layout
     */
    public void setLayoutType(int layoutType) {
        this.layoutType = layoutType;
        
        //hide all buttons
        setVisibility(R.id.actionbar_btnfirstdoubleaction, false);
        setVisibility(R.id.actionbar_btnseconddoubleaction, false);
        setVisibility(R.id.actionbar_btnthirddoubleaction, false);
        setVisibility(R.id.actionbar_btnforthdoubleaction, false);
        setVisibility(R.id.actionbar_btnlefttextaction, false);
        setVisibility(R.id.actionbar_btnrighttextaction, false);
        setVisibility(R.id.actionbar_btnleftimageaction, false);
        setVisibility(R.id.actionbar_btnrightimageaction, false);
    }
    
    public void setTransparentBackground(boolean transparent) {
        //change backgroud of actionbar, according to the transparent switch
        setBackgroundResource(
                transparent
                    ? R.drawable.common_shaseethrubackground
                    : R.drawable.actionbar_itembackground);
    }
    
    /**
     * Find the view associated with a particular position of the actionbar,
     * based on current actionbar layout
     * 
     * @param position
     * @return
     */
    private int getResourceIdForPosition(int position) {
        switch (layoutType) {
        case LAYOUTTYPE_1x1:
            switch (position) {
            case POSITION_LEFT:
                return R.id.actionbar_btnleftimageaction;
            case POSITION_RIGHT:
                return R.id.actionbar_btnrightimageaction;
            }
            break;

        case LAYOUTTYPE_TEXTxTEXT:
            switch (position) {
            case POSITION_LEFT:
                return R.id.actionbar_btnlefttextaction;
            case POSITION_RIGHT:
                return R.id.actionbar_btnrighttextaction;
            }
            break;

        case LAYOUTTYPE_2x2:
            switch (position) {
            case POSITION_FIRST_LEFT:
                return R.id.actionbar_btnfirstdoubleaction;
            case POSITION_SECOND_LEFT:
                return R.id.actionbar_btnseconddoubleaction;
            case POSITION_FIRST_RIGHT:
                return R.id.actionbar_btnthirddoubleaction;
            case POSITION_SECOND_RIGHT:
                return R.id.actionbar_btnforthdoubleaction;
            }
            break;
        }
         
        return WRONG_VALUE;
    }
    
    
    /**
     * Finds what buttons of actionbar contain {@link ActionBarAction} with
     * requested id.
     * 
     * @param actionId the action id to find
     * @return a list of buttons that contains the action
     */
    private List<View> getActionViewByActionId(int actionId) {
        List<View> foundViews = new ArrayList<View>();

        int[] viewIds = new int[] {
                R.id.actionbar_btnfirstdoubleaction,
                R.id.actionbar_btnseconddoubleaction,
                R.id.actionbar_btnthirddoubleaction,
                R.id.actionbar_btnforthdoubleaction,
                R.id.actionbar_btnlefttextaction,
                R.id.actionbar_btnrighttextaction,
                R.id.actionbar_btnleftimageaction,
                R.id.actionbar_btnrightimageaction };
        
        for (int viewId : viewIds) {
            View currentView = findViewById(viewId);
            if (null == currentView) continue;
            ActionBarAction actionView = (ActionBarAction) currentView.getTag();
            if (null == actionView) continue;
            if (actionId == actionView.getActionId()) foundViews.add(currentView);
        }
        
        return foundViews;
    }

    /**
     * Show/hide a view
     * 
     * @param resourceId
     * @param visibility
     */
    private void setVisibility(int resourceId, boolean visibility) {
        View view = findViewById(resourceId);
        if (null != view) view.setVisibility(visibility ? VISIBLE : GONE);
    }
     
    
    // ----------------------------------------------------------------- Widget
    public Object getContainerUiScreen() {
        //cycles thru parents until main activity is reached
        ViewParent parent = getParent();
        while (true) {
            if (null == parent) {
                break;
            }
            if (parent instanceof Activity) {
                return parent;
            }
            if (parent instanceof BasicFragment) {
                return ((BasicFragment) parent).getContainerActivity();
            }
            if (parent instanceof Fragment) {
                return ((Fragment) parent).getActivity();
            }
            parent = parent.getParent();
        }
        return null;
    }
     
    public void pause() {
    }

    public void resume() {
    }

    public void destroy() {
        
    }
    // ------------------------------------------------------------------------

    
    // ------------------------ Old code, can be used in future implementations
    /**
     * Add separator to the action
     * @param actionsContainer
     * @param index -1 add to the end, otherwise add to the index-position
     */
    private void addSeparator(ViewGroup actionsContainer, int index) {
        //inflate the separator and add it to the group
        View viewToAdd = mInflater.inflate(R.layout.vwactionbaractionseparator, null);
        if (WRONG_VALUE == index) {
            index = actionsContainer.getChildCount(); 
        }
        actionsContainer.addView(viewToAdd, index);
    }

    /**
     * Inflates a {@link View} with the given {@link Action}.
     * @param actionBarActionView the action to inflate
     * @return a view
     */
    private View inflateAction(ActionBarAction actionBarActionView) {
        //action image must have same size of actionbar
        int actionbarWidth = (int) getResources().getDimension(R.dimen.actionbar_actionwidth);
        int actionbarHeight = (int) getResources().getDimension(R.dimen.actionbar_actionheight);
        DisplayMetrics dm = getContext().getResources().getDisplayMetrics();
        int padding = (int) (2 * dm.density);
        
         
        ImageButton imageButton = new ImageButton(getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(actionbarWidth, actionbarHeight);
        imageButton.setPadding(padding, padding, padding, padding);
        imageButton.setLayoutParams(layoutParams);
        imageButton.setScaleType(ScaleType.FIT_CENTER);
        //background has the selection drawable when pressed
        imageButton.setBackgroundResource(R.drawable.actionbar_selbtnaction);
         
        //and the image has only the image of the action
        if (null != actionBarActionView.getActionBitmap()) {
            int drawableId = (Integer) actionBarActionView.getActionBitmap().getOpaqueDescriptor();
            imageButton.setImageResource(drawableId);
        } else {
            imageButton.setImageDrawable(null);
        }
         
        //TODO
        //set the text and the style of the text of the button
    
        imageButton.setTag(actionBarActionView);
        imageButton.setOnClickListener(actionOnClickListener);
        
        return imageButton;
    }
}
