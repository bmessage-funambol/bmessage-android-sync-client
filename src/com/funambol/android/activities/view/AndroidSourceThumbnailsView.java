/*
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2009 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.activities.view;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.funambol.android.AndroidUtils;
import com.funambol.android.AppInitializer;
import com.funambol.androidsync.R;

import com.funambol.client.configuration.Configuration;
import com.funambol.client.customization.Customization;
import com.funambol.client.localization.Inflexion;
import com.funambol.client.localization.Localization;
import com.funambol.client.source.AppSyncSource;
import com.funambol.client.ui.Screen;
import com.funambol.client.ui.Widget;
import com.funambol.client.ui.view.SourceThumbnailsView;
import com.funambol.client.ui.view.ThumbnailView;
import com.funambol.util.Log;
import com.funambol.util.StringUtil;

/**
 * Represents a generic source view containing thumbnails horizontally
 * scrollable
 */
public class AndroidSourceThumbnailsView extends LinearLayout implements SourceThumbnailsView {

    private static final String TAG_LOG = "AndroidSourceThumbnailsView";

    private final int DEFAULT_THUMBS_PADDING = 1; //dip

    private AppSyncSource appSource;

    private TextView titleView;
    private HorizontalScrollView scrollView;
    private LinearLayout thumbsContainer;
    private LinkToMoreView more = null;

    private Localization localization;
    private Customization customization;
    private Configuration configuration;

    private Activity activity;
    private Widget   widget;
    
    private enum Status {POPULATED, DISABLED, EMPTY, VERY_FIRST_EMPTY};
    private Status status;
    private View statusViews[] = new View[Status.values().length];

    public AndroidSourceThumbnailsView(Widget widget, AppSyncSource source) {
        super((Context)widget.getContainerUiScreen());

        this.appSource = source;
        this.widget = widget;
        this.activity = (Activity)widget.getContainerUiScreen();

        AppInitializer appInitializer = AppInitializer.i(activity.getApplicationContext());
        localization = appInitializer.getLocalization();
        customization = appInitializer.getCustomization();
        configuration = appInitializer.getConfiguration();

        scrollView = new HorizontalScrollView(getContext());
        scrollView.setBackgroundColor(0xFF2D2D2D);
        scrollView.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.FILL_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        thumbsContainer = new LinearLayout(getContext());
        thumbsContainer.setOrientation(LinearLayout.HORIZONTAL);
        thumbsContainer.setPadding(
                AndroidUtils.dipToPx(DEFAULT_THUMBS_PADDING, getContext()),
                AndroidUtils.dipToPx(DEFAULT_THUMBS_PADDING, getContext()),
                0,
                AndroidUtils.dipToPx(DEFAULT_THUMBS_PADDING, getContext()));
        thumbsContainer.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        scrollView.addView(thumbsContainer);

        setOrientation(LinearLayout.VERTICAL);
        setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.FILL_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        final View view = View.inflate(getContext(), R.layout.vwbucketseparator, null);
        titleView = (TextView)view.findViewById(R.id.sourceviewtitle_lbltitle);

        for(Status currentStatus : Status.values()) {
            if (currentStatus == Status.POPULATED) {
                statusViews[currentStatus.ordinal()] = scrollView;
            } else {
                statusViews[currentStatus.ordinal()] = createPlaceHolderView(activity, currentStatus);
            }
        }
        
        addView(view);
        
        // Now add all the possible views and set only one as visible
        for(View v : statusViews) {
            addView(v);
        }
       
        if (!appSource.getConfig().getEnabled()) {
            status = Status.DISABLED;
        } else if (configuration.getMediaVeryFirstSync()) {
            status = Status.VERY_FIRST_EMPTY;
        } else {
            status = Status.EMPTY;
        }
        updateContentView(status);
    }
    

    public void addThumbnail(final ThumbnailView thumb, final String newTitle) {
        addThumbnail(thumb, 0, newTitle);
    }

    public void addThumbnail(final ThumbnailView thumb, final int index, final String newTitle) {
        if(Log.isLoggable(Log.DEBUG)) {
            Log.debug(TAG_LOG, "Adding thumbnail to source view");
        }
        activity.runOnUiThread(new Runnable() {
            public void run() {
                View thumbView = (View)thumb.getThumbnailView();
                thumbView.setPadding(
                        0,
                        0,
                        AndroidUtils.dipToPx(DEFAULT_THUMBS_PADDING, getContext()),
                        0);
                if (index < thumbsContainer.getChildCount()) {
                    thumbsContainer.addView(thumbView, index);
                } else {
                    thumbsContainer.addView(thumbView);
                }
                if (newTitle != null) {
                    titleView.setText(newTitle);
                }
                // Remove place holder view if present
                if (status == Status.VERY_FIRST_EMPTY || status == Status.EMPTY) {
                    status = Status.POPULATED;
                    updateContentView(status);
                }
            }
        });
        // If we add at least one item, then this is no longer a very first sync
        if(configuration.getMediaVeryFirstSync()) {
            configuration.setMediaVeryFirstSync(false);
            configuration.save();
        }
    }
    
    public void showLinkToMore(final OnMoreListener listener) {
        Log.debug(TAG_LOG, "Showing link to more in source bar");
        activity.runOnUiThread(new Runnable() {

            public void run() {
                if (more == null) {
                    more = new LinkToMoreView(getContext(), listener);
                    more.setPadding(
                            0,
                            0,
                            AndroidUtils.dipToPx(DEFAULT_THUMBS_PADDING, getContext()),
                            0);
                }
                more.setOnMoreListener(listener);
                if (thumbsContainer.indexOfChild(more) == -1) {
                    thumbsContainer.addView(more);
                }
            }
        });
    }

    public void hideLinkToMore() {
        Log.debug(TAG_LOG, "Hiding link to more in source bar");
        activity.runOnUiThread(new Runnable() {

            public void run() {
                if (more != null) {
                    thumbsContainer.removeView(more);
                }
            }
        });
    }

    public void replaceThumbnail(final ThumbnailView thumb, final int index) {
        activity.runOnUiThread(new Runnable() {
            public void run() {
                View thumbView = (View)thumb.getThumbnailView();
                thumbView.setPadding(
                        0,
                        0,
                        AndroidUtils.dipToPx(DEFAULT_THUMBS_PADDING, getContext()),
                        0);
                thumbsContainer.removeViewAt(index);
                thumbsContainer.addView(thumbView, index);
                // Remove place holder  view if present
                if (status == Status.EMPTY || status == Status.VERY_FIRST_EMPTY) {
                    status = Status.POPULATED;
                    updateContentView(status);
                }
            }
        });
    }

    public void removeThumbnail(final int index, final String newTitle) {
        activity.runOnUiThread(new Runnable() {
            public void run() {
                thumbsContainer.removeViewAt(index);
                if (newTitle != null) {
                    titleView.setText(newTitle);
                }
                // Restore placeHolder view if no items are left
                if (status == Status.POPULATED && thumbsContainer.getChildCount() == 0) {
                    status = Status.EMPTY;
                    updateContentView(status);
                }
            }
        });
    }

    public void setTitle(final String title) {
        activity.runOnUiThread(new Runnable() {
            public void run() {
                titleView.setText(title);
            }
        });
    }

    public ThumbnailView createThumbnailView() {
        return appSource.createThumbnailView((Screen)widget.getContainerUiScreen());
    }
    
    public void destroy() {
    }
    
    public void resume() {
    }
    
    public void pause() {
    }
    
    public Object getContainerUiScreen() {
        return activity;
    }

    @Override
    public void setEnabled(final boolean enabled) {
        activity.runOnUiThread(new Runnable() {
            public void run() {
                if (enabled && status == Status.DISABLED) {
                    if (thumbsContainer.getChildCount() > 0) {
                        status = Status.POPULATED;
                    } else {
                        status = Status.EMPTY;
                    }
                    updateContentView(status);
                } else if (!enabled && status != Status.DISABLED){
                    status = Status.DISABLED;
                    updateContentView(status);
                }
            }
        });
    }
    
    private void updateContentView(Status newStatus) {
        for(View v : statusViews) {
            v.setVisibility(View.GONE);
        }
        View currentView = statusViews[newStatus.ordinal()];
        currentView.setVisibility(View.VISIBLE);
    }
    
    private LinearLayout createPlaceHolderView(Context context, Status status) {

        LinearLayout placeHolder = new LinearLayout(context);
        View.inflate(getContext(), R.layout.vwsourceviewplaceholder, placeHolder);

        TextView textView = (TextView)placeHolder.findViewById(R.id.sourceviewplaceholder_lblPlaceholder);
        ImageView imageView = (ImageView)placeHolder.findViewById(R.id.sourceviewplaceholder_imgPlaceholder);

        String text;
        if (status == Status.VERY_FIRST_EMPTY) {
            text = appSource.getEmptyHint();
        } else if (status == Status.DISABLED) {
            text = localization.getLanguage("source_placeholder_disabled_text");
            text = StringUtil.replaceAll(text, "__THING__",
                             appSource.getItemInflexion().titleCase().using(Inflexion.SINGULAR).toString());
            
        } else {
            text = localization.getLanguage("source_placeholder_text");
            text = StringUtil.replaceAll(text, "__THING__",
                                         appSource.getItemInflexion().using(Inflexion.SINGULAR).toString());
        }
        textView.setText(text);

        int resId = ((Integer)customization.getSourcePlaceHolderIcon(
                appSource.getId()).getOpaqueDescriptor()).intValue();
        imageView.setImageResource(resId);

        return placeHolder;
    }
}
