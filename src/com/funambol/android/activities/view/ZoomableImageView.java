/**
 * Copyright (C) 2010 Alfredo Morresi
 * 
 * This file is part of RainbowLibs project.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Copyright (C) 2010 Alfredo Morresi
 * 
 * This file is part of RainbowLibs project.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; If not, see <http://www.gnu.org/licenses/>.
 */

package com.funambol.android.activities.view;


import com.funambol.android.activities.view.MultiTouchController.*;
import com.funambol.util.Log;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * View that show an image and allow to zoom and pan on it
 * 
 * @author Alfredo "Rainbowbreeze" Morresi
 *
 */
public class ZoomableImageView
    extends View
    implements MultiTouchObjectCanvas<ZoomableImageView.Img>
{
    //---------------------------------------------------------- Private fields
    private static final int UI_MODE_ROTATE = 1;
    private static final int UI_MODE_ANISOTROPIC_SCALE = 2;

    private MultiTouchController<ZoomableImageView.Img> multiTouchController;
    private GestureDetector mGestureDetector;
    private Img mImgToDisplay;
    private int mLastWidth, mLastHeight;

    private int mUIMode = UI_MODE_ROTATE;
    
    
    //------------------------------------------------------------ Constructors
    public ZoomableImageView(Context context) {
        super(context);
        initVars(context);
    }

    /**
     * Generally, this is the constructor called
     */
    public ZoomableImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initVars(context);
    }    
    
    public ZoomableImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initVars(context);
    }
    
    
    //------------------------------------------------------- Public properties
    private boolean mAllowRotation;
    public boolean getAllowRotation() {
        return mAllowRotation;
    }
    public void setAllowRotation(boolean newValue) {
        mAllowRotation = newValue;
        if (null != mImgToDisplay) mImgToDisplay.setAllowRotation(newValue);
    }
    
    private boolean mTabletMode;
    public boolean getTabletMode() {
        return mTabletMode;
    }
    public void setTabletMode(boolean newValue) {
        mTabletMode = newValue;
        if (null != mImgToDisplay) mImgToDisplay.setTabletMode(newValue);
    }
    
    

    private boolean mLatestTouchEventProcessed;
    public boolean getLatestTouchEventProcessed() {
        return mLatestTouchEventProcessed;
    }
    
    private boolean mOverrideOnTouchEvent;
    public void setOverrideOnTouchEvent(boolean newValue) {
        mOverrideOnTouchEvent = newValue;
    }
    
    //------------------------------------------------------------------ Events
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (mOverrideOnTouchEvent) {
            //customized touch events management
            return onAlternativeTouchEvent(event);
        } else {
            //standard touch events management
            return super.onTouchEvent(event);
        }
    }

    /**
     * Event to call when the standard {@link #onTouchEvent(MotionEvent)} is
     * not overridden (using {@link #setOverrideOnTouchEvent(boolean)} with 
     * false value)
     * 
     * @param event
     * @return
     */
    public boolean onAlternativeTouchEvent(MotionEvent event) {
        boolean processed;
        //call standard gesture listener, just to be informed about double click
        processed = mGestureDetector.onTouchEvent(event);
        if (processed) return processed;
        
        processed = multiTouchController.onTouchEvent(event);
        //checks if, when the user release the image, the image have to be adjusted
        if (event.getAction() == MotionEvent.ACTION_POINTER_1_UP
                || event.getAction() == MotionEvent.ACTION_POINTER_2_UP
                || event.getAction() == MotionEvent.ACTION_POINTER_3_UP) {
            if (null != mImgToDisplay) mImgToDisplay.centerImageInsideDrawableAreaIfNeeded();
        }
        return processed;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (null != mImgToDisplay) mImgToDisplay.draw(canvas);
    }
    
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {  
        super.onSizeChanged(w, h, oldw, oldh);
        if (null != mImgToDisplay && (0 != w || 0 != h)) {
            mLastWidth = w;
            mLastHeight = h;
            mImgToDisplay.setViewSize(mLastWidth, mLastHeight);
        }
    }

    /**
     * Free image resources, if needed, to prevent OutOfMemory exception
     */
    public void onDestroy() {
        if (null != mImgToDisplay) mImgToDisplay.unload();
    }

    
    //---------------------------------------------------------- Public methods
    /**
     * Get the image that is under the single-touch point, or return null (cancelling
     * the drag operation) if none
     */
    public Img getDraggableObjectAtPoint(PointInfo touchPoint) {
        return mImgToDisplay;
    }

    /**
     * Get the current position and scale of the selected image.
     * Called whenever a drag starts or is reset.
     */
    public void getPositionAndScale(Img img, PositionAndScale objPosAndScaleOut) {
        objPosAndScaleOut.set(
                img.getCenterX(), img.getCenterY(),
                (mUIMode & UI_MODE_ANISOTROPIC_SCALE) == 0,
                (img.getScaleX() + img.getScaleY()) / 2,
                (mUIMode & UI_MODE_ANISOTROPIC_SCALE) != 0,
                img.getScaleX(), img.getScaleY(),
                (mUIMode & UI_MODE_ROTATE) != 0,
                img.getAngle());
    }

    /**
     * Set the position and scale of the dragged/stretched image.
     */
    public boolean setPositionAndScale(Img obj, PositionAndScale newObjPosAndScale, PointInfo touchPoint) {
        boolean ok = false;
        if (null != mImgToDisplay)
            ok = mImgToDisplay.setPos(newObjPosAndScale);
        if (ok) invalidate();
        mLatestTouchEventProcessed = ok;
        return ok;
    }

    /**
     * Select an object for dragging.
     * Called whenever an object is found to be under the point (non-null is returned by getDraggableObjectAtPoint())
     * and a drag operation is starting.
     * Called with null when drag op ends.
     */
    public void selectObject(Img obj, PointInfo touchPoint) {
        invalidate();
    }
    
    
    /**
     * Assigns the control image using a resource
     * @param res
     * @param resId
     */
    public void setImageResource(int resId) {
        if (null != mImgToDisplay) {
            mImgToDisplay.unload();
        }
        if (0 != resId) {
            //if called during activity initialization, width and height are both 0
            mImgToDisplay = new Img(getResources(), resId, mLastWidth, mLastHeight);
            performCommonImgInitializationTasks();
        }
    }
    
    /**
     * Assigns the control image using a Bitmap as source
     * @param bitmapDrawable
     */
    public void setImageBitmap(Bitmap bitmap) {
        if (null != bitmap) {
            BitmapDrawable bitmapDrawable = new BitmapDrawable(bitmap);
            setImageDrawable(bitmapDrawable);
        } else {
            //unload the image
            if (null != mImgToDisplay) {
                mImgToDisplay.unload();
            }
        }
    }

    /**
     * Assigns the control image using a {@link BitmapDrawable} as source
     * @param drawable
     */
    public void setImageDrawable(BitmapDrawable drawable) {
        if (null != mImgToDisplay) {
            mImgToDisplay.unload();
        }
        if (null != drawable) {
            //if called during activity initialization, width and height are both 0
            mImgToDisplay = new Img(drawable, mLastWidth, mLastHeight);
            performCommonImgInitializationTasks();
        }
    }

    /**
     * 
     */
    private void performCommonImgInitializationTasks() {
        mImgToDisplay.setAllowRotation(mAllowRotation);
        mImgToDisplay.setTabletMode(mTabletMode);
        invalidate();
    }

    public Drawable getDrawable() {
        if (null != mImgToDisplay) {
            return mImgToDisplay.getDrawable();
        } else {
            return null;
        }
    }
    
    /**
     * Initialize local variables
     * @param context
     */
    private void initVars(Context context) {
        //initialize only once
        if (null == multiTouchController) {
            multiTouchController = new MultiTouchController<ZoomableImageView.Img>(this);
            //default black background
            setBackgroundColor(Color.BLACK);
        }
        setAllowRotation(true);
        //default behaviuor, take care of {@link #onTouchEvent}
        setOverrideOnTouchEvent(true);
        mGestureDetector = new GestureDetector(context, mSimpleGestureListener);
    }
    
    
    //---------- Private methods
    
    GestureDetector.SimpleOnGestureListener mSimpleGestureListener = new GestureDetector.SimpleOnGestureListener() {
        //
        public boolean onDoubleTap(MotionEvent e) {
            boolean processed = false;
            if (null != mImgToDisplay) {
                processed = mImgToDisplay.handleDoubleTap();
                if (processed) {
                    invalidate();
                } 
            }
            return processed;
        };
    };
        

    
    
    
    //---------- Private classe
    
    class Img {
        private static final double TOLERANCE_FACTOR = 0.01;
        private static final float SCREEN_MARGIN = 100;
        private static final float MAX_SCALE_ALLOWED = 3.5f;
        private static final float MAX_INITIAL_SCALE_ALLOWED = 1.8f;

        private final int resId;
        private Drawable drawable;
        private boolean firstLoad;
        private int imageOriginalWidth, imageOriginalHeight, drawableAreaWidth, drawableAreaHeight;
        private float centerX, centerY, scaleX, scaleY, angle;
        private float minX, maxX, minY, maxY;
        private boolean mRotateImage;
        private boolean mTabletMode;
        private float maximumContainScale;

        public Img(Resources res,  int resId, int width, int height) {
            this(resId, width, height);
            this.drawable = res.getDrawable(resId);
            load();
        }

        public Img(BitmapDrawable drawable, int width, int height) {
            this(0, width, height);
            this.drawable = drawable;
            load();
        }
        
        private Img(int resId, int width, int height) {
            this.resId = resId; 
            this.drawableAreaWidth = width;
            this.drawableAreaHeight = height;
            this.firstLoad = true;
            this.mRotateImage = true;
        }

        /**
         * Update the size of the drawable area. This is needed because, if the
         * image control is created during onCreate method and similar, its width
         * and heigth are 0 due to an Android architectural choice
         *  
         * @param newWidth
         * @param newHeight
         */
        public void setViewSize(int newWidth, int newHeight) {
            drawableAreaWidth = newWidth;
            drawableAreaHeight = newHeight;
            firstLoad = true;
            centerImageInsideDrawableArea();
        }
        
        public void setAllowRotation(boolean newValue) {
            mRotateImage = newValue;
        }

        public void setTabletMode(boolean newValue) {
            mTabletMode = newValue;
        }

        /**
         * Called by activity's onResume() method to load the images
         */
        private void load() {
            if (null == drawable) {
                setPos(0, 0, 1, 1, 0);
                return;
            }
            //PAY ATTENTION: When a normal device returns
            //the right preview size (504x378), the Xperia Arc returns
            // 336x242 image size
            this.imageOriginalWidth = drawable.getIntrinsicWidth();
            this.imageOriginalHeight = drawable.getIntrinsicHeight();
            if (firstLoad) {
                centerImageInsideDrawableArea();
            } else {
                float cx, cy, sx, sy;
                // Reuse position and scale information if it is available
                // FIXME this doesn't actually work because the whole activity is turn down and re-created on rotate
                cx = this.centerX;
                cy = this.centerY;
                sx = this.scaleX;
                sy = this.scaleY;
                // Make sure the image is not off the screen after a screen rotation
                if (this.maxX < SCREEN_MARGIN)
                    cx = SCREEN_MARGIN;
                else if (this.minX > drawableAreaWidth - SCREEN_MARGIN)
                    cx = drawableAreaWidth - SCREEN_MARGIN;
                if (this.maxY > SCREEN_MARGIN)
                    cy = SCREEN_MARGIN;
                else if (this.minY > drawableAreaHeight - SCREEN_MARGIN)
                    cy = drawableAreaHeight - SCREEN_MARGIN;
                setPos(cx, cy, sx, sy, 0.0f);
            }
        }

        /**
         * Called by activity's onPause() method to free memory used for loading the images
         */
        public void unload() {
            if (0 == resId) {
                //the image has been loaded from a bitmap, so it's useful to dispose it
                if (null != drawable && drawable instanceof BitmapDrawable) {
                    BitmapDrawable bd = (BitmapDrawable) drawable;
                    if (bd.getBitmap() != null) {
                        bd.getBitmap().recycle();
                    }
                }
            }
            this.drawable = null;
            System.gc();
        }

        /**
         * Set the position and scale of an image in screen coordinates
         */
        public boolean setPos(PositionAndScale newImgPosAndScale) {
            return setPos(
                    newImgPosAndScale.getXOff(),
                    newImgPosAndScale.getYOff(),
                    (mUIMode & UI_MODE_ANISOTROPIC_SCALE) != 0 
                            ? newImgPosAndScale.getScaleX()
                            : newImgPosAndScale.getScale(),
                    (mUIMode & UI_MODE_ANISOTROPIC_SCALE) != 0
                            ? newImgPosAndScale.getScaleY()
                            : newImgPosAndScale.getScale(),
                    newImgPosAndScale.getAngle());
            // FIXME: anisotropic scaling jumps when axis-snapping
            // FIXME: affine-ize
            // return setPos(newImgPosAndScale.getXOff(), newImgPosAndScale.getYOff(), newImgPosAndScale.getScaleAnisotropicX(),
            // newImgPosAndScale.getScaleAnisotropicY(), 0.0f);
        }

        /**
         * Set the position and scale of an image in screen coordinates
         */
        private boolean setPos(float centerX, float centerY, float scaleX, float scaleY, float angle) {
            float ws = (imageOriginalWidth / 2) * scaleX;
            float hs = (imageOriginalHeight / 2) * scaleY;
            float newMinX = centerX - ws;
            float newMinY = centerY - hs;
            float newMaxX = centerX + ws;
            float newMaxY = centerY + hs;

//            if (Log.isLoggable(Log.DEBUG)) {
//                Log.debug("AAAA", 
//                        "dw:" + drawableAreaWidth + 
//                        ", dh:" + drawableAreaHeight + 
//                        ", w:" + imageOriginalWidth + 
//                        ", h:" + imageOriginalHeight + 
//                        ", ws:" + ws + 
//                        ", hs:" + hs + 
//                        ", cX:" + centerX + 
//                        ", cY:" + centerY + 
//                        ", sX:" + scaleX + 
//                        ", sY:" + scaleY + 
//                        ", newMnX:" + newMinX +
//                        ", newMaX:" + newMaxX + 
//                        ", newMnY:" + newMinY +
//                        ", newMaY:" + newMaxY 
//                        );
//            }

//            //prevent movements of the image outside the frame border
//            if ((newMinX > 0 && newMaxX > drawableAreaWidth) //move inside from left border
//                    || (newMinX < 0 && newMaxX < drawableAreaWidth) //move inside from right border
//                    || (newMinY > 0 && newMaxY > drawableAreaHeight) //move inside from upper border
//                    || (newMinY < 0 && newMaxY < drawableAreaHeight)) //move inside from bottom border
//            {
//                return false;
//            }
//            
//            //prevent that image is scaled more than the lower scale rate that fills
//            // width or height of the screen
//            if (newMinX > 0 && newMaxX < drawableAreaWidth && newMinY > 0 && newMaxY < drawableAreaHeight) {
//                return false;
//            }
            
            
            if (!firstLoad) {
//              //original code
//              if (newMinX > drawableAreaWidth - SCREEN_MARGIN || newMaxX < SCREEN_MARGIN || newMinY > drawableAreaHeight - SCREEN_MARGIN
//                      || newMaxY < SCREEN_MARGIN) {
//                  return false;
//              }
                        
                //prevent panning when image is fit on the drawable area
                if (isOnlyAPan(centerX, centerY, scaleX, scaleY, angle)
                        && Math.abs(scaleX - maximumContainScale) < TOLERANCE_FACTOR) {
                    return false;
                }
                //we care only partially about vertical margins
                int screenMarginHeigth = (int) (drawableAreaHeight * 0.20);
                if (newMinY > drawableAreaHeight - screenMarginHeigth
                        || newMaxY < screenMarginHeigth) {
                    return false;
                }
                
                //but we care about horizontal margins
                //not a zoom, only a pan
                if (this.scaleX == scaleX && this.scaleY == scaleY) {
                    if ((newMinX > 0 && newMaxX > drawableAreaWidth) //move inside from left border
                            || (newMinX < 0 && newMaxX < drawableAreaWidth)) //move inside from right border
                    {
                        return false;
                    }
                }
                
                //prevent max zoom
                //sometime scaleX e scaleY value is 3.5000002, so we apply a
                // round to the calculation, and not a simply "greater then" comparison
//                if (scaleX > MAX_SCALE_ALLOWED || scaleY > MAX_SCALE_ALLOWED) {
//                    return false;
//                }
                if ((scaleX - MAX_SCALE_ALLOWED) > TOLERANCE_FACTOR
                        || (scaleY - MAX_SCALE_ALLOWED) > TOLERANCE_FACTOR) {
                    return false;
                }
            }
            
            this.centerX = centerX;
            this.centerY = centerY;
            this.scaleX = scaleX;
            this.scaleY = scaleY;
            if (mRotateImage) this.angle = angle;
            this.minX = newMinX;
            this.minY = newMinY;
            this.maxX = newMaxX;
            this.maxY = newMaxY;
            return true;
        }

        /** Return whether or not the given screen coords are inside this image */
        public boolean containsPoint(float scrnX, float scrnY) {
            // FIXME: need to correctly account for image rotation
            return (scrnX >= minX && scrnX <= maxX && scrnY >= minY && scrnY <= maxY);
        }

        public void draw(Canvas canvas) {
            canvas.save();
            float dx = (maxX + minX) / 2;
            float dy = (maxY + minY) / 2;
            drawable.setBounds((int) minX, (int) minY, (int) maxX, (int) maxY);
            canvas.translate(dx, dy);
            if (mRotateImage) canvas.rotate(angle * 180.0f / (float) Math.PI);
            canvas.translate(-dx, -dy);
            drawable.draw(canvas);
            canvas.restore();
        }

        public Drawable getDrawable() {
            return drawable;
        }

        public int getWidth() {
            return imageOriginalWidth;
        }

        public int getHeight() {
            return imageOriginalHeight;
        }

        public float getCenterX() {
            return centerX;
        }

        public float getCenterY() {
            return centerY;
        }

        public float getScaleX() {
            return scaleX;
        }

        public float getScaleY() {
            return scaleY;
        }

        public float getAngle() {
            return angle;
        }

        // FIXME: these need to be updated for rotation
        public float getMinX() {
            return minX;
        }

        public float getMaxX() {
            return maxX;
        }

        public float getMinY() {
            return minY;
        }

        public float getMaxY() {
            return maxY;
        }
        
        /**
         * Sets the current image centered and inside the drawable area
         */
        private void centerImageInsideDrawableArea() {
            //extract code from load method
            float centerX, centerY, scaleX, scaleY;
            centerX = drawableAreaWidth / 2.0f;
            centerY = drawableAreaHeight / 2.0f;
            maximumContainScale = calculateBestScale();
            scaleX = scaleY = maximumContainScale;
            setPos(centerX, centerY, scaleX, scaleY, 0.0f);
            firstLoad = false;
        }

        /**
         * Checks for wrong size/position etc of the image, to be sure that it
         * isnt' smaller that the maximum image size to perfectly fit the
         * available space
         */
        public void centerImageInsideDrawableAreaIfNeeded() {
            float containScale = calculateBestScale();
            float centerX = drawableAreaWidth / 2.0f;
            float centerY = drawableAreaHeight / 2.0f;
            float ws = (imageOriginalWidth / 2) * containScale;
            float hs = (imageOriginalHeight / 2) * containScale;
            float containMinX = centerX - ws;
            float containMinY = centerY - hs;
            float containMaxX = centerX + ws;
            float containMaxY = centerY + hs;
            
            //if the image in smaller that the maximum image size to fit the available space
            if (Math.min(this.scaleX, this.scaleY) < containScale) {
                //
                if (this.minX > containMinX 
                        || this.maxX < containMaxX 
                        || this.minY > containMinY  
                        || this.maxY < containMaxY
                ){
                    setPos(centerX, centerY, containScale, containScale, 0);
                }
            }
        }

        /**
         * Handles double tap event: if the image was zoomed, it is restored to
         * original scale ratio, otherwise is zoomed 2x.
         * 
         * @return false if the image was left untouched, otherwise true 
         */
        public boolean handleDoubleTap() {
            float containScale = calculateBestScale();
            if (scaleX == containScale || scaleY == containScale) {
                //image is already at the standard size, so zoom 2x
                float newScaleFactor = Math.min(containScale * 2, MAX_SCALE_ALLOWED);
                setPos(centerX, centerY, newScaleFactor, newScaleFactor, 0.0f);
            } else {
                firstLoad = true; //force image update
                centerImageInsideDrawableArea();
            }
            return true;
        }

        /**
         * Calculates the best scale factor for fitting the image inside the
         * available space preserving its aspect ratio
         * @return
         */
        private float calculateBestScale() {
            float bestScale = Math.min(drawableAreaWidth / (float) imageOriginalWidth, drawableAreaHeight / (float) imageOriginalHeight);
            if (mTabletMode) {
                //on tables, images can apper too pixellated, so a maximum value for
                // the initial scale factor is set
                return Math.min(bestScale, MAX_INITIAL_SCALE_ALLOWED);
            } else {
                return Math.max(bestScale, maximumContainScale);
            }
        }
        
        private boolean isOnlyAPan(float newCenterX, float newCenterY, float newScaleX, float newScaleY, float newAngle) {
            //considers a tolerance is equality of values
            return Math.abs(newScaleX - this.scaleX) < TOLERANCE_FACTOR
                    && Math.abs(newScaleY - this.scaleY) < TOLERANCE_FACTOR
                    && Math.abs(newAngle - this.angle) < TOLERANCE_FACTOR;
        }
    }

}
