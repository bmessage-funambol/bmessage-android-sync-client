/*
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2009 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */
package com.funambol.android.activities.view;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.funambol.android.AppInitializer;
import com.funambol.androidsync.R;
import com.funambol.client.configuration.Configuration;
import com.funambol.client.controller.Controller;
import com.funambol.client.customization.Customization;
import com.funambol.client.localization.Inflexion;
import com.funambol.client.localization.Localization;
import com.funambol.client.source.AppSyncSource;
import com.funambol.client.source.ExternalAppManager;
import com.funambol.client.ui.Widget;
import com.funambol.client.ui.view.SourceNoThumbnailsView;
import com.funambol.util.Log;
import com.funambol.util.StringUtil;

public class AndroidSourceNoThumbnailsView extends LinearLayout implements SourceNoThumbnailsView {
    private String TAG_LOG = AndroidSourceNoThumbnailsView.class.getSimpleName();
    
    protected AppSyncSource appSource;
    protected Customization customization;
    protected Configuration configuration;
    protected Controller    controller;
    
    protected TextView titleView;
    protected TextView textView;
    protected LinearLayout contentView;
    protected ImageView imageView;
    
    protected Activity activity;
    
    protected enum Status {DISABLED, ENABLED, VERY_FIRST_EMPTY};
    protected LinearLayout statusViews[] = new LinearLayout[Status.values().length];
    protected Status status;
    protected View.OnClickListener onClickListener;
    
    public AndroidSourceNoThumbnailsView(Widget widget, AppSyncSource source) {
        super((Activity)widget.getContainerUiScreen());
        
        this.appSource = source;
        this.activity = (Activity)widget.getContainerUiScreen();

        AppInitializer appInitializer = AppInitializer.i(activity.getApplicationContext());
        customization = appInitializer.getCustomization();
        configuration = appInitializer.getConfiguration();
        controller    = appInitializer.getController();
        
        setOrientation(LinearLayout.VERTICAL);
        setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.FILL_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        
        onClickListener = createOnClickListener();

        final View view = View.inflate(getContext(), R.layout.vwbucketseparator, null);
        titleView = (TextView)view.findViewById(R.id.sourceviewtitle_lbltitle);
        contentView = createContentView(activity);
        
        LinearLayout disabledView = createDisabledView();   
        LinearLayout veryFirstEmptyView = createVeryFirstView();

        statusViews[Status.DISABLED.ordinal()] = disabledView;
        statusViews[Status.ENABLED.ordinal()] = contentView;
        statusViews[Status.VERY_FIRST_EMPTY.ordinal()] = veryFirstEmptyView;
        
        addView(view);
        for(LinearLayout v : statusViews) {
            addView(v);
        }
        
        status = appSource.getConfig().getEnabled() ?  Status.ENABLED : Status.DISABLED;
        if (status == Status.ENABLED) {
            if (configuration.getPimVeryFirstSync()) {
                status = Status.VERY_FIRST_EMPTY;
            } else {
                status = Status.ENABLED;
            }
        } else {
            status = Status.DISABLED;
        }
        updateContentView(status);
    }
    
    public void destroy() {
    }
    
    public void pause() {
    }
    
    public void resume() {
    }

    public Object getContainerUiScreen() {
        return activity;
    }
    
    public void setItemsCount(int count) {
        if (count > 0) {
            if (status == Status.VERY_FIRST_EMPTY) {
                // We have at least one item, show the standard content
                status = Status.ENABLED;
                updateContentView(status);
            }
        }
        updateEnabledView(count);
    }
    
    public void setTitle(String title) {
        // This is intentionally empty as the source computes the title by itself
    }
    
    private void updateEnabledView(final int count) {
        String message = controller.getLocalization().getLanguage("sourcebar_in_sync");        
        message = StringUtil.replaceAll(message, "__N_THINGS__", 
                appSource.getItemInflexion().withNumber(count).titleCase().toString());
        message = StringUtil.replaceAll(message, "__SOURCE__", appSource.getName());
        
        final String msg = message;
        activity.runOnUiThread(new Runnable() {
            public void run() {
                textView.setText(msg);
                titleView.setText(createViewTitle(count));
            }
        });
    }
    
    private String createViewTitle(int count) {
        StringBuffer title = new StringBuffer();
        title.append(appSource.getName().toUpperCase());
        if (appSource.getConfig().getEnabled()) {
            if(count > 0) {
                title.append(" (").append(count).append(")");
            }
        }
        return title.toString();
    }
    
    protected LinearLayout createVeryFirstView() {
        LinearLayout veryFirstEmptyView = (LinearLayout)View.inflate(
                getContext(), R.layout.vwsourceviewplaceholder, null);
        TextView veryFirstText = (TextView)veryFirstEmptyView.findViewById(
                R.id.sourceviewplaceholder_lblPlaceholder);
        String text = appSource.getEmptyHint();
        String accountLabel = activity.getString(R.string.account_label);
        text = StringUtil.replaceAll(text, "__ACCOUNT__", accountLabel);
        veryFirstText.setText(text);
        imageView = (ImageView)veryFirstEmptyView.findViewById(
                R.id.sourceviewplaceholder_imgPlaceholder);
        int resId = getVeryFirstIconId();
        imageView.setImageResource(resId);
        imageView.setOnClickListener(onClickListener);
        return veryFirstEmptyView;
    }
    
    protected LinearLayout createDisabledView() {
        LinearLayout disabledView = (LinearLayout)View.inflate(
                getContext(), R.layout.vwsourceviewplaceholder, null);
        TextView disabledText = (TextView)disabledView.findViewById(
                R.id.sourceviewplaceholder_lblPlaceholder);
       
        Localization localization = controller.getLocalization();
        String text = localization.getLanguage("source_placeholder_disabled_text");
        text = StringUtil.replaceAll(text, "__THING__",
                                     appSource.getItemInflexion().titleCase().using(Inflexion.SINGULAR).toString());
        disabledText.setText(text);
        ImageView imageView = (ImageView)disabledView.findViewById(
                R.id.sourceviewplaceholder_imgPlaceholder);
        int resId = ((Integer)customization.getSourceDisabledIcon(
                appSource.getId()).getOpaqueDescriptor()).intValue();
        imageView.setImageResource(resId);
        return disabledView;
    }
    
    protected int getVeryFirstIconId() {
        int resId = ((Integer)customization.getSourceIcon(appSource.getId())
                .getOpaqueDescriptor()).intValue();
        return resId;
    }
    
    protected View.OnClickListener createOnClickListener() {
        return new OnClickListener();
    }
    
    private LinearLayout createContentView(Context context) {
        LinearLayout placeHolder = new LinearLayout(context);
        View.inflate(getContext(), R.layout.vwsourceviewplaceholder, placeHolder);
        textView = (TextView)placeHolder.findViewById(
                R.id.sourceviewplaceholder_lblPlaceholder);
        imageView = (ImageView)placeHolder.findViewById(
                R.id.sourceviewplaceholder_imgPlaceholder);
        int resId = ((Integer)customization.getSourceIcon(
                appSource.getId()).getOpaqueDescriptor()).intValue();
        imageView.setImageResource(resId);
        imageView.setOnClickListener(onClickListener);

        return placeHolder;
    }
    
    protected void updateContentView(final Status newStatus) {
        activity.runOnUiThread(new Runnable() {
            public void run() {
                for(View v : statusViews) {
                    v.setVisibility(View.GONE);
                }
                View currentView = statusViews[newStatus.ordinal()];
                currentView.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void setEnabled(final boolean enabled) {
        activity.runOnUiThread(new Runnable() {
            public void run() {
                if (status == Status.DISABLED && enabled) {
                    if (configuration.getPimVeryFirstSync()) {
                        status = Status.VERY_FIRST_EMPTY;
                    } else {
                        status = Status.ENABLED;
                    }
                    updateContentView(status);
                } else if (status != Status.DISABLED && !enabled) {
                    status = Status.DISABLED;
                    updateContentView(status);
                }
            }
        });
    }
    
    private class OnClickListener implements View.OnClickListener {

        public void onClick(View view) {
            if (status != Status.DISABLED) {
                ExternalAppManager manager = appSource.getAppManager();
                if (manager != null) {
                    try {
                        manager.launch(appSource, null);
                    } catch (Exception e) {
                        Log.error(TAG_LOG, "Cannot launch external app manager, because: " + e);
                    }
                } else {
                    Log.error(TAG_LOG, "No external manager associated to source: " + appSource.getName());
                }
            }
        }
    }
}
