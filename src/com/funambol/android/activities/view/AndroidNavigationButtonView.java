/**
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2011 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.activities.view;

import com.funambol.android.AppInitializer;
import com.funambol.androidsync.R;
import com.funambol.client.controller.MainScreenController;
import com.funambol.client.controller.NavigationBarController;
import com.funambol.client.ui.view.NavigationBarButtonView;
import com.funambol.util.Log;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.RadioButton;

/**
 * This class extends the {@link RadioButton} view and customize it
 * for NavigationBar usage.
 *
 */
public abstract class AndroidNavigationButtonView
extends RadioButton implements NavigationBarButtonView {
    private static final String TAG_LOG = AndroidNavigationButtonView.class.getSimpleName();
    
    protected MainScreenController homeScreenController;
    protected NavigationBarController navigationBarController;
    
    public AndroidNavigationButtonView(Context context, final NavigationBarController navigationBarController) {
        super(context);
        if (Log.isLoggable(Log.TRACE)) {
            Log.trace(TAG_LOG, "Creating navigation bar UI for sync source id " + getButtonId());
        }

        //same parameters as the style "navbar_button", cannot find a way
        //to set items directly from the style
        //** REMEMBER ** to change according to the style
        RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(0, RadioGroup.LayoutParams.WRAP_CONTENT, 1.0f);
        params.bottomMargin = getRealPixelSize(-10);
        setLayoutParams(params);
        
        setButtonDrawable(new ColorDrawable(android.R.color.transparent));
        setBackgroundResource(R.drawable.navbar_selbackground);
        setCompoundDrawablePadding(getRealPixelSize(2));
        setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);
        //getDimension already convert dimension in right pixel size, no need to specify again DP 
        setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.navbar_txtsize));
        setTypeface(Typeface.DEFAULT_BOLD);
        
        //set customized items
        setText(getButtonTextId());
        setCompoundDrawablesWithIntrinsicBounds(0, getDrawableTopId(), 0, 0);

        //set checked listener
        AppInitializer appInitializer = AppInitializer.i(context);
        homeScreenController = appInitializer.getController().getMainScreenController();
        setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                navigationBarController.buttonSelected(getButtonId());
            }
        });
        
        setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                setClickable(!isChecked);
            }
        });
    }
    
    @Override
    public void setSelected(boolean selected) {
        //Android custom implementation, set checked state of the button
        setChecked(selected);
    }
    
    protected int getRealPixelSize(float dp) {
        //http://stackoverflow.com/questions/5012840/android-specifying-pixel-units-like-sp-px-dp-without-using-xml
        DisplayMetrics metrics = getContext().getResources().getDisplayMetrics();
        int pixels = (int) (metrics.density * dp + 0.5f); //0.5 to round up
        return pixels;
    }
    
    /**
     * Text to display as title of the button
     * @return
     */
    protected abstract int getButtonTextId();
    
    /**
     * The resouce id of the icon of the button
     * @return
     */
    protected abstract int getDrawableTopId();
}
