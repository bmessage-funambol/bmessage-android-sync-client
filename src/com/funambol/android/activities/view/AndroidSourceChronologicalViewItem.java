/**
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2011 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.activities.view;

import java.util.Vector;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.funambol.android.AndroidUtils;
import com.funambol.androidsync.R;

import com.funambol.client.controller.SourceChronologicalViewController;
import com.funambol.client.controller.TimeframeBucket;
import com.funambol.client.source.AppSyncSource;
import com.funambol.client.source.MediaMetadata;
import com.funambol.client.ui.Screen;
import com.funambol.client.ui.view.SourceChronologicalViewItem;
import com.funambol.client.ui.view.ThumbnailView;
import com.funambol.storage.Tuple;

import com.funambol.util.Log;

public class AndroidSourceChronologicalViewItem extends RelativeLayout
        implements SourceChronologicalViewItem {
    
    private static final String TAG_LOG = AndroidSourceChronologicalViewItem.class.getSimpleName();
    
    private AppSyncSource appSource;
    private Screen screen;
    private int type = SourceChronologicalViewItem.TYPE_THUMBNAILS;

    private Vector<ThumbnailView> thumbs;
    private Context context;
    private SourceChronologicalViewController controller;
    
    private TextView timeframeLabel;
    
    private int thumbPadding = -1;
    private int thumbSize;
    
    private long firstDate = 0;
    private long lastDate = Long.MAX_VALUE;
    private TimeframeBucket timeframeBucket;

    public AndroidSourceChronologicalViewItem(Screen screen,
            SourceChronologicalViewController controller, AppSyncSource appSource) {
        super(((Activity)screen.getUiScreen()).getApplicationContext());
        this.context = ((Activity)screen.getUiScreen()).getApplicationContext();
        this.controller = controller;
        this.screen = screen;
        this.appSource = appSource;
        this.thumbs = new Vector<ThumbnailView>();
    }
    
    public ThumbnailView addThumbnailInFront(final Tuple item) {
        return insertThumbnail(item, true);
    }
    
    public ThumbnailView addThumbnail(final Tuple item) {
        return insertThumbnail(item, false);
    }
   
    public ThumbnailView replaceThumbnail(int idx, Tuple item) {
        ThumbnailView tv = thumbs.get(idx);
        tv.setThumbnail(item);
        return tv;
    }
    
    public void setVisible(boolean visible) {
        // Set the visibility flag on all the thumbnails
        for(ThumbnailView tv : thumbs) {
            tv.setVisible(visible);
        }
    }
    
    private ThumbnailView insertThumbnail(final Tuple item, final boolean inFront) {
        if (Log.isLoggable(Log.TRACE)) {
            String thumbPath = item.getStringField(item.getColIndexOrThrow(
                    MediaMetadata.METADATA_THUMBNAIL_PATH));
            Log.trace(TAG_LOG, "Adding thumbnail " + thumbPath + " in front " + inFront);
        }
        final Long id = item.getLongField(item.getColIndexOrThrow(
                MediaMetadata.METADATA_ID));
        final long ts = item.getLongField(item.getColIndexOrThrow(
                MediaMetadata.METADATA_ORDER_DATE)).longValue();
        
        if (ts < lastDate) {
            lastDate = ts;
        }
        if (ts > firstDate) {
            firstDate = ts;
        }
        final ThumbnailView tv = appSource.createThumbnailView(screen);
        tv.setSize(thumbSize);
        tv.setThumbnail(item);
        tv.setOnClickListener(new ThumbnailView.OnClickListener() {
            public void onClick() {
                controller.onThumbnailClicked(tv, id);
            }
        });
        if(appSource.supportsMultiSelect()) {
            tv.setOnLongClickListener(new ThumbnailView.OnLongClickListener() {
                public void onLongClick() {
                    controller.onThumbnailLongClicked(tv, id);
                }
            });
        }
        final View view = (View)tv.getThumbnailView();
        if (inFront) {
            thumbs.insertElementAt(tv, 0);
        } else {
            thumbs.addElement(tv);
        }
        for(int i=0;i<thumbs.size();++i) {
            ThumbnailView ctv = thumbs.get(i);
            ctv.setPadding(0, 0, thumbPadding, thumbPadding);
        }
        // The rightmost item shall have no right padding
        ThumbnailView rightMostTv = thumbs.get(thumbs.size() - 1);
        rightMostTv.setPadding(0,0,0,thumbPadding);
        
        Activity activity = (Activity)screen.getUiScreen();

        // Set a proper id before we add to the layout
        view.setId(id.intValue());
        if(getChildCount() > 0) {
            final LayoutParams lp = new RelativeLayout.LayoutParams(
                    LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            if(inFront) {
                lp.addRule(RelativeLayout.LEFT_OF,
                        getChildAt(0).getId());
            } else {
                lp.addRule(RelativeLayout.RIGHT_OF,
                        getChildAt(getChildCount()-1).getId());
            }
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    addView(view, lp);
                }
            });
        } else {
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    addView(view);
                }
            });
        }
        return tv;
    }

    public void setType(int type) {
        if (type == SourceChronologicalViewItem.TYPE_TIMEFRAME_SEPARATOR) {
            LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            final View view = layoutInflater.inflate(R.layout.vwbucketseparator, null);
            final Activity activity = (Activity)screen.getUiScreen();
            timeframeLabel = (TextView)view.findViewById(R.id.sourceviewtitle_lbltitle);
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.FILL_PARENT,
                            AndroidUtils.dipToPx(28, activity));
                    lp.setMargins(0, 0, 0, AndroidUtils.dipToPx(4, activity));
                    addView(view, lp);
                }
            }); 
        }
        this.type = type;
    }
    
    public int getType() {
        return type;
    }

    public void setThumbSize(int thumbSize) {
        this.thumbSize = thumbSize;
    }
    
    public void setFirstLastDates(long first, long last) {
        firstDate = first;
        lastDate  = last;
    }
    
    public void setThumbPadding(int padding) {
        thumbPadding = padding;
    }

    public int getCount() {
        return thumbs.size();
    }

    public Tuple getItemAtPosition(int idx) {
        ThumbnailView tv = thumbs.get(idx);
        return tv.getItem();
    }

    public ThumbnailView getThumbnailAtPosition(int idx) {
        ThumbnailView tv = thumbs.get(idx);
        return tv;
    }

    public long getFirstDate() {
        return firstDate;
    }

    public long getLastDate() {
        return lastDate;
    }
    
    public void setTimeframeBucket(TimeframeBucket bucket) {
        this.timeframeBucket = bucket;
    }
    
    public TimeframeBucket getTimeframeBucket() {
        return timeframeBucket;
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        if (type == TYPE_TIMEFRAME_SEPARATOR && timeframeBucket != null) {
            String currentTitle = timeframeLabel.getText().toString();
            String newTitle = timeframeBucket.getLabel();
            if (currentTitle == null || !currentTitle.equals(newTitle)) {
                timeframeLabel.setText(newTitle);
            }
        }
        super.dispatchDraw(canvas);
    }
}
