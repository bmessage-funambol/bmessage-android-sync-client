/**
 * 
 */
package com.funambol.android.activities;

import com.funambol.android.AppInitializer;
import com.funambol.android.activities.view.AndroidActionBarView;
import com.funambol.androidsync.R;
import com.funambol.client.controller.Controller;
import com.funambol.client.controller.IntroScreenController;
import com.funambol.client.localization.Localization;
import com.funambol.client.ui.AppFlowNavigator;
import com.funambol.client.ui.Bitmap;
import com.funambol.client.ui.DisplayManager;
import com.funambol.client.ui.IntroScreen;
import com.funambol.client.ui.view.ActionBarWidget;
import com.funambol.util.Log;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.funambol.client.test.BasicConstants;
import com.funambol.util.StringUtil;

/**
 * Shows a brief explaination about the product
 */
public class AndroidIntroScreen extends BasicActivity implements IntroScreen {
    private static final String TAG_LOG = AndroidIntroScreen.class.getSimpleName();
    private static final String KEY_PHASE = "CurrentPhase";
    
    /** save current phase of the intro, to resume from on rotation and another */
    private int currentPhase;
    private Button btnBack;
    private Button btnNext;
    private TextView lblTitle;
    private TextView lblDesc;
    private ImageView imgGraphic;
    private Localization localization;
    private IntroScreenController introScreenController;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Log.isLoggable(Log.DEBUG)) {
            Log.debug(TAG_LOG, "Starting screen " + this.getClass().getSimpleName());
        }
        
        AppInitializer appInitializer = AppInitializer.i(getApplicationContext());
        localization = appInitializer.getLocalization();
        
        setContentView(R.layout.actintro);
        
        introScreenController = new IntroScreenController(appInitializer.getController());
        
        btnBack = (Button) findViewById(R.id.intro_btnback);
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                currentPhase--;
                moveToPhase(currentPhase);
            }
        });
        btnNext = (Button) findViewById(R.id.intro_btnnext);
        btnNext.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                currentPhase++;
                moveToPhase(currentPhase);
            }
        });
        lblTitle = (TextView) findViewById(R.id.intro_lbltitle);
        lblDesc = (TextView) findViewById(R.id.intro_lbldesc);
        imgGraphic = (ImageView) findViewById(R.id.intro_imggraphic);

        // Set buttons tag for testing purposes only
        btnNext.setTag(BasicConstants.BUTTON_NEXT);
        btnBack.setTag(BasicConstants.BUTTON_PREVIOUS);
        
        if (null == savedInstanceState) {
            currentPhase = 1;
        } else {
            currentPhase = savedInstanceState.getInt(KEY_PHASE, 1);
        }
        
        introScreenController.initScreen(this);
        moveToPhase(currentPhase);
    }
    
    
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_PHASE, currentPhase);
    }
    
    private void moveToPhase(int newPhase) {
        switch (newPhase) {
        case 1:
            btnBack.setVisibility(View.GONE);
            btnNext.setVisibility(View.VISIBLE);
            lblTitle.setText(localization.getLanguage("introscreen_title1"));
            imgGraphic.setImageResource(R.drawable.intro_imgslide1_300x215);

            String desc1 = localization.getLanguage("introscreen_desc1");
            desc1 = StringUtil.replaceAll(desc1, "__APP_NAME__",
                    localization.getLanguage("app_name"));
            lblDesc.setText(Html.fromHtml(desc1));
            break;

        case 2:
            btnBack.setVisibility(View.VISIBLE);
            btnNext.setVisibility(View.VISIBLE);
            lblTitle.setText(localization.getLanguage("introscreen_title2"));
            imgGraphic.setImageResource(R.drawable.intro_imgslide2_300x215);
            lblDesc.setText(Html.fromHtml(localization.getLanguage("introscreen_desc2")));
            break;

        case 3:
            btnBack.setVisibility(View.VISIBLE);
            btnNext.setVisibility(View.VISIBLE);
            lblTitle.setText(localization.getLanguage("introscreen_title3"));
            imgGraphic.setImageResource(R.drawable.intro_imgslide3_300x215);
            lblDesc.setText(Html.fromHtml(localization.getLanguage("introscreen_desc3")));
            break;

        case 4:
            AppInitializer appInitializer = AppInitializer.i(getApplicationContext());
            AppFlowNavigator appFlowNavigator = appInitializer.getController().getAppFlowNavigator();
            int nextScreenId = appFlowNavigator.getNextScreenInFlow(Controller.INTRO_SCREEN_ID);
            DisplayManager displayManager = appInitializer.getDisplayManager();
            try {
                Log.debug(TAG_LOG, "Starting screen " + nextScreenId);
                displayManager.showScreen(nextScreenId);
            } catch (Exception e) {
                Log.error(TAG_LOG, "Cannot instantiate the next screen", e);
            } finally {
                introScreenController.setIntroScreenAsShown();
            }
            finish();
            break;
        }
    }


    // ---------------------------------------------------------------- Screen
    public Object getUiScreen() {
        return this;
    }

    // ----------------------------------------------- ActionBarWidgetContainer
    public ActionBarWidget getActionBarWidget() {
        AndroidActionBarView actionBarView = (AndroidActionBarView) findViewById(R.id.actionbar);
        return actionBarView;
    }

    public Bitmap getActionBitmap(int actionId) {
        return null;
    }
}
