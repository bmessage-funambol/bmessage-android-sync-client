/**
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2011 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.activities;

import java.lang.reflect.Field;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;

import com.funambol.android.AppInitializer;
import com.funambol.android.BuildInfo;
import com.funambol.androidsync.R;
import com.funambol.util.Log;


/**
 * This is an extended activity which provides the functionality of restoring
 * dialogs when the activity is closed/re-opened (e.g. on device rotation).
 * If dialogs are created via DialogController/DisplayManager then when the
 * activity is resumed the onCreateDialog provided by this implementation checks
 * if a dialog exists already and restores it.
 */
public class BasicActivity extends Activity {
    private static final String TAG_LOG = BasicActivity.class.getSimpleName();
    
    private int maxId = 1;

    @Override
    protected Dialog onCreateDialog(int id) {
        if (Log.isLoggable(Log.TRACE)) {
            Log.trace(TAG_LOG, "onCreateDialog: " + id);
        }
        AndroidDisplayManager dm = (AndroidDisplayManager)AppInitializer.i(this).getDisplayManager();
        Dialog result = null;
        if(dm != null) {
            result = dm.createDialog(id);
        }
        if(result != null) {
            return result;
        } else {
            return super.onCreateDialog(id);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // This is useful only for automatic tests
        if(BuildInfo.isInDebugMode() && BuildInfo.AUTO_TEST) {
            // The activity becomes visible
            AndroidDisplayManager dm = (AndroidDisplayManager)AppInitializer.i(this).getDisplayManager();
            dm.addActivityOnStack(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // This is useful only for automatic tests
        if(BuildInfo.isInDebugMode() && BuildInfo.UNIT_TEST) {
            // The activity is no longer visible
            AndroidDisplayManager dm = (AndroidDisplayManager)AppInitializer.i(this).getDisplayManager();
            dm.removeActivityFromStack(this);
        }
    }
    
    /**
     * This method is to be used when several ToggleButtons within a View have the same ID.
     * It gives them new IDs higher than the highest static ID to be found in R.id.
     * This is necessary for the correct behavior of the automatic preservation of the state
     * of these controls after a screen rotation. 
     * 
     * @param id the ID shared by the ToggleButtons
     * @return the highest ID dynamically assigned
     */
    protected int assignNewIdsTo(int id) {
        if (Log.isLoggable(Log.TRACE)) {
            Log.trace(TAG_LOG, "assignNewIdsTo " + id);
        }
        Class<R.id> idClass = R.id.class;
        Field[] idFields = idClass.getFields();
        final Object doesntMatter = new Object();
        for (Field idField : idFields) {
            try {
                maxId = Math.max(maxId, idField.getInt(doesntMatter));
            } catch (IllegalAccessException iae) {
                // Skips this one
            }
        }
        View view;
        while ((view = findViewById(id)) != null) {
            view.setId(++maxId);
            if (Log.isLoggable(Log.DEBUG)) {
                Log.debug(TAG_LOG, "Dynamically reassigned ID " + maxId +
                        " to a " + view.getClass().getSimpleName());
            }
        }
        return maxId;
    }
}
