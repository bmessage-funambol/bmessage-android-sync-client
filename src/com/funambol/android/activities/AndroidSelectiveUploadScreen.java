/**
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2011 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.activities;

import android.os.Bundle;

import com.funambol.androidsync.R;
import com.funambol.android.AppInitializer;
import com.funambol.android.activities.view.AndroidActionBarView;
import com.funambol.android.controller.AndroidSelectiveUploadScreenController;

import com.funambol.client.ui.Bitmap;
import com.funambol.client.ui.view.ActionBarWidget;
import com.funambol.client.controller.Controller;
import com.funambol.client.controller.SelectiveUploadScreenController;
import com.funambol.client.source.AppSyncSource;
import com.funambol.client.ui.SelectiveUploadScreen;
import com.funambol.util.Log;


public class AndroidSelectiveUploadScreen extends BasicFragmentActivity implements SelectiveUploadScreen {

    private static final String TAG_LOG = AndroidSelectiveUploadScreen.class.getSimpleName();

    private Controller controller;
    private SelectiveUploadScreenController selectiveUploadScreenController;

    /**
     * Called with the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(Log.isLoggable(Log.TRACE)) {
            Log.trace(TAG_LOG, "onCreate");
        }
        AppInitializer appInitializer = AppInitializer.i(this);
        controller = appInitializer.getController();
        
        AppSyncSource appSource = null;

        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey(
                AndroidDisplayManager.APP_SOURCE_ID_PARAM)) {
            int sourceId = extras.getInt(AndroidDisplayManager.APP_SOURCE_ID_PARAM);
            appSource = controller.getAppSyncSourceManager().getSource(sourceId);
        } else {
            throw new IllegalStateException("Source id extra not found");
        }
        selectiveUploadScreenController = new AndroidSelectiveUploadScreenController(
                this, controller, appSource);
        screenController = selectiveUploadScreenController;
        setContentView(R.layout.actselectiveupload);
        selectiveUploadScreenController.initScreen(null == savedInstanceState);
    }

    public SelectiveUploadScreenController getController() {
        return selectiveUploadScreenController;
    }

    // ----------------------------------------------- ActionBarWidgetContainer
    
    public ActionBarWidget getActionBarWidget() {
        AndroidActionBarView actionBarView = (AndroidActionBarView) findViewById(R.id.actionbar);
        return actionBarView;
    }
    
    public Bitmap getActionBitmap(int actionId) {
        return null;
    }
    
}
