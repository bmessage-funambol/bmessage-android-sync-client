/*
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2011 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.activities;

import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;

import com.funambol.android.AppInitializer;
import com.funambol.android.BuildInfo;
import com.funambol.client.controller.ScreenController;
import com.funambol.client.ui.Screen;
import com.funambol.util.Log;

/**
 * This is an extended activity which provides the functionality of managing
 * common task when using fragment (open a dialog etc).
 *
 * TODO quite everything must be done.
 */
public class BasicFragmentActivity extends FragmentActivity implements Screen {
    private static final String TAG_LOG = BasicFragmentActivity.class.getSimpleName();
    
    protected ScreenController screenController;
    private boolean isPaused = false;

    private DialogFragment pendingDialogOnResume = null;
   
    public Object getUiScreen() {
        return this;
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        if(screenController != null) {
            screenController.onResume(this);
        }
        if(BuildInfo.isInDebugMode() && BuildInfo.AUTO_TEST) {
            // The activity becomes visible
            AndroidDisplayManager dm = (AndroidDisplayManager)AppInitializer.i(this).getDisplayManager();
            dm.addActivityOnStack(this);
        }
        isPaused = false;
        // This shall be done after the isPased flag has been set
        if(pendingDialogOnResume != null) {
            if(Log.isLoggable(Log.DEBUG)) {
                Log.debug(TAG_LOG, "Showing pending dialog on resume");
            }
            AndroidDisplayManager dm = (AndroidDisplayManager)AppInitializer.i(this).getDisplayManager();
            dm.showDialogFragment(this, pendingDialogOnResume);
            pendingDialogOnResume = null;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // The activity is no longer visible
        if(screenController != null) {
            screenController.onPause();
        }
        if(BuildInfo.isInDebugMode() && BuildInfo.AUTO_TEST) {
            AndroidDisplayManager dm = (AndroidDisplayManager)AppInitializer.i(this).getDisplayManager();
            dm.removeActivityFromStack(this);
        }
        isPaused = true;
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        // Inform the screen controller we are about to die
        if(screenController != null) {
            screenController.onDestroy();
        }
        screenController = null;
    }

    public boolean isPaused() {
        return isPaused;
    }

    public void showDialogOnResume(DialogFragment dialogToShow) {
        if(Log.isLoggable(Log.TRACE)) {
            Log.trace(TAG_LOG, "showDialogOnResume");
        }
        pendingDialogOnResume = dialogToShow;
    }
}
