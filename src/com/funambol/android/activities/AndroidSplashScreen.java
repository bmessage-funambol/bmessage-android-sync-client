/**
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2010 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.MotionEvent;

import com.funambol.android.AndroidUtils;
import com.funambol.android.App;
import com.funambol.android.AppInitializer;
import com.funambol.android.services.AutoSyncService;
import com.funambol.androidsync.R;
import com.funambol.client.configuration.Configuration;
import com.funambol.client.controller.Controller;
import com.funambol.client.localization.Localization;
import com.funambol.client.services.ExternalServiceCache;
import com.funambol.client.ui.AppFlowNavigator;
import com.funambol.client.ui.DisplayManager;
import com.funambol.client.ui.SplashScreen;
import com.funambol.client.ui.view.ImagePool;
import com.funambol.util.Log;
import com.funambol.util.StringUtil;

/**
 * Splash screen. This is the entry point for the app, it just shows a splash
 * screen and after some time automatically opens the Funambol app. 
 * 
 * If you install package on your device and suddenly run the application, the
 * splashscreen activity will not appear on screen immediately, but some time
 * will pass. This is because the {@link AutoSyncService} is started in
 * background and it requires a complete initialization of the application,
 * via a call to {@link App} class.
 * 
 * But if you run the app in normal way, splashscreen will popup immediately.
 * 
 */
public class AndroidSplashScreen extends BasicActivity implements SplashScreen {
    
    private static final String TAG_LOG = AndroidSplashScreen.class.getSimpleName();

    /** true when the user want to dismiss the splash clicking on it */
    private boolean forceDismiss;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        setContentView(R.layout.actsplashscreen);
        forceDismiss = false;
        // Start the autoclosing thread
        //no problem with rotation, because activity doesn't rotate at all!
        SplashScreenTimer timer = new SplashScreenTimer(2000);
        timer.start();
    }

    private class SplashScreenTimer extends Thread {
        private final int millisecondsDelay;

        public SplashScreenTimer(int millisecondsDelay) {
            this.millisecondsDelay = millisecondsDelay;
        }

        @Override
        public void run() {            

            long startTime = System.currentTimeMillis();
            
            // Starts long task of the initialization
            AppInitializer appInitializer = AppInitializer.i(AndroidSplashScreen.this);
            Controller globalController = appInitializer.getController();
            Configuration configuration = appInitializer.getConfiguration();

            if(!AndroidUtils.isSDCardMounted()) {
                Log.error(TAG_LOG, "SD Card is not mounted. "
                        + "The application cannot work properly.");
                Localization localization = globalController.getLocalization();
                DisplayManager displayManager = globalController.getDisplayManager();
                String message = localization.getLanguage("message_sd_card_unmounted");
                String okLabel = localization.getLanguage("dialog_ok");
                String appName = localization.getLanguage("app_name");
                message = StringUtil.replaceAll(message, "__APP_NAME__", appName);
                displayManager.showOkDialog(AndroidSplashScreen.this, message, okLabel, new Runnable() {
                    public void run() {
                        finish();
                    }
                }, false);
                return;
            }

            // Trigger a refresh for the external service cache
            ExternalServiceCache externalServiceCache = globalController.getExternalServiceCache();
            externalServiceCache.refreshServiceCacheDelayed();

            // Reset image pool
            ImagePool.getInstance().reset();

            try {
                long elapsedTime = System.currentTimeMillis() - startTime;
                while(!forceDismiss && (elapsedTime < millisecondsDelay)) {
                    sleep(100);
                    elapsedTime = System.currentTimeMillis() - startTime;
                }
            } catch(InterruptedException e) {
                // do nothing
            } finally {
                AppFlowNavigator appFlowNavigator = globalController.getAppFlowNavigator();
                final DisplayManager displayManager = globalController.getDisplayManager();
                int nextScreenId = appFlowNavigator.getNextScreenInFlow(Controller.SPLASH_SCREEN_ID);
                
                if (configuration.getExitAppBecauseOfOldClient()) {
                    if (Log.isLoggable(Log.INFO)) {
                        Log.info(TAG_LOG, "Exiting from app because the server is an old one");
                    }
                    //breaking action
                    Localization localization = appInitializer.getLocalization();

                    displayManager.showOkDialog(
                            AndroidSplashScreen.this,
                            localization.getLanguage("splashscreen_oldclientwarning"),
                            localization.getLanguage("splashscreen_btnExit"),
                            new Runnable() {
                                public void run() {
                                    displayManager.hideScreen(AndroidSplashScreen.this);
                                }
                            },
                            false);
                } else {
                    //flow continues normally
                    try {
                        displayManager.hideScreen(AndroidSplashScreen.this);
                        displayManager.showScreen(nextScreenId);
                    } catch(Exception ex) {
                        Log.error(TAG_LOG, "Unable to show next screen", ex);
                    }
                }
            }
        }
    }
    
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            forceDismiss = true;
        }
        return super.onTouchEvent(event);
    }

    public Object getUiScreen() {
        return this;
    }
}
