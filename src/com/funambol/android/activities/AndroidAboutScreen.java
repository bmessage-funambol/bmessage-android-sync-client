/*
 * Funambol is a mobile platform developed by Funambol, Inc. 
 * Copyright (C) 2009 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission 
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE 
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License 
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite 
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably 
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol". 
 */

package com.funambol.android.activities;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.os.Bundle;
import android.text.util.Linkify;
import android.view.View;
import android.widget.TextView;
import android.widget.ImageView;
import android.widget.TableLayout;

import com.funambol.androidsync.R;
import com.funambol.android.AppInitializer;
import com.funambol.android.activities.view.AndroidActionBarView;
import com.funambol.client.ui.AboutScreen;
import com.funambol.client.ui.Bitmap;
import com.funambol.client.controller.AboutScreenController;
import com.funambol.client.ui.view.ActionBarWidget;
import com.funambol.util.StringUtil;

public class AndroidAboutScreen extends BasicActivity implements AboutScreen
{   
    private AboutScreenController controller;

    private TextView labelAppVersion;
    private TextView labelCopyright;
    private TextView labelUsername;
    private TextView labelPortalInfo;
    private TextView labelLicence;
    private TextView labelPoweredBy;
    private ImageView logoPoweredBy;
    private TextView labelTerms;
    private TableLayout portalInfoList;

    private static int incrementalId = 0;

    @Override
    public void onCreate(Bundle icicle)
    { 
        super.onCreate(icicle);
        setContentView(R.layout.actabout);

        labelAppVersion = (TextView)findViewById(R.id.about_appversion);
        labelCopyright = (TextView)findViewById(R.id.about_copyright);
        labelUsername = (TextView)findViewById(R.id.about_username);
        labelPortalInfo = (TextView)findViewById(R.id.about_portalinfo);
        portalInfoList = (TableLayout)findViewById(R.id.about_portalinfoList);
        labelLicence = (TextView)findViewById(R.id.about_licence);
        labelPoweredBy = (TextView)findViewById(R.id.about_poweredByText);
        logoPoweredBy = (ImageView)findViewById(R.id.about_poweredByLogo);
        labelTerms = (TextView) findViewById(R.id.about_lblterms);
        
        // Initialize the view for this controller
        AppInitializer initializer = AppInitializer.i(this);
        controller = new AboutScreenController(this, initializer.getController());
        controller.initScreen();
    }

    public Object getUiScreen() {
        return this;
    }

    public void addApplicationVersion(String value) {
        labelAppVersion.setText(value);
        labelAppVersion.setVisibility(View.VISIBLE);
    }

    public void addCopyright(String value) {
        labelCopyright.setText(value);
        labelCopyright.setVisibility(View.VISIBLE);
    }

    public void addUsername(String value) {
        labelUsername.setText(value);
        labelUsername.setVisibility(View.VISIBLE);
    }

    public void addPortalInfo(String value, String[] list) {
        labelPortalInfo.setText(value);
        labelPortalInfo.setVisibility(View.VISIBLE);
        portalInfoList.setVisibility(View.VISIBLE);
        for(int i=0; i<list.length; i++) {
            addPortalInfoItem(list[i]);
        }
    }

    private void addPortalInfoItem(String item) {
        View result = View.inflate(this, R.layout.vwbulletitem, portalInfoList);
        TextView tv = (TextView)result.findViewById(R.id.vwbulletitem_text);
        // This is just to ensure we don't have different views with the same id
        tv.setId(incrementalId++);
        tv.setText(item);
    }

    public void addLicence(String value) {
        labelLicence.setText(value);
        labelLicence.setVisibility(View.VISIBLE);
    }

    public void addPoweredByText(String value) {
        labelPoweredBy.setText(value);
        labelPoweredBy.setVisibility(View.VISIBLE);
    }
    
    public void addPoweredByLogo(Bitmap logo) {
        Integer id = (Integer)logo.getOpaqueDescriptor();
        logoPoweredBy.setImageResource(id.intValue());
        logoPoweredBy.setVisibility(View.VISIBLE);
    }

    public void addTermsAndCondition(
            String fullTermsAndPrivatePolicyText,
            String termsText,
            String privatePolicyText,
            final String termsLink,
            final String privacyPolicyLink) {

        labelTerms.setText(fullTermsAndPrivatePolicyText);
        labelTerms.setVisibility(View.VISIBLE);
        Linkify.addLinks(labelTerms, Pattern.compile(termsText), 
                "", null, new Linkify.TransformFilter() {
            public final String transformUrl(final Matcher match, String url) {
                return termsLink;
            }
        });

        Linkify.addLinks(labelTerms, Pattern.compile(privatePolicyText), 
                "", null, new Linkify.TransformFilter() {
            public final String transformUrl(final Matcher match, String url) {
                return privacyPolicyLink;
            }
        });
    }

    // ----------------------------------------------- ActionBarWidgetContainer

    public ActionBarWidget getActionBarWidget() {
        AndroidActionBarView actionBarView = (AndroidActionBarView)
                findViewById(R.id.actionbar);
        return actionBarView;
    }

    public com.funambol.client.ui.Bitmap getActionBitmap(int actionId) {
        return null;
    }
}
