/**
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2011 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.funambol.androidsync.R;
import com.funambol.android.AndroidUtils;
import com.funambol.android.BuildInfo;
import com.funambol.android.AppInitializer;
import com.funambol.android.activities.view.AndroidActionBarView;
import com.funambol.android.activities.view.BalloonHelpView;
import com.funambol.android.controller.AndroidMainScreenController;
import com.funambol.android.source.pim.PimTestRecorder;

import com.funambol.client.ui.Bitmap;
import com.funambol.client.ui.MainScreen;
import com.funambol.client.ui.Widget;
import com.funambol.client.ui.view.ActionBarWidget;
import com.funambol.client.ui.view.ImagePool;
import com.funambol.client.ui.view.NavigationBarButtonView;
import com.funambol.client.controller.Controller;
import com.funambol.client.localization.Localization;
import com.funambol.util.Log;
import com.funambol.util.StringUtil;


/**
 * Application main view. Inside this activity sources are explored both with
 * a general resume for all of them and with detailed views for each one
 */
public class AndroidMainScreen extends BasicFragmentActivity implements MainScreen {
    private static final String TAG_LOG = AndroidMainScreen.class.getSimpleName();

    private static final int MENUID_LOGOUT = Menu.FIRST + 1;
    private static final int MENUID_ABOUT = Menu.FIRST + 2;
    private static final int MENUID_REFRESH = Menu.FIRST + 3;
    private static final int MENUID_SETTINGS = Menu.FIRST + 4;
    private static final int MENUID_UPLOAD = Menu.FIRST + 5;
    private static final int MENUID_NOTIFICATIONS = Menu.FIRST + 6;

    // These two constants are used only in test recording mode /////////
    private static final int MENUID_START_TEST = Menu.FIRST + 7;
    private static final int MENUID_END_TEST = Menu.FIRST + 8;
    /////////////////////////////////////////////////////////////////////

    private static final int MULTI_SELECT_MAX_ITEMS_COUNT = 20;

    private Localization localization;
    private AndroidMainScreenController mainScreenController;
    private enum RefreshMenuState {
        Refresh,
        Cancel,
        Cancelling
    }
    //saved button status to avoid mismatch between the moment when the user
    // click on menu button and the moment when user clicked on menu. For
    // example, user open the option menu and the button says "Cancel", but
    // the refresh is already ended when the user, some time after, click on
    // the button. 
    private RefreshMenuState refreshMenuButtonState;
    private boolean pendingError;
    
    private BalloonHelpView balloonHelpView;
    private View mainPanel;

    /**
     * Called with the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AppInitializer appInitializer = AppInitializer.i(this);
        Controller controller = appInitializer.getController();
        localization = appInitializer.getLocalization();
       
        // Make sure when the app is opened (for any reason) the ImagePool is clean
        ImagePool.getInstance().reset();
        
        // Create the controller
        // TODO Wouldn't be better to put this code inside the getHomeScreenController method?
        mainScreenController = (AndroidMainScreenController)controller.getMainScreenController();
        if (mainScreenController == null) {
            mainScreenController = new AndroidMainScreenController(
                    getApplicationContext(),
                    controller,
                    this);
            controller.setMainScreenController(mainScreenController);
        }
        mainScreenController.setMainScreen(this);
        screenController = mainScreenController;

        setContentView(R.layout.acthome);
        
        mainPanel = findViewById(R.id.home_mainpanel);
        
        balloonHelpView = (BalloonHelpView)findViewById(R.id.home_balloonhelp);
        balloonHelpView.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View arg0, MotionEvent arg1) {
                mainScreenController.balloonHelpTouched();
                return true;
            }
        });
        balloonHelpView.setTextSize(getResources().getDimension(R.dimen.help_fontsize));
        balloonHelpView.setTextColor(getResources().getColor(R.color.darkGray1));
        balloonHelpView.hide();
        
        mainScreenController.initScreen(null == savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!AndroidUtils.isSDCardMounted()) {
            Log.error(TAG_LOG, "SD Card is not mounted. "
                    + "The application cannot work properly.");
            String message = localization.getLanguage("message_sd_card_unmounted");
            String okLabel = localization.getLanguage("dialog_ok");
            String appName = localization.getLanguage("app_name");
            message = StringUtil.replaceAll(message, "__APP_NAME__", appName);
            mainScreenController.getController().getDisplayManager().
                    showOkDialog(this, message, okLabel, new Runnable() {
                public void run() {
                    finish();
                }
            }, false);
            return;
        }
    }
   
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        // On old version of android, the onBackPressed was not invoked
        if (Integer.parseInt(android.os.Build.VERSION.SDK) < 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            onBackPressed();
        }

        return super.onKeyDown(keyCode, event);
    }
    
    @Override
    public void onBackPressed() {
        // We do not exit the activity in all cases, it actually depends on the current tab being shown
        mainScreenController.backPressed();
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (Log.isLoggable(Log.DEBUG)) {
            Log.debug(TAG_LOG, "Nullifying home screen controller reference");
        }
        mainScreenController = null;
    }
    
    /** Create the Activity menu. */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuItem refreshItem = menu.add(0, MENUID_REFRESH, Menu.NONE, 
                localization.getLanguage("menu_refresh"));
        refreshItem.setIcon(R.drawable.menu_imgrefresh_48x48);
        
        MenuItem uploadItem = menu.add(0, MENUID_UPLOAD, Menu.NONE, 
                localization.getLanguage("menu_upload"));
        uploadItem.setIcon(R.drawable.menu_imgupload);
        
        MenuItem notificationsItem = menu.add(0, MENUID_NOTIFICATIONS, Menu.NONE, 
                localization.getLanguage("menu_notifications"));
        notificationsItem.setIcon(R.drawable.menu_imgnotifications_48x48);
        
        MenuItem settingsItem = menu.add(0, MENUID_SETTINGS, Menu.NONE, 
                localization.getLanguage("menu_settings"));
        settingsItem.setIcon(R.drawable.menu_imgsettings_48x48);
        
        MenuItem aboutItem = menu.add(0, MENUID_ABOUT, Menu.NONE, 
                localization.getLanguage("menu_about"));
        aboutItem.setShortcut('0', 'A');
        aboutItem.setIcon(R.drawable.menu_imgabout_48x48);
        
        MenuItem logoutItem = menu.add(0, MENUID_LOGOUT, Menu.NONE, 
                localization.getLanguage("menu_logout"));
        logoutItem.setIcon(R.drawable.ic_menu_logout);

        // This code is here only for the test recording build
        if (BuildInfo.TEST_RECORDING_ENABLED) {
            menu.add(0, MENUID_START_TEST, Menu.NONE, "Start new test");
            menu.add(0, MENUID_END_TEST, Menu.NONE, "End test");
        }

        return true;
    }
    
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        //checks state of refresh menu
        MenuItem refreshMenu = menu.findItem(MENUID_REFRESH);
        
        if (mainScreenController.isSynchronizing()) {
            //cancel was already launched
            if (mainScreenController.isRefreshCancelling()) {
                refreshMenu.setTitle(localization.getLanguage("menu_cancel_refresh_in_progress"));
                refreshMenu.setIcon(R.drawable.menu_imgcancel_48x48);
                refreshMenu.setEnabled(false);
                refreshMenuButtonState = RefreshMenuState.Cancelling;
            } else {
                //change to cancel
                refreshMenu.setTitle(localization.getLanguage("menu_cancel_refresh"));
                refreshMenu.setIcon(R.drawable.menu_imgcancel_48x48);
                refreshMenuButtonState = RefreshMenuState.Cancel;
            }
        } else {
            //restore to sync icon
            refreshMenu.setTitle(localization.getLanguage("menu_refresh"));
            refreshMenu.setIcon(R.drawable.menu_imgrefresh_48x48);
            refreshMenuButtonState = RefreshMenuState.Refresh;
        }
        refreshMenu.setEnabled(true);
        
        // Set the selective upload status
        boolean uploadEnabled = mainScreenController.isUploadEnabled();
        MenuItem uploadMenu = menu.findItem(MENUID_UPLOAD);
        uploadMenu.setEnabled(uploadEnabled);
       
        return super.onPrepareOptionsMenu(menu);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean result = false;
        
        // Handle all of the possible menu actions
        switch (item.getItemId()) {
            case MENUID_REFRESH:
                if (mainScreenController.isSynchronizing()) {
                    if (mainScreenController.isRefreshCancelling()) {
                        if (RefreshMenuState.Cancelling == refreshMenuButtonState) { }  //nothing to do if the sync in already in a cancelling phase
                    } else {
                        if (RefreshMenuState.Cancel == refreshMenuButtonState) mainScreenController.cancelSync();
                    }
                } else {
                    if (RefreshMenuState.Refresh == refreshMenuButtonState) mainScreenController.startRefresh();
                }
                result = true;
                break;
            case MENUID_LOGOUT:
                mainScreenController.logout();
                result = true;
                break;
            case MENUID_SETTINGS:
                mainScreenController.showSettingsScreen();
                result = true;
                break;
            case MENUID_ABOUT:
                mainScreenController.showAboutScreen();
                result = true;
                break;
            case MENUID_NOTIFICATIONS:
                mainScreenController.showNotificationScreen();
                result = true;
                break;
            case MENUID_UPLOAD:
                mainScreenController.selectiveUpload();
                result = true;
                break;

            //// This code is for test recording mode only ///////////////
            case MENUID_START_TEST:
                PimTestRecorder.getInstance().startTestPressed(this);
                result = true;
                break;
            case MENUID_END_TEST:
                PimTestRecorder.getInstance().endTestPressed();
                result = true;
                break;
            //// This code is for test recording mode only ///////////////

            default:
                result = super.onOptionsItemSelected(item); 
                break;
        }
        return result;
    }

    /**
     * This method allows the controller to set a pending error. This implementation changes the icon of the
     * notifications into a warning/error icon
     */
    public void setPendingError(boolean value) {
        pendingError = value;
        if (mainScreenController != null) {
            mainScreenController.updateActionBar();
        }
    }
    
    public void showBalloonHelp(final Widget widget, final String help) {
        this.runOnUiThread(new Runnable() {
            public void run() {
                View view = (View)widget;
                int viewCoords[] = new int[2];
                int parentCoords[] = new int[2];
                view.getLocationInWindow(viewCoords);
                mainPanel.getLocationOnScreen(parentCoords);
                
                //finds position of the control inside main windows, because
                // coordintates returned from getLocationOnScreen included also
                // android notification bar etc...
                int startingX = viewCoords[0] - parentCoords[0];
                int startingY = viewCoords[1] - parentCoords[1];
                
                //add some pixels to the top-left edge
                //int x = (int) (startingX + getResources().getDimension(R.dimen.common_thumbnail_max_width) / 2);
                //new requirement: ballon at the center of the help
                int x = startingX + view.getWidth() / 2;
                
                //if the view is in the upper part of the screen, preferred
                // balloon direction is below the control, otherwise is  above
                DisplayMetrics displaymetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
                int preferredPosition;
                int y;
                
                if (viewCoords[1] < displaymetrics.heightPixels / 2) {
                    //upper part of the screen, balloon below
                    y =  startingY + view.getHeight() - AndroidUtils.dipToPx(15, getBaseContext());
                    preferredPosition = BalloonHelpView.PREFERRED_POSITION_BELOW;
                } else {
                    //lower part of the screen, balloon above
                    y =  startingY + AndroidUtils.dipToPx(10, getBaseContext());
                    preferredPosition = BalloonHelpView.PREFERRED_POSITION_ABOVE;
                }
                balloonHelpView.show(x, y, help, preferredPosition);
            }
        });
    }
    
    public void hideBalloonHelp() {
        this.runOnUiThread(new Runnable() {
            public void run() {
                balloonHelpView.hide();
            }
        });
    }

    // ----------------------------------------------------------------- Screen
    
    public void close() {
        finish();
    }

    @Override
    public Object getUiScreen() {
        return this;
    }

    // ----------------------------------------------------- NavigationBarWidget
    
    public void addNavigationBarButton(NavigationBarButtonView buttonToAdd) {
        if (Log.isLoggable(Log.TRACE)) {
            Log.trace(TAG_LOG, "Adding new button to navigation bar");
        }
        ViewGroup viewGroup = (ViewGroup) findViewById(R.id.navigationbar);
        //not safe cast, but how can we prevent this framework not-so-good behavior?
        View viewToAdd = (View) buttonToAdd;
        viewGroup.addView(viewToAdd);
    }

    public void setNavigationBarVisible(boolean visible) {
        View view = findViewById(R.id.navigationbar);
        if(view != null) {
            view.setVisibility(visible ? View.VISIBLE : View.GONE);
        }
    }

    // ---------------------------------------------------- MultiSelectBarWidget

    public void setMultiSelectBarVisible(boolean visible) {
        View view = findViewById(R.id.multiselect_btnbar);
        if(view != null) {
            view.setVisibility(visible ? View.VISIBLE : View.GONE);
        }
    }

    public void setOnButtonClickedListener(final OnButtonClickedListener onButtonClickedListener) {
        ViewGroup viewGroup = (ViewGroup)findViewById(R.id.multiselect_btnbar);
        Button saveButton = (Button)viewGroup.findViewById(R.id.multiselect_btnSave);
        Button shareButton = (Button)viewGroup.findViewById(R.id.multiselect_btnShare);
        if(onButtonClickedListener != null) {
            saveButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    onButtonClickedListener.buttonClicked(BUTTON_ID_SAVE);
                }
            });
            shareButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    onButtonClickedListener.buttonClicked(BUTTON_ID_SHARE);
                }
            });
        } else {
            saveButton.setOnClickListener(null);
            shareButton.setOnClickListener(null);
        }
    }

    public void selectedItemsCountChanged(int count) {
        ViewGroup viewGroup = (ViewGroup)findViewById(R.id.multiselect_btnbar);
        Button saveButton = (Button)viewGroup.findViewById(R.id.multiselect_btnSave);
        Button shareButton = (Button)viewGroup.findViewById(R.id.multiselect_btnShare);
        if(count <= 0 || count > MULTI_SELECT_MAX_ITEMS_COUNT) {
            shareButton.setEnabled(false);
            saveButton.setEnabled(false);
        } else {
            shareButton.setEnabled(true);
            saveButton.setEnabled(true);
        }
    }
    
    // ------------------------------------------------ ActionBarWidgetContainer
    
    public ActionBarWidget getActionBarWidget() {
        AndroidActionBarView actionBarView = (AndroidActionBarView) findViewById(R.id.actionbar);
        return actionBarView;
    }
    
    public Bitmap getActionBitmap(int actionId) {
        switch (actionId) {
        case AndroidMainScreenController.ActionBarIds.HISTORY:
            if (pendingError) {
                return new Bitmap(new Integer(R.drawable.actionbar_imghistory_error_16x16));
            } else {
                return new Bitmap(new Integer(R.drawable.actionbar_imghistory_16x16));
            }
        case AndroidMainScreenController.ActionBarIds.START_REFRESH:
            return new Bitmap(new Integer(R.drawable.actionbar_selrefresh_16x16));
        case AndroidMainScreenController.ActionBarIds.SETTINGS:
            return new Bitmap(new Integer(R.drawable.actionbar_imgsettings_16x16));
        case AndroidMainScreenController.ActionBarIds.UPLOADITEM:
            return new Bitmap(new Integer(R.drawable.actionbar_imgupload_16x16));
        case AndroidMainScreenController.ActionBarIds.MULTISELECT:
            return new Bitmap(new Integer(R.drawable.actionbar_imgmultiselect_16x16));
        default:
            return null;
        }
    }
    
    // ----------------------------------------------------------------- Widget
    public Object getContainerUiScreen() {
        return this;
    }

    public void pause() {
        // TODO Auto-generated method stub
    }

    public void resume() {
        // TODO Auto-generated method stub
    }

    public void destroy() {
        // TODO Auto-generated method stub
    }
    // ------------------------------------------------------------------------
}