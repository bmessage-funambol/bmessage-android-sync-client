/*
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2011 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.activities;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.funambol.android.AppInitializer;
import com.funambol.android.activities.view.AndroidActionBarView;
import com.funambol.androidsync.R;
import com.funambol.client.configuration.Configuration;
import com.funambol.client.controller.Controller;
import com.funambol.client.controller.ServiceAuthenticatorScreenController;
import com.funambol.client.controller.ServiceAuthenticatorScreenController.WebAuthenticationListener;
import com.funambol.client.localization.Localization;
import com.funambol.client.ui.DisplayManager;
import com.funambol.client.ui.ServiceAuthenticatorScreen;
import com.funambol.client.ui.view.ActionBarWidget;
import com.funambol.sapisync.SapiSyncHandler;
import com.funambol.util.Log;
import com.funambol.util.StringUtil;


public class AndroidServiceAuthenticatorScreen extends BasicActivity
        implements ServiceAuthenticatorScreen {

    private static final String TAG_LOG =
            AndroidServiceAuthenticatorScreen.class.getSimpleName();

    private Controller controller;
    private Configuration configuration;
    private Localization localization;
    private DisplayManager displayManager;
    private ServiceAuthenticatorScreenController screenController;

    private WebView webView;

    private String lastAuthUrl;

    private static int pDialogId = -1;

    private static AuthWebViewClient webViewClient;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Log.isLoggable(Log.TRACE)) {
            Log.trace(TAG_LOG, "onCreate");
        }
        AppInitializer appInitializer = AppInitializer.i(this);
        controller = appInitializer.getController();
        configuration = controller.getConfiguration();
        localization = controller.getLocalization();
        displayManager = controller.getDisplayManager();

        setContentView(R.layout.actserviceauthenticator);
        
        webView = (WebView)findViewById(R.id.serviceauthenticator_weburl);
        webView.getSettings().setJavaScriptEnabled(true);

        screenController = controller.getServiceAuthenticatorScreenController();
        screenController.setScreen(this);
        screenController.initScreen(null == savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(final Bundle b) {
        super.onSaveInstanceState(b);
        if(webView != null) {
            hideProgressDialog();
            String authUrl = lastAuthUrl;
            if(webView.getUrl() != null) {
                authUrl = webView.getUrl();
            }
            b.putString("AUTH_URL", authUrl);
        }
    }

    @Override
    public void onRestoreInstanceState(Bundle b) {
        super.onRestoreInstanceState(b);
        if(webView != null) {
            webView.setWebViewClient(webViewClient);
            if(b.containsKey("AUTH_URL")) {
                lastAuthUrl = b.getString("AUTH_URL");
                loadUrl(lastAuthUrl);
            }
        }
    }

    public Object getUiScreen() {
        return this;
    }

    @Override
    public void onDestroy() {
        if(webView != null) {
            webView.destroy();
            webView = null;
        }
        super.onDestroy();        
    }

    public void startAuthenticationFromUrl(String authUrl, WebAuthenticationListener listener) {
        if(Log.isLoggable(Log.DEBUG)) {
            Log.debug(TAG_LOG, "Starting authentication from url: " + authUrl);
        }
        // Reset all cookies first.
        CookieSyncManager.createInstance(getApplicationContext());
        CookieManager.getInstance().removeAllCookie();

        lastAuthUrl = authUrl;
        webViewClient = new AuthWebViewClient(listener, authUrl);

        webView.setWebViewClient(webViewClient);
        new AuthThead(webViewClient.getAuthUrl()).start();
    }

    private void loadUrl(final String authUrl) {
        runOnUiThread(new Runnable() {
            public void run() {
                if(webView != null) {
                    webView.loadUrl(authUrl);
                }
            }
        });
    }
    
    private class AuthThead extends Thread {

        private String authUrl;
        
        public AuthThead(String authUrl) {
            this.authUrl = authUrl;
        }
        
        @Override
        public void run() {

            showProgressDialog();
            
            // Perform a SAPI login to ensure a valid session
            SapiSyncHandler sapi = new SapiSyncHandler(configuration.getSyncUrl(),
                                                       configuration.getUsername(),
                                                       configuration.getPassword());

            try {
                // Make sure we have a valid jsession id
                sapi.login(null /* deviceid is not necessary */);
                String jsessionId = sapi.getLastUsedJSessionId();

                // If the jsessionid is not set here we have to reset it by logging out
                // and login again
                if(jsessionId == null) {
                    sapi.logout();
                    sapi.login(null /* deviceid is not necessary */);
                    jsessionId = sapi.getLastUsedJSessionId();
                }

                String serverUrl = configuration.getSyncUrl();
                String hostName = StringUtil.extractAddressFromUrl(serverUrl,
                        StringUtil.getProtocolFromUrl(serverUrl));

                // Update cookie with the new JSESSIONID
                StringBuffer newCookie = new StringBuffer();
                newCookie.append("JSESSIONID=").append(jsessionId);

                if(Log.isLoggable(Log.DEBUG)) {
                    Log.debug(TAG_LOG, "Updating cookie for host: "  + hostName + " cookie: "+ newCookie);
                }

                // Commit the new cookie
                CookieManager.getInstance().setCookie(hostName, newCookie.toString());

            } catch(Exception ex) {
                Log.error(TAG_LOG, "Failed to update cookie", ex);
            }
            hideProgressDialog();
            loadUrl(authUrl);
        }
    }

    private class AuthWebViewClient extends WebViewClient {

        private final String TAG_LOG = AuthWebViewClient.class.getSimpleName();
        
        private WebAuthenticationListener listener;
        private String authUrl;

        public AuthWebViewClient(WebAuthenticationListener listener, String authUrl) {
            this.listener = listener;
            this.authUrl = authUrl;
        }
        
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if(Log.isLoggable(Log.TRACE)) {
                Log.trace(TAG_LOG, "shouldOverrideUrlLoading: " + url);
            }
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onReceivedError(WebView view, int errorCode,
                String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            if(Log.isLoggable(Log.TRACE)) {
                Log.trace(TAG_LOG, "onReceivedError: " + errorCode + " " + description);
            }
            listener.onAuthError(description);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            if(Log.isLoggable(Log.TRACE)) {
                Log.trace(TAG_LOG, "onPageStarted: " + url);
            }
            // The authentication callback url contains "landing_success"
            if (url.contains("landing_success")) {
                listener.onAuthComplete();
            } else {
                showProgressDialog();
            }
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if(Log.isLoggable(Log.TRACE)) {
                Log.trace(TAG_LOG, "onPageFinished: " + url);
            }
            hideProgressDialog();
        }

        public String getAuthUrl() {
            return authUrl;
        }
    }

    private void showProgressDialog() {
        if(pDialogId == -1) {
            String message = localization.getLanguage("message_please_wait");
            pDialogId = displayManager.showProgressDialog(this, message, new Runnable() {
                public void run() {
                    displayManager.hideScreen(AndroidServiceAuthenticatorScreen.this);
                }
            });
        }
    }

    private void hideProgressDialog() {
        if(pDialogId != -1) {
            displayManager.dismissProgressDialog(this, pDialogId);
            pDialogId = -1;
        }
    }

    // ----------------------------------------------- ActionBarWidgetContainer
    
    public ActionBarWidget getActionBarWidget() {
        AndroidActionBarView actionBarView = (AndroidActionBarView) findViewById(R.id.actionbar);
        return actionBarView;
    }

    public com.funambol.client.ui.Bitmap getActionBitmap(int actionId) {
        return null;
    }
    
}
