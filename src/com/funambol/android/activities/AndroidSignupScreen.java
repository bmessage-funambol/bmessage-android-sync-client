/*
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2009 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.activities;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.accounts.Account;
import android.app.Dialog;
import android.os.Bundle;
import android.accounts.AccountAuthenticatorActivity;
import android.graphics.BitmapFactory;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.text.util.Linkify;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.funambol.androidsync.R;
import com.funambol.android.AndroidAccountManager;
import com.funambol.android.AppInitializer;
import com.funambol.android.activities.view.AndroidActionBarView;
import com.funambol.android.controller.AndroidController;
import com.funambol.android.controller.AndroidSignupScreenController;
import com.funambol.client.controller.SignupScreenController;
import com.funambol.client.customization.Customization;
import com.funambol.client.localization.Localization;
import com.funambol.client.ui.SignupScreen;
import com.funambol.client.configuration.Configuration;
import com.funambol.client.test.BasicConstants;
import com.funambol.client.ui.view.ActionBarWidget;
import com.funambol.platform.NetworkStatus;
import com.funambol.util.Log;
import com.funambol.util.StringUtil;


public class AndroidSignupScreen extends AccountAuthenticatorActivity implements SignupScreen {

    private static final String TAG_LOG = AndroidSignupScreen.class.getSimpleName();

    private EditText  userEditField;
    private EditText  passEditField;
    private EditText  serverEditField;

    private EditText  captchaField;
    private ImageView captchaImage;
    private byte[]    captchaBitmap;

    private CheckBox  showPassword;
    
    private TextView  termsAndConditionsField;

    private Button    signupButton;
    private Button    finishButton;

    private ViewGroup signupViewsContainer;
    private ViewGroup credentialsViewsContainer;
    private ViewGroup captchaViewsContainer;

    private View captchaFocusView;
    
    private SignupScreenController signupScreenController;

    private Customization customization;
    private Localization localization;

    private AndroidDisplayManager displayManager;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        AppInitializer appInitializer = AppInitializer.i(this);
        AndroidController gc = appInitializer.getController();
        Configuration configuration = appInitializer.getConfiguration();

        Account account = AndroidAccountManager.getNativeAccount(this);
        // Check if a Funambol account already exists
        if(account != null && !configuration.getCredentialsCheckPending()) {
            String label = getString(R.string.alert_one_account_supported_1) + " " +
                           getString(R.string.account_label) + " " +
                           getString(R.string.alert_one_account_supported_2);
            Toast.makeText(this, label, Toast.LENGTH_LONG).show();
            gc.getDisplayManager().hideScreen(this);
        }

        customization = appInitializer.getCustomization();
        localization  = appInitializer.getLocalization();
        displayManager = appInitializer.getDisplayManager();

        setContentView(R.layout.actsignup);

        signupViewsContainer = (ViewGroup)findViewById(R.id.signup_container);
        
        credentialsViewsContainer = new LinearLayout(this);
        captchaViewsContainer = new LinearLayout(this);

        View.inflate(this, R.layout.vwsignupcredentials, credentialsViewsContainer);
        View.inflate(this, R.layout.vwsignupcaptcha, captchaViewsContainer);

        userEditField = (EditText)credentialsViewsContainer.findViewById(R.id.signupscreen_lblusername);
        userEditField.setInputType(InputType.TYPE_CLASS_PHONE);

        userEditField.setOnTouchListener(new ShowServerUrlEasterEgg());

        passEditField = (EditText)credentialsViewsContainer.findViewById(R.id.signupscreen_lblpassword);
        serverEditField = (EditText)credentialsViewsContainer.findViewById(R.id.signupscreen_lblserver);

        captchaField = (EditText)captchaViewsContainer.findViewById(R.id.signupscreen_lbltoken);
        captchaField.setInputType(InputType.TYPE_CLASS_NUMBER);
        
        captchaImage = (ImageView)captchaViewsContainer.findViewById(R.id.signupscreen_imgcaptcha);

        captchaFocusView = captchaViewsContainer.findViewById(R.id.signupscreen_layfocus);

        termsAndConditionsField = (TextView)credentialsViewsContainer.findViewById(
                R.id.signupscreen_lblterms);
        
        String terms = localization.getLanguage("signupscreen_lblTerms");
        String termsAndConditions = localization.getLanguage("common_lblTermsAndConditions");
        String privacyPolicy = localization.getLanguage("common_lblPrivacyPolicy");

        terms = StringUtil.replaceAll(terms, "__TERMS_AND_CONDITIONS__", termsAndConditions);
        terms = StringUtil.replaceAll(terms, "__PRIVACY_POLICY__", privacyPolicy);

        termsAndConditionsField.setText(terms);
        Linkify.addLinks(termsAndConditionsField, Pattern.compile(termsAndConditions), 
                "", null, new Linkify.TransformFilter() {
            public final String transformUrl(final Matcher match, String url) {
                return customization.getTermsAndConditionsUrl();
            }
        });

        Linkify.addLinks(termsAndConditionsField, Pattern.compile(privacyPolicy), 
                "", null, new Linkify.TransformFilter() {
            public final String transformUrl(final Matcher match, String url) {
                return customization.getPrivacyPolicyUrl();
            }
        });
        
        showPassword = (CheckBox)credentialsViewsContainer.findViewById(
                R.id.signupscreen_lblshowpassword);
        showPassword.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton cb, boolean isChecked) {
                if(isChecked) {
                    passEditField.setTransformationMethod(null);
                } else {
                    passEditField.setTransformationMethod(
                            new PasswordTransformationMethod());
                }
            }
        });
        showPassword.setChecked(true);

        signupButton = (Button)credentialsViewsContainer.findViewById(R.id.signupscreen_btnsignup);
        signupButton.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                signupScreenController.continueWithSignup();
            }
        });

        finishButton = (Button)captchaViewsContainer.findViewById(R.id.signupscreen_btncaptcha);
        finishButton.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                signupScreenController.signup();
            }
        });

        // Set buttons tag for testing purposes only
        signupButton.setTag(BasicConstants.BUTTON_SIGNUP);
        finishButton.setTag(BasicConstants.BUTTON_FINISH);

        promptCredentials();

        // We must initialize the controller here
        NetworkStatus networkStatus = new NetworkStatus();
        signupScreenController = new AndroidSignupScreenController(gc, this, networkStatus);
        signupScreenController.initScreen();
    }

    @Override
    public void onSaveInstanceState(Bundle b) {
        super.onSaveInstanceState(b);
        b.putBoolean("captcha", isCaptchaPrompted());
        if(captchaBitmap != null) {
            b.putByteArray("captcha_image", captchaBitmap);
        }
        b.putString("captcha_token", getCaptchaToken());

        b.putString("account_username", getUsername());
        b.putString("account_password", getPassword());

        b.putBoolean("account_show_password", showPassword.isChecked());

        b.putString("jsessionid", signupScreenController.getCurrentJSessionId());
    }

    @Override
    public void onRestoreInstanceState(Bundle b) {
        super.onRestoreInstanceState(b);
        if(b.getBoolean("captcha", false)) {
            promptCaptcha();
        }
        if(b.containsKey("captcha_token")) {
            setCaptchaToken(b.getString("captcha_token"));
        }
        if(b.containsKey("captcha_image")) {
            setCaptchaImage(b.getByteArray("captcha_image"));
        }
        if(b.containsKey("account_username")) {
            setUsername(b.getString("account_username"));
        }
        if(b.containsKey("account_password")) {
            setPassword(b.getString("account_password"));
        }
        if(b.getBoolean("account_show_password", false)) {
            showPassword.setChecked(true);
        }
        if(b.containsKey("jsessionid")) {
            signupScreenController.setCurrentJSessionId(b.getString("jsessionid"));
        }
    }

    public String getServerUrl() {
        return serverEditField.getText().toString();
    }
    
    public void setServerUrl(String serverUrl) {
        serverEditField.setText(serverUrl);
    }

    public String getUsername() {
        return userEditField.getText().toString();
    }

    public void setUsername(String username) {
        userEditField.setText(username);
    }

    public String getPassword() {
        return passEditField.getText().toString();
    }

    public void setPassword(String password) {
        passEditField.setText(password);
    }

    public void setCaptchaToken(final String token) {
        runOnUiThread(new Runnable() {
            public void run() {
                captchaField.setText(token);
                if(captchaFocusView != null) {
                    captchaFocusView.requestFocus();
                }
            }
        });
    }

    public String getCaptchaToken() {
        return captchaField.getText().toString();
    }

    public void setCaptchaImage(final byte[] image) {
        runOnUiThread(new Runnable() {
            public void run() {
                android.graphics.Bitmap captcha =
                        BitmapFactory.decodeByteArray(image, 0, image.length);
                captchaImage.setImageBitmap(captcha);
                captchaBitmap = image;
            }
        });
    }

    public void promptCredentials() {
        promptView(credentialsViewsContainer);
    }

    public void promptCaptcha() {
        promptView(captchaViewsContainer);
    }

    private void promptView(final View view) {
        runOnUiThread(new Runnable() {
            public void run() {
                if(signupViewsContainer.getChildCount() == 1 &&
                   signupViewsContainer.getChildAt(0) != view) {
                    signupViewsContainer.removeView(signupViewsContainer.getChildAt(0));
                    signupViewsContainer.addView(view);
                } else if(signupViewsContainer.getChildCount() == 0) {
                    signupViewsContainer.addView(view);
                }
            }
        });
    }

    private boolean isCaptchaPrompted() {
        return (signupViewsContainer.getChildCount() == 1 &&
                signupViewsContainer.getChildAt(0) == captchaViewsContainer);
    }

    public boolean isPasswordShowed() {
        return !(passEditField.getTransformationMethod()
                instanceof PasswordTransformationMethod);
    }

    public void addShowPasswordField(boolean checked) {
        View v = credentialsViewsContainer.findViewById(R.id.signupscreen_layshowpassword);
        v.setVisibility(View.VISIBLE);
    }

    public Object getUiScreen() {
        return this;
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        if (Log.isLoggable(Log.TRACE)) {
            Log.trace(TAG_LOG, "onCreateDialog: " + id);
        }
        Dialog result = null;
        if(displayManager != null) {
            result = displayManager.createDialog(id);
        }
        if(result != null) {
            return result;
        } else {
            return super.onCreateDialog(id);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // The activity becomes visible
        displayManager.addActivityOnStack(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // The activity is no longer visible
        displayManager.removeActivityFromStack(this);
    }

    public ActionBarWidget getActionBarWidget() {
        AndroidActionBarView actionBarView = (AndroidActionBarView) findViewById(R.id.actionbar);
        return actionBarView;
    }

    public com.funambol.client.ui.Bitmap getActionBitmap(int actionId) {
        return null;
    }

    private class ShowServerUrlEasterEgg implements View.OnTouchListener {
        private int clicks = 10;

        public boolean onTouch(View view, MotionEvent me) {
            switch(me.getAction()) {
                case MotionEvent.ACTION_UP:
                    if(--clicks <= 0) {
                        View serverEditView = findViewById(R.id.signupscreen_layserver);
                        serverEditView.setVisibility(View.VISIBLE);
                    }
                    break;
            }
            return false;
        }
    }
    
    @Override
    public void onBackPressed() {
        if(!signupScreenController.backPressed()) {
            super.onBackPressed();
        }
    }
}
