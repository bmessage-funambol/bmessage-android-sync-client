/**
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2011 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */
package com.funambol.android.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.View;

/**
 * Fragment used to manage dialog (informative, yes/no, progress)
 * 
 * In addition to normal parameters (title, message etc), I need to set also
 * listeners for yes/no events and progress appear/disappear, but they cannot 
 * be stored with {@link DialogFragment#setArguments(Bundle)}.
 * 
 * So, a call to {@link Fragment#setRetainInstance(boolean)} is made,
 * field values are not lost during configuration changes and arguments set
 * is no more mandatory.
 * I maintained arguments way of pass values to take trace of this way, just in
 * case my memory will fail in the future ;)
 * 
 * WARNING: Could not work on some Android 3.x devices, because of issue #17423
 * http://code.google.com/p/android/issues/detail?id=17423
 * 
 * @author Alfredo "Rainbowbreeze" Morresi
 */
public class AlertDialogFragment extends DialogFragment {
    // ------------------------------------------ Private Fields
    private static final String KEY_TYPE = "type";
    private static final String KEY_TITLE = "title";
    private static final String KEY_MESSAGE = "message";
    private static final String KEY_CANCELABLE = "cancelable";
    private static final String KEY_NEUTRAL_LABEL = "neutralLabel";
    private static final String KEY_POSITIVE_LABEL = "positiveLabel";
    private static final String KEY_NEGATIVE_LABEL = "negativeLabel";
    private static final String KEY_INDETERMINATED_PROGRESS = "indeterminateProgress";
    
    private static final int DIALOG_INFO = 1;
    private static final int DIALOG_YESNO = 2;
    private static final int DIALOG_PROGRESS = 3;
    private static final int DIALOG_RAW = 4;

    // -------------------------------------------- Constructors
    
    // --------------------------------------- Public Properties
    DialogInterface.OnClickListener positiveClickListener;
    DialogInterface.OnClickListener negativeClickListener;
    DialogInterface.OnClickListener neutralClickListener;
    DialogInterface.OnCancelListener onCancelListener;
    DialogInterface.OnDismissListener onDismissListener;
    RawDialogViewsFactory viewsFactory;

    static AlertDialogFragment newInstance() {
        AlertDialogFragment d = new AlertDialogFragment();
        return d;
    }

    // -------------------------------------------------- Events
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    };

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        if(onCancelListener != null) {
            onCancelListener.onCancel(dialog);
        }
    };

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int type = getArguments().getInt(KEY_TYPE);
        String title = getArguments().getString(KEY_TITLE);
        String message = getArguments().getString(KEY_MESSAGE);
        boolean cancelable = getArguments().getBoolean(KEY_CANCELABLE);
        boolean indeterminateProgress = getArguments().getBoolean(KEY_INDETERMINATED_PROGRESS);

        Dialog dialog;
        switch (type) {
        case DIALOG_INFO:
            String okButtonLabel = getArguments().getString(KEY_NEUTRAL_LABEL);
            dialog = new AlertDialog.Builder(getActivity())
                    .setTitle(title)
                    .setMessage(message)
                    .setCancelable(cancelable)
                    .setNeutralButton(okButtonLabel, neutralClickListener)
                    .create();
            break;
        case DIALOG_YESNO:
            String positiveButtonLabel = getArguments().getString(KEY_POSITIVE_LABEL);
            String negativeButtonLabel = getArguments().getString(KEY_NEGATIVE_LABEL);
            dialog = new AlertDialog.Builder(getActivity())
                    .setTitle(title)
                    .setMessage(message)
                    .setCancelable(cancelable)
                    .setPositiveButton(positiveButtonLabel, positiveClickListener)
                    .setNegativeButton(negativeButtonLabel, negativeClickListener)
                    .create();
            break;
        case DIALOG_PROGRESS:
            ProgressDialog progressDialog = new ProgressDialog(getActivity());
            if (!TextUtils.isEmpty(title)) progressDialog.setTitle(title);
            if (!TextUtils.isEmpty(message)) progressDialog.setMessage(message);
            progressDialog.setIndeterminate(indeterminateProgress);
            progressDialog.setCancelable(cancelable);
            progressDialog.setOnDismissListener(onDismissListener);
            progressDialog.setOnCancelListener(onCancelListener);
            dialog = progressDialog;
            break;
        case DIALOG_RAW:
            dialog = new AlertDialog.Builder(getActivity())
                    .setCustomTitle(viewsFactory.createTitleView(getActivity()))
                    .setView(viewsFactory.createBodyView(getActivity()))
                    .setCancelable(cancelable)
                    .setOnCancelListener(onCancelListener)
                    .create();
            break;
        default:
            dialog = null;
            break;
        }
        return dialog;
    }
    
    /**
     * workaround for issue #17423
     */
    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance()) {
            getDialog().setOnDismissListener(null);
        }
        super.onDestroyView();
    }
    
    // ------------------------------------------ Public Methods
    public static class Factory {
        public static DialogFragment createInformativeDialog(
                String title,
                String message,
                String neutralButtonLabel) {
            return createInformativeDialog(title, message, neutralButtonLabel, null, false);
        }
        
        public static DialogFragment createInformativeDialog(
                String title,
                String message,
                String neutralButtonLabel,
                DialogInterface.OnClickListener neutralClickListener,
                boolean cancelable)
        {
            AlertDialogFragment dialog = AlertDialogFragment.newInstance();
            Bundle args = new Bundle();
            args.putInt(KEY_TYPE, DIALOG_INFO);
            args.putString(KEY_TITLE, title);
            args.putString(KEY_MESSAGE, message);
            args.putString(KEY_NEUTRAL_LABEL, neutralButtonLabel);
            args.putBoolean(KEY_CANCELABLE, cancelable);
            dialog.setArguments(args);
            dialog.neutralClickListener = neutralClickListener;
            return dialog;
        }
        
        public static DialogFragment createYesNoDialog(
                String title,
                String message,
                String positiveButtonLabel,
                DialogInterface.OnClickListener positiveClickListener,
                String negativeButtonLabel,
                DialogInterface.OnClickListener negativeClickListener)
        {
            AlertDialogFragment dialog = AlertDialogFragment.newInstance();
            Bundle args = new Bundle();
            args.putInt(KEY_TYPE, DIALOG_YESNO);
            args.putString(KEY_TITLE, title);
            args.putString(KEY_MESSAGE, message);
            args.putString(KEY_POSITIVE_LABEL, positiveButtonLabel);
            args.putString(KEY_NEGATIVE_LABEL, negativeButtonLabel);
            args.putBoolean(KEY_CANCELABLE, false);
            dialog.setArguments(args);
            dialog.positiveClickListener = positiveClickListener;
            dialog.negativeClickListener = negativeClickListener;
            return dialog;
        }
        
        public static DialogFragment createProgressDialog(
                String title,
                String message,
                boolean indeterminate,
                DialogInterface.OnCancelListener onCancelListener)
        {
            AlertDialogFragment dialog = AlertDialogFragment.newInstance();
            Bundle args = new Bundle();
            args.putInt(KEY_TYPE, DIALOG_PROGRESS);
            if (!TextUtils.isEmpty(title)) args.putString(KEY_TITLE, title);
            if (!TextUtils.isEmpty(message)) args.putString(KEY_MESSAGE, message);
            args.putBoolean(KEY_INDETERMINATED_PROGRESS, indeterminate);
            args.putBoolean(KEY_CANCELABLE, (null != onCancelListener));
            dialog.setArguments(args);
            dialog.onCancelListener = onCancelListener;
            return dialog;
        }
        
        /**
         * Create a dialog, using a predefined View and an optional cancel listener
         * 
         * @param titleRawView content of the title of the dialog
         * @param bodyRawView content of the dialog
         * @param onCancelListener
         * @return
         */
        public static DialogFragment createRawDialog(
                RawDialogViewsFactory viewsFactory,
                OnCancelListener onCancelListener)
        {
            //should work, never tested. In case of errors, let me know (Alf)
            AlertDialogFragment dialog = AlertDialogFragment.newInstance();
            Bundle args = new Bundle();
            args.putInt(KEY_TYPE, DIALOG_RAW);
            args.putBoolean(KEY_CANCELABLE, (null != onCancelListener));
            dialog.setArguments(args);
            dialog.viewsFactory = viewsFactory;
            dialog.onCancelListener = onCancelListener;
            return dialog;
        }
    }

    // ----------------------------------------- Private Methods
    
    public interface RawDialogViewsFactory {
        public View createTitleView(FragmentActivity a);
        public View createBodyView(FragmentActivity a);
    }
}
