/**
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2011 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.activities;

import com.funambol.androidsync.R;
import com.funambol.util.Log;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;

/**
 * Various helpers for fragment management
 *
 */
public class FragmentHelper {
    private static final String LOG_TAG = FragmentHelper.class.getSimpleName();

    /**
     * 
     * @param fragmentGenerator
     * @param containerActivity
     * @param currentFragment
     * @param fragmentId
     * @param backStackEnabled
     * @return
     */
    public static Fragment switchFragment(
            FragmentGenerator fragmentGenerator,
            FragmentActivity containerActivity,
            Fragment currentFragment,
            int nextWidgetId,
            boolean backStackEnabled) {
        //the canonical name of the fragment class is used as fragmentId, so
        //be aware and don't use same fragment with different instances,
        //otherwise the switch mechanism will not work
        String currentFragmentId = fragmentGenerator.getFragmentUniqueId(currentFragment);
        String nextFragmentUniqueId = fragmentGenerator.getFragmentUniqueId(nextWidgetId);
        if (currentFragment != null &&
                (currentFragmentId.equals(nextFragmentUniqueId))) {
            return currentFragment;
        }
        
        FragmentManager fragmentManager = containerActivity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        
        //show/hide mechanism works as long as the fragment instance is retained upon
        //activity rotation
        if (!TextUtils.isEmpty(currentFragmentId)) {
            hideFragment(fragmentManager, fragmentTransaction, currentFragmentId);
        }
        
        Fragment nextFragment = findFragment(containerActivity, nextFragmentUniqueId);
        if (null != nextFragment) {
            fragmentTransaction.show(nextFragment);
        } else {
            if (Log.isLoggable(Log.TRACE)) {
                Log.trace(LOG_TAG, "Adding fragment " + nextFragmentUniqueId);
            }
            nextFragment = fragmentGenerator.createFragment(nextWidgetId);
            fragmentTransaction.add(
                    R.id.common_fragmentcontainer,
                    nextFragment,
                    nextFragmentUniqueId);
        }
        
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_ENTER_MASK);
        if (backStackEnabled) {
            // Keep the transaction in the back stack so it will be reversed when back button is pressed
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.commit();
        return nextFragment;
    }

    /**
     * Shows the given fragment
     */
    public static Fragment showFragment(int fragmentId, FragmentGenerator fragmentGenerator,
            FragmentActivity containerActivity) {
        String fragmentUniqueId = fragmentGenerator.getFragmentUniqueId(fragmentId);
        Fragment fragment = findFragment(containerActivity, fragmentUniqueId);
        FragmentManager fragmentManager = containerActivity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if(fragment == null) {
            fragment = fragmentGenerator.createFragment(fragmentId);
            fragmentTransaction.add(R.id.common_fragmentcontainer, fragment, fragmentUniqueId);
        }
        fragmentTransaction.show(fragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_ENTER_MASK);
        fragmentTransaction.commit();
        return fragment;
    }
    
    public static Fragment findFragment(FragmentActivity containerActivity,
            String fragmentTag) {
        FragmentManager fm = containerActivity.getSupportFragmentManager();
        return findFragment(fm, fragmentTag);
    }
    
    private static Fragment findFragment(FragmentManager fragmentManager,
            String fragmentTag) {
        Fragment fragment = fragmentManager.findFragmentByTag(fragmentTag);
        return fragment;
    }
    
    /**
     * Hides a fragment, if it is loaded.
     * 
     * @param containerActivity
     * @param fragmentToHide
     */
    private static void hideFragment(
            FragmentManager fragmentManager,
            FragmentTransaction fragmentTransaction,
            String fragmentIdToHide) {
        
        Fragment fragmentToHide = findFragment(fragmentManager, fragmentIdToHide);
        if (null != fragmentToHide) {
            fragmentTransaction.hide(fragmentToHide);
        }
    }
}
