/** Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2011 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */
package com.funambol.android.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.funambol.android.source.media.MediaAppSyncSource;
import com.funambol.android.source.media.MediaDirectoryScanner;
import com.funambol.android.source.media.MediaDirectoryScanner.ScanMessage;
import com.funambol.androidsync.R;
import com.funambol.client.controller.ActionBarController;
import com.funambol.client.controller.SourceChronologicalViewController;
import com.funambol.client.controller.SourceSelectiveUploadViewController;
import com.funambol.client.localization.Inflexion;
import com.funambol.client.ui.Screen;
import com.funambol.client.ui.SelectiveUploadScreen;
import com.funambol.util.StringUtil;
import com.funambol.util.bus.BusMessage;
import com.funambol.util.bus.BusMessageHandler;
import com.funambol.util.bus.BusService;


public class SourceSelectiveUploadFragment extends SourceChronologicalFragment
        implements BusMessageHandler {

    private boolean isFirstScan = false;
    private boolean scanning = false;
    private final Object lock = new Object();

    private static int pDialogId = -1;

    public SourceSelectiveUploadFragment() {
        super(false);
        BusService.registerMessageHandler(ScanMessage.class, this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(appSource != null) {
            // Trigger a refresh of the current source directory at first run
            boolean firstRun = (savedInstanceState == null);
            if(firstRun) {
                MediaDirectoryScanner scanner = ((MediaAppSyncSource)appSource)
                        .getMediaDirectoryScanner();
                synchronized(lock) {
                    isFirstScan = scanner.getIsFirstScan();
                    scanner.scan(true);
                    scanning = true;
                    // If this is the first directory refresh it may take much
                    // time to complete
                    if(isFirstScan) {
                        showProgressDialog();
                    }
                }
            }
        } else {
            throw new IllegalStateException("appSource is null");
        }
    }

    protected SourceChronologicalViewController createController() {
        return new SourceSelectiveUploadViewController(this, appSource, 
                localization, displayManager, globalController);
    }

    public void receiveMessage(BusMessage message) {
        if(message instanceof ScanMessage) {
            ScanMessage sm = (ScanMessage)message;
            if(sm.getAppSource() == appSource &&
               sm.getCode() == ScanMessage.SCAN_COMPLETED) {
                scanCompleted();
            }
        }
    }

    public boolean runOnSeparateThread() {
        return false;
    }

    @Override
    protected View createPlaceHolderView(Context context,  boolean enabled) {
        if(scanning) {
            // If the directory refresh task is still in progress we display a
            // spinning wheel as placeholder
            LinearLayout placeHolder = new LinearLayout(context);
            View.inflate(getContainerActivity(), R.layout.vwsourceviewplaceholderprogress, placeHolder);
            return placeHolder;
        } else {
            View placeHolder = super.createPlaceHolderView(context, enabled);
            TextView titleView = (TextView)placeHolder.findViewById(
                    R.id.sourceviewplaceholder_lblTitle);
            TextView textView = (TextView)placeHolder.findViewById(
                    R.id.sourceviewplaceholder_lblPlaceholder);
            String title = localization.getLanguage("source_placeholder_selective_upload_title");
            title = StringUtil.replaceAll(title, "__THINGS__",
                    appSource.getItemInflexion().using(Inflexion.PLURAL).toString());
            titleView.setText(title);
            String text = localization.getLanguage("source_placeholder_selective_upload_text");
            text = StringUtil.replaceAll(text, "__THINGS__",
                    appSource.getItemInflexion().using(Inflexion.PLURAL).toString());
            textView.setText(text);
            return placeHolder;
        }
    }

    public void scanCompleted() {
        synchronized(lock) {
            scanning = false;
            Activity act = (Activity)getContainerUiScreen();
            if (act != null) {
                act.runOnUiThread(new Runnable() {
                    public void run() {
                        // Force the layout to be reinizialized
                        status = Status.EMPTY;
                        dataChanged();
                    }
                });
            }
            hideProgressDialog();
            if(isFirstScan) {
                // If this was a first scan we have to trigger an additional scan
                // in order to ensure the thumbnails creation
                // NOTE: the first scan does not create thumbnails
                MediaDirectoryScanner scanner = ((MediaAppSyncSource)appSource)
                        .getMediaDirectoryScanner();
                scanner.scan(true);
                isFirstScan = false;
            }
        }
    }

    @Override
    protected void setListView() {
        super.setListView();
        // If we have at least one element in the ui we change the bar title
        if (controller.getWindowSize() != 0) {
            String newTitle = localization.getLanguage("actionbar_select_items");
            ActionBarController actionBarController = ((SelectiveUploadScreen)
                    getContainerUiScreen()).getController().getActionBarController();
            actionBarController.setTitle(newTitle);
        }
    }

    @Override
    protected void setEmptyView() {
        super.setEmptyView();
        ActionBarController actionBarController = ((SelectiveUploadScreen)
                getContainerUiScreen()).getController().getActionBarController();
        actionBarController.setTitle(appSource.getName());
    }

    private void showProgressDialog() {
        final String message = localization.getLanguage("message_please_wait");
        final Activity activity = getContainerActivity();
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    pDialogId = displayManager.showProgressDialog((Screen)activity, message,
                            new Runnable() {
                                public void run() {
                                    displayManager.hideScreen((Screen)activity);
                                }
                            });
                }
            });
        }
    }

    private void hideProgressDialog() {
        final Activity activity = getContainerActivity();
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    if(pDialogId != -1){
                        displayManager.dismissProgressDialog((Screen)activity, pDialogId);
                        pDialogId = -1;
                    }
                }
            });
        }
    }
}
