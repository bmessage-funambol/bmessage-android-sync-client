/*
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2009 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.activities;

import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.funambol.androidsync.R;
import com.funambol.android.AppInitializer;
import com.funambol.android.activities.view.AndroidActionBarView;
import com.funambol.client.controller.Controller;
import com.funambol.client.controller.EmailRequestScreenController;
import com.funambol.client.test.BasicConstants;
import com.funambol.client.ui.EmailRequestScreen;
import com.funambol.client.ui.view.ActionBarWidget;


public class AndroidEmailRequestScreen extends BasicActivity implements EmailRequestScreen {

    private static final String TAG_LOG = AndroidEmailRequestScreen.class.getSimpleName();

    private EditText emailEditField;

    private EmailRequestScreenController emailRequestScreenController;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        AppInitializer appInitializer = AppInitializer.i(this);
        Controller controller = appInitializer.getController();

        setContentView(R.layout.actemailrequest);

        emailEditField = (EditText)findViewById(R.id.emailrequestscreen_lblemail);
        emailEditField.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);

        Button continueButton = (Button)findViewById(R.id.emailrequestscreen_btncontinue);
        continueButton.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                emailRequestScreenController.updateEmail();
            }
        });
        
        // Set buttons tag for testing purposes only
        continueButton.setTag(BasicConstants.BUTTON_CONTINUE);

        emailRequestScreenController = new EmailRequestScreenController(this, controller);
        emailRequestScreenController.initScreen();
    }

    public String getEmail() {
        return emailEditField.getText().toString();
    }

    public void setEmail(String email) {
        emailEditField.setText(email);
    }

    public Object getUiScreen() {
        return this;
    }

    public ActionBarWidget getActionBarWidget() {
        AndroidActionBarView actionBarView = (AndroidActionBarView) findViewById(R.id.actionbar);
        return actionBarView;
    }

    public com.funambol.client.ui.Bitmap getActionBitmap(int actionId) {
        return null;
    }

    @Override
    public void onBackPressed() {
        if(!emailRequestScreenController.backPressed()) {
            super.onBackPressed();
        }
    }
}
