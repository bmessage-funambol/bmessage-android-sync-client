/**
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2011 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.activities;

import java.util.Vector;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.funambol.android.AndroidUtils;

import com.funambol.android.AppInitializer;
import com.funambol.android.activities.view.AndroidActionBarView;
import com.funambol.android.activities.view.ZoomableImageView;
import com.funambol.androidsync.R;
import com.funambol.client.configuration.Configuration;
import com.funambol.client.controller.Controller;
import com.funambol.client.controller.OpenItemScreenController;
import com.funambol.client.services.ExternalServiceCache;
import com.funambol.client.services.Service;
import com.funambol.client.source.AppSyncSource;
import com.funambol.client.source.AppSyncSourceManager;
import com.funambol.client.test.BasicConstants;
import com.funambol.client.ui.Bitmap;
import com.funambol.client.ui.OpenItemScreen;
import com.funambol.client.ui.view.ActionBarWidget;
import com.funambol.util.Log;

public class AndroidOpenItemScreen extends BasicActivity implements OpenItemScreen, GestureDetector.OnGestureListener {
    private static final String TAG_LOG = AndroidOpenItemScreen.class.getSimpleName();

    private static final int SWIPE_MIN_DISTANCE_DP = 120;
    private static final int SWIPE_MAX_OFF_PATH_DP = 250;
    private static final int SWIPE_THRESHOLD_VELOCITY_DP = 200;

    private Controller controller;
    private OpenItemScreenController openItemScreenController;

    //private ImageView imageView;
    private ZoomableImageView imageView;
    private TextView message;
    private View emptyArea;
    private LinearLayout detailsContainer;
    private TextView detailsName;
    private ImageButton buttonMore;
    private ImageButton buttonNext;
    private ImageButton buttonPrevious;
    private ImageButton buttonDelete;
    private AppSyncSource appSource;
    private GestureDetector gestureDetector;
    private View layItemInfoPanel;
    private ImageView playControl;
    private View openControl;
    private LinearLayout playControlContainer;
    private TextView unplayableLabel;
    private boolean bitmapRecyclable = false;
    private String imagePath = null;

    private int swipeMinDispace;
    private int swipeMaxOffPath;
    private int swipeThresholdVelocity;
    
    /**
     * Called with the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Log.isLoggable(Log.TRACE)) {
            Log.trace(TAG_LOG, "Creating screen");
        }

        AppInitializer appInitializer = AppInitializer.i(this);
        controller = appInitializer.getController();
        Configuration configuration = appInitializer.getConfiguration();

        setContentView(R.layout.actopenitem);

        imageView = (ZoomableImageView) findViewById(R.id.openitem_imgpreview);
        //image cannot be rotate by touch gestures
        imageView.setAllowRotation(false);
        imageView.setTabletMode(configuration.getDeviceInfo().isTablet());
        //this screen manages fling gesture to move between images, so the image
        //cannot manage the onTouchEvent event by itself in its customized way
        imageView.setOverrideOnTouchEvent(false);
        message = (TextView) findViewById(R.id.openitem_lblmessage);
        emptyArea = (View) findViewById(R.id.openitem_vwemptyarea);
        detailsName = (TextView) findViewById(R.id.openitem_lblname);
        detailsContainer = (LinearLayout) findViewById(R.id.openitem_layitemdetails);
        buttonMore = (ImageButton) findViewById(R.id.openitem_btnshare);
        buttonPrevious = (ImageButton) findViewById(R.id.openitem_btnprevious);
        buttonNext = (ImageButton) findViewById(R.id.openitem_btnnext);
        buttonDelete = (ImageButton) findViewById(R.id.openitem_btndelete);
        layItemInfoPanel = findViewById(R.id.openitem_layinfopanel);
        playControlContainer = (LinearLayout)findViewById(R.id.openitem_layplay);
        playControl = (ImageView) findViewById(R.id.openitem_btnplay);
        unplayableLabel = (TextView) findViewById(R.id.openitem_lblunplayable);
        setItemDetailVisible(false);
        
        // If the user clicks on the details, we close them (we allow clicking on the details and on the black space)
        LinearLayout layItemDetails = (LinearLayout) findViewById(R.id.openitem_layitemdetails);
        layItemDetails.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                setItemDetailVisible(false);
            }
        });
        
        layItemInfoPanel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                setItemDetailVisible(false);
            }
        });

        // Set buttons tag for testing purposes only
        buttonMore.setTag(BasicConstants.BUTTON_SHARE);
        buttonPrevious.setTag(BasicConstants.BUTTON_PREVIOUS);
        buttonNext.setTag(BasicConstants.BUTTON_NEXT);
        buttonDelete.setTag(BasicConstants.BUTTON_DELETE);

        // We expect the source id in the extras, otherwise we cannot find the
        // proper controller
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        int appSourceId = -1;
        if (extras != null) {
            appSourceId = extras.getInt(AndroidDisplayManager.APP_SOURCE_ID_PARAM, -1);
        }
        if (appSourceId != -1) {
            AppSyncSourceManager appSyncSourceManager = appInitializer
                    .getAppSyncSourceManager();
            appSource = appSyncSourceManager.getSource(appSourceId);
            openItemScreenController = appSource.getOpenItemScreenController();
            
            if (openItemScreenController == null) {
                Log.error(TAG_LOG,
                        "No controller! This screen needs a controller before being created");
            } else {
                openItemScreenController.setOpenItemScreen(this);
                openItemScreenController.initScreen(null == savedInstanceState);
                
                buttonMore.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        openItemScreenController.moreButtonSelected();
                    }
                });
                buttonPrevious.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        // Execute this in a different thread so that the UI gets updated immediately, before
                        // loading the image
                        Thread t = new Thread(new Runnable() {
                            public void run() {
                                openItemScreenController.previous();
                            }
                        });
                        t.start();
                    }
                });
                buttonNext.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        // Execute this in a different thread so that the UI gets updated immediately, before
                        // loading the image
                        Thread t = new Thread(new Runnable() {
                            public void run() {
                                openItemScreenController.next();
                            }
                        });
                        t.start();
                    }
                });
                buttonDelete.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        openItemScreenController.deleteButtonSelected();
                    }
                });
                Log.info(TAG_LOG, "Screen fully initialized");
            }
        } else {
            Log.error(TAG_LOG, "Unknwon app source id, cannot show preview");
            controller.getDisplayManager().hideScreen(this);
            return;
        }

        gestureDetector = new GestureDetector(this, this);
        
        //swipe gesture detection values must be adapted to the screen density
        swipeMinDispace = AndroidUtils.dipToPx(SWIPE_MIN_DISTANCE_DP, this);
        swipeMaxOffPath = AndroidUtils.dipToPx(SWIPE_MAX_OFF_PATH_DP, this);
        swipeThresholdVelocity = AndroidUtils.dipToPx(SWIPE_THRESHOLD_VELOCITY_DP, this);
    }

    //////////////////////////////////////////////////////////////////////

    @Override
    public void onDestroy() {
        super.onDestroy();
        openItemScreenController.setOpenItemScreen(null);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            // If the details are currently shown, we just close them on the
            // back
            // key. Otherwise we really close the screen
            if (layItemInfoPanel.getVisibility() == View.VISIBLE) {
                toggleDetails();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        try {
            // jump right out if not a swipe/fling
            if (Math.abs(e1.getY() - e2.getY()) > swipeMaxOffPath) {
                return false;
            }

            // right to left swipe
            if (e1.getX() - e2.getX() > swipeMinDispace
                    && Math.abs(velocityX) > swipeThresholdVelocity) {
                openItemScreenController.next();
            } else if (e2.getX() - e1.getX() > swipeMinDispace
                    && Math.abs(velocityX) > swipeThresholdVelocity) {
                openItemScreenController.previous();
            }
        } catch (Exception e) {
            Log.error(TAG_LOG, "Cannot detect fling", e);
        }
        return true;

    }

    @Override
    public boolean onTouchEvent(MotionEvent motionEvent) {
        imageView.onAlternativeTouchEvent(motionEvent);
        //if the image hasn't managed the event (because the fling tries
        //to move the image over its border without success, for example),
        //the event is passed to this view.
        if (imageView.getLatestTouchEventProcessed()) {
            return true;
        } else {
            return gestureDetector.onTouchEvent(motionEvent);
        }
    }

    public void onLongPress(MotionEvent e) {
        // Intentionally not handling - must be overridden by listener class
    }

    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
            float distanceY) {
        return true;
    }

    public void onShowPress(MotionEvent e) {
        // Intentionally not handling - must be overridden by listener class
    }

    public boolean onSingleTapUp(MotionEvent e) {
        // Intentionally not handling - must be overridden by listener class
        // Intentionally returning true - per code examples
        return true;
    }

    public boolean onDown(MotionEvent e) {
        // Intentionally not handling - must be overridden by listener class
        // Intentionally returning true - per code examples
        return true;
    }

    // ////////////////////////////////////////////////////////////////////

    public Object getUiScreen() {
        return this;
    }

    public void close() {
        controller.getDisplayManager().hideScreen(this);
    }
    

    public void setImage(final String path) {
        
        if (path != null && path.length() != 0) {
            
            if (path.equals(imagePath)) {
                Log.debug(TAG_LOG, "Image did not change, no need to refresh it");
                
            } else {
                
                this.imagePath = path;
                runOnUiThread(new Runnable() {
                    public void run() {
                        freeBitmap();
                        // Allow the previous image to be collected
                        imageView.setImageBitmap(null);
                        // This is a good time for GC
                        System.gc();
                        try {
                            BitmapDrawable drawable = new BitmapDrawable(getResources(), path);
                            imageView.setImageDrawable(drawable);
                            bitmapRecyclable = true;
                        } catch (OutOfMemoryError oom) {
                            // The image cannot be shown
                            Log.error(TAG_LOG, "Cannot show image", oom);
                            controller.getDisplayManager().showMessage(AndroidOpenItemScreen.this,
                                              controller.getLocalization().getLanguage("dialog_insufficient_memory"));
                        } catch (Exception e) {
                            Log.error(TAG_LOG, "Cannot show image", e);
                            controller.getDisplayManager().showMessage(AndroidOpenItemScreen.this,
                                              controller.getLocalization().getLanguage("dialog_cannot_open_image"));
                        }
                    }
                });
            }
        }
    }
    
    public void setImage(final Bitmap bitmap) {
        
        this.imagePath = null;
        
        if (bitmap != null) {
            runOnUiThread(new Runnable() {
                public void run() {
                    freeBitmap();
                    // Allow the previous image to be collected
                    imageView.setImageBitmap(null);
                    // This is a good time for GC
                    System.gc();
                    bitmapRecyclable = false;
                    try {
                        imageView.setImageResource( (Integer) bitmap.getOpaqueDescriptor());
                    } catch (OutOfMemoryError oom) {
                        // The image cannot be shown
                        Log.error(TAG_LOG, "Cannot show image", oom);
                        controller.getDisplayManager().showMessage(AndroidOpenItemScreen.this,
                                          controller.getLocalization().getLanguage("dialog_insufficient_memory"));
                    } catch (Exception e) {
                        Log.error(TAG_LOG, "Cannot show image", e);
                        controller.getDisplayManager().showMessage(AndroidOpenItemScreen.this,
                                          controller.getLocalization().getLanguage("dialog_cannot_open_image"));
                    }
                }
            });
        }
    }
    
    public void setImageWithMessage(final Bitmap bitmap, final String text) {

        Log.trace(TAG_LOG, "start setImageWithMessage \"" + text + '\"');
        
        setMessage(text);
        if (bitmap != null) {
            
           setPreviewSmall(true);           
           setImage(bitmap);
        }        
        
        Log.trace(TAG_LOG, "end setImageWithMessage");
    }
    
    private void setPreviewSmall(boolean visible) {
        
        if (Log.isLoggable(Log.TRACE)) {
            Log.trace(TAG_LOG, "setPreviewSmall " + visible);
        }
        final int visibility;
        if (visible) {
            visibility = View.VISIBLE;
        } else {
            visibility = View.GONE;
        }
        runOnUiThread(new Runnable() {
            public void run() {
                emptyArea.setVisibility(visibility);
                message.setVisibility(visibility);
            }
        });
    }

    public void setMessage(final String text) {
        
        if (Log.isLoggable(Log.TRACE)) {
            Log.trace(TAG_LOG, "setMessage \"" + text + '\"');
        }
        
        runOnUiThread(new Runnable() {
            public void run() {
                message.setText(text);
            }
        });
    }
    
    public void setTitle(final String title) {
        runOnUiThread(new Runnable() {
            public void run() {
                openItemScreenController.setTitle(title);
            }
        });
    }
    
    public void setDetails(final String name, final Vector labels, final Vector values, 
            final String exportedServicesLabel, final String exportedServicesValues[]) {
        
        runOnUiThread(new Runnable() {
            public void run() {
                setDetailsInternal(name, labels, values, exportedServicesLabel, exportedServicesValues);
            }
        });
    }

    private void setDetailsInternal(String name, Vector labels, Vector values, 
            String exportedServicesLabel, String exportedServicesValues[]) {

        if (detailsContainer == null) {
            Log.error(TAG_LOG, "Cannot set details: screen not initialized");
            return;
        }

        detailsName.setText(name);

        detailsContainer.removeViews(1, detailsContainer.getChildCount() - 1);

        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        for (int i = 0; i < labels.size(); i++) {
            String label = (String) labels.elementAt(i);
            String value = (String) values.elementAt(i);
            if (Log.isLoggable(Log.INFO)) {
                Log.info(TAG_LOG, "Setting " + label + " to \"" + value + "\"");
            }
            View detailView = layoutInflater.inflate(
                    R.layout.vwopenitemdetail, null);
            TextView labelView = (TextView) detailView
                    .findViewById(R.id.openitemdetail_lblLabel);
            TextView valueView = (TextView) detailView
                    .findViewById(R.id.openitemdetail_lblValue);
            labelView.setText(label);
            valueView.setText(value);

            detailsContainer.addView(detailView);
        }

        // Add the exported information
        if (exportedServicesLabel != null) {
            
            View publishedView = layoutInflater.inflate(R.layout.vwopenitempublished, null);
            TextView labelView = (TextView)publishedView.findViewById(R.id.openitempublished_lblLabel);
            LinearLayout serviceIcons = (LinearLayout)publishedView.findViewById(
                                                                      R.id.openitempublished_layIcons);
            labelView.setText(exportedServicesLabel);

            ExternalServiceCache services = controller.getExternalServiceCache();
            for(int i=0;i<exportedServicesValues.length;++i) {
                String serviceName = exportedServicesValues[i];
                ImageView serviceIcon = new ImageView(this);

                Service service = services.getCachedService(serviceName);
                if (service != null) {
                    byte serviceIconBytes[] = service.getIcon();
                    android.graphics.Bitmap bitmap = BitmapFactory.decodeByteArray(serviceIconBytes, 0, serviceIconBytes.length);
                    serviceIcon.setImageBitmap(bitmap);
                }
                serviceIcon.setPadding(AndroidUtils.dipToPx(2, this), 0,
                                       AndroidUtils.dipToPx(2, this), 0);
                serviceIcon.setAdjustViewBounds(true);
                serviceIcon.setMaxHeight(AndroidUtils.dipToPx(20, this));
                serviceIcon.setMaxWidth(AndroidUtils.dipToPx(20, this));
                serviceIcons.addView(serviceIcon);
            }
            detailsContainer.addView(publishedView);
        }
    }

    public void toggleDetails() {
        if (layItemInfoPanel.getVisibility() == View.VISIBLE) {
            setItemDetailVisible(false);
        } else {
            setItemDetailVisible(true);
        }
    }

    private void setItemDetailVisible(boolean show) {
        layItemInfoPanel.setVisibility(show ? View.VISIBLE : View.GONE);
    }


    public void hidePlayControl(final String errorMessage) {
        runOnUiThread(new Runnable() {
            public void run() {
                playControlContainer.setVisibility(View.GONE);
                playControl.setVisibility(View.GONE);
                if (errorMessage != null) {
                    unplayableLabel.setText(errorMessage);
                    unplayableLabel.setVisibility(View.VISIBLE);
                    playControl.setVisibility(View.VISIBLE);
                } else {
                    unplayableLabel.setVisibility(View.GONE);
                }
            }
        });
    }

    public void showPlayControl(final OnPlayListener onPlayListener, final int iconType) {
       
        if (Log.isLoggable(Log.DEBUG)) {
            Log.debug(TAG_LOG, "Is imageView shown? " + (imageView.isShown() ? "yes" : "no"));
        }
        runOnUiThread(new Runnable() {
            public void run() {    
                playControl.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        onPlayListener.onPlay();
                    }
                });
                playControlContainer.setVisibility(View.VISIBLE);
                unplayableLabel.setVisibility(View.GONE);
                imageView.setVisibility(View.VISIBLE);
                switch (iconType) {
                    case OPEN:
                        playControl.setImageResource(R.drawable.openitem_btnopen);
                        break;
                    case PLAY:
                    default:
                        playControl.setImageResource(R.drawable.openitem_btnplay);
                }
                playControl.setVisibility(View.VISIBLE);
            }
        });
    }
    
    private void freeBitmap() {
        if (bitmapRecyclable) {
            if (imageView.getDrawable() instanceof BitmapDrawable) {
                BitmapDrawable bd = (BitmapDrawable) imageView.getDrawable();
                if (bd.getBitmap() != null) {
                    if (Log.isLoggable(Log.TRACE)) {
                        Log.trace(TAG_LOG, "Recycling bitmap " + bd.getBitmap());
                    }
                    bd.getBitmap().recycle();
                }
            }
        }
    }

    // ----------------------------------------------- ActionBarWidgetContainer
    
    public ActionBarWidget getActionBarWidget() {
        AndroidActionBarView actionBarView = (AndroidActionBarView) findViewById(R.id.actionbar);
        return actionBarView;
    }

    public Bitmap getActionBitmap(int actionId) {
        switch (actionId) {
        case OpenItemScreenController.ActionBarIds.INFO:
            return new Bitmap(new Integer(R.drawable.actionbar_imginfo_16x16));
        default:
            return null;
        }
    }
    
}
