/*
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2011 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.activities;

import android.widget.AdapterView;
import java.util.Vector;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.funambol.android.AppInitializer;
import com.funambol.android.activities.view.AndroidActionBarView;
import com.funambol.android.controller.AndroidNotificationScreenController;
import com.funambol.androidsync.R;
import com.funambol.client.controller.Controller;
import com.funambol.client.controller.NotificationScreenController;
import com.funambol.client.notification.Notification;
import com.funambol.client.ui.Bitmap;
import com.funambol.client.ui.NotificationScreen;
import com.funambol.client.ui.view.ActionBarWidget;
import com.funambol.util.Log;
import com.funambol.util.StringUtil;
import java.text.DateFormat;


public class AndroidNotificationScreen extends BasicActivity implements NotificationScreen {

    private static final String TAG_LOG = AndroidNotificationScreen.class.getSimpleName();

    private Controller controller;
    
    private NotificationScreenController notificationScreenController;

    private ListView notificationList;
    private NotificationListAdapter notificationListAdapter;

    private LayoutInflater layoutInflater;

    /**
     * Called with the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Log.isLoggable(Log.TRACE)) {
            Log.trace(TAG_LOG, "onCreate");
        }
        AppInitializer appInitializer = AppInitializer.i(this);
        controller = appInitializer.getController();

        layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        setContentView(R.layout.actnotification);

        notificationList = (ListView)findViewById(R.id.actnotification_list);

        notificationList.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Notification n = (Notification)notificationList.getItemAtPosition(position);
                notificationScreenController.notificationPressed(n);
            }
        });

        notificationScreenController = new AndroidNotificationScreenController(this, controller);
        notificationScreenController.initScreen(null == savedInstanceState);
    }

    public void setNotifications(Vector notifications) {
        if(notificationListAdapter == null) {
            notificationListAdapter = new NotificationListAdapter(notifications);
        } else {
            notificationListAdapter.updateNotifications(notifications);
        }
        notificationList.setAdapter(notificationListAdapter);
    }

    private class NotificationListAdapter extends BaseAdapter {

        private Vector notifications;

        public NotificationListAdapter(Vector notifications) {
            this.notifications = notifications;
        }

        public int getCount() {
            return notifications.size();
        }

        public Object getItem(int position) {
            return notifications.elementAt(position);
        }

        public long getItemId(int position) {
            return position;
        }

        /**
         * Make a SpeechView to hold each row.
         *
         * @see android.widget.ListAdapter#getView(int, android.view.View,
         *      android.view.ViewGroup)
         */
        public View getView(int position, View convertView, ViewGroup parent) {
            Notification notification = (Notification)notifications.elementAt(position);
            if (convertView == null) {
                convertView = (LinearLayout)layoutInflater.inflate(R.layout.rownotification, null);
            }
            ImageView statusView = (ImageView)convertView.findViewById(R.id.rownotification_imgstatusicon);
            ImageView moreView = (ImageView)convertView.findViewById(R.id.rownotification_imgmoreicon);
            TextView dateView = (TextView)convertView.findViewById(R.id.rownotification_lbldate);
            TextView timeView = (TextView)convertView.findViewById(R.id.rownotification_lbltime);
            TextView shortMessageView = (TextView)convertView.findViewById(R.id.rownotification_lblmessage);

            DateFormat df = android.text.format.DateFormat.getDateFormat(AndroidNotificationScreen.this);
            DateFormat tf = android.text.format.DateFormat.getTimeFormat(AndroidNotificationScreen.this);

            dateView.setText(df.format(notification.getTimestamp()));
            timeView.setText(tf.format(notification.getTimestamp()));

            shortMessageView.setText(notification.getShortMessage());

            // Update status icon and colors depending on the notification severity
            switch(notification.getSeverity()) {
                case Notification.SEVERITY_OK:
                    shortMessageView.setTextColor(Color.WHITE);
                    statusView.setImageResource(R.drawable.common_imgcompleted_32x32);
                    break;
                case Notification.SEVERITY_ERROR:
                case Notification.SEVERITY_WARNING:
                    shortMessageView.setTextColor(0xFFDB3634);
                    statusView.setImageResource(R.drawable.common_imgfailed_32x32);
                    break;
            }
            if(!StringUtil.isNullOrEmpty(notification.getDetailedMessage())) {
                moreView.setVisibility(View.VISIBLE);
            } else {
                moreView.setVisibility(View.GONE);
            }
            return convertView;
        }

        public void updateNotifications(Vector notifications) {
            this.notifications = notifications;
            notifyDataSetChanged();
        }
    }

    
    // ----------------------------------------------- ActionBarWidgetContainer
    
    public ActionBarWidget getActionBarWidget() {
        AndroidActionBarView actionBarView = (AndroidActionBarView) findViewById(R.id.actionbar);
        return actionBarView;
    }

    public Bitmap getActionBitmap(int actionId) {
        return null;
    }

    
    // ----------------------------------------------------------------- Screen
    public Object getUiScreen() {
        return this;
    }
}
