/**
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2009 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.activities.settings;

import android.util.AttributeSet;
import android.view.View;
import android.content.Context;
import android.widget.Button;
import android.graphics.drawable.Drawable;

import android.widget.LinearLayout;
import android.widget.ToggleButton;

import com.funambol.androidsync.R;
import com.funambol.android.AppInitializer;
import com.funambol.android.activities.AndroidSettingsScreen;
import com.funambol.android.activities.AndroidDisplayManager;
import com.funambol.android.controller.AndroidAdvancedSettingsScreenController;
import com.funambol.android.controller.AndroidController;
import com.funambol.android.controller.AndroidSettingsScreenController;

import com.funambol.client.ui.AdvancedSettingsScreen;
import com.funambol.client.controller.Controller;
import com.funambol.client.ui.Screen;
import com.funambol.util.Log;

/**
 * Realize the Advanced Settings screen tab into the android sync client. The
 * command items to send and view log and perform reset actions are represented
 * by buttons in this realization. Refer to the related controllers and screen
 * interfaces for further informations.
 */
public class AndroidAdvancedSettingsTab extends AndroidSettingsTab
        implements AdvancedSettingsScreen {
    private static final String TAG_LOG = AndroidAdvancedSettingsTab.class.getSimpleName();

    // Log section elements
    private ToggleButton logBtn;
    private Button viewLogBtn;
    private Button sendLogBtn;
    private LinearLayout logSection;
    private LinearLayout logButtonsRaw;

    // Reset section elements
    private Button resetBtn;
    private LinearLayout resetSection;

    // Import section elements
    private Button importBtn;
    private LinearLayout importSection;

    // Source remote uris elements
    private Button devSettingsBtn;
    private LinearLayout devSettingsSection;

    private LinearLayout settingsContainer;

    private int originalLogLevel;
    private AndroidDisplayManager displayManager;
    private AndroidAdvancedSettingsScreenController screenController;

    
    public AndroidAdvancedSettingsTab(Context context, AttributeSet attrSet) {
    	super(context, attrSet);
	}
    
    public void initialize(AndroidSettingsScreenController settingsController) {
    	
        super.initialize(getContext());

        AppInitializer appInitializer = AppInitializer.i(getContext());
        AndroidController cont = appInitializer.getController();
        displayManager = appInitializer.getDisplayManager();

        screenController = new AndroidAdvancedSettingsScreenController(cont, settingsController, this);

        initScreenElements();
        screenController.initialize();
    }

    /**
     * Get the icon related to the AdvancedSettingsScreenTab that is visible
     * on the edge of the tab over the tab title
     * @return Drawable the indicator icon related to this tab
     */
    public Drawable getIndicatorIcon() {
        return getResources().getDrawable(R.drawable.ic_advanced_tab);
    }

    /**
     * Get the title related to the AdvancedSettingsScreenTab that is visible
     * under the tab icon.
     * @return Stirng the title related to this tab
     */
    public String getIndicatorLabel() {
        return localization.getLanguage("settings_advanced_tab");
    }

    /**
     * Save the values contained into this view using the dedicated controller
     */
    public void saveSettings(SaveSettingsCallback callback) {
        screenController.checkAndSave();
        originalLogLevel = configuration.getLogLevel();
        callback.saveSettingsResult(true);
    }

    public boolean hasChanges() {
        boolean hasChanges = false;
        if((logSection != null) && (originalLogLevel != getViewLogLevel())) {
            hasChanges = true;
        }
        return hasChanges;
    }

    public void enableResetCommand(boolean enable) {
        resetBtn.setEnabled(enable);
    }

    public void enableSendLogCommand(boolean enable) {
        sendLogBtn.setEnabled(enable);
    }

    public void hideLogsSection() {
        settingsContainer.removeView(logSection);
        logSection = null;
    }

    public void hideDevSettingsSection() {
        settingsContainer.removeView(devSettingsSection);
        devSettingsSection = null;
    }

    public void hideImportContactsSection() {
        settingsContainer.removeView(importSection);
        importSection = null;
    }

    public void hideSendLogCommand() {
        logButtonsRaw.removeView(sendLogBtn);
    }

    public void hideViewLogCommand() {
        //removeView(viewLogBtn);
    }

    /**
     * The implementation returns the activity related to this view.
     * @return Object the Activity passed to this view as a constructor
     * parameter
     */
    public Object getUiScreen() {
        return getContext();
    }

    public void setViewLogLevel(int logLevel) {
        boolean checked = logLevel > Log.INFO;
        originalLogLevel = checked ? Log.TRACE : Log.INFO;
        logBtn.setChecked(checked);
    }

    public int getViewLogLevel() {
        return logBtn.isChecked() ? Log.TRACE : Log.INFO;
    }

    private void initScreenElements() {
        View.inflate(getContext(), R.layout.actsettingsadvanced, this);

        settingsContainer = (LinearLayout) findViewById(R.id.settingsadvanced_layMainPanel);

        importSection = (LinearLayout) findViewById(R.id.settingsadvanced_layimport);
        importBtn = (Button) findViewById(R.id.settingsadvanced_btnimport);
        importBtn.setOnClickListener(new ImportListener());
        
        logSection = (LinearLayout) findViewById(R.id.settingsadvanced_laylog);
        logBtn = (ToggleButton) findViewById(R.id.settingsadvanced_tglloglevel);

        logButtonsRaw = (LinearLayout) findViewById(R.id.settingsadvanced_laylogbuttons);
        viewLogBtn = (Button) findViewById(R.id.settingsadvanced_btnviewlog);
        viewLogBtn.setOnClickListener(new ViewLogListener());
        sendLogBtn = (Button) findViewById(R.id.settingsadvanced_btnsendlog);
        sendLogBtn.setOnClickListener(new SendLogListener());

        resetSection = (LinearLayout) findViewById(R.id.settingsadvanced_layreset);
        resetBtn = (Button) findViewById(R.id.settingsadvanced_btnreset);
        resetBtn.setOnClickListener(new ResetListener());

        devSettingsSection = (LinearLayout) findViewById(R.id.settingsadvanced_laydevsettings);
        devSettingsBtn = (Button) findViewById(R.id.settingsadvanced_btndevsettings);
        devSettingsBtn.setOnClickListener(new DevSettingsListener());
        
        this.setVisibility(View.GONE);
    }

    public void cancelSettings() {
    }
    
    public void hideResetSection(){
        settingsContainer.removeView(resetSection);
        resetSection = null;
    }

    /**
     * A call-back invoked when the user presses the reset button.
     */
    private class ResetListener implements OnClickListener {
        public void onClick(View v) {
            AndroidSettingsScreen ass = (AndroidSettingsScreen) getUiScreen();
            //check the changes on other settings tabs before refresh
            if (ass.hasChanges()) {
                    displayManager.askYesNoQuestion(ass, localization.getLanguage(
                    "settings_changed_alert"),
                    new Runnable() {
                        AndroidSettingsScreen ass = (AndroidSettingsScreen) getUiScreen();
                        public void run() {
                            // Start reset through the SaveCallback
                            ass.save(false, new ResetSaveCallback());
                        }
                    },
                    new Runnable() {
                        public void run() {
                        }
                }, 0);
            } else {
                screenController.reset();
            }
        }
    }

    /**
     * A call-back invoked when the user presses the reset button.
     */
    private class DevSettingsListener implements OnClickListener {
        public void onClick(View v) {
            // We open a new activity here
            try {
                displayManager.showScreen(Controller.DEV_SETTINGS_SCREEN_ID);
            } catch (Exception e) {
                Log.error(TAG_LOG, "Cannot show dev settings screen", e);
            }
        }
    }

    private class ResetSaveCallback extends SaveSettingsCallback {

        public ResetSaveCallback() {
            super(false, 0);
        }
        
        @Override
        public void tabSettingsSaved(boolean changes) {
            super.tabSettingsSaved(changes);

            if(count == 0 && result == true) {

                displayManager.showMessage(
                        (Screen)getUiScreen(),
                        localization.getLanguage("settings_saved"));
                screenController.reset();
            }
        }
    }

    /**
     * A call-back for when the user presses the view log button.
     */
    private class ViewLogListener implements OnClickListener {
        public void onClick(View v) {
            screenController.viewLog();
        }
    }

    /**
     * A call-back for when the user presses the send log button.
     */
    private class SendLogListener implements OnClickListener {
        public void onClick(View v) {
            screenController.sendLog();
        }
    }

    /**
     * A call-back for when the user presses the Import button.
     */
    private class ImportListener implements OnClickListener {
        public void onClick(View v) {
            screenController.importContacts();
        }
    }
}
