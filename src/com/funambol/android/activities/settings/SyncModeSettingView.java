/**
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2010 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.activities.settings;

import java.util.Hashtable;
import java.util.Vector;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Spinner;
import android.widget.ArrayAdapter;
import android.widget.AdapterView.OnItemSelectedListener;
import android.content.ContentResolver;
import android.content.Context;
import android.net.ConnectivityManager;
import android.widget.AdapterView;

import com.funambol.android.AppInitializer;
import com.funambol.androidsync.R;
import com.funambol.client.ui.SettingsUIItem;
import com.funambol.client.configuration.Configuration;
import com.funambol.client.localization.Localization;
import com.funambol.util.Log;

/**
 * Implements a LinearLayout to display the sync mode setting (manual, push or
 * scheduled) in Android
 */
public class SyncModeSettingView extends LinearLayout implements SettingsUIItem {
    private static final String TAG_LOG = SyncModeSettingView.class.getSimpleName();

    private Spinner syncModeSpinner;
    private TextView syncModeDescription;

    private final Vector<String> syncModeStrings = new Vector<String>();

    private int originalSyncMode;

    private Hashtable<String, Integer> syncModesReference = new Hashtable<String, Integer>();
    private Hashtable<Integer, String> syncModeDescriptions = new Hashtable<Integer, String>();

    public SyncModeSettingView(Context context, int[] syncModes) {

        super(context);

        AppInitializer appInitializer = AppInitializer.i(getContext());
        Localization loc = appInitializer.getLocalization();
        
        // Init the sourceSyncModes
        syncModeStrings.add(loc.getLanguage("sync_mode_manual"));
        syncModeStrings.add(loc.getLanguage("sync_mode_automatic"));
        syncModeDescriptions.put(Configuration.SYNC_MODE_MANUAL,
        		loc.getLanguage("sync_mode_manual_description"));
        syncModeDescriptions.put(Configuration.SYNC_MODE_AUTO,
        		loc.getLanguage("sync_mode_automatic_description"));

        syncModesReference.put(syncModeStrings.get(0), Configuration.SYNC_MODE_MANUAL);        
        syncModesReference.put(syncModeStrings.get(1), Configuration.SYNC_MODE_AUTO);

        boolean isGlobalAutoSyncEnabled = isGlobalAutoSyncEnabled();
        boolean isAutoSyncAvailable = isGlobalAutoSyncEnabled;

        View view = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
        		.inflate(R.layout.vwsettingssyncmode, null);
        this.addView(view);
         
        syncModeSpinner = (Spinner) findViewById(R.id.settings_spisyncmode);
        syncModeSpinner.setPrompt(loc.getLanguage("sync_mode_title"));
        ArrayAdapter<CharSequence> adapter = new ArrayAdapter<CharSequence>(context,
                android.R.layout.simple_spinner_item);

        // Add the sync modes to the spinner adapter
        for(int i=0; i<syncModes.length; i++) {
            if (syncModes[i] != Configuration.SYNC_MODE_AUTO ||
                isAutoSyncAvailable)
            {
                adapter.add(getSyncModeString(syncModes[i]));
            }
        }

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        syncModeSpinner.setAdapter(adapter);

        setLayoutParams(new LinearLayout.LayoutParams(
                LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
        setOrientation(LinearLayout.VERTICAL);

        syncModeDescription = (TextView) findViewById(R.id.settings_lblsyncmodedesc);
        
        if (!isGlobalAutoSyncEnabled) {
            syncModeDescription.setText(loc.getLanguage("conf_automatically_help"));
        } else {
            refreshSyncModeExplanation();
        }

        setSelectedItemListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                    int arg2, long arg3) {
                refreshSyncModeExplanation();
            }
            public void onNothingSelected(AdapterView<?> arg0) {
                // Does nothing
            }
        });
    }

    public void setSyncMode(int mode) {
        ArrayAdapter<CharSequence> adapter = (ArrayAdapter<CharSequence>)syncModeSpinner.getAdapter();
        String syncModeLabel = getSyncModeString(mode);
        boolean done = false;
        for(int i=0;i<adapter.getCount();++i) {
            String currentLabel = adapter.getItem(i).toString();
            if (currentLabel.equals(syncModeLabel)) {
                originalSyncMode = mode;
                syncModeSpinner.setSelection(i);
                done = true;
                break;
            }
        }
        if (!done) {
            if (Log.isLoggable(Log.DEBUG)) {
                Log.debug(TAG_LOG, "Reverting to default sync mode");
            }
            syncModeSpinner.setSelection(0);
        }
    }

    private void refreshSyncModeExplanation() {
    	syncModeDescription.setText(syncModeDescriptions.get(getSyncMode()));
    }

    public void setSelectedItemListener(OnItemSelectedListener listener){
        syncModeSpinner.setOnItemSelectedListener(listener);
    }

    public int getSyncMode() {
        String selectedMode = (String)syncModeSpinner.getSelectedItem();
        return syncModesReference.get(selectedMode);
    }

    public boolean hasChanges() {
        return originalSyncMode != getSyncMode();
    }

    public void loadSettings(Configuration configuration, boolean fromScratch) {
        setSyncMode(configuration.getSyncMode());
    }
    
    public void saveSettings(Configuration conf) {
        conf.setSyncMode(getSyncMode());
    }

    public void enableSyncModeList(boolean enable) {
        syncModeSpinner.setEnabled(enable);
    }
    
    private String getSyncModeString(int mode) {
        for(int i=0; i<syncModeStrings.size(); i++) {
            if(syncModesReference.get(syncModeStrings.get(i)) == mode) {
                return syncModeStrings.get(i);
            }
        }
        return null;
    }

    private boolean isGlobalAutoSyncEnabled() {

        // Get global auto-sync setting
        boolean autoSyncEnabled = ContentResolver.getMasterSyncAutomatically();

        // Get the background data setting
        ConnectivityManager cm = (ConnectivityManager)getContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean backgroundDataEnabled = cm.getBackgroundDataSetting();

        return autoSyncEnabled && backgroundDataEnabled;
    }
 
}

