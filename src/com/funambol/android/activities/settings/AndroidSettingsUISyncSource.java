/*
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2010 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.activities.settings;

import android.app.Activity;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.funambol.android.AndroidCustomization;
import com.funambol.android.AppInitializer;
import com.funambol.androidsync.R;
import com.funambol.client.configuration.Configuration;
import com.funambol.client.localization.Localization;
import com.funambol.client.source.AppSyncSource;
import com.funambol.client.source.AppSyncSourceConfig;
import com.funambol.client.ui.Bitmap;
import com.funambol.client.ui.SettingsUISyncSource;
import com.funambol.sync.SyncSource;
import com.funambol.util.Log;

public class AndroidSettingsUISyncSource extends LinearLayout implements SettingsUISyncSource {
	
    private static final String TAG_LOG = AndroidSettingsUISyncSource.class.getSimpleName();

    protected AppSyncSource appSyncSource;

    protected ImageView sourceIconView;
    protected TextView  titleTextView;
    protected ToggleButton onOffView;
    protected TextView  descTextView;

    private int enabledTextColor;
    private int disabledTextColor;

    private Bitmap enabledIcon;
    private Bitmap disabledIcon;

    protected Localization loc;
    protected AndroidCustomization customization;

    public AndroidSettingsUISyncSource(Activity activity, AppSyncSource appSyncSource) {

        super(activity);
        setSource(appSyncSource);
        
        AppInitializer appInitializer = AppInitializer.i(activity);
        loc = appInitializer.getLocalization();
        customization = appInitializer.getCustomization();
        
        this.setOrientation(LinearLayout.VERTICAL);
        
        enabledTextColor = getResources().getColor(R.color.gray);
        disabledTextColor = getResources().getColor(R.color.darkGray1);        
        
        // Inflate the view
        final View view = View.inflate(getContext(), R.layout.vwsettingssource, null);
       
        titleTextView = (TextView)view.findViewById(R.id.sourcebasicsettings_lblSourceName);
        onOffView = (ToggleButton)view.findViewById(R.id.sourcebasicsettings_btnOnOff);        
        sourceIconView = (ImageView)view.findViewById(R.id.sourcebasicsettings_imgSourceIcon);
        descTextView = (TextView)view.findViewById(R.id.sourcebasicsettings_lblDesc);
        
        addView(view);
                
        // Register a listener on the spinner
        onOffView.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                onUpdateSyncDirection();
            }
        });
    }

    public void setEnabledIcon(Bitmap image) {
        enabledIcon = image;
    }

    public void setDisabledIcon(Bitmap image) {
        disabledIcon = image;
    }

    public void setTitle(String title) {
        titleTextView.setText(title);
    }

    public int getSyncMode() {
        return onOffView.isChecked() ? SyncSource.INCREMENTAL_SYNC : SyncSource.NO_SYNC;
    }

    public boolean hasChanges() {
        return (getSyncMode() != getLastSavedSyncMode());
    }

    public void loadSettings(Configuration configuration, boolean fromScratch) {
    	
    	if (Log.isLoggable(Log.TRACE)) {
    		Log.trace(TAG_LOG, (fromScratch ? "loadSettings from scratch" : "loadSettings"));
    	}
    	if (fromScratch) {
	    	onOffView.setChecked(getLastSavedSyncMode() != SyncSource.NO_SYNC);        
    	}
    }
    
    private int getLastSavedSyncMode() {
    	boolean isEnabled = appSyncSource.isWorking() && appSyncSource.getConfig().getEnabled();    		
        int syncMode = isEnabled ? appSyncSource.getConfig().getSyncMode() : SyncSource.NO_SYNC;
        return syncMode;
    }

    public void saveSettings(Configuration conf) {
        int syncMode = getSyncMode();
        AppSyncSourceConfig config = appSyncSource.getConfig();
        boolean enabled = syncMode != SyncSource.NO_SYNC;
        config.setEnabled(enabled);
    }

    /**
     * @return the AppSyncSource this item represents
     */
    public AppSyncSource getSource() {
        return appSyncSource;
    }

    @Override
    public void setEnabled(boolean enabled) {
        int id;
        int color;
        if(enabled) {
            id = (Integer)enabledIcon.getOpaqueDescriptor();
            color = enabledTextColor;
        } else {
            id = (Integer)disabledIcon.getOpaqueDescriptor();
            color = disabledTextColor;
        }
        sourceIconView.setImageResource(id);
        titleTextView.setTextColor(color);
    }

    public void setAllowed(boolean allowed) {
        setEnabled(allowed);
    }
    
    public void layout() {
    }
	    
    /**
     * Changes the text of the sync direction label so that it matches the
     * selection of the sync direction control.
     */
    protected void onUpdateSyncDirection() {
        if (Log.isLoggable(Log.TRACE)) {
            Log.trace(TAG_LOG, "onUpdateSyncDirection for sync source " + getSource().getName() + "" +
            " with sync mode " + getSyncMode());
        }
    }

    public void setSource(AppSyncSource source) {
        this.appSyncSource = source;
    }
}


