/**
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2011 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.activities.settings;

import java.util.Vector;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.funambol.androidsync.R;
import com.funambol.client.controller.SyncSettingsScreenController;
import com.funambol.client.ui.SettingsUIItem;
import com.funambol.client.ui.SettingsUISyncSource;
import com.funambol.client.ui.SyncSettingsScreen;

/**
 * Represents the Android Sync Settings tab
 */
public class AndroidSyncSettingsTab extends AndroidSettingsTab implements SyncSettingsScreen {
    
    private static final String TAG_LOG = AndroidSyncSettingsTab.class.getSimpleName();
    
    private SyncModeSettingView syncModeView;
    private BandwidthSaverSettingView bandwidthSaverView;

    private Vector<AndroidSettingsUISyncSource> sourceItems =
            new Vector<AndroidSettingsUISyncSource>();

    private SyncSettingsScreenController screenController;

    private LinearLayout ll;

    public AndroidSyncSettingsTab(Context context, AttributeSet attrSet) {
    	super(context, attrSet);
    }
    
    public void initialize(boolean reset) {
    	
    	super.initialize(getContext());
    	
        screenController = new SyncSettingsScreenController(controller, this, 
        		reset); // if it's a rotation, don't reset the toggle buttons

        ll = new LinearLayout(getContext());
        ll.setOrientation(LinearLayout.VERTICAL);
        ll.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT,
                                                         LayoutParams.WRAP_CONTENT));

        // Add the global sync mode setting
        if(syncModeView != null) {
            ll.addView(syncModeView);
        }
       
        // Add the bandwidth saver
        if (bandwidthSaverView != null) {
            if (syncModeView != null) {
                addDivider(ll);
            }
            ll.addView(bandwidthSaverView);
        }
        
        // Add all the source settings
        boolean first = true;
        for(AndroidSettingsUISyncSource item : sourceItems) {
            if(item == null) {
                continue;
            }
            if(!first) {
                addDivider(ll);
            } else {
                first = false;
                if(syncModeView != null) {
                    addDivider(ll);
                }
            }
            ll.addView(item);
        }
        
        addView(ll);
    }    

    //---------------------------------------- AndroidSettingsTab implementation

    public void saveSettings(SaveSettingsCallback callback) {
        screenController.saveSettings();
        callback.saveSettingsResult(true);
    }

    public boolean hasChanges() {
        boolean hasChanges = false;
        hasChanges |= screenController.hasChanges();
        if(bandwidthSaverView != null) {
            hasChanges |= bandwidthSaverView.hasChanges();
        }
        return hasChanges;
    }

    public Drawable getIndicatorIcon() {
        return getResources().getDrawable(R.drawable.ic_sync_tab);
    }

    public String getIndicatorLabel() {
        return localization.getLanguage("settings_sync_tab");
    }

    //---------------------------------------- SyncSettingsScreen implementation

    public void setSettingsUISyncSource(SettingsUISyncSource item, int index) {
        sourceItems.setElementAt((AndroidSettingsUISyncSource)item, index);
    }

    public void setSettingsUISyncSourceCount(int count) {
        sourceItems.setSize(count);
    }
    
    public SettingsUIItem createSyncModeSetting(boolean fromScratch) {
        // Setup SyncMode setting View
        int[] modes = customization.getAvailableSyncModes();
        if(modes.length > 1) {
            syncModeView = new SyncModeSettingView(getContext(), modes);
            syncModeView.loadSettings(configuration, fromScratch);
        }
        return syncModeView;
    }

    public SettingsUIItem createBandwidthSaverSetting(boolean fromScratch) {
        bandwidthSaverView = new BandwidthSaverSettingView(getContext());
        bandwidthSaverView.loadSettings(configuration, fromScratch);
        return bandwidthSaverView;
    }
    
    public void removeAllItems() {
        syncModeView = null;
        sourceItems.clear();
    }

    public Object getUiScreen() {
        return getContext();
    }

    public void cancelSettings() {
    }

    public void enableSyncModeList(boolean enable) {
        syncModeView.enableSyncModeList(enable);
    }

    public boolean getBandwidthSaverEnabled(){
        if(bandwidthSaverView != null) {
            return bandwidthSaverView.getBandwidthSaverEnabled();
        }
        return false;
    }
    
}
