/**
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2010 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.activities.settings;

import java.util.ArrayList;
import java.util.List;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.ArrayAdapter;
import android.content.Context;

import com.funambol.android.AppInitializer;
import com.funambol.androidsync.R;

import com.funambol.client.ui.SettingsUIItem;
import com.funambol.client.configuration.Configuration;
import com.funambol.client.localization.Localization;

/**
 * Implements a LinearLayout to display the sync mode setting (manual, push or
 * scheduled) in Android
 */
public class BandwidthSaverSettingView extends LinearLayout implements SettingsUIItem {
    private static final String TAG_LOG = BandwidthSaverSettingView.class.getSimpleName();

    private Spinner bandwidthSaverSpinner;
    private BandwidthAdapter adapter;
    private View noBandwithSaverWarning;
    private boolean originalBandwidthValue;

    public BandwidthSaverSettingView(Context context) {
        super(context);

        AppInitializer appInitializer = AppInitializer.i(getContext());
        Localization loc = appInitializer.getLocalization();
        
        View view = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
        		.inflate(R.layout.vwsettingsbandwidthsaver, null);
        this.addView(view);
        
        noBandwithSaverWarning = findViewById(R.id.settings_lblbandwidthtext2);

        bandwidthSaverSpinner = (Spinner) view.findViewById(R.id.settings_spibandwidthsaver);
        bandwidthSaverSpinner.setPrompt(loc.getLanguage("settings_spibandwidthprompt"));

        //fills with the list of possibile spinner items
        List<BandwidthItem> listItems = new ArrayList<BandwidthSaverSettingView.BandwidthItem>();
        listItems.add(new BandwidthItem(
                loc.getLanguage("settings_spibandwidthwifimobile"),
                false));
        listItems.add(new BandwidthItem(
                loc.getLanguage("settings_spibandwidthwifionly"),
                true));
        
        adapter = new BandwidthAdapter(context, android.R.layout.simple_spinner_item, listItems);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        bandwidthSaverSpinner.setAdapter(adapter);
        bandwidthSaverSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view,
                    int position, long id) {
                showWarningIfNeeded();
            }
            public void onNothingSelected(AdapterView<?> arg0) {
                // Does nothing
            }
        });
    }

    public boolean hasChanges() {
        return originalBandwidthValue != getBandwidthSaverEnabled();
    }

    public void loadSettings(Configuration configuration, boolean fromScratch) {
        originalBandwidthValue = configuration.getBandwidthSaverActivated(); 
        setBandwidthSaverValue(originalBandwidthValue);
        showWarningIfNeeded();
    }

    public void saveSettings(Configuration conf) {
        conf.setBandwidthSaver(getBandwidthSaverEnabled());
    }

    public boolean getBandwidthSaverEnabled() {
        BandwidthItem selectedMode = (BandwidthItem) bandwidthSaverSpinner.getSelectedItem();
        return selectedMode.value;
    }
    
    private void setBandwidthSaverValue(boolean enabled) {
        for (int position=0; position<adapter.getCount(); position++) {
            if (adapter.getItem(position).value == enabled) {
                bandwidthSaverSpinner.setSelection(position);
                break;
            }
        }
    }
    
    private void showWarningIfNeeded() {
        if (adapter.getItem(bandwidthSaverSpinner.getSelectedItemPosition()).value) {
            noBandwithSaverWarning.setVisibility(View.GONE);
        } else {
            noBandwithSaverWarning.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Item used in the spinner
     *
     */
    private class BandwidthItem {
        public final String description;
        public final boolean value;
        
        public BandwidthItem(String description, boolean value) {
            this.description = description;
            this.value = value;
        }
        
        @Override
        public String toString() {
            return description;
        }
    }
    
    /**
     * Adapter to show elements of the spinner
     *
     */
    private class BandwidthAdapter extends ArrayAdapter<BandwidthItem> {

        public BandwidthAdapter(Context context, int textViewResourceId,
                List<BandwidthItem> objects) {
            super(context, textViewResourceId, objects);
        }
    }
}

