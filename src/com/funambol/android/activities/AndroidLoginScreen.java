/*
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2009 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.activities;

import android.accounts.Account;
import android.app.Dialog;
import android.os.Bundle;
import android.accounts.AccountAuthenticatorActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.funambol.android.AndroidAccountManager;
import com.funambol.androidsync.R;
import com.funambol.android.AppInitializer;
import com.funambol.android.activities.view.AndroidActionBarView;
import com.funambol.android.controller.AndroidLoginScreenController;
import com.funambol.client.configuration.Configuration;
import com.funambol.client.controller.Controller;
import com.funambol.client.test.BasicConstants;
import com.funambol.client.ui.LoginScreen;
import com.funambol.client.ui.view.ActionBarWidget;
import com.funambol.platform.NetworkStatus;
import com.funambol.util.Log;


public class AndroidLoginScreen extends AccountAuthenticatorActivity implements LoginScreen {

    private static final String TAG_LOG = AndroidLoginScreen.class.getSimpleName();

    private EditText userEditField;
    private EditText passEditField;
    private EditText serverEditField;

    private AndroidLoginScreenController loginScreenController;

    private Controller controller;
    private Configuration configuration;
    private AndroidDisplayManager displayManager;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        AppInitializer appInitializer = AppInitializer.i(this);
        controller = appInitializer.getController();
        
        configuration = controller.getConfiguration();
        displayManager = (AndroidDisplayManager)controller.getDisplayManager();

        // Check if a Funambol account already exists
         Account existingAccount = AndroidAccountManager.getNativeAccount(this);
        if(existingAccount != null && !configuration.getCredentialsCheckPending()) {
            String label = getString(R.string.alert_one_account_supported_1) + " " +
                           getString(R.string.account_label) + " " +
                           getString(R.string.alert_one_account_supported_2);
            Toast.makeText(this, label, Toast.LENGTH_LONG).show();
            displayManager.hideScreen(this);
        }

        setContentView(R.layout.actlogin);

        userEditField = (EditText)findViewById(R.id.loginscreen_lblusername);
        passEditField = (EditText)findViewById(R.id.loginscreen_lblpassword);
        serverEditField = (EditText)findViewById(R.id.loginscreen_lblserver);

        userEditField.setOnTouchListener(new ShowServerUrlEasterEgg());
        
        Button loginButton = (Button)findViewById(R.id.loginscreen_btnlogin);
        loginButton.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                loginScreenController.login();
            }
        });
        
        // Set buttons tag for testing purposes only
        loginButton.setTag(BasicConstants.BUTTON_LOGIN);

        loginScreenController = new AndroidLoginScreenController(
                appInitializer.getController(), this, new NetworkStatus());
        loginScreenController.initScreen();
    }

    public String getServerUrl() {
        return serverEditField.getText().toString();
    }

    public void setServerUrl(String originalUrl) {
        serverEditField.setText(originalUrl);
    }

    public String getUsername() {
        return userEditField.getText().toString();
    }

    public void setUsername(String originalUser) {
        userEditField.setText(originalUser);
    }

    public String getPassword() {
        return passEditField.getText().toString();
    }

    public void setPassword(String originalPassword) {
        passEditField.setText(originalPassword);
    }

    public Object getUiScreen() {
        return this;
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        if (Log.isLoggable(Log.TRACE)) {
            Log.trace(TAG_LOG, "onCreateDialog: " + id);
        }
        Dialog result = null;
        if(displayManager != null) {
            result = displayManager.createDialog(id);
        }
        if(result != null) {
            return result;
        } else {
            return super.onCreateDialog(id);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // The activity becomes visible
        displayManager.addActivityOnStack(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // The activity is no longer visible
        displayManager.removeActivityFromStack(this);
    }

    public ActionBarWidget getActionBarWidget() {
        AndroidActionBarView actionBarView = (AndroidActionBarView) findViewById(R.id.actionbar);
        return actionBarView;
    }

    public com.funambol.client.ui.Bitmap getActionBitmap(int actionId) {
        return null;
    }

    private class ShowServerUrlEasterEgg implements View.OnTouchListener {
        private int clicks = 10;

        public boolean onTouch(View view, MotionEvent me) {
            switch(me.getAction()) {
                case MotionEvent.ACTION_UP:
                    if(--clicks <= 0) {
                        View serverEditView = findViewById(R.id.loginscreen_layserver);
                        serverEditView.setVisibility(View.VISIBLE);
                        passEditField.setNextFocusDownId(R.id.loginscreen_layserver);
                    }
                    break;
            }
            return false;
        }
    }

    @Override
    public void onBackPressed() {
        if(!loginScreenController.backPressed()) {
            super.onBackPressed();
        }
    }
}
