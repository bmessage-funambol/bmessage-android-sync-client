/**
 * Funambol is a mobile platform developed by Funambol, Inc. 
 * Copyright (C) 2011 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission 
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE 
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License 
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite 
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably 
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol". 
 */


package com.funambol.android.activities;

import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.FrameLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;
import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import com.funambol.android.AndroidUtils;
import com.funambol.android.AppInitializer;
import com.funambol.android.source.media.MediaAppSyncSource;
import com.funambol.androidsync.R;
import com.funambol.client.controller.Controller;

import com.funambol.client.controller.SourceChronologicalViewController;
import com.funambol.client.customization.Customization;
import com.funambol.client.localization.Inflexion;
import com.funambol.client.localization.Localization;
import com.funambol.client.source.AppSyncSource;
import com.funambol.client.source.AppSyncSourceManager;
import com.funambol.client.ui.DisplayManager;
import com.funambol.client.ui.view.SourceChronologicalView;
import com.funambol.client.ui.view.SourceChronologicalViewItem;
import com.funambol.util.Log;
import com.funambol.util.StringUtil;

/**
 * TODO
 * Pull to refresh gesture has an incomplete implementation, so don't use it
 *
 */
public abstract class SourceChronologicalFragment extends BasicFragment implements SourceChronologicalView {

    private static final String TAG_LOG = SourceChronologicalFragment.class.getSimpleName();

    protected enum Status {EMPTY, DISABLED, POPULATED};
    private static final int MAX_THUMBNAIL_WIDTH = 80; // dip
    private static final int MIN_THUMBNAIL_PADDING = 1; //dip

    protected AppSyncSource appSource;
    protected SourceChronologicalViewController controller;

    private ChronologicalListView listView;
    private DataAdapter dataAdapter;
    private int previousFirstVisible = -1;
    private int previousLastVisible  = -1;
    
    private boolean fastScrolling = false;

    protected Controller globalController;
    protected DisplayManager displayManager;
    protected Customization customization;
    protected Localization  localization;
    
    private FrameLayout mainView;
    private int lastWidth;

    private LoaderThread loader;
    private final boolean isPullToScrollEnabled;
    
    protected Status status;

    public AppSyncSource getAppSyncSource() {
        return appSource;
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setRetainInstance(true);
        AppInitializer appInitializer = AppInitializer.i(getContainerActivity());
        globalController = appInitializer.getController();
        displayManager = appInitializer.getDisplayManager();
        customization = appInitializer.getCustomization();
        localization = appInitializer.getLocalization();
    }
    
    public SourceChronologicalFragment(boolean isPullToScrollEnabled) {
        this.isPullToScrollEnabled = isPullToScrollEnabled;
    }
   
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        status = Status.EMPTY;
        
        // We are going to create a new list view but we need to detach the old one from the adapter to avoid
        // it invokes it any longer
        if (listView != null) {
            listView.setAdapter(null);
        }
        
        mainView = (FrameLayout)inflater.inflate(R.layout.frachronological, container, false);
        mainView.setBackgroundResource(R.color.darkGray2);
        
        // Create the list view that contains all the items
        listView = new ChronologicalListView(getContainerActivity());
        listView.setDividerHeight(0);
        listView.setWillNotCacheDrawing(false);
        listView.setChoiceMode(ListView.CHOICE_MODE_NONE);
        listView.setFocusableInTouchMode(false);
        listView.setSelected(false);
        listView.setFocusable(false);
        listView.setClickable(false);
        
        listView.setSelector(new ColorDrawable(R.color.darkGray2));
       
        /*
        if (isPullToScrollEnabled) {
            listView.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {
                public void onRefresh() {
                    controller.startRefresh();
                }
            });
        } else {
            listView.disablePullToRefresh();
        }
        */

       
        // This is to preserve the background color of the main view
        listView.setCacheColorHint(0x00000000);
        
        // Compute the best combination for the number of thumbs per row, their 
        // size, the paddings and the margins. This code makes the assumption
        // that the fragment occupies the entire width. This may become invalid
        // once we support tablets
        Display display = getContainerActivity().getWindowManager().getDefaultDisplay(); 
        int width = display.getWidth(); // px
        if (lastWidth != width) {
            if (controller == null) {
                dataAdapter = new DataAdapter();
                controller = createController();
                this.setWidgetController(controller);
            } else {
                previousFirstVisible = -1;
                previousLastVisible  = -1;
                fastScrolling        = false;
            }
            listView.setAdapter(dataAdapter);
            listView.setOnScrollListener(new ListViewOnScrollListener());

            lastWidth = width;
            int cw = width; // px
            
            int maxWidth = MAX_THUMBNAIL_WIDTH;
            int minWidth = (maxWidth * 88) / 100;
            
            if (appSource instanceof MediaAppSyncSource) {
                MediaAppSyncSource mAppSyncSource = (MediaAppSyncSource)appSource;
                MediaAppSyncSource.ImageProperties props = mAppSyncSource.getThumbnailProperties();
                maxWidth = props.maxWidth;
                minWidth = props.minWidth;
                if(props.isDip) {
                    minWidth = AndroidUtils.dipToPx(minWidth, getContainerActivity());
                    maxWidth = AndroidUtils.dipToPx(maxWidth, getContainerActivity());
                }
            }

            // Search for the number of thumbnails which minimize the paddings and margins
            int minPadding = Integer.MAX_VALUE;
            int bestNum = 4;
            int bestSize = minWidth;
            int bestPadding = AndroidUtils.dipToPx(MIN_THUMBNAIL_PADDING, getContainerActivity());

            for(int s=minWidth;s<=maxWidth;s++) {
                // Compute the padding size with this size
                // n is the number of thumbnails per row with this size
                int n = cw / s;
                int singlePadding;
                if (n > 1) {
                    int totalPadding = cw - n * s;
                    singlePadding = totalPadding / (n - 1);
                } else {
                    singlePadding = 0;
                }
                if (singlePadding <= minPadding &&
                    singlePadding >= AndroidUtils.dipToPx(MIN_THUMBNAIL_PADDING, getContainerActivity())) {
                    bestNum = n;
                    bestSize = s;
                    bestPadding = singlePadding;
                    minPadding = singlePadding;
                }
            }
            if (Log.isLoggable(Log.TRACE)) {
                Log.trace(TAG_LOG, "The best number of thumbnails per row is " + bestNum);
            }
            status = appSource.getConfig().getEnabled() ? Status.EMPTY : Status.DISABLED;
            init(bestNum, bestSize, bestPadding);
        }
       
        AppInitializer appInitializer = AppInitializer.i(getContainerActivity());
        if (!appInitializer.getController().getRefreshTrigger().isRefreshInProgress()) {
            listView.setFastScrollEnabled(true);
        }
        return mainView;
    }
    
    @Override
    public void onDestroy() {
        mainView.removeAllViews();
        listView = null;
        controller = null;
        dataAdapter = null;
        super.onDestroy();
    }

    public boolean backPressed() {
        return controller.backPressed();
    }

    // Create a controller to be associated to this view
    // Subclasses can override this method to return a proper controller
    protected SourceChronologicalViewController createController() {
        return new SourceChronologicalViewController(this, appSource, 
                localization, displayManager, globalController);
    }

    public SourceChronologicalViewController getController() {
        return controller;
    }
    
    public void refreshStarted() {
        Activity act = getContainerActivity();
        if (act != null) {
            act.runOnUiThread(new Runnable() {
                public void run() {
                    listView.setFastScrollEnabled(false);
                }
            });
        }
    }
    
    public void refreshEnded() {
        Activity act = getContainerActivity();
        if (act != null) {
            act.runOnUiThread(new Runnable() {
                public void run() {
                    listView.setFastScrollEnabled(true);
                    //listView.onRefreshComplete();
                }
            });
        }
    }
   
    public void rowAdded(SourceChronologicalViewItem row) {
        dataChanged();
    }
    
    public void rowUpdated(int rowIdx) {
        dataChanged();
    }
    
    public void rowDeleted(int rowIdx) {
        dataChanged();
    }
   
    /**
     * Make sure this invoked from the UI thread
     */
    public void dataChanged() {
        if(controller == null) {
            // No data to update
            return;
        }
        // Depending on what happened we need to change view
        if (status != Status.DISABLED) {
            if (controller.getWindowSize() == 0) {
                setEmptyView();
            } else {
                setListView();
            }
        }
        previousFirstVisible = -1;
        previousLastVisible = -1;
        listView.reset();
        dataAdapter.notifyDataSetChanged();
    }
    
    /**
     * Execute the given runnable on the UI thread if the activity is available.
     * Otherwise the runnable is not executed.
     */
    public void runOnUIThread(Runnable runnable) {
        Activity activity = getContainerActivity();
        if (activity != null) {
            getContainerActivity().runOnUiThread(runnable);
        }
    }
   
    protected void setAppSyncSource(int sourceId) {
        AppInitializer appInitializer = AppInitializer.i(getContainerActivity());
        AppSyncSourceManager appSyncSourceManager = appInitializer.getAppSyncSourceManager();
        appSource = appSyncSourceManager.getSource(sourceId);
    }
    
    private void init(int bestNum, int bestSize, int bestPadding) {
        if (loader != null) {
            loader.block();
        }
        controller.releaseInitLock();
        try {
            controller.acquireInitLock();
            controller.setUIParams(bestNum, bestSize, bestPadding);
            loader = new LoaderThread();
            loader.start();
        } catch (InterruptedException e) { }
    }

    private final Object initLock = new Object();
    private class LoaderThread extends Thread {

        private final int LOADER_COUNT = 10;

        private final Object doneLock = new Object();
        private boolean done = false;

        @Override
        public void run() {
            // We can have at most one instance of the loader thread running at a time
            synchronized(initLock) {
                load();
                if (status == Status.DISABLED) {
                    runOnUIThread(new Runnable() {
                        public void run() {
                            setDisabledView();
                        }
                    });
                }
            }
        }
        
        private void load() {
            try {
                int totalLoaded = 0;
                while(!isDone()) {
                    Thread.sleep(100);
                    int loaded = controller.loadRows(totalLoaded, LOADER_COUNT, true);
                    totalLoaded += loaded;
                    if(loaded < LOADER_COUNT) {
                        if (Log.isLoggable(Log.DEBUG)) {
                            Log.debug(TAG_LOG, "Initialization completed " + totalLoaded);
                        }
                        if (status != Status.DISABLED && totalLoaded == 0) {
                            runOnUIThread(new Runnable() {
                                public void run() {
                                    setEmptyView();
                                }
                            });
                        }
                        done();
                    } else {
                        if (status == Status.EMPTY && totalLoaded == LOADER_COUNT) {
                            runOnUIThread(new Runnable() {
                                public void run() {
                                    setListView();
                                }
                            });
                        }
                    }
                }
            } catch (Exception e) {
                Log.error(TAG_LOG, "Error loading items, terminate init sequence", e);
                done();
            } finally {
                if (isDone()) {
                    controller.releaseInitLock();
                }
            }
        }

        public void block() {
            done();
        }

        private void done() {
            synchronized(doneLock) {
                done = true;
            }
        }

        private boolean isDone() {
            synchronized(doneLock) {
                return done;
            }
        }
    }

    private void updateVisibleItems(int firstVisible, int lastVisible) {
        // It may happen that the firstVisible is -1
        if(firstVisible < 0) {
            firstVisible = 0;
        }
        if (firstVisible == previousFirstVisible && lastVisible == previousLastVisible) {
            // No changes on visible items
            return;
        }
        int hideFrom = -1, hideTo = -1;
        int showFrom = -1, showTo = -1;
        if(firstVisible > previousFirstVisible) {
            hideFrom = previousFirstVisible;
            hideTo = firstVisible;
        } else if(lastVisible < previousLastVisible) {
            hideFrom = lastVisible+1;
            hideTo = previousLastVisible+1;
        }
        if(firstVisible < previousFirstVisible) {
            showFrom = firstVisible;
            showTo = previousFirstVisible;
        } else if(lastVisible > previousLastVisible) {
            showFrom = previousLastVisible+1;
            showTo = lastVisible+1;
        }
        if(showFrom < firstVisible) {
            showFrom = firstVisible;
        }
        if(showTo > lastVisible) {
            showTo = lastVisible+1;
        }
        if(hideFrom != -1 && hideTo != -1 && hideFrom < hideTo) {
            if (Log.isLoggable(Log.TRACE)) {
                Log.trace(TAG_LOG, "Setting visibility to false for items from " + hideFrom + " to " + hideTo);
            }
            controller.setRowsVisibility(hideFrom, hideTo, false);
        }
        if(showFrom != -1 && showTo != -1 && showFrom < showTo) {
            if (Log.isLoggable(Log.TRACE)) {
                Log.trace(TAG_LOG, "Setting visibility to true for items from " + showFrom + " to " + showTo);
            }
            controller.setRowsVisibility(showFrom, showTo, true);
        }
        previousFirstVisible = firstVisible;
        previousLastVisible  = lastVisible;
    }
    
    protected View createPlaceHolderView(Context context, boolean enabled) {

        LinearLayout placeHolder = new LinearLayout(context);
        View.inflate(getContainerActivity(), R.layout.vwsourceviewplaceholderbig, placeHolder);

        TextView titleView = (TextView)placeHolder.findViewById(R.id.sourceviewplaceholder_lblTitle);
        TextView textView = (TextView)placeHolder.findViewById(R.id.sourceviewplaceholder_lblPlaceholder);
        ImageView imageView = (ImageView)placeHolder.findViewById(R.id.sourceviewplaceholder_imgPlaceholder);

        String title;
        if (enabled) {
            title = localization.getLanguage("source_placeholder_title");
            title = StringUtil.replaceAll(title, "__THINGS__",
                    appSource.getItemInflexion().using(Inflexion.PLURAL).titleCase().toString());
        } else {
            title = localization.getLanguage("source_placeholder_disabled_title");
            title = StringUtil.replaceAll(title, "__THING__",
                    appSource.getItemInflexion().using(Inflexion.SINGULAR).titleCase().toString());
        }
        titleView.setText(title);
      
        String text;
        if (enabled) {
            text = localization.getLanguage("source_placeholder_text");
            text = StringUtil.replaceAll(text, "__THING__",
                                        appSource.getItemInflexion().using(Inflexion.SINGULAR).toString());
        } else {
            text = localization.getLanguage("source_placeholder_disabled_text");
            text = StringUtil.replaceAll(text, "__THING__",
                                        appSource.getItemInflexion().using(Inflexion.SINGULAR).titleCase().toString());
        }
        textView.setText(text);

        int resId = ((Integer)customization.getSourcePlaceHolderIcon(
                appSource.getId()).getOpaqueDescriptor()).intValue();
        imageView.setImageResource(resId);

        // Setup place holder if in landscape mode
        Display display = getContainerActivity().getWindowManager().getDefaultDisplay();
        if(display.getWidth() > display.getHeight()) {
            LinearLayout container = (LinearLayout)placeHolder.findViewById(
                    R.id.sourceviewplaceholder_container);
            container.setOrientation(LinearLayout.HORIZONTAL);
            LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            lParams.gravity = Gravity.LEFT;
            titleView.setLayoutParams(lParams);
            textView.setLayoutParams(lParams);
            textView.setGravity(Gravity.LEFT);
        }
        return placeHolder;
    }
    
    protected void setEmptyView() {
        mainView.removeAllViews();
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
        lp.gravity = Gravity.CENTER;
        mainView.addView(createPlaceHolderView(getContainerActivity(), true), lp);
        status = Status.EMPTY;
    }
    
    protected void setDisabledView() {
        mainView.removeAllViews();
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
        lp.gravity = Gravity.CENTER;
        mainView.addView(createPlaceHolderView(getContainerActivity(), false), lp);
        status = Status.DISABLED;
    }
 
    
    protected void setListView() {
        mainView.removeAllViews();
        mainView.addView(listView);
        status = Status.POPULATED;
    }
    
    public void setEnabled(final boolean enabled) {
        mainView.post(new Runnable() {
            public void run() {
                if (status != Status.DISABLED && !enabled) {
                    setDisabledView();
                    status = Status.DISABLED;
                } else if (status == Status.DISABLED && enabled) {
                    if (controller.getWindowSize() == 0) {
                        setEmptyView();
                    } else {
                        setListView();
                    }
                }
            }
        });
    }
    
    protected class DataAdapter extends BaseAdapter implements SectionIndexer {

        @Override
        public boolean areAllItemsEnabled() {
            if (Log.isLoggable(Log.TRACE)) {
                Log.trace(TAG_LOG, "Are all items enabled");
            }
            return true;
        }

        @Override
        public boolean isEnabled(int id) {
            if (Log.isLoggable(Log.TRACE)) {
                Log.trace(TAG_LOG, "Is enabled " + id);
            }
            return true;
        }

        public int getCount() {
            int count = controller.getWindowSize();
            return count;
        }

        public Object getItem(int idx) {
            if (Log.isLoggable(Log.TRACE)) {
                Log.trace(TAG_LOG, "Getting item at " + idx);
            }
            return controller.getItemRow(idx);
        }

        public long getItemId(int idx) {
            if (Log.isLoggable(Log.TRACE)) {
                Log.trace(TAG_LOG, "Getting item id at " + idx);
            }
            return idx;
        }

        @Override
        public int getItemViewType(int idx) {
            SourceChronologicalViewItem item = (SourceChronologicalViewItem)getView(idx, null, null);
            return item.getType();
        }

        public View getView(int idx, View oldView, ViewGroup vg) {
            try {
                View view = (View) controller.getItem(idx, (SourceChronologicalViewItem) oldView, fastScrolling);
                view.setBackgroundResource(R.color.darkGray2);
                return view;
            } catch (Exception e) {
                Log.error(TAG_LOG, "Cannot load view at " + idx, e);
            }
            return null;
        }

        @Override
        public int getViewTypeCount() {
            return SourceChronologicalViewItem.NUM_TYPES;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public boolean isEmpty() {
            return getCount() == 0;
        }

        public int getPositionForSection(int section) {
            return controller.getBucketFirstRow(section);
        }

        public int getSectionForPosition(int position) {
            return 0;
        }

        public Object[] getSections() {
            return controller.getTimeframeBuckets();
        }
    }
    
    private class ListViewOnScrollListener implements AbsListView.OnScrollListener {

        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            // TODO Auto-generated method stub
        }

        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
                updateVisibleItems(listView.getFirstVisiblePosition(), listView.getLastVisiblePosition());
                fastScrolling = false;
            }
        }
    }
    
    private class ChronologicalListView extends ListView {
        private long lastTs;
        private int lastFirstVisible = -1;
        private int lastLastVisible  = -1;

        public ChronologicalListView(Context context) {
            super(context);
        }
        
        public void reset() {
            lastTs = 0;
            lastFirstVisible = -1;
            lastLastVisible  = -1;
        }
       
        @Override
        protected void layoutChildren() {
            try {
                super.layoutChildren();
            } catch (Exception e) {
                if (Log.isLoggable(Log.INFO)) {
                    Log.info(TAG_LOG, "Cannot layout children, this is likely to be a temporary error during rotation, "
                                      + "ignore it");
                }
            }
        }
        
        public int getFirstVisiblePosition() {
            if (isPullToScrollEnabled) {
                return super.getFirstVisiblePosition() - 1;
            } else {
                return super.getFirstVisiblePosition(); 
            }
        }
        
        public int getLastVisiblePosition() {
            if (isPullToScrollEnabled) {
                return super.getLastVisiblePosition() - 1;
            } else {
                return super.getLastVisiblePosition();
            }
        }
       
        @Override
        protected void dispatchDraw(Canvas canvas) {
            super.dispatchDraw(canvas);
            
            int firstVisible = getFirstVisiblePosition();
            int deltaRows = lastFirstVisible - firstVisible;
            long speedInv = 0;
            int lastVisible = getLastVisiblePosition();

            // It may happen that after a fast scrolling the delta rows is 0.
            if(deltaRows == 0 && fastScrolling) {
                fastScrolling = false;
                updateVisibleItems(getFirstVisiblePosition(), getLastVisiblePosition());
            } else if (deltaRows != 0) {
                long now = System.currentTimeMillis();
                long deltaT = lastTs - now;
                speedInv = deltaT / deltaRows;

                lastFirstVisible = firstVisible;
                lastTs = now;

                // Update the visible items only if the user is scrolling slowly.
                // On fast scroll we just show the placeholders and the timeframe buckets
                // -1 because of pull-to-refresh header in the list
                if (speedInv > 400 || speedInv < -400) {
                    updateVisibleItems(getFirstVisiblePosition(), getLastVisiblePosition());
                    fastScrolling = false;
                } else {
                    fastScrolling = true;
                }
            } else if (lastVisible != lastLastVisible) {
                // -1 because of pull-to-refresh header in the list
                if (isPullToScrollEnabled) {
                    updateVisibleItems(firstVisible - 1, lastVisible - 1);
                } else {
                    updateVisibleItems(firstVisible, lastVisible);
                }
            }
            lastLastVisible = lastVisible;
        }
    }
}
