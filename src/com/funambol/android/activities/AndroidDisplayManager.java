/**
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2011 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */
package com.funambol.android.activities;

import java.util.Hashtable;
import java.util.List;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnCancelListener;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;
import com.funambol.android.AndroidUtils;
import com.funambol.androidsync.R;

import com.funambol.client.controller.Controller;
import com.funambol.client.controller.DialogOption;
import com.funambol.client.controller.NotificationController;
import com.funambol.client.controller.NotificationData;
import com.funambol.client.source.AppSyncSource;
import com.funambol.client.ui.DisplayManager;
import com.funambol.client.ui.Screen;
import com.funambol.platform.TimerHandler;
import com.funambol.util.Log;

/**
 * The Display Manager implementation for the Android Client. Manages 3 types of
 * screen views:
 * - Android Client Screen;
 * - Native Alert Dialogs
 * - Custom Alert Dialogs (Selection Dialogs)
 * See DisplayManager interface implementation for further details
 */
public class AndroidDisplayManager implements DisplayManager {

    /** The tag to be written into log messages*/
    private static final String TAG_LOG = "AndroidDisplayManager";

    public static final String APP_SOURCE_ID_PARAM = "AppSourceId";

    /** Reference object for the native alert dialogs*/
    private Hashtable<Integer, Dialog> holdingDialogs = new Hashtable<Integer, Dialog>();
    /** References the native alert dialog with a db sequence-like progression*/
    private static int incrementalId;
    /** Holds the last message shown by the showMessage method. Used for test purpose */
    private String lastMessage = null;
    
    private final Context appContext;

    private List<Activity> screenStack = new ArrayList<Activity>();

    /**
     * Default constructor
     * 
     * @param appContext the application context
     */
    public AndroidDisplayManager(Context appContext) {
        this.appContext = appContext.getApplicationContext();
    }

    /**
     * Hide a screen calling the Activity finish method
     * @param screen the Screen to be hidden
     * @throws Exception if the activity related to the encounters
     * any problem
     */
    public void hideScreen(Screen screen) {
        Activity activity = (Activity) screen.getUiScreen();
        activity.finish();
    }

    public void showScreen(int screenId) {
        showScreen(screenId, (Bundle)null);
    }

    public void showScreen(int screenId, AppSyncSource appSource) {
        Bundle extras = new Bundle();
        extras.putInt(APP_SOURCE_ID_PARAM, appSource.getId());
        showScreen(screenId, extras);
    }
    
    /**
     * Use the screen's related activity to put the give screen in foreground.
     * The implementation relies on the Intent mechanism which is peculiar to
     * Android OS: screens are shown calling the startActivity() methods and
     * passing the related intent as parameter.
     * @param context
     * @param screenId the Screen related id
     * @throws Exception if the activity related to the screen encounters
     * any problem
     */
    public void showScreen(int screenId, Bundle extras) {
        Intent intent = null;
        switch (screenId) {
            case Controller.SETTINGS_SCREEN_ID: {
                intent = new Intent(appContext, AndroidSettingsScreen.class);
                break;
            }
            case Controller.SPLASH_SCREEN_ID: {
                intent = new Intent(appContext, AndroidHomeScreen.class);
                break;
            }
            case Controller.INTRO_SCREEN_ID: {
                intent = new Intent(appContext, AndroidIntroScreen.class);
                break;
            }
            case Controller.LOGIN_SCREEN_ID: {
                intent = new Intent(appContext, AndroidLoginScreen.class);
                break;
            }
            case Controller.SIGNUP_SCREEN_ID: {
                intent = new Intent(appContext, AndroidSignupScreen.class);
                break;
            }
            case Controller.EMAIL_REQUEST_SCREEN_ID: {
                intent = new Intent(appContext, AndroidEmailRequestScreen.class);
                break;
            }
            case Controller.ABOUT_SCREEN_ID: {
                intent = new Intent(appContext, AndroidAboutScreen.class);
                break;
            }
            case Controller.DEV_SETTINGS_SCREEN_ID: {
                intent = new Intent(appContext, AndroidDevSettingsScreen.class);
                break;
            }
            case Controller.OPEN_ITEM_SCREEN_ID: {
                intent = new Intent(appContext, AndroidOpenItemScreen.class);
                break;
            }
            case Controller.SEND_ITEM_SCREEN_ID: {
                intent = new Intent(appContext, AndroidSendItemScreen.class);
                break;
            }       
            case Controller.SELECTIVE_UPLOAD_SCREEN_ID: {
                intent = new Intent(appContext, AndroidSelectiveUploadScreen.class);
                break;
            }
            case Controller.SERVICE_AUTHENTICATOR_SCREEN_ID: {
                intent = new Intent(appContext, AndroidServiceAuthenticatorScreen.class);
                break;
            }
            case Controller.NOTIFICATION_SCREEN_ID: {
                intent = new Intent(appContext, AndroidNotificationScreen.class);
                break;
            }
            case Controller.HOME_SCREEN_ID: {
                intent = new Intent(appContext, AndroidMainScreen.class);
                break;
            }
            case Controller.WELCOME_SCREEN_ID: {
                intent = new Intent(appContext, AndroidWelcomeScreen.class);
                break;
            }
            default:
                Log.error(TAG_LOG, "Cannot show unknown screen: " + screenId);
        }
        if (intent != null) {
            if(extras != null) {
                intent.putExtras(extras);
            }
            // Start new activity task if the request doesn't come from an
            // activity context
            if(!(appContext instanceof Activity)) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            appContext.startActivity(intent);
        }
    }

    /**
     */
    public void showScreen(int screenId, boolean donotwait) {
        showScreen(screenId, (Bundle)null);
    }

    /**
     * Create a native alert dialog with the 2 options "Yes" and "No".
     * This kind of alert are managed by the activity owner when the call to
     * onCreateDialog is done.
     * 
     * TODO
     * At the end of current version development, when all the activities will
     * extend {@link FragmentActivity}, only {@link AndroidDisplayManager#askYesNoQuestionFragment(Screen, String, Runnable, Runnable, long)}
     * will remain, but with name of this method
     * 
     * @param screen the native alert dialog owner Screen
     * @param question the question to be displayed
     * @param yesAction the runnable that defines the yes option
     * @param noAction the runnable that defines the no option
     * @param timeToWait to be defined
     */
    public int askYesNoQuestion(Screen screen, String question,
            final Runnable yesAction,
            final Runnable noAction, long timeToWait) {
    	
    	String yesString = appContext.getString(R.string.dialog_yes);
    	String noString = appContext.getString(R.string.dialog_no);
        DialogOption yes = new DialogOption(this, screen, yesString, 0) {
            @Override
            public void onClick() {
                yesAction.run();
            };
        };
        yes.setDark(false);
        DialogOption no = new DialogOption(this, screen, noString, 0) {
            @Override
            public void onClick() {
                noAction.run();
            };
        };
        no.setDark(true);
    	return promptSelection(screen, question, new DialogOption[] { yes, no }, 0);
    }

    /**
     * TODO
     * Temporary transition, at the end only this method must remain, but
     * without "Fragment" world in the name
     * 
     * @param screen
     * @param question
     * @param yesAction
     * @param noAction
     * @param timeToWait
     * @return
     */
    private int askYesNoQuestionFragment(Screen screen, String question,
            Runnable yesAction, Runnable noAction, long timeToWait) {
        if(screen == null) {
            if (Log.isLoggable(Log.TRACE)) {
                Log.trace(TAG_LOG, "Cannot show yes/no dialog, the screen reference is null");
            }
            return -1;
        }
        
        FragmentActivity activity = (FragmentActivity) screen.getUiScreen();
        OnButtonListener yesListener = new OnButtonListener(yesAction);
        OnButtonListener noListener = new OnButtonListener(noAction);
        int dialogId = getNextDialogId();
        DialogFragment dialog = AlertDialogFragment.Factory.createYesNoDialog(
                null, question,
                appContext.getString(R.string.dialog_yes),
                yesListener,
                appContext.getString(R.string.dialog_no),
                noListener);
        showDialogFragment(activity, dialog);
        return dialogId;
    }

    /**
     * Create a native dialog referencing it from the ones contained into the
     * native dialog reference container "holdingDialogs". Call this method 
     * when the onCreateDialog is invoked on the activity that must manage
     * this native dialog.
     * @param id the id of the alert dialog to be created/retrieved
     * @return Dialog the AlertDialog instance corresponding to the given id 
     * 
     * @deprecated Now fragment are used
     */
    public Dialog createDialog(int id) {
        if (Log.isLoggable(Log.DEBUG)) {
            Log.debug(TAG_LOG, "Creating dialog " + id);
        }
        Dialog dialog = holdingDialogs.get(id);
        if (dialog != null) {
            return dialog;
        } else {
            if (Log.isLoggable(Log.DEBUG)) {
                Log.debug(TAG_LOG, "Unknown dialog id: " + id);
            }
            return null;
        }
    }

    /**
     * Create a native alert dialog with a visual rotating spinner. To be used
     * to inform the user that some backgorund process is in progress.
     * This kind of alert are managed by the activity owner when the call to
     * onCreateDialog is done. For this reason the progress dialog instance is
     * saved on the holdingDialogs reference with its progressive id.
     * @param screen the native alert dialog owner Screen
     * @param prompt the message prompted on the native alert
     * @return int the dialog id value
     */
    public int showProgressDialog(Screen screen, String prompt) {
        return showProgressDialog(screen, prompt, true);
    }

    public int showProgressDialog(Screen screen, String prompt, boolean indeterminate) {
        return showProgressDialog(screen, prompt, indeterminate, null);
    }

    public int showProgressDialog(Screen screen, String prompt, Runnable onCancelAction) {
        return showProgressDialog(screen, prompt, true, onCancelAction);
    }

    /**
     * TODO
     * When transition to fragment will be completed, only {@link AndroidDisplayManager#showProgressDialogFragment(Screen, String, boolean, Runnable)}
     * will remain, but with name of this method
     * 
     * @param screen
     * @param prompt
     * @param indeterminate
     * @param onCancelAction
     * @return
     */
    public int showProgressDialog(Screen screen, String prompt, boolean indeterminate, Runnable onCancelAction) {
        if(screen == null) {
            if (Log.isLoggable(Log.TRACE)) {
                Log.trace(TAG_LOG, "Cannot show progress dialog, the screen "
                        + "reference is null");
            }
            return -1;
        }
        return showProgressDialog((Activity)screen.getUiScreen(), prompt, indeterminate, onCancelAction);
    }
        
    public int showProgressDialog(Activity activity, String prompt, boolean indeterminate, Runnable onCancelAction) {
        if (activity instanceof FragmentActivity) {
            return showProgressDialogFragment((FragmentActivity)activity, prompt, indeterminate, onCancelAction);
        }
        
        int dialogId = getNextDialogId();
        activity.runOnUiThread(new ProgressDialogCreator(activity, dialogId, prompt, indeterminate, onCancelAction));
        return dialogId;
    }

    /**
     * TODO
     * When transition to fragment will be completed, remove "Fragment" from the name of this method
     *  
     * @param screen
     * @param prompt
     * @param indeterminate
     * @param onCancelAction
     * @return
     */
    private int showProgressDialogFragment(Screen screen, String prompt, boolean indeterminate, Runnable onCancelAction) {
        if(screen == null) {
            if (Log.isLoggable(Log.TRACE)) {
                Log.trace(TAG_LOG, "Cannot show progress dialog, the screen reference is null");
            }
            return -1;
        }
        return showProgressDialogFragment((FragmentActivity)screen.getUiScreen(), prompt, indeterminate, onCancelAction);
    }
    
    private int showProgressDialogFragment(FragmentActivity activity, String prompt, boolean indeterminate,
                                           Runnable onCancelAction)
    {
        int dialogId = getNextDialogId();
        DialogCancelListener cancelListener = null;
        if(onCancelAction != null) {
            cancelListener = new DialogCancelListener(dialogId, onCancelAction, true);
        }
        DialogFragment dialog = AlertDialogFragment.Factory.createProgressDialog(
                null,
                prompt,
                indeterminate,
                cancelListener);
        dialog.setCancelable(cancelListener != null);
        showDialogFragment(activity, dialog);
        return dialogId;
    }

    /**
     * Dismiss a progress dialog from a screen given its id
     * 
     * TODO
     * At the end of current version development, when all the activities will
     * extend {@link FragmentActivity}, only {@link AndroidDisplayManager#dismissDialogFragment(Screen, int)}
     * will remain, but with name of this method
     * 
     * @param screen the native alert dialog owner Screen
     * @param id the id of the dialog to be dismissed
     */
    public void dismissProgressDialog(Screen screen, final int id) {
        if (Log.isLoggable(Log.DEBUG)) {
            Log.debug(TAG_LOG, "Dismissing progress dialog " + id);
        }
        if(screen == null) {
            if (Log.isLoggable(Log.TRACE)) {
                Log.trace(TAG_LOG, "Cannot dismiss progress dialog, the screenreference is null");
            }
            return;
        }
        final Activity activity = (Activity) screen.getUiScreen();
        dismissProgressDialog(activity, id);
    }
    
    public void dismissProgressDialog(final Activity activity, final int id) {
        if (activity instanceof FragmentActivity) {
            dismissDialogFragment((FragmentActivity)activity, id);
            return;
        }
        activity.runOnUiThread(new Runnable() {
            public void run() {
                try {
                    activity.removeDialog(id);
                } catch (Exception e) {
                    Log.error(TAG_LOG, "Failed at dismissing dialog " + id, e);
                }
            }
        });
        holdingDialogs.remove(id);
    }

    public void setProgressDialogMaxValue(int dialogId, int value) {
        //TODO find a way to obtain dialog without screen reference (save
        //the current progress in a field, for example)
        Dialog dialog = holdingDialogs.get(dialogId);
        if(dialog != null && dialog instanceof ProgressDialog) {
            ((ProgressDialog)dialog).setMax(value);
        }
    }

    public void setProgressDialogProgressValue(int dialogId, int value) {
        //TODO find a way to obtain dialog without screen reference (save
        //the current progress in a field, for example)
        Dialog dialog = holdingDialogs.get(dialogId);
        if(dialog != null && dialog instanceof ProgressDialog) {
            ((ProgressDialog)dialog).setProgress(value);
        }
    }

    /**
     * This method shows a dialog with multiple selections. This means the user is presented a list of
     * multiple choices, each one with its own check box
     */
    public int promptMultiChoiceSelection(Screen screen, String title,
            String okButtonLabel, String cancelButtonLabel, String[] choices,
            boolean[] checkedChoices, final MultipleSelectionClickListener listener) {
        
        DialogInterface.OnMultiChoiceClickListener multiChoiceClickListener =             
            new DialogInterface.OnMultiChoiceClickListener() {
            public void onClick(DialogInterface dialog, int which,
                    boolean isChecked) {
                listener.onSelectionClick(dialog, which, isChecked);
            }
        };        
        DialogInterface.OnClickListener okButtonClickListener =             
            new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                listener.onPositiveButtonClick(dialog);
            }
        };        
        DialogInterface.OnClickListener cancelButtonClickListener = null;
        if (cancelButtonLabel != null) {
            cancelButtonClickListener =             
                new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    listener.onNegativeButtonClick(dialog);
                }
            };
        }
        
        return promptMultipleSelection(screen, title, okButtonLabel, cancelButtonLabel,
                choices, checkedChoices, multiChoiceClickListener, okButtonClickListener,
                cancelButtonClickListener);
        
    }

    public void addActivityOnStack(Activity activity) {
        screenStack.add(activity);
    }

    public void removeActivityFromStack(Activity activity) {
        for(int i=screenStack.size() - 1;i>=0;--i) {
            Activity act = screenStack.get(i);
            if (act == activity) {
                screenStack.remove(i);
                break;
            }
        }
    }
    
    /**
     */
    private int promptMultipleSelection(Screen screen, String title,
            String okButtonLabel, String cancelButtonLabel,
            String[] choices, boolean[] checkedChoices,
            DialogInterface.OnMultiChoiceClickListener multiChoiceClickListener,
            DialogInterface.OnClickListener okButtonClickListener,
            DialogInterface.OnClickListener cancelButtonClickListener) {
        
        int dialogId = getNextDialogId();
        Activity a = (Activity) screen.getUiScreen();
        
        if (a instanceof FragmentActivity) {
            FragmentActivity activity = (FragmentActivity) screen.getUiScreen();
            
            MultiChoicesViewsFactory factory = new MultiChoicesViewsFactory(title, choices, checkedChoices,
                                                                            dialogId,
                                                                            okButtonLabel, cancelButtonLabel,
                                                                            multiChoiceClickListener,
                                                                            okButtonClickListener,
                                                                            cancelButtonClickListener);
            
            DialogFragment dialog = AlertDialogFragment.Factory.createRawDialog(factory, null);
            showDialogFragment(activity, dialog);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(a)
                    .setCustomTitle(buildCustomDialogTitle(a, title, 18))
                    .setMultiChoiceItems(choices, checkedChoices, multiChoiceClickListener)
                    .setPositiveButton(okButtonLabel, okButtonClickListener);        
            if (cancelButtonLabel != null) {
                builder.setNegativeButton(cancelButtonLabel, cancelButtonClickListener);
            }
            a.runOnUiThread(new PromptSelection(a, builder, dialogId));
        }
        return dialogId;
    }
    

    public int showOkDialog(Screen screen, String message, String okButtonLabel) {
        return showOkDialog(screen, message, okButtonLabel, null);
    }

    public int showOkDialog(Screen screen, String message, String okButtonLabel, Runnable onClickAction) {
        return showOkDialog(screen, message, okButtonLabel, onClickAction, true);
    }

    /**
     * TODO
     * At the end of current version development, when all the activities will
     * extend {@link FragmentActivity}, only {@link AndroidDisplayManager#showOkDialogFragment(Screen, String, String, Runnable, boolean)}
     * will remain, but with name of this method
     * 
     * @param screen
     * @param message
     * @param okButtonLabel
     * @param onClickAction
     * @param cancelable
     * @return
     */
    public int showOkDialog(Screen screen, String message, String okButtonLabel, Runnable onClickAction,
            boolean cancelable) {
        if(screen == null) {
            if (Log.isLoggable(Log.TRACE)) {
                Log.trace(TAG_LOG, "Cannot show dialog, the screen reference is null");
            }
            return -1;
        }
        return showOkDialog((Activity)screen.getUiScreen(), message, okButtonLabel, onClickAction, cancelable);
    }
    
    public int showOkDialog(Activity a, String message, String okButtonLabel, Runnable onClickAction,
            boolean cancelable) {
        //TOOD fix screen and getuiscreen
        int dialogId = getNextDialogId();
        
        //TOOD fix screen and getuiscreen
        if (a instanceof FragmentActivity) {
            return showOkDialogFragment((FragmentActivity)a, message, okButtonLabel, onClickAction, cancelable);
        }

        OnButtonListener lis = null;
        if (onClickAction != null) {
            lis = new OnButtonListener(onClickAction);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(a)
                .setMessage(message)
                .setPositiveButton(okButtonLabel, lis)
                .setCancelable(cancelable);

        a.runOnUiThread(new PromptSelection(a, builder, dialogId));

        return dialogId;
    }
    
    /**
     * TODO
     * When transition to fragment will be completed, remove "Fragment" from
     * name of this method
     * 
     * @param screen
     * @param message
     * @param okButtonLabel
     * @param onClickAction
     * @param cancelable
     * @return
     */
    public int showOkDialogFragment(
            Screen screen,
            String message,
            String okButtonLabel,
            Runnable onClickAction,
            boolean cancelable) {
        if(screen == null) {
            if (Log.isLoggable(Log.TRACE)) {
                Log.trace(TAG_LOG, "Cannot show dialog, the screen reference is null");
            }
            return -1;
        }
        FragmentActivity activity = (FragmentActivity) screen.getUiScreen();
        return showOkDialogFragment(activity, message, okButtonLabel, onClickAction, cancelable);
    }
    
    public int showOkDialogFragment(FragmentActivity activity, String message, String okButtonLabel,
                                    Runnable onClickAction, boolean cancelable) {

        int dialogId = getNextDialogId();
        OnButtonListener lis = null;
        if (onClickAction != null) {
            lis = new OnButtonListener(onClickAction);
        }
        DialogFragment dialog = AlertDialogFragment.Factory.createInformativeDialog(
                null, message, okButtonLabel, lis, cancelable);
        
        showDialogFragment(activity, dialog);
        return dialogId;
    }
    

    /**
     * Create a custom alert dialog based on the given DialogOptions. This kind 
     * of dialog has fixed dialog id that depends on the one set by the caller
     * (Usually an instance of DialogController class). The created dialog is
     * built with a custom title and content and must be managed outside the
     * standard activity dialog management onCreatedialog, but using the
     * activity Bundle passed into the native activity methods onCreate() and
     * onSaveInstanceState(). The Funambol Android Client implementation use a
     * DialogController instance and the activity related to the give screen to
     * realize this kind of management
     * @param screen the native alert dialog owner Screen
     * @param message the description of this dialog options
     * @param options the options array to be displayed to the user
     * @param defaultValue the default selection option int formatted
     */
    public int promptSelection(Screen screen, String message, DialogOption[] options, int defaultValue) {
        return promptSelection(screen, message, options, defaultValue, getNextDialogId());
    }

    /**
     * Creates a custom alert dialog based on the given DialogOptions. This kind 
     * of dialog has fixed dialog id that depends on the one set by the caller
     * (Usually an instance of DialogController class). The created dialog is
     * built with a custom title and content and must be managed outside the
     * standard activity dialog management onCreatedialog, but using the
     * acitvity Bundle passed into the native activity methods onCreate() and
     * onSaveInstanceState(). The Funambol Android Client implementation use a
     * DialogController instance and the activity related to the give screen to
     * realize this kind of management
     * 
     * TODO
     * At the end of current version development, when all the activities will
     * extend {@link FragmentActivity}, only {@link AndroidDisplayManager#promptSelectionFragment(Screen, String, DialogOption[], int, int)}
     * will remain, but with name of this method
     * 
     * @param screen the native alert dialog owner Screen
     * @param message the description of this dialog options
     * @param options the options array to be displayed to the user
     * @param defaultValue the default selection option int formatted
     * @param dialogId the fixed dialog id set by the caller
     */
    public int promptSelection(Screen screen, String message, DialogOption[] options, int defaultValue, int dialogId) {
        if(screen == null) {
            if (Log.isLoggable(Log.TRACE)) {
                Log.trace(TAG_LOG, "Cannot show selection dialog, the screen reference is null");
            }
            return -1;
        }
        if (screen.getUiScreen() instanceof FragmentActivity) {
            return promptSelectionFragment(screen, message, options, dialogId);
        }
        Activity a = (Activity) screen.getUiScreen();
        AlertDialog.Builder builder = new AlertDialog.Builder(a);
        if(message != null) {
            LinearLayout titleLayout = buildCustomDialogTitle(a, message, 20);
            builder.setCustomTitle(titleLayout);
        }
        View bodyView = buildDialogCustomBody(a, options, dialogId);
        builder.setView(bodyView);
        builder.setCancelable(true);
        builder.setOnCancelListener(new SelectionCancelListener(screen, dialogId));

        a.runOnUiThread(new PromptSelection(a, builder, dialogId));

        return dialogId;
    }

    /**
     * TODO
     * When transition to fragment will be completed, remove "Fragment" from
     * name of this method
     * 
     * @param screen
     * @param message
     * @param options
     * @param defaultValue
     * @param dialogId
     * @return
     */
    private int promptSelectionFragment(Screen screen, String message, DialogOption[] options, int dialogId) {
        if(screen == null) {
            if (Log.isLoggable(Log.TRACE)) {
                Log.trace(TAG_LOG, "Cannot show selection dialog, the screen reference is null");
            }
            return -1;
        }
        FragmentActivity activity = (FragmentActivity) screen.getUiScreen();

        Dialog.OnCancelListener cancelListener = new SelectionCancelListener(screen, dialogId);
        MultiSelectionViewsFactory factory = new MultiSelectionViewsFactory(message, options, dialogId);
        DialogFragment dialog = AlertDialogFragment.Factory.createRawDialog(factory, cancelListener);
        showDialogFragment(activity, dialog);
        return dialogId;
    }

    /**
     * Dismiss a previously created Selection dialog using the call to 
     * promptSelection(...). Not to be used for native dialog like
     * ProgressDialog as the management logic is different and not delegated to
     * the native Activity method onCreateDialogs.
     * 
     * TODO remove when transition to fragment will be completed
     * 
     * @param id the id of the selection dialog to be dismissed.
     */
    public void dismissSelectionDialog(Screen screen, int id) {
        if (screen != null) {
            dismissSelectionDialog((Activity)screen.getUiScreen(), id);
        } else {
            dismissSelectionDialog((Activity)screen, id);
        }
    }
    
    private void dismissSelectionDialog(Activity activity, int id) {
        if (Log.isLoggable(Log.DEBUG)) {
            Log.debug(TAG_LOG, "Dismissing selection dialog " + id);
        }
        Dialog d = holdingDialogs.get(id);
        if (d != null) {
            if (d.isShowing()) {
                try {
                    d.dismiss();
                } catch (Exception e) {
                    // If the dialog has already been dismissed, then this is
                    // not a problem
                }
            }
            holdingDialogs.remove(id);
        } else if (activity instanceof FragmentActivity){
            // this is probably a fragment dialog
            dismissDialogFragment((FragmentActivity)activity, id);
        }
    }

    /**
     * Not yet implemented into the Android Client.
     * @param message the question to be prompted
     * @return boolean to be defined
     */
    public boolean promptNext(String message) {
        return true;
    }

    /**
     * Display a time-boxed toast message to be displayed to the user. This kind
     * of prompt alert are internally managed by the Toast android class
     * implementation, so the management is implicit and they are not referenced
     * in any other way.
     * @param screen the Screen on which the message must be displayed
     * @param message the mesage to be displayed
     */
    public void showMessage(final Screen screen, final String message) {
        if (screen != null) {
            final Activity activity = (Activity) screen.getUiScreen();
            showMessage(activity, message);
        }
    }
    
    public void showMessage(final Activity activity, final String message) {
       if (activity != null) {
            final int time = message.length() > 40 ? Toast.LENGTH_LONG :
                                                     Toast.LENGTH_SHORT;
            //we need runOnUiThread call to avoid
            // FATAL EXCEPTION: Thread-10
            // E/AndroidRuntime( 2141): java.lang.RuntimeException: Can't create handler inside thread that has not called Looper.prepare()
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    Toast t = Toast.makeText(appContext, message, time);
                    t.show();
                }
            });
        }
    }

    /**
     * Read the last message displayed into the toast and reset it
     * @return String the String formatted last message displayed with the toast
     */
    public String readAndResetLastMessage() {
        String result = lastMessage;
        lastMessage = null;
        return result;
    }

    /**
     * Container for the selection dialog built by the promptSelection method
     * 
     * TODO remove at when fragment migration is done
     */
    private class PromptSelection implements Runnable {

        private int dialogId;
        private Activity a;
        private AlertDialog.Builder builder;
        private Dialog dialog;

        public PromptSelection(Activity a, AlertDialog.Builder builder, int dialogId) {
            this.a = a;
            this.dialogId = dialogId;
            this.builder = builder;
        }

        public PromptSelection(Activity a, Dialog dialog, int dialogId) {
            this.a = a;
            this.dialog = dialog;
            this.dialogId = dialogId;
        }

        /**
         * Shows the alert and put it into the reference container pendingAlerts
         * with its id for further reference.
         */
        public void run() {
            if (Log.isLoggable(Log.DEBUG)) {
                Log.debug(TAG_LOG, "Showing dialog: " + dialogId);
            }
            
            if (builder != null) {
                holdingDialogs.put(dialogId, builder.create());
            } else if (dialog != null) {
                holdingDialogs.put(dialogId, dialog);
            }
            a.showDialog(dialogId);
        }
    }

    /**
     * Container for the selection dialog built by the promptSelection method
     * 
     * TODO remove when transition to fragment will be completed
     */
    public class ProgressDialogCreator implements Runnable {

        private int dialogId;
        private Activity a;
        private String prompt;
        private boolean indeterminate;
        private Runnable onCancelAction;

        public ProgressDialogCreator(Activity a, int dialogId, String prompt, boolean indeterminate,
                                     Runnable onCancelAction) {
            this.a = a;
            this.dialogId = dialogId;
            this.prompt = prompt;
            this.indeterminate = indeterminate;
            this.onCancelAction = onCancelAction;
        }

        /**
         * Shows the alert and put it into the reference container pendingAlerts
         * with its id for further reference.
         */
        public void run() {
            if (Log.isLoggable(Log.DEBUG)) {
                Log.debug(TAG_LOG, "Showing progress dialog: " + dialogId);
            }

            ProgressDialog dialog = new ProgressDialog(a);
            dialog.setMessage(prompt);
            dialog.setIndeterminate(indeterminate);
            dialog.setProgressStyle(indeterminate ? ProgressDialog.STYLE_SPINNER :
                                                    ProgressDialog.STYLE_HORIZONTAL );
            if (onCancelAction != null) {
                DialogCancelListener lis = new DialogCancelListener(dialogId, onCancelAction, false);
                dialog.setCancelable(true);
                dialog.setOnCancelListener(lis);
            } else {
                dialog.setCancelable(false);
            }

            holdingDialogs.put(dialogId, dialog);
            a.showDialog(dialogId);
        }
    }


    /**
     * To be implemented
     */
    public void toForeground() {
    }

    /**
     * To be implemented
     */
    public void toBackground() {
    }

    public Object getCurrentUiScreen() {
        if (screenStack.size() > 0) {
            return screenStack.get(screenStack.size() - 1);
        }
        return null;
    }

    /**
     * To be implemented
     */
    public void loadBrowser(Screen screen, String url) {
        Activity activity = (Activity) screen.getUiScreen();
        Intent viewIntent = new Intent("android.intent.action.VIEW", Uri.parse(url));
        activity.startActivity(viewIntent); 
    }

    /**
     * Display a notification to the user
     */
    public void showNotification(NotificationData notificationData) {
        if (null == appContext) {
            if (Log.isLoggable(Log.INFO)) {
                Log.info(TAG_LOG, "Cannot show notification because context reference is null");
            }
            return;
        } else if (null == notificationData) {
            if (Log.isLoggable(Log.INFO)) {
                Log.info(TAG_LOG, "Cannot show notification because notification data is null");
            }
            return;
        } else {
            if (Log.isLoggable(Log.DEBUG)) {
                Log.debug(TAG_LOG, "Show notification " + notificationData.getTitle());
            }
        }
        
        int icon = notificationData.getIconResource();
        if(icon == 0) {
            //find icon based on notification id
            switch (notificationData.getId()) {
            case NotificationController.NOTIFICATION_ID_REFRESH_IN_PROGRESS:
            case NotificationController.NOTIFICATION_ID_REFRESH_CANCELING:
                icon = android.R.drawable.stat_notify_sync;
                break;
            }
        }
        if(icon == 0) {
            //find icon based on severity
            switch (notificationData.getSeverity()) {
                case NotificationData.SEVERITY_ERROR:
                case NotificationData.SEVERITY_WARNING:
                    icon = android.R.drawable.stat_notify_error;
                    break;
            }
        }
        
        long when = System.currentTimeMillis();
        CharSequence tickerText = notificationData.getTicker();
        
        CharSequence contentTitle = notificationData.getTitle();
        CharSequence contentText = notificationData.getMessage();
        //for Android, tag value contains the class of the activity to open
        Class activityClass = (Class) notificationData.getTag();
        Intent notificationIntent;
        if (null != activityClass) {
            notificationIntent = new Intent(appContext, activityClass);
        } else {
            //insert fallback activity for a notification
            notificationIntent = new Intent(appContext, AndroidMainScreen.class);
            //prevent to open the activity twice
            notificationIntent.setAction("android.intent.action.MAIN");
            notificationIntent.addCategory("android.intent.category.LAUNCHER");
        }
        PendingIntent contentIntent = PendingIntent.getActivity(appContext,
                                                                TimerHandler.DISPLAY_MANAGER_PENDING_INTENT_ID,
                                                                notificationIntent, 0);
        
        Notification notification = new Notification(icon, tickerText, when);
        //OLD
        //notification.flags = Notification.DEFAULT_LIGHTS | Notification.FLAG_AUTO_CANCEL;
        //NEW
        notification.flags = Notification.FLAG_ONGOING_EVENT | Notification.FLAG_NO_CLEAR;
        notification.setLatestEventInfo(appContext, contentTitle, contentText, contentIntent);
        
        
        // gets android notification manager service.
        NotificationManager notificationManager = (NotificationManager)
                appContext.getSystemService(Context.NOTIFICATION_SERVICE);
        //finally, launches the notification
        notificationManager.notify(notificationData.getId(), notification);        
    }
    
    /**
     * Remove a notification from the android notification area
     */
    public void hideNotification(int notificationId) {
        NotificationManager notificationManager = (NotificationManager)
                appContext.getSystemService(Context.NOTIFICATION_SERVICE);
        try {
            notificationManager.cancel(notificationId);
        } catch (Exception e) {
        }
    }

    /**
     * Listener for user click events. This can be referenced both as a click
     * listener for the DialogInterface and view. When the click event happens
     * this listener runs the related Runnable option.
     */
    private class OnButtonListener implements DialogInterface.OnClickListener, OnClickListener {

        private Runnable action;

        public OnButtonListener(Runnable action) {
            this.action = action;
        }

        public void onClick(DialogInterface dialog, int which) {
            if (action != null) {
                Thread t = new Thread(action);
                t.start();
            }
        }

        public void onClick(View arg0) {
            if (action != null) {
                Thread t = new Thread(action);
                t.start();
            }
        }
    }

    /**
     * Cancel listener for the Selection Alert Dialog. Not to be used for native
     * dialogs.
     */
    private class SelectionCancelListener implements OnCancelListener {

        private int id;
        private Screen screen;
        
        public SelectionCancelListener(Screen screen, int id) {
            this.screen = screen;
            this.id = id;
        }

        public void onCancel(DialogInterface arg0) {
            dismissSelectionDialog(screen, id);
        }
    }

    private class DialogCancelListener implements DialogInterface.OnCancelListener {

        private Runnable action;
        private int id;
        private boolean fragment;

        public DialogCancelListener(int id, Runnable action, boolean fragment) {
            this.id = id;
            this.action = action;
            this.fragment = fragment;
        }

        public void onCancel(DialogInterface dialog) {
            Activity currentActivity = null;
            if (dialog instanceof Dialog) {
                currentActivity = ((Dialog)dialog).getOwnerActivity();
            }
            if (fragment) {
                dismissDialogFragment((FragmentActivity)currentActivity, id);
            } else {
                dismissSelectionDialog(currentActivity, id);
            }
            action.run();
        }

    }

    private LinearLayout buildCustomDialogTitle(Activity a, String message, int fontSize) {
        LinearLayout titleLayout = new LinearLayout(a);
        titleLayout.setLayoutParams(new LinearLayout.LayoutParams(
                LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
        titleLayout.setGravity(Gravity.FILL_VERTICAL);
        TextView title = new TextView(a);
        title.setText(message);
        title.setPadding(AndroidUtils.dipToPx(5, a), AndroidUtils.dipToPx(5, a),
                         AndroidUtils.dipToPx(5, a), AndroidUtils.dipToPx(5, a));
        title.setGravity(Gravity.CENTER_HORIZONTAL);
        title.setTextColor(Color.WHITE);
        title.setTextSize(fontSize);
        title.setLayoutParams(new LinearLayout.LayoutParams(
                LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
        titleLayout.addView(title);
        return titleLayout;
    }
    
    private View buildMultiChoicesDialogBody(final FragmentActivity a, String choices[], boolean status[],
                                             final int dialogId, String okText, String cancelText,
                                             DialogInterface.OnMultiChoiceClickListener multiChoiceClickListener,
                                             final DialogInterface.OnClickListener okButtonClickListener,
                                             final DialogInterface.OnClickListener cancelButtonClickListener)
    {
        LayoutInflater layoutInflater = (LayoutInflater)a.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        
        ScrollView scrollView = (ScrollView)layoutInflater.inflate(R.layout.vwdialogmultichoice, null, false);
        
        LinearLayout choicesLayout = (LinearLayout)scrollView.findViewById(R.id.dialogmultichoice_layChoices);
        
        for(int i=0;i<choices.length;++i) {
            String choice = choices[i];
            
            LinearLayout itemLayout = (LinearLayout)layoutInflater.inflate(R.layout.vwdialogmultichoiceitem,
                                                                           choicesLayout);
            TextView label = (TextView)itemLayout.findViewById(R.id.dialogmultichoiceitem_lblText);
            label.setText(choice);
            
            CheckBox checkBox = (CheckBox)itemLayout.findViewById(R.id.dialogmultichoiceitem_chkSelected);
            checkBox.setChecked(status[i]);
            // Set a listener for the item
            MultiChoiceButtonListener listener = new MultiChoiceButtonListener(multiChoiceClickListener, i);
            checkBox.setOnCheckedChangeListener(listener);
        }
        
        // Now add the buttons bar listener and text
        Button okButton = (Button)scrollView.findViewById(R.id.dialogmultichoice_btnSave);
        Button cancelButton = (Button)scrollView.findViewById(R.id.dialogmultichoice_btnCancel);
        
        okButton.setText(okText);
        cancelButton.setText(cancelText);
        
        okButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Close the dialog
                DialogFragment dialog = findDialogFragment(a);
                Dialog d = dialog.getDialog();
                dismissDialogFragment(a, dialogId);
                okButtonClickListener.onClick(d, DialogInterface.BUTTON_POSITIVE);
            }
        });
        
        cancelButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DialogFragment dialog = findDialogFragment(a);
                Dialog d = dialog.getDialog();
                dismissDialogFragment(a, dialogId);
                cancelButtonClickListener.onClick(d, DialogInterface.BUTTON_NEGATIVE);
            }
        });
        
        return scrollView;
    }

    /**
     * Creates custom dialog body for custom dialog
     * 
     * @param options
     * @param a
     * @param dialogId
     * @return
     */
    private View buildDialogCustomBody(Context context, DialogOption[] options, int dialogId) {
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout containerView = new LinearLayout(context);
        containerView.setOrientation(LinearLayout.VERTICAL);
        containerView.setPadding(AndroidUtils.dipToPx(10, context),
                                 AndroidUtils.dipToPx(10, context),
                                 AndroidUtils.dipToPx(10, context),
                                 AndroidUtils.dipToPx(5 /* The buttons already have a bottom margin*/, context));
        containerView.setGravity(Gravity.FILL_VERTICAL | Gravity.CENTER_HORIZONTAL);
        containerView.setLayoutParams(new LinearLayout.LayoutParams(
                LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
        for (int i = 0; i < options.length; i++) {
            Button b;
            if (options[i].isDark()) {
                b = (Button)layoutInflater.inflate(R.layout.vwdialogbuttondark, containerView, false);
            } else if (options[i].isCritical()) {
                b = (Button)layoutInflater.inflate(R.layout.vwdialogbuttonalert, containerView, false);
            } else {
                b = (Button)layoutInflater.inflate(R.layout.vwdialogbuttonlight, containerView, false);
            }
            b.setEnabled(options[i].isEnabled());
            b.setText(options[i].getDescription());
            b.setOnClickListener(new OnButtonListener(options[i]));
            containerView.addView(b);
            options[i].setDialogId(dialogId);
        }
        ScrollView scrollView = new ScrollView(appContext);
        scrollView.setLayoutParams(new LinearLayout.LayoutParams(
                LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
        scrollView.addView(containerView);
        return scrollView;
    }

    private int getNextDialogId() {
        return incrementalId++;
    }

    /**
     * Shows a {@link DialogFragment} inside the current activity
     * 
     * @param activity
     * @param dialogToShow
     */
    public void showDialogFragment(FragmentActivity activity, DialogFragment dialogToShow) {
        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        if (Log.isLoggable(Log.TRACE)) {
            Log.trace(TAG_LOG, "Show fragment dialog");
        }
        // Dialogs cannot be shown on paused fragment activities
        if(activity instanceof BasicFragmentActivity) {
            BasicFragmentActivity bfa = (BasicFragmentActivity)activity;
            if(bfa.isPaused()) {
                bfa.showDialogOnResume(dialogToShow);
                return;
            }
        }
        FragmentManager fm = activity.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment prev = findDialogFragment(activity);
        if (prev != null) {
            try {
                ft.remove(prev);
            } catch (Exception e) {
                Log.error(TAG_LOG, "The fragment no longer exists, it probably was closed");
            }
        }
        // We do not avoid to the back stack even if Google official examples does it. The reason is twofold:
        // 1) On some devices with Android 3.0 we had crashes on rotation/dialog dismiss
        // 2) The compatibility library does not do it when a dialog is shown via show(FragmentManager, String)
        //ft.addToBackStack(null);
        dialogToShow.show(ft, "dialog");
    }
    
    /**
     * Finds a fragment in the activity's fragment stack
     * 
     * @param activity
     * @return the fragment object, otherwise null
     */
    private DialogFragment findDialogFragment(FragmentActivity activity) {
        FragmentManager fm = activity.getSupportFragmentManager();
        Fragment prev = fm.findFragmentByTag("dialog");
        return (prev instanceof DialogFragment) ? (DialogFragment) prev : null;
    }

    /**
     * Removes currently displayed progress dialog.
     * 
     * TODO
     * At the end of develope, remove the "Fragment" world in the method name,
     * and remove dialogId 
     * @param screen
     */
    public void dismissDialogFragment(Screen screen, int dialogId) {
        if(screen == null) {
            if (Log.isLoggable(Log.TRACE)) {
                Log.trace(TAG_LOG, "Cannot dismiss dialog, the screen reference is null");
            }
            return;
        }
        if (!(screen instanceof FragmentActivity)) {
            if (Log.isLoggable(Log.TRACE)) {
                Log.trace(TAG_LOG, "Cannot dismiss dialog, the screen reference is not a FragmentActivity");
            }
            return;
        }
        FragmentActivity activity = (FragmentActivity) screen;   
        dismissDialogFragment(activity, dialogId);
    }
    
    private void dismissDialogFragment(FragmentActivity activity, int dialogId) {
        if (Log.isLoggable(Log.DEBUG)) {
            Log.debug(TAG_LOG, "Dismissing dialog " + dialogId);
        }
        DialogFragment dialog = findDialogFragment(activity);
        if (null != dialog) {
            dialog.dismiss();
        } else {
            if (Log.isLoggable(Log.DEBUG)) {
                Log.debug(TAG_LOG, "No dialog associated this fragment, nothing to dismiss");
            }
        }
    }
    
    private class MultiChoiceButtonListener implements CompoundButton.OnCheckedChangeListener {
        private DialogInterface.OnMultiChoiceClickListener multiListener;
        private int idx;
        
        public MultiChoiceButtonListener(DialogInterface.OnMultiChoiceClickListener multiListener, int idx) {
            this.multiListener = multiListener;
            this.idx = idx;
        }

        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            multiListener.onClick(null, idx, isChecked);
        }
    }
    
    private class MultiSelectionViewsFactory implements AlertDialogFragment.RawDialogViewsFactory {
        
        private String message;
        private DialogOption options[];
        private int dialogId;
        
        public MultiSelectionViewsFactory(String message, DialogOption options[], int dialogId) {
            this.message = message;
            this.options = options;
            this.dialogId = dialogId;
        }

        public View createTitleView(FragmentActivity a) {
            View titleView = null;
            if(message != null) {
                titleView = buildCustomDialogTitle(a, message, 20);
            }
            return titleView;
        }

        public View createBodyView(FragmentActivity a) {
            return buildDialogCustomBody(a, options, dialogId);
        }
    }
    
    private class MultiChoicesViewsFactory implements AlertDialogFragment.RawDialogViewsFactory {
        
        private String title;
        private String choices[];
        private boolean checkedChoices[];
        private int dialogId;
        private String okButtonLabel;
        private String cancelButtonLabel;
        private DialogInterface.OnMultiChoiceClickListener multiChoiceClickListener;
        private DialogInterface.OnClickListener okButtonClickListener;
        private DialogInterface.OnClickListener cancelButtonClickListener;
        
        public MultiChoicesViewsFactory(String title, String choices[], boolean checkedChoices[],
                                       int dialogId, String okButtonLabel, String cancelButtonLabel,
                                       DialogInterface.OnMultiChoiceClickListener multiChoiceClickListener,
                                       DialogInterface.OnClickListener okButtonClickListener,
                                       DialogInterface.OnClickListener cancelButtonClickListener)
        {
            this.title = title;
            this.choices = choices;
            this.checkedChoices = checkedChoices;
            this.dialogId = dialogId;
            this.okButtonLabel = okButtonLabel;
            this.cancelButtonLabel = cancelButtonLabel;
            this.multiChoiceClickListener = multiChoiceClickListener;
            this.okButtonClickListener = okButtonClickListener;
            this.cancelButtonClickListener = cancelButtonClickListener;
        }

        public View createTitleView(FragmentActivity a) {
            return buildCustomDialogTitle(a, title, 18);
        }

        public View createBodyView(FragmentActivity a) {
            return buildMultiChoicesDialogBody(a, choices, checkedChoices, dialogId,
                                               okButtonLabel, cancelButtonLabel, multiChoiceClickListener,
                                               okButtonClickListener, cancelButtonClickListener);
        }
    }

    
}
