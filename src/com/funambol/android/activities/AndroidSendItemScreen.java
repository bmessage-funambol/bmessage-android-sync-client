/**
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2011 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.activities;

import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.util.Linkify;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.funambol.android.AndroidUtils;
import com.funambol.android.AppInitializer;
import com.funambol.android.activities.view.AndroidActionBarView;
import com.funambol.androidsync.R;
import com.funambol.client.controller.Controller;
import com.funambol.client.controller.SendItemScreenController;
import com.funambol.client.localization.Localization;
import com.funambol.client.services.Album;
import com.funambol.client.ui.SendItemScreen;
import com.funambol.client.ui.view.ActionBarWidget;
import com.funambol.client.ui.view.ThumbnailView;
import com.funambol.storage.Tuple;
import com.funambol.util.Log;
import com.funambol.util.StringUtil;


public class AndroidSendItemScreen extends BasicActivity implements SendItemScreen {
    private static final String TAG_LOG = AndroidSendItemScreen.class.getSimpleName();

    private final int DEFAULT_THUMBS_PADDING = 4;

    private Controller controller;
    private LinearLayout serviceInfoContainer;

    private Spinner albumSpinner;
    private Spinner privacySpinner;
    private EditText description;

    private LayoutInflater layoutInflater;

    private SendItemScreenController sendItemScreenController;

    private static boolean albumRefreshDone = false;

    /**
     * Called with the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Log.isLoggable(Log.TRACE)) {
            Log.trace(TAG_LOG, "onCreate");
        }
        AppInitializer appInitializer = AppInitializer.i(this);
        controller = appInitializer.getController();

        setContentView(R.layout.actsenditem);

        serviceInfoContainer = (LinearLayout)findViewById(R.id.senditem_layserviceinfo);

        layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        sendItemScreenController = controller.getSendItemScreenController();
        sendItemScreenController.setScreen(this);
        
        boolean firstRun = savedInstanceState == null;

        albumRefreshDone &= !firstRun;

        sendItemScreenController.initScreen(firstRun);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(albumSpinner != null) {
            sendItemScreenController.updateDefaultAlbum(getSelectedAlbum());
        }
        if(privacySpinner != null) {
            sendItemScreenController.updateDefaultPrivacy(getSelectedPrivacy());
        }
    }

    public Object getUiScreen() {
        return this;
    }

    public void setAccountInfo(String text, byte[] icon) {
        TextView accountInfo = (TextView)findViewById(R.id.senditem_accountInfo);
        accountInfo.setText(text);
        ImageView accountIcon = (ImageView)findViewById(R.id.senditem_imgserviceicon);
        Bitmap bitmap = BitmapFactory.decodeByteArray(icon, 0, icon.length);
        accountIcon.setImageBitmap(bitmap);

        // Give focus to an item which is not an EditText. This is to prevent
        // the virtual keyboard to be shown
        accountIcon.setFocusable(true);
        accountIcon.setFocusableInTouchMode(true);
        accountIcon.requestFocus();
    }

    public void setTermsInfo(String text, String linkText, final String link) {
        TextView termsInfo = (TextView)findViewById(R.id.senditem_lblterms);
        termsInfo.setText(text);
        Linkify.addLinks(termsInfo, Pattern.compile(linkText), "",
                null, new Linkify.TransformFilter() {
            public final String transformUrl(final Matcher match, String url) {
                return link;
            }
        });
    }

    public void addAlbumSelector() {
        LinearLayout newRow = (LinearLayout)layoutInflater.inflate(
                R.layout.vwsenditemalbumselection, null);
        LinearLayout spinnerContainer = (LinearLayout)newRow.findViewById(
                R.id.senditem_spialbumcontainer);
        albumSpinner = new Customspinner(this);
        albumSpinner.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.FILL_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        albumSpinner.setPrompt(controller.getLocalization().getLanguage(
                "send_item_select_album"));
        spinnerContainer.addView(albumSpinner);
        addNewRowToServiceInfo(newRow);
    }

    public void updateAlbums(Vector albums, Album defaultAlbum) {

        if(albumSpinner == null) {
            return;
        }

        final Context dialogContext = new ContextThemeWrapper(this, android.R.style.Theme_Light);

        final LayoutInflater dialogInflater = (LayoutInflater)dialogContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        ArrayAdapter adapter = new ArrayAdapter<Album>(this, android.R.layout.simple_spinner_item) {
            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                Album album = getItem(position);
                String albumName = album.getName();
                String albumPrivacy = album.getPrivacy();
                // Inflate view if needed
                if (convertView == null) {
                    int viewRes = android.R.layout.simple_list_item_1;
                    if(albumPrivacy != null) {
                        viewRes = android.R.layout.simple_list_item_2;
                    }
                    convertView = dialogInflater.inflate(viewRes, parent, false);
                }
                TextView albumNameView = (TextView)convertView.findViewById(android.R.id.text1);
                albumNameView.setText(albumName);
                if(albumPrivacy != null) {
                    TextView albumPrivacyView = (TextView)convertView.findViewById(android.R.id.text2);
                    String privacyText = controller.getLocalization()
                            .getLanguage("send_item_shared_with");
                    privacyText = StringUtil.replaceAll(privacyText,
                            "__PRIVACY_TYPE__",
                            getPrivacyTypeLabel(albumPrivacy));
                    albumPrivacyView.setText(privacyText);
                }
                return convertView;
            }
        };
        for(int i=0; i<albums.size(); i++) {
            Album album = (Album)albums.elementAt(i);
            adapter.add(album);
        }
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        albumSpinner.setAdapter(adapter);
        if(defaultAlbum != null) {
            int defaultPosition = adapter.getPosition(defaultAlbum);
            if(defaultPosition >= 0) {
                albumSpinner.setSelection(defaultPosition);
            }
        }
    }

    public void addDescription() {
        LinearLayout newRow = (LinearLayout)layoutInflater.inflate(
                R.layout.vwsenditemdescription, null);
        description = (EditText)newRow.findViewById(R.id.senditem_lbldescription);
        addNewRowToServiceInfo(newRow);
    }

    public void addPrivacySelector(Vector privacyTypes, String defaultType) {
        LinearLayout newRow = (LinearLayout)layoutInflater.inflate(
                R.layout.vwsenditemprivacyselection, null);
        privacySpinner = (Spinner)newRow.findViewById(R.id.senditem_spiprivacy);
        ArrayAdapter adapter = new ArrayAdapter<Album>(this,
                android.R.layout.simple_spinner_item);
        for(int i=0; i<privacyTypes.size(); i++) {
            String type = (String)privacyTypes.elementAt(i);
            adapter.add(type);
        }
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        privacySpinner.setAdapter(adapter);
        if(defaultType != null) {
            int defaultPosition = adapter.getPosition(defaultType);
            if(defaultPosition >= 0) {
                privacySpinner.setSelection(defaultPosition);
            }
        }
        addNewRowToServiceInfo(newRow);
    }

    public void addItemsThumbnails(Vector tuples) {
        LinearLayout newRow = (LinearLayout)layoutInflater.inflate(
                R.layout.vwsenditemthumbnails, null);
        TextView itemsCount = (TextView)newRow.findViewById(
                R.id.senditem_lblitemscount);
        Localization localization = controller.getLocalization();
        String text = localization.getLanguage("send_item_n_items");            
        text = StringUtil.replaceAll(text, "__N_ITEMS__",
                localization.getInflector().inflectFrom("item_inflexion_generic")
                .withNumber(tuples.size()).toString());
        itemsCount.setText(text);
        LinearLayout thumbsContainer = (LinearLayout)newRow.findViewById(
                R.id.senditem_laythumbs);
        thumbsContainer.setPadding(
                        AndroidUtils.dipToPx(DEFAULT_THUMBS_PADDING, this),
                        AndroidUtils.dipToPx(DEFAULT_THUMBS_PADDING, this),
                        0,
                        AndroidUtils.dipToPx(DEFAULT_THUMBS_PADDING, this));
        for(int i=0; i<tuples.size(); i++) {
            Tuple tuple = (Tuple)tuples.elementAt(i);
            ThumbnailView tv = sendItemScreenController.createThumbnailView(tuple);
            tv.setPadding(0, 0, AndroidUtils.dipToPx(DEFAULT_THUMBS_PADDING, this), 0);
            tv.setVisible(true);
            thumbsContainer.addView((View)tv.getThumbnailView());
        }
        addNewRowToServiceInfo(newRow);
    }

    public Album getSelectedAlbum() {
        if(albumSpinner != null) {
            return (Album)albumSpinner.getSelectedItem();
        } else {
            return null;
        }
    }

    public String getDescription() {
        if(description != null) {
            return description.getText().toString();
        } else {
            return null;
        }
    }
    
    public String getSelectedPrivacy() {
        if(privacySpinner != null) {
            return (String)privacySpinner.getSelectedItem();
        } else {
            return null;
        }
    }

    private void addNewRowToServiceInfo(LinearLayout row) {
        if(serviceInfoContainer.getChildCount() > 0) {
            View divider = layoutInflater.inflate(
                    R.layout.vwroundedboxdivider, null);
            serviceInfoContainer.addView(divider);
        }
        serviceInfoContainer.addView(row);
    }

    private String getPrivacyTypeLabel(String type) {
        Localization localization = controller.getLocalization();
        if(type.equalsIgnoreCase("everyone")) {
            return localization.getLanguage("send_item_privacy_everyone");
        } else if(type.equalsIgnoreCase("friends") || type.equalsIgnoreCase("is_friend")) {
            return localization.getLanguage("send_item_privacy_friends");
        } else if(type.equalsIgnoreCase("friends-of-friends")) {
            return localization.getLanguage("send_item_privacy_friends_of_friends");
        } else if(type.equalsIgnoreCase("public") || type.equalsIgnoreCase("is_public")) {
            return localization.getLanguage("send_item_privacy_public");
        } else if(type.equalsIgnoreCase("private") || type.equalsIgnoreCase("is_private")) {
            return localization.getLanguage("send_item_privacy_private");
        } else if(type.equalsIgnoreCase("protected") || type.equalsIgnoreCase("is_protected")) {
            return localization.getLanguage("send_item_privacy_protected");
        } else if(type.equalsIgnoreCase("family") || type.equalsIgnoreCase("is_family")) {
            return localization.getLanguage("send_item_privacy_family");
        } else if(type.equalsIgnoreCase("family_and_friend") || type.equalsIgnoreCase("is_family_and_friend")) {
            return localization.getLanguage("send_item_privacy_family_and_friends");
        } else if(type.equalsIgnoreCase("custom")) {
            return localization.getLanguage("send_item_privacy_custom");
        }
        return type;
    }

    // Defines a custom spinner which is able to refresh the albums information
    // when the user clicks on it
    private class Customspinner extends Spinner {

        public Customspinner(Context context) {
            super(context);
        }

        @Override
        public boolean performClick() {
            if(albumRefreshDone) {
                return super.performClick();
            } else {
                sendItemScreenController.refreshAlbumsInformation(
                        new SendItemScreenController.RefreshCompletedCallback() {
                        public void refreshCompleted(final boolean deprecated) {
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    albumRefreshDone = true;
                                    if(!deprecated) {
                                        Customspinner.super.performClick();
                                    }
                                }
                            });
                        }
                }, false);
                return true;
            }
        }
    }

    // ----------------------------------------------- ActionBarWidgetContainer
    
    public ActionBarWidget getActionBarWidget() {
        AndroidActionBarView actionBarView = (AndroidActionBarView)
                findViewById(R.id.actionbar);
        return actionBarView;
    }

    public com.funambol.client.ui.Bitmap getActionBitmap(int actionId) {
        return null;
    }
}
