/*
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2011 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */
package com.funambol.android.controller;

import java.io.File;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;

import com.funambol.android.AppInitializer;
import com.funambol.android.activities.AndroidOpenItemScreen;
import com.funambol.client.controller.OpenFileScreenController;
import com.funambol.client.localization.Inflexion;
import com.funambol.util.Log;
import com.funambol.util.StringUtil;

/**
 * This is the controller, specific for Android, to open videos.
 */
public class AndroidOpenFileScreenController extends OpenFileScreenController {

    private static final String TAG_LOG = AndroidOpenFileScreenController.class.getSimpleName();
        
    public AndroidOpenFileScreenController() {
        super();
    }
    
    public boolean play(String urlOrPath) {
        
        String mimeType = getOrInferMimeType();
        
        if (Log.isLoggable(Log.INFO)) {
            Log.info(TAG_LOG, "Viewing " + mimeType + " at " + urlOrPath);
        }        

        AndroidOpenItemScreen activity = ((AndroidOpenItemScreen) getOpenItemScreen().getUiScreen());
        Uri uri;
        
        if (urlOrPath.startsWith("https:") &&
                AppInitializer.i(activity).getCustomization().isStreamingOverHttpOnly()) {
            Log.info(TAG_LOG, "Cannot stream over HTTPS, switching to HTTP");
            uri = Uri.parse(urlOrPath.replaceFirst("https:", "http:"));
        } else if (urlOrPath.matches("[a-z]+:.*")) {
            uri = Uri.parse(urlOrPath);
        } else {
            uri = Uri.fromFile(new File(urlOrPath));
        }
        
        Intent intent = new Intent(Intent.ACTION_VIEW);
        if (!StringUtil.isNullOrEmpty(mimeType)) {
            intent.setDataAndType(uri, mimeType);
        } else {
            intent.setData(uri); // risks to launch a browser if it's http:something
        }
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        try {
            activity.startActivity(intent);
            return true;
        } catch (ActivityNotFoundException anfe) {
            if (Log.isLoggable(Log.ERROR)) {
                Log.error(TAG_LOG, "No activity able to view this file: " + anfe.getMessage());
            }
            String message;
            if (urlOrPath.startsWith("http")) {
                message = localization.getLanguage("open_item_no_player_remote");
            } else {
                message = localization.getLanguage("open_item_no_player_local");
            }
            message = StringUtil.replaceAll(message, "__THING__", 
                    appSource.getItemInflexion().using(Inflexion.SINGULAR).toString());
            AppInitializer.i(activity).getDisplayManager().showOkDialog(activity,
                    message, localization.getLanguage("dialog_ok"));
            return false;
        }
    }
}
