/*
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2009 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.controller;

import android.content.Context;

import com.funambol.android.AndroidEmailSender;
import com.funambol.android.AppInitializer;
import com.funambol.android.activities.AndroidDisplayManager;

import com.funambol.client.localization.Localization;
import com.funambol.client.controller.Controller;
import com.funambol.client.controller.ControllerDataFactory;
import com.funambol.client.configuration.Configuration;
import com.funambol.client.customization.Customization;
import com.funambol.client.source.AppSyncSourceManager;
import com.funambol.concurrent.TaskExecutor;
import com.funambol.util.MediaUtils;

public class AndroidController extends Controller {
    private static final String TAG_LOG = AndroidController.class.getSimpleName();

    private Context appContext;

    // Constructor
    public AndroidController(
            ControllerDataFactory fact,
            Configuration configuration,
            Customization customization,
            Localization localization,
            AppSyncSourceManager appSyncSourceManager,
            AndroidSyncModeHandler androidSyncModeHandler,
            TaskExecutor networkTaskExecutor,
            AndroidEmailSender emailSender,
            Context appContext)
    {
        super(fact, configuration, customization, localization, appSyncSourceManager,
              androidSyncModeHandler, networkTaskExecutor, emailSender);
        this.appContext = appContext;
   }

    /**
     * @deprecated
     * Do not use this method, is here only for compatibility reason and will be removed soon.
     * User {@link AppInitializer#getDisplayManager()} instead.
     * 
     * @return
     */
    public AndroidDisplayManager getAndroidDisplayManager() {
        return (AndroidDisplayManager) getDisplayManager();
    }

    protected MediaUtils createMediaUtils() {
        return new MediaUtils(appContext);
    }
}
