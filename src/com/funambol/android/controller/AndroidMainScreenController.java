/**
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2010 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.controller;

import java.util.Vector;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import com.funambol.android.AppInitializer;
import com.funambol.android.activities.AllSourcesSummaryFragment;
import com.funambol.android.activities.FragmentGenerator;
import com.funambol.android.activities.FragmentHelper;
import com.funambol.android.activities.view.AllSourcesSummaryNavigationBarButtonView;
import com.funambol.android.services.AutoSyncServiceHandler;
import com.funambol.platform.NetworkStatus;
import com.funambol.client.controller.MainScreenController;
import com.funambol.client.controller.Controller;
import com.funambol.client.controller.NavigationBarController;
import com.funambol.client.source.AppSyncSource;
import com.funambol.client.ui.MainScreen;
import com.funambol.client.ui.Widget;
import com.funambol.client.ui.view.NavigationBarButtonView;
import com.funambol.util.Log;

public class AndroidMainScreenController extends MainScreenController implements FragmentGenerator {
    private static final String TAG_LOG = AndroidMainScreenController.class.getSimpleName();

    protected final Context context;
    protected final AutoSyncServiceHandler autoSyncServiceHandler;
    
    public AndroidMainScreenController(
            Context appContext,
            Controller controller,
            MainScreen mainScreen) {
        super(controller, mainScreen, new NetworkStatus());
        this.context = appContext.getApplicationContext();
        autoSyncServiceHandler = new AutoSyncServiceHandler(appContext);
    }
    

    @Override
    protected void fireSynchronization(String syncType, Vector syncSources, boolean reset, int direction) {
        autoSyncServiceHandler.startSync(syncType, syncSources);
    }

    @Override
    protected void executeRefreshStartTasks() {
        super.executeRefreshStartTasks();
        //lock wifi
        AppInitializer.i(context).acquireWiFiLock(context);
    }

    @Override
    protected void executeRefreshEndTasks() {
        // Even during a sync all we are sure the code will end up here
        // because the syncAll is set to false in both sourceEnded and
        // sourceFailed.
        super.executeRefreshEndTasks(); // and all end-of-sync tasks are managed by its superclasses

        //release wifi lock
        AppInitializer.i(context).releaseWiFiLock(true);
    }
    
    /**
     * Creates a new fragment to use for a specific navigation bar button
     * @param widgetId
     * @return
     */
    public Fragment createFragment(int widgetId) {
        Fragment newFragment = null;
        
        //check if the buttonId is equal to all source summary id
        if (BUTTONID_ALLSOURCESSUMMARY == widgetId) {
            newFragment = new AllSourcesSummaryFragment();
        } else {
            //cycles thru sync sources to find the right widget to display,
            //based on the fact that widget id is equal to sync source id.
            AppSyncSource appSyncSource = appSyncSourceManager.getSource(widgetId);
            if (null != appSyncSource) {
                Widget syncSourceWidget = appSyncSource.createSourceDetailedWidget();
                newFragment = (Fragment)syncSourceWidget;
            }
        }
        
        if (null != newFragment) {
            if (Log.isLoggable(Log.TRACE)) {
                Log.trace(TAG_LOG, "Created fragment for widget id " + widgetId);
            }
            //updates the list of created widget
            newWidgetCreated((Widget)newFragment);
        } else {
            if (Log.isLoggable(Log.INFO)) {
                Log.info(TAG_LOG, "Cannot find an appropriate fragment for widget id " + widgetId);
            }
        }
        return newFragment;
    }

    /**
     * Generates a unique id for each type of widget (fragment) to show.
     * Actually id generation is based on canonical name of the widget class
     * 
     * @param widgetId id of the widget to show
     * @return
     */
    public String getFragmentUniqueId(int widgetId) {
        if (BUTTONID_ALLSOURCESSUMMARY == widgetId) {
            return AllSourcesSummaryFragment.class.getCanonicalName();
        } else {
            AppSyncSource appSyncSource = appSyncSourceManager.getSource(widgetId);
            if (null != appSyncSource) {
                //only the conversion to string is needed, because the object
                //is already a class
                return appSyncSource.getChronologicalWidgetClass().getCanonicalName();
            }
        }
        if (Log.isLoggable(Log.DEBUG)) {
            Log.debug(TAG_LOG, "Cannot find a fragment id from widget id " + widgetId);
        }
        return null;
    }

    /**
     * Generates a unique id for each type of widget (fragment) to show.
     * Actually id generation is based on canonical name of the widget class.
     * Must be maintained consistent with {@link AndroidMainScreenController#getFragmentId(int)
     * 
     * @param fragment
     * @return
     */
    public String getFragmentUniqueId(Fragment fragment) {
        if (null != fragment) {
            return fragment.getClass().getCanonicalName();
        } else {
            return null;
        }
    }
    
    @Override
    protected NavigationBarButtonView createAllSourcesResumeNavigationBarButtonView(
            NavigationBarController navigationBarController) {
        return new AllSourcesSummaryNavigationBarButtonView(context, navigationBarController);
    }
    
    @Override
    protected Widget showWidgetPlatform(int widgetId) {
        //in Android implementation, all widgets are {@link Fragment}
        Fragment currentFragment = (Fragment) currentTab;
        FragmentActivity container = (FragmentActivity) mainScreen;
        
        //switch fragment
        Widget widget = (Widget)FragmentHelper.switchFragment(this, container, currentFragment, widgetId, false);
        // Notify the controller about the existence of this widget. This is necesary when the device is rotated
        // because otherwise the controller loses track of this widget
        newWidgetCreated(widget);
        return widget;
    }
}
