/**
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2010 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android.controller;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import com.funambol.android.activities.FragmentGenerator;
import com.funambol.android.activities.FragmentHelper;
import com.funambol.client.controller.Controller;
import com.funambol.client.controller.CreateThumbnailsTask;
import com.funambol.client.controller.SelectiveUploadScreenController;
import com.funambol.client.source.AppSyncSource;
import com.funambol.client.ui.SelectiveUploadScreen;
import com.funambol.client.ui.Widget;
import com.funambol.storage.Table;
import com.funambol.storage.Tuple;
import com.funambol.util.Log;


public class AndroidSelectiveUploadScreenController extends SelectiveUploadScreenController
        implements FragmentGenerator {
    
    private static final String TAG_LOG = AndroidSelectiveUploadScreenController.class.getSimpleName();

    private static final int SELECTIVE_UPLOAD_FRAGMENT_ID = 1000;
    
    public AndroidSelectiveUploadScreenController(
            SelectiveUploadScreen screen, Controller controller,
            AppSyncSource appSource) {
        super(screen, controller, appSource);
    }

    public Fragment createFragment(int id) {
        if(id == SELECTIVE_UPLOAD_FRAGMENT_ID) {
            Widget result = appSource.createSourceSelectiveUploadWidget();
            return (Fragment)result;
        }
        if (Log.isLoggable(Log.INFO)) {
            Log.info(TAG_LOG, "Cannot find a fragment for id " + id);
        }
        return null;
    }

    public String getFragmentUniqueId(int id) {
        if(id == SELECTIVE_UPLOAD_FRAGMENT_ID) {
            return appSource.getSelectiveUploadWidgetClass().getCanonicalName();
        }
        if (Log.isLoggable(Log.INFO)) {
            Log.info(TAG_LOG, "Cannot find a fragment for id " + id);
        }
        return null;
    }

    public String getFragmentUniqueId(Fragment fragment) {
        if (null != fragment) {
            return fragment.getClass().getCanonicalName();
        } else {
            return null;
        }
    }
   
    @Override
    protected CreateThumbnailsTask createThumbnailsTask(Table table, Tuple item, AppSyncSource appSource) {
        return new AndroidCreateThumbnailsTask(table, item, appSource);
    }

    protected Widget showWidgetPlatform() {
        FragmentActivity container = (FragmentActivity) screen;
        return (Widget)FragmentHelper.showFragment(SELECTIVE_UPLOAD_FRAGMENT_ID,
                this, container);
    }
}
