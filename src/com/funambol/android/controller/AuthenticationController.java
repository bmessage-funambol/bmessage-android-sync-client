/**
 * Funambol is a mobile platform developed by Funambol, Inc. 
 * Copyright (C) 2011 Funambol, Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission 
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE 
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License 
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 * 
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite 
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably 
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol". 
 */


package com.funambol.android.controller;

import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.app.Activity;

import com.funambol.android.AndroidAccountManager;
import com.funambol.android.AppInitializer;
import com.funambol.android.ExternalAccountManager;
import com.funambol.android.source.media.MediaAppSyncSource;
import com.funambol.android.source.media.MediaAppSyncSourceConfig;
import com.funambol.android.source.media.MediaDirectoryScanner;
import com.funambol.android.source.media.MediaDirectoryScanner.ScanMessage;
import com.funambol.client.controller.Controller;
import com.funambol.client.source.AppSyncSource;
import com.funambol.client.source.AppSyncSourceManager;
import com.funambol.util.Log;
import com.funambol.util.bus.Bus;
import com.funambol.util.bus.BusMessage;
import com.funambol.util.bus.BusMessageHandler;

public class AuthenticationController {
   
    private static final String TAG_LOG = AuthenticationController.class.getSimpleName();
    
    private AndroidLoginScreenController loginController;
    private AndroidSignupScreenController signupController;
    private AppSyncSourceManager appSyncSourceManager;
    private Controller controller;
    private Set<AppSyncSource> sourcesToScan = new HashSet<AppSyncSource>();
    private boolean pendingAuthentication;
    private ScannerListener scannerListener;
    
    public AuthenticationController(AndroidLoginScreenController loginController, Controller controller) {
        this.loginController = loginController;
        this.controller = controller;
        this.appSyncSourceManager = controller.getAppSyncSourceManager();
    }
    
    public AuthenticationController(AndroidSignupScreenController signupController, Controller controller) {
        this.signupController = signupController;
        this.controller = controller;
        this.appSyncSourceManager = controller.getAppSyncSourceManager();
    }
    
    public void userAuthenticated(Activity activity, boolean hasChanges, String username) {
        if (Log.isLoggable(Log.INFO)) {
            Log.info(TAG_LOG, "User authenticated");
        }
        Account nativeAccount = AndroidAccountManager.getNativeAccount(activity);
        if(hasChanges || nativeAccount == null) {
            AndroidAccountManager.addNewFunambolAccount(activity, username, (AccountAuthenticatorActivity)activity);
        }
        
        // Init the external account manager used to handle contacts belonging
        // to other accounts
        final ExternalAccountManager aManager = AppInitializer.i(activity).getExternalAccountManager();
        aManager.reset();
       
        // Computes the set of sources for which a scan is required
        Enumeration sources = appSyncSourceManager.getWorkingSources();
        while(sources.hasMoreElements()) {
            AppSyncSource source = (AppSyncSource)sources.nextElement();
            if (source.getIsMedia()) {
                sourcesToScan.add(source);
            }
        }
        
        pendingAuthentication = true;
        scannerListener = new ScannerListener();
        Bus.getInstance().registerMessageHandler(ScanMessage.class, scannerListener);
        
        // Start a new scan of the SDCard to find local items
        sources = appSyncSourceManager.getWorkingSources();
        while(sources.hasMoreElements()) {
            AppSyncSource source = (AppSyncSource)sources.nextElement();
            if(source instanceof MediaAppSyncSource) {
                MediaAppSyncSource mediaSource = (MediaAppSyncSource)source;
                // Reset the first scan flag
                MediaAppSyncSourceConfig config = (MediaAppSyncSourceConfig)mediaSource.getConfig();
                config.setFirstScanDone(false);
                // Now trigger a scan
                MediaDirectoryScanner scanner = mediaSource.getMediaDirectoryScanner();
               
                int numItemsToImport = mediaSource.getNumberOfItemsToImportOnVeryFirstSync();
                // If the source does not need to import everything, we compute the number of items to import
                // depending on the fact that this is a first sync or not
                if (numItemsToImport != Integer.MAX_VALUE) {
                    numItemsToImport = controller.getConfiguration().getMediaVeryFirstSync() ? numItemsToImport : 0;
                }
                scanner.scan(true, numItemsToImport, true);
            }
        }
        
        // Now we shall wait for first import phase to complete on all the triggered scans
        if (sourcesToScan.isEmpty() && pendingAuthentication) {
            completeAuthentication();
        }
    }
    
    private void completeAuthentication() {
        if (Log.isLoggable(Log.TRACE)) {
            Log.trace(TAG_LOG, "Scan is completed, about to complete authentication");
        }
        pendingAuthentication = false;
        Bus.getInstance().unregisterMessageHandler(ScanMessage.class, scannerListener);
        
        if (loginController != null) {
            loginController.completeAuthentication();
        } else if (signupController != null) {
            signupController.completeAuthentication();
        }
    }
    
    private class ScannerListener implements BusMessageHandler {
        
        public void receiveMessage(BusMessage message) {
            if (message instanceof ScanMessage) {
                ScanMessage scanMessage = (ScanMessage)message;
                if (scanMessage.getCode() == ScanMessage.FIRST_IMPORT_COMPLETED ||
                    scanMessage.getCode() == ScanMessage.SCAN_COMPLETED)
                {
                    AppSyncSource appSource = scanMessage.getAppSource();
                    sourcesToScan.remove(appSource);
                    if (sourcesToScan.isEmpty() && pendingAuthentication) {
                        completeAuthentication();
                    }
                }
            }
        }

        public boolean runOnSeparateThread() {
            return false;
        }
        
    }
}
