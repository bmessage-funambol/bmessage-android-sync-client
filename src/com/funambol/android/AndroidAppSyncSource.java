/**
 * Funambol is a mobile platform developed by Funambol, Inc.
 * Copyright (C) 2011 Funambol, Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by
 * the Free Software Foundation with the addition of the following permission
 * added to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED
 * WORK IN WHICH THE COPYRIGHT IS OWNED BY FUNAMBOL, FUNAMBOL DISCLAIMS THE
 * WARRANTY OF NON INFRINGEMENT  OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301 USA.
 *
 * You can contact Funambol, Inc. headquarters at 643 Bair Island Road, Suite
 * 305, Redwood City, CA 94063, USA, or at email address info@funambol.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License
 * version 3, these Appropriate Legal Notices must retain the display of the
 * "Powered by Funambol" logo. If the display of the logo is not reasonably
 * feasible for technical reasons, the Appropriate Legal Notices must display
 * the words "Powered by Funambol".
 */

package com.funambol.android;

import java.io.File;
import java.lang.reflect.Constructor;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;

import com.funambol.android.source.AndroidItemDownloadToGalleryTask;
import com.funambol.client.controller.NavigationBarController;
import com.funambol.client.controller.SourceChronologicalViewController;
import com.funambol.client.engine.ItemDownloadToGalleryTask;
import com.funambol.client.localization.Localization;
import com.funambol.client.source.AppSyncSource;
import com.funambol.client.ui.DevSettingsUISyncSource;
import com.funambol.client.ui.Screen;
import com.funambol.client.ui.SettingsUISyncSource;
import com.funambol.client.ui.Widget;
import com.funambol.client.ui.view.NavigationBarButtonView;
import com.funambol.client.ui.view.NavigationBarWidget;
import com.funambol.client.ui.view.SourceBarView;
import com.funambol.client.ui.view.SourceChronologicalViewItem;
import com.funambol.client.ui.view.ThumbnailView;
import com.funambol.storage.Table;
import com.funambol.sync.SyncSource;
import com.funambol.util.Log;

/**
 * Implementation of the AppSyncSource for the Android client. Define the
 * generic AppSyncSource for this particular client
 */
public class AndroidAppSyncSource extends AppSyncSource {
    
    private static final String TAG_LOG = AndroidAppSyncSource.class.getSimpleName();

    private boolean isMedia = false;

    private Uri providerUri;
    private Context appContext;

    /**
     * Constructor
     * @param name is the String formatted name of the AndroidAppSyncSource to
     * be built
     * @param source is the SyncSource object to be wrapped by this object
     */
    public AndroidAppSyncSource(String name, SyncSource source) {
        super(name, source);
    }

    /**
     * Constructor
     * @param name is the String formatted name of the AndroidAppSyncSource to
     * be built
     */
    public AndroidAppSyncSource(String name) {
        this(name, null);
    }

    @Override
    public boolean getIsMedia() {
        return isMedia;
    }

    @Override
    public void setIsMedia(boolean value) {
        isMedia = value;
    }

    public void setContext(Context context) {
        this.appContext = context;
    }

    protected Context getAppContext() {
        return appContext;
    }

    public Uri getProviderUri() {
        return providerUri;
    }

    public void setProviderUri(Uri providerUri) {
        this.providerUri = providerUri;
    }

    public void accountCreated(String accountName, String accountType) {
        // By default we don't do anything
    }
    
    @Override
    protected DevSettingsUISyncSource createDevSettingsUISyncSourceNewInstance(
            Class devSettingsClass, Screen screen)
    throws Exception {
        Activity activity = (Activity)screen.getUiScreen();
        Constructor c = devSettingsClass.getConstructor(new Class[]{Activity.class});
        return (DevSettingsUISyncSource)c.newInstance(activity);
    }

    @Override
    protected SettingsUISyncSource createSettingsUISyncSourceNewInstance(
            Class settingsUISyncSourceClass, Screen screen)
    throws Exception {
        Activity activity = (Activity)screen.getUiScreen();
        Constructor c = settingsUISyncSourceClass.getConstructor(new Class[]{Activity.class, AppSyncSource.class});
        return (SettingsUISyncSource)c.newInstance(activity, this);
    }

    @Override
    protected ThumbnailView createThumbnailViewNewInstance(Class thumbnailViewClass, Screen screen)
    throws Exception {
        Activity activity = ((Activity)screen.getUiScreen());
        Constructor c = thumbnailViewClass.getConstructor(new Class[] {Activity.class});
        return (ThumbnailView)c.newInstance(activity);
    }

    @Override
    protected SourceBarView createSourceBarViewNewInstance(Class sourceBarViewClass, Widget parent)
    throws Exception {
        SourceBarView tv;
        Constructor c = sourceBarViewClass.getConstructor(new Class[] {
                    Widget.class,
                    AppSyncSource.class } );
        tv = (SourceBarView) c.newInstance(parent, this);
        return tv;
    }
    
    @Override
    protected NavigationBarButtonView createSourceNavigationBarButtonViewNewInstance(
            Class sourceNavigationBarButtonViewClass,
            NavigationBarController navigationBarController,
            NavigationBarWidget navigationBarWidget)
    throws Exception {
        NavigationBarButtonView buttonView;
        Context context = ((Activity)navigationBarWidget.getContainerUiScreen()).getApplicationContext();
        Constructor c = sourceNavigationBarButtonViewClass.getConstructor(new Class[] {
                            Context.class, NavigationBarController.class } );
        buttonView = (NavigationBarButtonView)c.newInstance(context, navigationBarController);
        return buttonView;
    }

    @Override
    public ItemDownloadToGalleryTask createDownloadItemToGalleryTask(String url,
            String fileName, Long id, String galleryDirectory, String tempDirectory,
            int initialDelay, Table metadata, boolean suspendable, Localization localization) {
        if(appContext == null) {
            throw new IllegalStateException("Context is null");
        }
        return new AndroidItemDownloadToGalleryTask(url, fileName, id, 
                galleryDirectory, tempDirectory, initialDelay, metadata,
                suspendable, localization, this, appContext);
    }

    /**
     * Infers the MIME type of a local item.
     * In the default implementation, the MIME type cannot be inferred. Individual
     * environments (e.g., Android) can provide their specific way of MIME type inference;
     * in this case, this method must be overridden.
     * 
     * @return null if no MIME type can be inferred, the MIME type otherwise
     */
    @Override
    public String inferMimeType(String urlOrPath) {
                
        String mimeTypeInferred = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                MimeTypeMap.getFileExtensionFromUrl(urlOrPath));
        if (mimeTypeInferred != null) {
            if (Log.isLoggable(Log.DEBUG)) {
                Log.debug(TAG_LOG, "MIME type inferred as " + mimeTypeInferred);
            }
        } else {
            Log.info(TAG_LOG, "MIME type not recognized");
        }
        return mimeTypeInferred;
    }
    
    @Override
    public SourceChronologicalViewItem createSourceChronologicalViewItem(
            Widget parent, SourceChronologicalViewController controller)
    {
        SourceChronologicalViewItem item;
        try {
            Constructor c = sourceChronologicalViewItemClass.getConstructor(new Class[] {
                                Screen.class, 
                                SourceChronologicalViewController.class,
                                AppSyncSource.class } );
            item = (SourceChronologicalViewItem)c.newInstance(parent.getContainerUiScreen(), controller, this);
            return item;
        } catch (Exception e) {
            throw new IllegalStateException("Cannot instantiate source chronological view item", e);
        }
    }
    
    @Override
    protected boolean isFileSupported(String itemUrl, String mimeType) {
        if(Log.isLoggable(Log.TRACE)) {
            Log.trace(TAG_LOG, "isFileSupported " + itemUrl + "," + mimeType + '?');
        }
        // Workaround for MP3 files etc.
        if (MimeTypeMap.isAlwaysSupportedMimeType(mimeType)) {
            Log.debug(TAG_LOG, "Yes (by default)");
            return true;
        }
        if(appContext == null) {
            throw new IllegalStateException("Context is null");
        }
        // We build the intent exactly as when the item is played so the package manager answer is more reliable
        PackageManager packageManager = appContext.getPackageManager();
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setType(mimeType);
        Uri uri;
        if (itemUrl.startsWith(("file://"))) {
            uri = Uri.fromFile(new File(itemUrl));
        } else {
            uri = Uri.parse(itemUrl);
        }
        intent.setData(uri);
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        ResolveInfo ri = packageManager.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);
        if (ri != null) { //mimeType is known
            if (Log.isLoggable(Log.DEBUG)) {
                Log.debug(TAG_LOG, "Yes, " + mimeType + " is best resolved by " + ri.activityInfo.packageName);
            }
            return true;
        }
        
        // Some devices (such as Acer Icon 500) fail at finding an intent for local files. We apply some intelligence
        // here (and heuristic too, we prefer to have a failure when playing rather than a "video not supported" msg)
        if (itemUrl.startsWith("file://") && itemUrl.indexOf("DCIM") != 0) {
            if (Log.isLoggable(Log.DEBUG)) {
                Log.debug(TAG_LOG, "Likely, the file is local and belongs to the DCIM folder");
            }
            return true;
        }
        
        Log.debug(TAG_LOG, "No");
        return false; 
    }
}
